"""Set of functions to assist when deploying a trained model.
Classes
-------

Functions
---------
convert_to_keras_agent - Convert stable-baselines DQN agent into keras model.
parameters_from_dict - Extract parameters as numpy arrays from a DQN model.
"""

import gym
from stable_baselines import DQN
import tensorflow as tf
import numpy as np

def convert_to_keras_agent(input_file):
    """Convert a Stable-Baselines DQN agent to a Tensorflow Keras model
        version for prediction purposes only.

        NOTE - This function was found to produce agents with prediction errors.
             - The cause was found to be small errors in the value distribution
               between the two versions, which causes prediction errors when the
               argmax function is used.
             - The exact cause of this could not be resolved, but was likely due
               to implementation differences between keras and the original raw
               tensorflow implementation.
             - As a result, attempts to use this approach were abandoned, and the
               original agent files were used instead.

        This function has been left for reference - newer versions of stable-baselines
        may produce better results.

        This function assumes a model of 2 fully connected 64 node layers.

        Parameters:
        input_file - Path to a .zip file describing a DQN agent.
    """
    # Try to load model as DQN network
    try:
        agent = DQN.load(input_file)
    except:
        raise ValueError('Could not loat file ', str(input_file))

    # Get ordered dict of (name, parameter array) values for agent.
    agent_params = agent.get_parameters()
    # print(agent.get_parameter_list())

    # State is the overall input
    state = tf.keras.Input(shape=(6,))

    # # Create the action value network.
    # Two 64 node layers
    action_out = state
    for i in [0,1]:
        weights, bias = parameters_from_dict(agent_params, i)
        action_out = tf.keras.layers.Dense(64, activation=None,
                        kernel_initializer=tf.constant_initializer(weights),
                        bias_initializer=tf.constant_initializer(bias) )(action_out)
        action_out = tf.nn.relu(action_out)

    # Add output to 5 action evaluations.
    weights, bias = parameters_from_dict(agent_params, 2)
    action_value = tf.keras.layers.Dense(5, activation=None,
                kernel_initializer=tf.constant_initializer(weights),
                bias_initializer=tf.constant_initializer(bias) )(action_out)

    # Center action values
    action_value_mean = tf.reduce_mean(action_value, keep_dims=True)
    action_value_centered = tf.keras.layers.subtract([action_value, action_value_mean])

    # # Create the state value network
    # Add two fully connected 64 node layers
    state_out = state
    for i in [0,1]:
        weights, bias = parameters_from_dict(agent_params, i, net_type='State')
        state_out = tf.keras.layers.Dense(64, activation=None,
                        kernel_initializer=tf.constant_initializer(weights),
                        bias_initializer=tf.constant_initializer(bias) )(state_out)
        state_out = tf.nn.relu(state_out)

    # Add output to single state value.
    weights, bias = parameters_from_dict(agent_params, 2, net_type='State')
    state_value = tf.keras.layers.Dense(1, activation=None,
                kernel_initializer=tf.constant_initializer(weights),
                bias_initializer=tf.constant_initializer(bias) )(state_out)

    # Function output is sum of state & normalised action
    q_out = tf.keras.layers.add([state_value, action_value_centered])

    # Save model as .h5 file
    action_model = tf.keras.models.Model(inputs=state, outputs=q_out)
    action_output_file = input_file.split('.')[0] + '_network' + '.h5'
    action_model.save(action_output_file, save_format='tf')


def parameters_from_dict(agent_params, layer_number, net_type='Action'):
    """Helper function to return parameters for a specific layer of the action value
        or state value functions for a DQN agent.
        Parameters:
        agent - Agent file from which we want to extract parameters.
        layer_number - Number of the layer we want to extract.
                     - Integer in the range [0,2] in this case.
        net_type - Either 'Action' for action value parameters, or 'State' for
               state value parameters.
        Returns:
        weights - Numpy array of weights.
        biases - Numpy array of biases.
    """

    if net_type not in ['Action', 'State']:
        raise ValueError('Unknown network type - should be either Action or State')
    if layer_number not in [0, 1, 2]:
        raise ValueError('Layer number out of scope for this network - should be in [0,1,2].')

    # The variable scope can be found from the keys of agent.get_parameters() method.
    # Base scope changes function, but layers are the same for both State & Action.
    if net_type is 'Action':
        base_scope = 'deepq/model/action_value/'
    elif net_type is 'State':
        base_scope = 'deepq/model/state_value/'
    layer_names = ['fully_connected/', 'fully_connected_1/', 'fully_connected_2/']
    weights_key = base_scope + layer_names[layer_number] + 'weights:0'
    biases_key = base_scope + layer_names[layer_number] + 'biases:0'

    if weights_key not in agent_params.keys() or biases_key not in agent_params.keys():
        raise RuntimeError('Unexpected error when extracting DQN parameters - could not find parameters by key.')

    return agent_params[weights_key].astype(np.float64), agent_params[biases_key].astype(np.float64)

