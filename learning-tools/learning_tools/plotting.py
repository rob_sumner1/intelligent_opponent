"""Plotting tools for use with the MSR gym.
Classes
-------
VelodromePlot - Implement plotting of the full velodrome.
TextLabel - Geometry class to add PyGlet text to plots.

Functions
---------
circular_plot - Plot race history as a circular plot around velodrome.
                The race is split into equal distance or equal time sections.
velodrome_layout_plot - Create plot showing velodrome layout.
distance_index - Return the index corresponding to the first time a rider reaches
action_graph - Create a matplotlib action graph.
"""

from scipy import interpolate
import numpy as np
import lapysim
import pickle
import os
import matplotlib.pyplot as plt
import pyglet
from PIL import Image
from gym.envs.classic_control import rendering



class VelodromePlot:
    """Class implementing velodrome plotting.
        Parameters
        ----------
        size_scale - Scale value to increase size of plot.
                   - This is useful for high quality plots, eg. set = 2.

        Public Methods
        --------------
        pixel_coord - Convert a point in meters to a pixel point.
        add_track - Add all geometries that make up velodrome.
        add_rider - Add a plottable and movable rider to the plot.
        add_colorbar - Add a custom colorbar to the plot.
        add_power_indicator - Add a power/action indicator to plot.
        add_action_graph - Add rider action graph to the centre of the plot.
        add_helper_arrows - Add arrows to indicate race direction.
        move_rider - Move a rider to new position.
        add_label - Add a text label to the plot.
        add_geometry - Add geometry to the viewer.
        show- Show the viewer as an RBG image.
        reset - Reset the viewer to it's initial state.
        find_rider_points - Return (x,y) positions of a riders in velodrome
                            coordinate system.
        scale_track - Scale the black line track data by distance value.
   """

    def __init__(self, size_scale=1):
        """Initialise velodrome geometry and viewer parameters.
            Parameters:
            size_scale - Viewer size scaling to be applied.
                       - This is useful for high quality plots, where a higher
                         scale will produce better quality.
        """
        # Track data.
        match_sprint_track = lapysim.Manchester()
        # Ignore triple duplicated value at [-1]
        self.x_survey = match_sprint_track._track_data_from_file[1][:-1]
        self.y_survey = match_sprint_track._track_data_from_file[2][:-1]
        self.x_range = np.max(self.x_survey) - np.min(self.x_survey)
        self.y_range = np.max(self.y_survey) - np.min(self.y_survey)
        self.x_min = np.min(self.x_survey)
        self.y_min = np.min(self.y_survey)

        # Index of survey mapping to start line.
        self.start_index = 560

        # Sizing constraints
        if size_scale > 0:
            self.size_scale = size_scale
        else:
            raise ValueError("Size scale cannot be negative or 0.")
        self.screen_width = int(2000*self.size_scale)
        self.render_buffer_x = 0.2 # White space around plot.
        self.render_buffer_y = 0.3
        # Width + height need to be divisible by 2.
        height_scale = int(self.screen_width * self.y_range / self.x_range) // 2
        self.screen_height = height_scale*2
        self.x_scale = self.screen_width*(1-self.render_buffer_x)/self.x_range
        self.y_scale = self.screen_height*(1-self.render_buffer_y)/self.y_range
        self.x_offset = self.screen_width*self.render_buffer_x*0.5
        self.y_offset = self.screen_height*self.render_buffer_y*0.5

        # Viewer initially set to None
        self.viewer = None

        # Store details on riders
        self.riders = {}
        self.labels = {}

    def pixel_coord(self, x, y):
        """Convert coordinate x,y into a pixel rendering coordinate.
            Parameters:
            x - The x coordinate or vector of local coordinates (in m).
            y - The y coordinate or vector of loval coordinates (in m).
            Returns:
            x_pixel, y_pixel - Converted coordinates
        """
        try:
            # Convert to pixel coordinates
            x_pixel = (x - self.x_min)*self.x_scale + self.x_offset
            y_pixel = (y - self.y_min)*self.y_scale + self.y_offset

            return x_pixel, y_pixel
        except:
            raise ValueError('Invalid input types')

    def add_track(self):
        """Add components of velodrome track.
            These include: Black line, sprinter's line, stayer's line,
                           full track width, Cote D'Azure & start line.
        """
        # Convert track black line survey to pixel coordinates
        xp_bl, yp_bl = self.pixel_coord(self.x_survey, self.y_survey)
        v_bl = []
        for i in range(len(xp_bl)): v_bl.append([xp_bl[i], yp_bl[i]])

        # Sprinters line is 70 cm from black line
        sprint_gap = +0.7
        sprint_line = self.scale_track(sprint_gap)
        xp_sprint, yp_sprint = self.pixel_coord(sprint_line[:,0], sprint_line[:,1])
        v_sprint = []
        for i in range(len(xp_sprint)): v_sprint.append([xp_sprint[i], yp_sprint[i]])

        # Stayers line is 2.3 m from black line
        stay_gap = +2.3
        stay_line = self.scale_track(stay_gap)
        xp_stay, yp_stay = self.pixel_coord(stay_line[:,0], stay_line[:,1])
        v_stay = []
        for i in range(len(xp_stay)): v_stay.append([xp_stay[i], yp_stay[i]])

        # Inner edge of track is 20 cm away from black line.
        track_inner_gap = -0.2
        inner_track = self.scale_track(track_inner_gap)
        xp_inner, yp_inner = self.pixel_coord(inner_track[:,0], inner_track[:,1])
        v_track_inner = []
        for i in range(len(xp_inner)): v_track_inner.append([xp_inner[i], yp_inner[i]])

        # Simple outer track is 5m away in plan view.
        track_width = +5
        outer_track = self.scale_track(track_width)
        xp_outer, yp_outer = self.pixel_coord(outer_track[:,0], outer_track[:,1])
        v_track_outer = []
        for i in range(len(xp_outer)): v_track_outer.append([xp_outer[i], yp_outer[i]])

        # Cote D'Azure is blue strip between v_track_inner and v_cote_inner
        cote_width = -1.5
        cote_inner = self.scale_track(cote_width)
        xp_cote, yp_cote = self.pixel_coord(cote_inner[:,0], cote_inner[:,1])
        v_cote_inner = []
        for i in range(len(xp_cote)): v_cote_inner.append([xp_cote[i], yp_cote[i]])

        # Add the full track
        full_track = rendering.FilledPolygon(v_track_outer)
        full_track.set_color(0.847,0.733,0.475)
        self.add_geometry(full_track)

        # Add blue zone
        b_zone = rendering.FilledPolygon(v_track_inner)
        b_zone.set_color(0.820,0.894,0.949)
        self.add_geometry(b_zone)

        # Remove centre
        centre = rendering.FilledPolygon(v_cote_inner)
        centre.set_color(1,1,1)
        self.add_geometry(centre)

        # Add Stayers line
        stayers_line = rendering.PolyLine(v_stay, close=True)
        stayers_line.set_color(0, 0, 1)
        stayers_line.set_linewidth(2)
        self.add_geometry(stayers_line)

        # Add sprinters line
        sprinters_line = rendering.PolyLine(v_sprint, close=True)
        sprinters_line.set_color(1, 0, 0)
        sprinters_line.set_linewidth(2)
        self.add_geometry(sprinters_line)

        # Add black line
        black_line = rendering.PolyLine(v_bl, close=True)
        black_line.set_color(0, 0, 0)
        black_line.set_linewidth(5)
        self.add_geometry(black_line)

        # Add white box surrounding start line
        width = 8*self.size_scale
        p_inner = v_track_inner[self.start_index]
        p_outer = v_track_outer[self.start_index]
        line_sep = rendering.FilledPolygon([(p_inner[0] - width, p_inner[1]),
                                            (p_inner[0] + width, p_inner[1]),
                                            (p_outer[0] + width, p_outer[1]),
                                            (p_outer[0] - width, p_outer[1])])
        line_sep.set_color(1,1,1)
        self.add_geometry(line_sep)

        # Add start line
        start_line = rendering.PolyLine([(p_inner[0], p_inner[1]),
                                         (p_outer[0], p_outer[1])], close=False)
        start_line.set_color(0,0,0)
        start_line.set_linewidth(30*self.size_scale)
        start_line.linewidth.stroke = 3
        self.add_geometry(start_line)

        # Lower pursuit line
        p_inner = v_track_inner[0]
        p_outer = v_track_outer[0]
        pursuit_1 = rendering.PolyLine([(p_inner[0], p_inner[1]),
                                        (p_outer[0], p_inner[1] + 0.6*(p_outer[1] - p_inner[1]) )],
                                        close=False)
        pursuit_1.set_color(1, 0, 0)
        pursuit_1.set_linewidth(30*self.size_scale)
        pursuit_1.linewidth.stroke = 3
        self.add_geometry(pursuit_1)

        # Upper pursuit line
        p_inner = v_track_inner[500]
        p_outer = v_track_outer[500]
        pursuit_1 = rendering.PolyLine([(p_inner[0], p_inner[1]),
                                        (p_outer[0], p_inner[1] + 0.6*(p_outer[1] - p_inner[1]) )],
                                        close=False)
        pursuit_1.set_color(1, 0, 0)
        pursuit_1.set_linewidth(30*self.size_scale)
        pursuit_1.linewidth.stroke = 3
        self.add_geometry(pursuit_1)

    def add_rider(self, rider_id, color, distance=0, pos=None, radius=20):
        """Add a rider
            Parameters:
            rider_id - Unique rider id number.
                     - This is used as a handle for retrieving class info.
            color - [R, G, B] array controlling icon color.
                  - Or [R, G, B, alpha] array, where alpha is opacity in
                    range [0,1].
            distance - The distance of the rider around the track in m.
                     - If distance is not provided, rider is placed at start line.
            pos - [xp, yp] array for non-track rider positioning.
        """
        if rider_id in self.riders.keys():
            raise ValueError("Rider ID already in use.")

        rider_icon = rendering.make_circle(radius=radius*self.size_scale,
                                            res=30*self.size_scale, filled=True)
        rider_icon.set_color(color[0],color[1],color[2])
        # Set opacity.
        if len(color) == 4:
            rider_icon._color.vec4 = (color[0],color[1],color[2], color[3])
        rider_trans = rendering.Transform()
        rider_icon.add_attr(rider_trans)
        self.add_geometry(rider_icon)

        # Update rider position
        if pos is None:
            rider_pos = self.find_rider_points(distance)
            xp, yp = self.pixel_coord(rider_pos[0], rider_pos[1])
            rider_trans.set_translation(xp, yp)
        else:
            rider_trans.set_translation(pos[0], pos[1])

        # Store classes so they can be changed later if needed.
        self.riders[rider_id] = [rider_icon, rider_trans]

    def add_colorbar(self, x, y, length=100, horizontal=True, col_range=[0,1],
                    color=[0,0,0], steps=None, labels=None, width=6):
        """Add a opacity colorbar to the velodrome plot.
            Parameters:
            x, y - Coordinates of centre of the colorbar.
            length - Length of colorbar in total.
            horizontal - Bool, colorbar will be horizontal if true and
                         vertical otherwise.
            col_range - Range of alpha values.
            color - Color of bar.
            steps - Number of lines to break bar into.
                  - Higher number will give smoother plot.
            labels - [min, max, dscription] strings to label colorbar.
            width  - The width/thickness of the colorbar.
        """
        if steps is None:
            # One pixel per step by default.
            steps = length

        inc = length/steps
        width *= self.size_scale
        for i in range(steps):
            # Filled polygon gives more thickness than line
            if horizontal:
                x_start = x - 0.5*length + i*inc
                block = rendering.FilledPolygon([(x_start, y-width), (x_start+inc, y-width),
                                                 (x_start+inc, y+width), (x_start, y+width)])
            else:
                y_start = y - 0.5*length + i*inc
                block = rendering.FilledPolygon([(x - width, y_start),
                                                 (x - width, y_start + inc),
                                                 (x + width , y_start + inc),
                                                 (x + width, y_start)])
            alpha = col_range[0] + (i+1)*(col_range[1] - col_range[0])/steps
            block.set_color(color[0],color[1],color[2])
            block._color.vec4 = (color[0],color[1],color[2], alpha)
            self.add_geometry(block)

        if labels is not None:
            if len(labels) != 3:
                raise ValueError("Incorrect label format.")
            elif horizontal:
                self.add_label(x-0.5*length, y-25*self.size_scale, labels[0], size=10*self.size_scale)
                self.add_label(x+0.5*length, y-25*self.size_scale, labels[1], size=10*self.size_scale)
                self.add_label(x, y-30*self.size_scale, labels[2], size=10*self.size_scale)
            else:
                self.add_label(x+25*self.size_scale, y-0.5*length, labels[0], size=10*self.size_scale)
                self.add_label(x+25*self.size_scale, y+0.5*length, labels[1], size=10*self.size_scale)
                self.add_label(x+30*self.size_scale, y, labels[2], size=10*self.size_scale)

    def add_power_indicator(self, x, y, p, p_range = [0,1], steps=10, length=10,
                                col_range=[0,1], color=[1,0,0], labels=None):
        """Add a power indicator logo.
            Parameters:
            x, y - x and y pixel locations for the indicator.
            p - Power value for the indicator.
            p_range - Power range for the indicator.
            steps - Number of step increments on the indicator.
            length - Length of the indicator.
            color - Indicator color.
        """

        # Size scalings
        xc, yc = self.pixel_coord(0,0)
        x_shift = x - xc
        y_shift = y - yc

        # Create colorbar
        self.add_colorbar(x, y, length=length, color=color, steps=steps,
                            col_range = col_range, labels=None)

        # Add arrow.
        p_width = 0.5
        ar_height = 1.0
        points = [(-p_width, -ar_height/2), (0, ar_height/2),
                    (p_width, -ar_height/2)]
        # Calbirate height shift
        xs, ys = self.pixel_coord(0, 1.25*ar_height)
        xs -= xc
        ys -= yc

        # Draw the arrow at middle of colorbar.
        v = []
        for vertex in points:
            x0, y0 = self.pixel_coord(vertex[0], vertex[1])
            v.append((x0 + x_shift - xs, y0 + y_shift - ys))
        arrow = rendering.FilledPolygon(v)

        # Add arrow translation
        p_step = (p_range[1] - p_range[0])/steps
        p_num = (p - p_range[0])//p_step
        x_move = (-0.5 + (p_num + 0.5)/steps)*length
        transform = rendering.Transform()
        arrow.add_attr(transform)
        transform.set_translation(x_move, 0)

        # Set triangle alpha value
        alpha = col_range[0] + p_num*(col_range[1] - col_range[0])/steps
        arrow._color.vec4 = (color[0],color[1],color[2], alpha)
        self.add_geometry(arrow)

        if labels is not None:
            if len(labels) != 3:
                raise ValueError("Incorrect label format.")
            # Add labels
            self.add_label(x-0.5*length, y-35*self.size_scale, labels[0], size=10*self.size_scale)
            self.add_label(x+0.5*length, y-35*self.size_scale, labels[1], size=10*self.size_scale)
            self.add_label(x, y-40*self.size_scale, labels[2], size=10*self.size_scale)

    def add_action_graph(self, x, y, actions, t_range=[0,1], ax_width=10, ax_height=10, act_range=[0,1],
                        highlight_index=None, highlight_alpha=None, color=[0,1,0]):
        """Add action graph to plot.
            x, y - Pixel locations of graph origin.
            actions - List/array of actions to plot.
            ax_width - Width of axes.
            ax_height - Height of axes.
            act_range - Range of the possible actions.
            highlight_index - Set of indices to highlight in the plot.
            highlight_alpha - Set of alpha/transparency values corresponding to
                              highlight indices.
        """
        # Size scalings - shift origin to (x,y)
        xc, yc = self.pixel_coord(0,0)
        x_shift = x - xc
        y_shift = y - yc

        # Add polyline for action plot
        v = []
        for i, act in enumerate(actions):
            x_point = i/len(actions)*ax_width
            y_point = act/(act_range[1] - act_range[0])*ax_height
            xp, yp = self.pixel_coord(x_point, y_point)
            v.append((xp + x_shift, yp + y_shift))
        line = rendering.PolyLine(v, close=False)
        line.set_color(1,0,0)
        line.set_linewidth(30*self.size_scale)
        self.add_geometry(line)

        # Add highlight points
        if highlight_index is not None:
            if highlight_alpha is not None:
                if len(highlight_alpha) != len(highlight_index):
                    raise ValueError("Highlight parameters must match")
                # Add a rider point for each value
                for i, index in enumerate(highlight_index):
                    alpha = highlight_alpha[i]
                    act = actions[index]
                    x_point = index/len(actions)*ax_width
                    # Plot on action
                    # y_point = act/(act_range[1] - act_range[0])*ax_height
                    # Plot on x axis
                    y_point = act_range[0]/(act_range[1] - act_range[0])*ax_height
                    xp, yp = self.pixel_coord(x_point, y_point)
                    rider_id = max(self.riders.keys()) + 1
                    self.add_rider(rider_id, color + [alpha], radius=7.5,
                                    pos=[xp + x_shift, yp + y_shift])

        # Create axes
        x_max, y_max = self.pixel_coord(ax_width, ax_height)
        dash = min(ax_width, ax_height)
        x_dash_min1, y_dash_min1 = self.pixel_coord(-0.05*dash, -0.05*dash)
        x_dash_min2, y_dash_min2 = self.pixel_coord(-0.03*dash, -0.03*dash)
        x_dash_max2, y_dash_max2 = self.pixel_coord( 0.03*dash,  0.03*dash)
        v = [(x_dash_max2 + x_shift, y_max + y_shift),
             (x_dash_min2 + x_shift, y_max + y_shift),
             (x, y_max + y_shift),
             (x, y + 0.5*(y_max + y_shift - y)),
             (x_dash_min1 + x_shift, y + 0.5*(y_max + y_shift - y)),
             (x, y + 0.5*(y_max + y_shift - y)),
             (x, y_dash_min1 + y_shift),
             (x, y),
             (x_dash_min1 + x_shift, y),
             (x, y),
             (x + 1/3*(x_max + x_shift - x), y),
             (x + 1/3*(x_max + x_shift - x), y_dash_min2 + y_shift),
             (x + 1/3*(x_max + x_shift - x), y),
             (x + 2/3*(x_max + x_shift - x), y),
             (x + 2/3*(x_max + x_shift - x), y_dash_min2 + y_shift),
             (x + 2/3*(x_max + x_shift - x), y),
             (x_max + x_shift, y),
             (x_max + x_shift, y_dash_max2 + y_shift),
             (x_max + x_shift, y_dash_min2 + y_shift)]
        axis = rendering.PolyLine(v, close=False)
        axis.set_color(0,0,0)
        axis.set_linewidth(3*self.size_scale)
        self.add_geometry(axis)

        # Add y axis labels
        self.add_label(x_dash_min1 + x_shift - self.size_scale*20, y,
                        "0.0", size=10*self.size_scale)
        self.add_label(x_dash_min1 + x_shift - self.size_scale*20,
                        y + 0.5*(y_max + y_shift - y), "0.5", size=10*self.size_scale)
        self.add_label(x_dash_min1 + x_shift - self.size_scale*20,
                        y_max + y_shift, "1.0", size=10*self.size_scale)
        self.add_label(x_dash_min1 + x_shift - self.size_scale*60, 0.5*(y + y_max + y_shift),
                        "Control Action", size=10*self.size_scale, rotation=np.pi/2)

        # Add x axis labels
        t_r = t_range[1] - t_range[0]
        self.add_label(x, y_dash_min1 + y_shift - self.size_scale*10,
                        str(round(t_range[0])), size=10*self.size_scale)
        self.add_label(x + 1/3*(x_max + x_shift - x), y_dash_min1 + y_shift - self.size_scale*10,
                        str(round(t_range[0] + t_r/3)), size=10*self.size_scale)
        self.add_label(x + 2/3*(x_max + x_shift - x), y_dash_min1 + y_shift - self.size_scale*10,
                        str(round(t_range[0] + 2*t_r/3)), size=10*self.size_scale)
        self.add_label(x_max + x_shift, y_dash_min2 + y_shift - self.size_scale*10,
                        str(round(t_range[1])), size=10*self.size_scale)
        self.add_label(0.5*(x + x_max + x_shift), y_dash_min2 + y_shift - self.size_scale*25,
                        "Time (s)", size=10*self.size_scale)

        self.add_label(0.5*(x + x_max + x_shift), y_max + y_shift + self.size_scale*35,
                        "Rider A Control Action Plot", size=15*self.size_scale)

    def add_straight_arrow(self, start, end, color=(0,0,0), point_width=1.5):
        """Add straight arrow between two points.
            Parameters:
            start - Tuple/Array of [xp_start, yp_start] (i.e. pixel coords)
                    for base point of arrow.
            end - Tuple/Array of [xp_end, yp_end] (i.e. pixel coords) for
                    point of arrow.
            color - Tuple defining color of arrow, values in [0,255].
                  - Default is black (0,0,0).
            point_width - Size value for width of arrow point (in metres).
        """
        if len(start) != 2 or len(end) != 2:
            raise ValueError('Start/End coordinates are wrong dimensions.')

        # Add arrow line geometry.
        arrow_line = rendering.PolyLine([start, end], close=False)
        arrow_line.set_color(color[0], color[1], color[2])
        arrow_line.set_linewidth(5)
        self.add_geometry(arrow_line)

        # Create arrow head
        points = [(-point_width/2, 0), (0, point_width), (point_width/2, 0)]
        xc, yc = self.pixel_coord(0, 0)
        v = []
        for vertex in points:
            x0, y0 = self.pixel_coord(vertex[0], vertex[1])
            v.append((x0 - xc, y0 - yc))
        arrow = rendering.FilledPolygon(v)
        arrow.set_color(color[0], color[1], color[2])

        # Add arrow translation
        transform = rendering.Transform()
        arrow.add_attr(transform)
        transform.set_translation(end[0], end[1])

        # Set arrow rotation
        arr_vect = (end[0] - start[0], end[1] - start[1])
        angle = np.arctan2(arr_vect[1], arr_vect[0]) - np.pi/2
        transform.set_rotation(angle)

        self.add_geometry(arrow)

    def add_direction_arrow(self, distance, arrow_length=20, point_width=1.5, black_line_gap=-5):
        """Add arrow to indicate race direction.
            Parameters:
            distance - Distance along the black line to position middle of arrow.
            arrow_length - Length of arrow (in meters).
            point_width - Width for point of arrow (in meters).
            black_line_gap - Gap between the black line and the arrow body (in metres).
        """
        # Get indexes for arrow limits
        i_start = self.find_distance_index(distance - 0.5*arrow_length, use_race_start=False)
        i_end = self.find_distance_index(distance + 0.5*arrow_length - point_width, use_race_start=False)

        # Place arrow inside black line.
        all_points = self.scale_track(black_line_gap)
        arrow_points = all_points[i_start:i_end+1, :]

        # Convert to vertex form
        xp_arrow, yp_arrow = self.pixel_coord(arrow_points[:,0], arrow_points[:,1])
        v_arrow = []
        for i in range(len(xp_arrow)): v_arrow.append([xp_arrow[i], yp_arrow[i]])

        # Add arrow line geometry.
        arrow_line = rendering.PolyLine(v_arrow, close=False)
        arrow_line.set_color(0, 0, 0)
        arrow_line.set_linewidth(5)
        self.add_geometry(arrow_line)

        # Create arrow head
        points = [(-point_width/2, 0), (0, point_width), (point_width/2, 0)]
        xc, yc = self.pixel_coord(0, 0)
        v = []
        for vertex in points:
            x0, y0 = self.pixel_coord(vertex[0], vertex[1])
            v.append((x0 - xc, y0 - yc))
        arrow = rendering.FilledPolygon(v)
        arrow.set_color(0, 0, 0)

        # Add arrow translation
        transform = rendering.Transform()
        arrow.add_attr(transform)
        transform.set_translation(xp_arrow[-1], yp_arrow[-1])

        # Set arrow rotation
        arr_vect = (xp_arrow[-1] - xp_arrow[-2], yp_arrow[-1] - yp_arrow[-2])
        angle = np.arctan2(arr_vect[1], arr_vect[0]) - np.pi/2
        transform.set_rotation(angle)

        self.add_geometry(arrow)

    def add_banking(self, apex, base, angle, angle_radius=2):
        """Add banking icon.
            Parameters:
            apex - [x, y] coords (in metres) for point/apex of angle indicator.
            base - Length of base (in metres).
            angle - Angle of indicator (in radians).
            angle_width - Radius of angle indicator.
        """

        # Define triangle for track surface.
        height = base*np.tan(angle)
        x_coords = [apex[0], apex[0]+base, apex[0]+base, apex[0]]
        y_coords = [apex[1], apex[1], apex[1] + height, apex[1]]
        xp_banking, yp_banking = self.pixel_coord(x_coords, y_coords)
        v = []
        for i in range(len(xp_banking)): v.append([xp_banking[i], yp_banking[i]])

        # Add angle indicator
        theta = np.linspace(0,1,200) * angle
        xp_angle, yp_angle = self.pixel_coord( apex[0] + angle_radius*np.cos(theta),
                                               apex[1] + angle_radius*np.sin(theta) )
        for i in range(len(xp_angle)): v.append([xp_angle[i], yp_angle[i]])
        v.append( [xp_banking[0], yp_banking[0]] )
        banking_line = rendering.PolyLine(v, close=False)
        banking_line.set_color(0, 0, 0)
        banking_line.set_linewidth(2.5*self.size_scale)
        self.add_geometry(banking_line)

    def move_rider(self, rider_id, distance=0):
        """Move a rider to new position.
            Parameters:
            distance - The new distance the rider has moved around the track.
        """
        if rider_id not in self.riders.keys():
            raise ValueError("Rider id is unused - cannot move rider.")

        rider_pos = self.find_rider_points(distance)
        xp, yp = self.pixel_coord(rider_pos[0], rider_pos[1])
        self.riders[rider_id][1].set_translation(xp, yp)

    def add_label(self, xp, yp, text, size=40, id=None, rotation=0, color=(0,0,0) ):
        """Add a text label to the plot.
            Parameters:
            id - Label id, used to retrieve and edit label later.
            xp - The x pixel position.
            yp - The y pixel position.
            text - Text that label will contain.
            font - Size of label font. Default is 40.
            color - RGB tuple defining label colour.
        """
        if id is None:
            if len(self.labels.keys()) > 0:
                id = len(self.labels.keys()) + 1
            else:
                id = 0
        elif id in self.labels.keys():
            raise ValueError("Label ID already in use.")

        label = TextLabel(xp, yp, text, size=size*self.size_scale, color=color)
        if rotation != 0:
            label.rotate(rotation)
        self.add_geometry(label)
        self.labels[id] = label

    def add_geometry(self, geom):
        """Safely add geometry to the viewer.
            Parameters:
            geom - Geometry to add to the viewer.
        """
        if self.viewer is None:
            self.viewer = rendering.Viewer(self.screen_width, self.screen_height)
        self.viewer.add_geom(geom)

    def show(self, file_name=None, test=False, image=False):
        """Show the viewer as an RBG image.
            Parameters:
            file_name - String for save location.
                      - If none provided, image is not saved.
            test - Bool, if true no image plots are created.
                 - Used to keep automated tests clean.
            image - Bool, if true the RBG array is returned.
        """
        img = self.viewer.render(return_rgb_array=True)
        if image:
            return img
        im = Image.fromarray(img)
        if file_name is not None:
            im.save(file_name + '.png', dpi=(300,300))
        if not test:
            im.show()

    def reset(self):
        """Reset the viewer to it's initial state."""
        if self.viewer is not None:
            self.viewer.close()
            self.viewer = None
        self.riders = {}
        self.labels = {}

    def find_distance_index(self, distance, use_race_start=True):
        """Find the index corresponding to a given distance around
            the black line.
            Parameters:
            distance - Distance around the black line.
            use_race_start - If True, race start line is used as starting
                             point. If False, index[0] of survey is used.
            Returns:
            index - Index of point corresponding to this distance.
                  - Eg. self.x_survey[index] would be x coord of point.
        """
        distance_covered = 0
        last_distance = 0
        if use_race_start:
            index = self.start_index
        else:
            index = 0

        while (distance_covered <= distance):
            # Update index so it loops track as required.
            index += 1
            index = index % len(self.x_survey)

            x_distance = (self.x_survey[index] - self.x_survey[index-1])
            y_distance = (self.y_survey[index] - self.y_survey[index-1])
            last_distance = distance_covered
            distance_covered +=  np.sqrt(x_distance**2 + y_distance**2)

        # Interpolate to sub-index accuracy
        fraction = (distance - last_distance) / (distance_covered - last_distance)

        return index, fraction

    def find_rider_points(self, distance, index=False):
        """Return (x,y) positions of a rider in velodrome coordinate system.
            Parameters:
            distance - The state distance variable for the rider.
            Returns:
            rider_pos - [x,y] Position of rider in velodrome coordinate system
                        if index is true.
        """
        index, fraction = self.find_distance_index(distance)
        # Interpolate to sub-pixel accuracy
        x_pos = self.x_survey[index-1] + fraction*(self.x_survey[index] - self.x_survey[index-1])
        y_pos = self.y_survey[index-1] + fraction*(self.y_survey[index] - self.y_survey[index-1])
        return [x_pos, y_pos]

    def scale_track(self, distance):
        """Scale the black line track data by distance value.
            Parameters:
            distance - Size (in m) to shift track by.
                     - Positive is up the slope, negative down.
            Returns:
            points - (N x 2) Array of x and y points.
        """
        N = len(self.x_survey)
        points = np.zeros((N, 2))

        for i in range(N):
            # Find vector parallel to edge.
            par_x = self.x_survey[(i+1)%N] - self.x_survey[i-1]
            par_y = self.y_survey[(i+1)%N] - self.y_survey[i-1]

            # Convert into normalised perpendicular vector, direction outwards.
            perp_x = par_y
            perp_y = -1*par_x
            norm = np.sqrt(par_x**2 + par_y**2)

            # Normalise if required
            if norm > 0:
                perp_x /= norm
                perp_y /= norm

            # New point = Old point + distance * perpendicular vector.
            points[i,0] = self.x_survey[i] + distance*perp_x
            points[i,1] = self.y_survey[i] + distance*perp_y
        return points


class TextLabel(rendering.Geom):
    """Class to plot a text label in a PyGlet Viewer.
        This class follows the structure used in the Gym rendering module.

        Parameters
        ----------
        x, y - Pixel coordinates to add label.
        text - Text to show on label.
        size - Font size.

        Public Methods
        --------------
        rotate - Rotate the label about its centre.
        render1 - Used by the viewer to render this geometry.
        set_text - Set label text. This can be used to iteratively update text.
    """
    def __init__(self, x, y, text, size=40, color=(0, 0, 0) ):
        rendering.Geom.__init__(self)
        self.x = x
        self.y = y
        self.font_size = size
        self.text = text
        self.color = color

        # Create label
        self.label = pyglet.text.Label(text,
                          font_name='CMU Serif',
                          font_size=self.font_size,
                          x=0, y=0,
                          anchor_x='center', anchor_y='center',
                          color=(self.color[0], self.color[1], self.color[2], 255))

        # Set position
        self.transform = rendering.Transform()
        self.add_attr(self.transform)
        self.transform.set_translation(x, y)

    def rotate(self, theta):
        self.transform.set_rotation(theta)

    def render1(self):
        if self.label is not None:
            self.label.draw()

    def set_text(self, text):
        self.text = text
        self.label = pyglet.text.Label(text,
                          font_name='CMU Serif',
                          font_size=self.font_size,
                          x=0, y=0,
                          anchor_x='center', anchor_y='center',
                          color=(self.color[0], self.color[1], self.color[2], 255))


def circular_plot(history, save_name=None, title=None, steps=10, time=False,
                    action='graph'):
    """Plot race history as a circular plot around velodrome. The race is split
        into equal distance or equal time sections.
        Parameters:
        history - Race history file to plot.
        save_name - Name or path including name to save image to.
                  - Does not need to include .png, this is added automatically.
        steps - Number of steps/sections to split the race into.
        time - If True, race is split based upon time.
             - If False, race is split based upon distance.
        action - Controls display of actions on plot.
               - Bar uses a power indicator bar.
               - graph uses a plot of actions during sequence.
               - join blends a matplotlib graph image with the current plot.
               - None doesn't display actions.
    """

    # Load history from pickle file.
    try:
        data = pickle.load(open(history, 'rb'))
    except:
        raise ValueError("Could not open provided history file.")
    times = data[0]
    rider_a = data[1]['state']
    rider_b = data[2]['state']
    actions = np.array(data[1]['control'])

    # Parameters
    color_a = [1, 0, 0]
    color_b = [0, 0.5, 0]
    size_scale = 2
    op_min = 0.3

    # Create velodrome plot
    velo = VelodromePlot(size_scale=size_scale)
    velo.add_track()

    # Split up race into sections
    if time:
        n = len(times)
        t_step = n // (steps + 1)
        plot_points = np.arange(1,steps+1)*t_step
        dist_points = []
        for i in range(len(plot_points)):
            dist_points.append(max(rider_a[i][0], rider_b[i][0]))
    else:
        d_max = max(rider_a[-1][0], rider_b[-1][0])
        d_step = d_max // (steps)
        distances = np.flip(np.arange(d_max, d_step, -d_step))
        plot_points = []
        dist_points = []
        for distance in distances:
            ind, dist = distance_index(data, distance)
            plot_points.append(ind)
            dist_points.append(dist)

    # Plot split riders.
    i = 0
    highlight_alpha = []
    for scale, index in enumerate(plot_points):
        opacity = [op_min + (1-op_min)*(scale+1)/steps]
        highlight_alpha.append(opacity[0])
        velo.add_rider(i, color_a + opacity, rider_a[index][0])
        velo.add_rider(i+1, color_b + opacity, rider_b[index][0])
        i += 2

    # Add 'Lap Finish' Label
    xp, yp = velo.pixel_coord(velo.x_survey[velo.start_index],
                              velo.y_survey[velo.start_index] + 7)
    velo.add_label(xp, yp, "Lap Finish", size=12.5*velo.size_scale)

    # Create legend
    top_level = 250
    velo.add_label(velo.screen_width/2 - size_scale*30,
                    velo.screen_height/2 + size_scale*top_level, "Rider A", size=17*velo.size_scale)
    velo.add_rider(-1, color_a, pos=[velo.screen_width/2 + size_scale*120,
                                        velo.screen_height/2 + size_scale*top_level])
    velo.add_label(velo.screen_width/2 - size_scale*30,
                    velo.screen_height/2 + size_scale*(top_level - 50), "Rider B", size=17*velo.size_scale)
    velo.add_rider(-2, color_b, pos=[velo.screen_width/2 + size_scale*120,
                                        velo.screen_height/2 + size_scale*(top_level - 50)])

    # Add colorbars for distance
    velo.add_colorbar(velo.screen_width/2, velo.screen_height/2 + size_scale*(top_level-90),
                      length=750, col_range=[op_min,1], color=color_a,
                      steps=50)
    velo.add_colorbar(velo.screen_width/2, velo.screen_height/2 + size_scale*(top_level-102),
                      length=750, col_range=[op_min,1], color=color_b,
                      steps=50, labels=["0", "250", "Rider Distance (m)"])

    # Add title if it has been given
    if title is not None:
        velo.add_label(velo.screen_width/2, velo.screen_height*0.965, title, size=22.5*velo.size_scale)

    # Add race direction helper arrows
    velo.add_direction_arrow(0.25*250)
    velo.add_direction_arrow(0.75*250)

    # Add helper arrow text label
    gap = -11
    index = velo.find_distance_index(0.75*250, use_race_start=False)
    mag = np.sqrt(velo.x_survey[index]**2 + velo.y_survey[index]**2)
    scale = (mag + gap) / mag
    xp, yp = velo.pixel_coord(velo.x_survey[index]*scale,
                                velo.y_survey[index]*scale)
    velo.add_label(xp, yp, 'Race Direction', size=10*velo.size_scale)

    # Plot action descriptors
    if action == 'bar':
        i_prev = 0
        for index in plot_points:
            pos_a = velo.find_rider_points(rider_a[index][0])
            pos_b = velo.find_rider_points(rider_b[index][0])
            x_label = (pos_a[0] + pos_b[0]) / 2
            y_label = (pos_a[1] + pos_b[1]) / 2
            gap = 6
            mag = np.sqrt(x_label**2 + y_label**2)
            scale = (mag - gap) / mag
            x_label *= scale
            y_label *= scale
            xp, yp = velo.pixel_coord(x_label, y_label)
            # Add power indicator
            p = np.mean(actions[i_prev:index])
            i_prev = index
            velo.add_power_indicator(xp, yp, p, length=200, col_range=[op_min, 1])
    elif action == 'graph':
        velo.add_action_graph(0.3*velo.screen_width, 0.29*velo.screen_height, actions,
                                t_range=[times[0],times[-1]], ax_width=49, ax_height=15,
                                highlight_index=plot_points,
                                highlight_alpha=highlight_alpha,
                                color=color_a)
    elif action == 'join':
        # Set sizes
        ax_width = 49
        ax_height = 15
        xc, yc = velo.pixel_coord(0,0)
        x0, y0 = velo.pixel_coord(ax_width, ax_height)
        p_width = int(x0 - xc)

        # Save plot as an image
        action_graph(actions, filename="action_image.png", aspect=(ax_height/ax_width))
        plot = Image.open("action_image.png")
        scale = p_width/plot.size[0]
        new_size = (int(plot.size[0]*scale), int(plot.size[1]*scale))
        plot = plot.resize(new_size)

        # Get velo image
        rgb_array = velo.show(image=True)
        im = Image.fromarray(rgb_array)

        # Merge images
        im.paste(plot, (int(0.3*velo.screen_width), int(0.5*velo.screen_height)))

        # Save the plot if save location is given.
        if save_name is None:
            im.show()
        else:
            im.save(save_name + '.png')
        velo.reset()
        return

    # Save the plot if save location is given.
    if save_name is None:
        velo.show()
    else:
        velo.show(file_name=save_name)
    velo.reset()


def velodrome_layout_plot(save_name=None):
    """Create plot demonstrating the layout of the velodrome.
        Parameters:
        save_name - Name or path including name to save image to.
                  - Does not need to include .png, this is added automatically.
    """
    # Create velodrome plot
    velo = VelodromePlot(size_scale=2)
    velo.add_track()

    # Add 'Lap Finish' Label
    xp, yp = velo.pixel_coord(velo.x_survey[velo.start_index],
                                velo.y_survey[velo.start_index] + 7)
    velo.add_label(xp, yp, "Start and Finish Line", size=12.5*velo.size_scale)

    # Red Sprinter's line
    xp, yp = velo.pixel_coord(13, -10)
    velo.add_label(xp, yp, 'Sprinter\'s Line', size=12.5*velo.size_scale, color=(255,0,0) )
    xp, yp = velo.pixel_coord(13, -12.5)
    velo.add_label(xp, yp, '(Limits overtaking position)', size=10*velo.size_scale, color=(255,0,0) )
    xp_arrow, yp_arrow = velo.pixel_coord([13,13], [-14.5, -23.6])
    velo.add_straight_arrow( (xp_arrow[0], yp_arrow[0]),
                             (xp_arrow[1], yp_arrow[1]),
                             color=(255,0,0) )

    # Black Measurement line
    xp, yp = velo.pixel_coord(0, 0)
    velo.add_label(xp, yp, 'Measurement Line', size=12.5*velo.size_scale, color=(0,0,0) )
    xp, yp = velo.pixel_coord(0, -2.5)
    velo.add_label(xp, yp, '(250 m marker line)', size=10*velo.size_scale, color=(0,0,0) )
    xp_arrow, yp_arrow = velo.pixel_coord([0,0], [-4.5, -22.9])
    velo.add_straight_arrow( (xp_arrow[0], yp_arrow[0]),
                             (xp_arrow[1], yp_arrow[1]),
                             color=(0,0,0) )

    # Blue Stayer's line
    xp, yp = velo.pixel_coord(-13, -10)
    velo.add_label(xp, yp, 'Stayer\'s Line', size=12.5*velo.size_scale, color=(0,0,255) )
    xp, yp = velo.pixel_coord(-13, -12.5)
    velo.add_label(xp, yp, '(Used in Madison races)', size=10*velo.size_scale, color=(0,0,255) )
    xp_arrow, yp_arrow = velo.pixel_coord([-13,-13], [-14.5, -25.1])
    velo.add_straight_arrow( (xp_arrow[0], yp_arrow[0]),
                             (xp_arrow[1], yp_arrow[1]),
                             color=(0,0,255) )

    # Add race direction helper arrow & label
    velo.add_direction_arrow(0.75*250)
    index = velo.find_distance_index(0.75*250, use_race_start=False)
    mag = np.sqrt(velo.x_survey[index]**2 + velo.y_survey[index]**2)
    scale = (mag - 13) / mag
    xp, yp = velo.pixel_coord(velo.x_survey[index]*scale,
                                velo.y_survey[index]*scale)
    velo.add_label(xp, yp, 'Race Direction', size=12.5*velo.size_scale)

    # Add 45 degree banking
    velo.add_banking([27.5, -6], 15, np.pi/4)
    xp, yp = velo.pixel_coord(35, -8)
    velo.add_label(xp, yp, 'Banking in Corner', size=12.5*velo.size_scale)
    xp, yp = velo.pixel_coord(35, -10.5)
    velo.add_label(xp, yp, ' Maximum 45\xb0', size=10*velo.size_scale)

    # Add 12 degree banking
    velo.add_banking([-7.5, 16], 15, 12*np.pi/180)
    xp, yp = velo.pixel_coord(0, 14)
    velo.add_label(xp, yp, 'Banking on Straight', size=12.5*velo.size_scale)
    xp, yp = velo.pixel_coord(0, 11.5)
    velo.add_label(xp, yp, 'Minimum 12\xb0', size=10*velo.size_scale)

    # Save the plot if save location is given.
    if save_name is None:
        velo.show()
    else:
        velo.show(file_name=save_name)
    velo.reset()


def distance_index(data, distance):
    """Return the index corresponding to the first time a rider reaches
        a given distance.
        Parameters:
        data - Loaded history data file.
        distance - The distance value riders must reach.
        Returns
        index - Index of the data files.
              - None if the distance is never reached.
    """

    rider_a = data[1]['state']
    rider_b = data[2]['state']

    if distance > max(rider_a[-1][0], rider_b[-1][0]):
        raise ValueError("Distance exceeds maximum simulation distance.")
    else:
        i = 0
        dist = 0
        while max(rider_a[i][0], rider_b[i][0]) < distance:
            i += 1
            dist = max(rider_a[i][0], rider_b[i][0])
    return i, dist

def action_graph(actions, filename=None, aspect=1):
    """ Create a matplotlib action graph.
        actions - Set of actions to plot.
    """
    from matplotlib import rc
    rc('font',**{'family':'serif','serif':['Computer Modern Roman'], 'size':35})
    rc('text', usetex=True)
    w, h = plt.figaspect(aspect)
    plt.figure(figsize=(w, h))

    plt.plot(np.linspace(0,len(actions), len(actions)), actions,
                color=(1.0,0.0,0.0,1.0), linewidth=5)
    plt.xlim([0,len(actions)])
    plt.ylim([0,1.1])
    plt.xlabel('Time Increment')
    plt.ylabel('Control Action')
    plt.title('Rider A Control Action Plot')
    plt.tight_layout()
    if filename is None:
        plt.savefig('mat_image.png')
    else:
        plt.savefig(filename)










