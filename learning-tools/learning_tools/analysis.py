"""A set of functions for analysing policies.
Classes
-------

Functions
---------
analyse_policies - Analyse a given policy by running races against it.
calculate_win_rate - Calculate the win rate for a policy given policy evaluation
                     data.
measure_win_rate - Measure win rate directly by running races.
plot_policy_analysis - Plot results of policy analysis.
plot_fatigue_analysis - Plot results of fatigue analysis.
"""

import gym
import gym_msr
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from matplotlib.transforms import Bbox
import scipy.interpolate
from stable_baselines import DQN
from gym_msr.envs.msr_policies import MsrPolicy, MsrNetPolicy

def analyse_policies(rider_a_agent, n_steps=10, n_step_runs=1, action_dim=5,
                        save_name=None):
    """Analysis tool for policies.
        Function uses msr_duel environment (version 2) to race a given policy
        against a varying opponent policy.
        The opponent policy varies their sprint distance between a set of distances.

        Parameters:
        rider_a_agent - The agent (or policy) to be investigated.
        n_steps - Number of steps to use when varying sprint range.
        n_step_runs - Number of runs to use at each step.
        action_dim - The size of action space to be used if an expert policy
                     is being used. In this case, the action is cast to a value
                     which matches this action space.
        save_name - If provided, results file is also saved to this name.

        Returns:
        results - Array of results for Rider A, with column titles:
                  [sprint distance,
                   lead win rate, lead avg. reward, lead avg. final sep., lead avg. overtakes,
                   trail win rate, trail avg. reward, trail avg. final sep., trail avg. overtakes]
    """

    # Version 2 environment.
    env = gym.make('msr-duel-fixed-v2', action_dim=action_dim)

    # Check the form of the agent
    if isinstance(rider_a_agent, DQN):
        action_dim = rider_a_agent.action_space.n
        rider_a_agent.set_env(env)

    # Sprint distances
    rider_b_sprint_distances = np.linspace(0, 750, n_steps)
    col_titles = ['Win Rate', 'Avg Reward', 'Avg Final Sep', 'Avg Overtakes']
    lead_results = np.zeros((n_steps, len(col_titles)))
    trail_results = np.zeros((n_steps, len(col_titles)))

    # Run races
    for i, distance in enumerate(rider_b_sprint_distances):
        print('Distance = ', distance)
        lead_races_won, lead_rewards, lead_seps, lead_overtakes = 0, 0, 0, 0
        trail_races_won, trail_rewards, trail_seps, trail_overtakes = 0, 0, 0, 0
        for _ in range(n_step_runs):
            # Rider A is leading with set sprint distance for rider B
            env.reset(leading_rider='Rider A', verbose=False,
                      rider_b_sprint_distance=distance)

            # If MsrPolicy, need to reset/randomise it.
            if isinstance(rider_a_agent, MsrPolicy):
                rider_a_agent.reset()

            while not env.complete():
                if isinstance(rider_a_agent, MsrPolicy):
                    rider_a_eq_exer = env.get_draft_exertion(drafting_rider='Rider A')
                    action = int(rider_a_agent.predict(env.state, rider_a_eq_exer)*(action_dim-1))
                elif isinstance(rider_a_agent, MsrNetPolicy):
                    action = int(rider_a_agent.predict(env.state)*(action_dim-1))
                else:
                    action = rider_a_agent.predict( env.normalised_state() )[0]
                env.step(action)

            # Store results of this run
            lead_rewards += env.reward
            lead_seps += (env.state[0] - env.state[3])
            lead_overtakes += env.overtakes
            if env.state[0] > env.state[3]:
                lead_races_won += 1

            # Rider B is leading with set sprint distance for rider B
            env.reset(leading_rider='Rider B', verbose=False,
                      rider_b_sprint_distance=distance)

            # If MsrPolicy, need to reset/randomise it.
            if isinstance(rider_a_agent, MsrPolicy):
                rider_a_agent.reset()

            while not env.complete():
                if isinstance(rider_a_agent, MsrPolicy):
                    rider_a_eq_exer = env.get_draft_exertion(drafting_rider='Rider A')
                    action = int(rider_a_agent.predict(env.state, rider_a_eq_exer)*(action_dim-1))
                elif isinstance(rider_a_agent, MsrNetPolicy):
                    action = int(rider_a_agent.predict(env.state)*(action_dim-1))
                else:
                    action = rider_a_agent.predict( env.normalised_state() )[0]
                env.step(action)

            # Store results of this run
            trail_rewards += env.reward
            trail_seps += (env.state[0] - env.state[3])
            trail_overtakes += env.overtakes
            if env.state[0] > env.state[3]:
                trail_races_won += 1

        # Store win rate for distance
        lead_results[i] = [lead_races_won/n_step_runs, lead_rewards/n_step_runs,
                             lead_seps/n_step_runs, lead_overtakes/n_step_runs]
        trail_results[i] = [trail_races_won/n_step_runs, trail_rewards/n_step_runs,
                             trail_seps/n_step_runs, trail_overtakes/n_step_runs]

    # Convert results
    results = np.concatenate( ( rider_b_sprint_distances.reshape((n_steps, 1)),
                                lead_results, trail_results ), axis=1)

    # Save results if needed
    if save_name is not None:
        np.save(save_name, results)

    return results

def calculate_win_rate(filepath):
    """Calculate the win rate against expert policy given a numpy
        array from policy evaluation.
        Parameters:
        file - The numpy file name to use.
        Returns:
        overall_win_rate - Overall win rate
        lead_win_rate - Win rate when rider A leads.
        trail_win_rate - Win rate when rider A trails.
    """
    # Load the result array.
    data = np.load(filepath)

    # Assume data generated from equally spaced points.
    dist_step = data[1,0] - data[0,0]

    # Get policy parameters
    pol = MsrPolicy()
    min_trail = pol.min_trail_sprint_distance
    max_trail = pol.max_trail_sprint_distance
    min_lead = pol.min_lead_sprint_distance
    max_lead = pol.max_lead_sprint_distance
    trail_weight = dist_step / (max_trail - min_trail)
    lead_weight = dist_step / (max_lead - min_lead)

    # Win rates
    overall_win_rate = 0
    trail_win_rate = 0
    lead_win_rate = 0

    for i in range(len(data)):
        # Rider A leads, Rider B trails & sprints in expert range
        if data[i,0] >= min_trail and data[i,0] <= max_trail:
            # Consider edge cases
            if data[i,0] - dist_step < min_trail:
                lead_win_rate += data[i,1] * (data[i,0] - min_trail) / (max_trail - min_trail)
            elif data[i,0] + dist_step > max_trail:
                lead_win_rate += data[i,1] * (max_trail - data[i,0]) / (max_trail - min_trail)
            else:
                lead_win_rate += data[i,1] * trail_weight
        # Rider A trails, Rider B leads & sprints in expert range.
        elif data[i,0] >= min_lead and data[i,0] <= max_lead:
            # Consider edge cases
            if data[i,0] - dist_step < min_lead:
                trail_win_rate += data[i,1] * (data[i,0] - min_lead) / (max_lead - min_lead)
            elif data[i,0] + dist_step > max_lead:
                trail_win_rate += data[i,1] * (max_lead - data[i,0]) / (max_lead - min_lead)
            else:
                trail_win_rate += data[i,2] * lead_weight

    overall_win_rate = 0.5 * (trail_win_rate + lead_win_rate)

    return overall_win_rate, lead_win_rate, trail_win_rate


def measure_win_rate(env, agent, n_runs=40):
    """Measure win rate for a policy against expert MSR policy.
        Uses a Monte Carlo approach - i.e. run it a lot and see what happens.
        Parameters:
        env - Environment used for evaluation.
        agent - Rider A agent to evaluate.
        n_runs - Number of runs to evaluate over.
        Returns:
        overall_win_rate - Average win rate over runs.
        lead_win_rate - Win rate when rider A leads.
        trail_win_rate - Win rate when rider B leads.
    """

    lead_wins, trail_wins = 0, 0

    # Rider A leading
    for _ in range(n_runs):
        state = env.reset(leading_rider='Rider A', verbose=False)
        while not env.complete():
            action, _ = agent.predict(state, deterministic=True)
            state, _, _, _ = env.step(action)
        # Store results
        if state[0] > state[3]:
            lead_wins += 1

    # Rider B leading
    for _ in range(n_runs):
        state = env.reset(leading_rider='Rider B', verbose=False)
        while not env.complete():
            action, _ = agent.predict(state, deterministic=True)
            state, _, _, _ = env.step(action)
        # Store results
        if state[0] > state[3]:
            trail_wins += 1

    # Results
    overall_win_rate = ( lead_wins + trail_wins ) / (2 * n_runs)
    lead_win_rate = lead_wins / n_runs
    trail_win_rate = trail_wins / n_runs

    return overall_win_rate, lead_win_rate, trail_win_rate


def plot_policy_analysis(files, labels=None, interpolate=True, title=None,
                         show_sprint_zones=True, show_overtake_zones=False,
                         show=True, save_name=None, text_labels=None):
    """Plot results of policy anaylysis.
        Parameters:
        files - List of filenames for numpy saved result arrays.
        labels - Description strings to be used for plots.
        interpolate - If true, policy points are interpolated.
        title - Plot title.
        show_sprint_zones - If True, expert policy sprint zones
                            plotted for rider A position.
        show_overtake_zones - If True, plot bands with average number
                              of overtakes.
                            - This is plotted for first array in files.
        show - If true, plot is show,
        save_name - If given, plot is saved to this location.
        text_labels - Set of text labels to add to specific points on plot.
                    - (axis (1 or 2), text_x, text_y, text) for each label.
    """

    from matplotlib import rc
    rc('font', **{'family':'CMU Serif'})
    rc('text', usetex=True)

    if labels is not None and len(labels) != len(files):
        raise ValueError("Number of titles does not match number of histories.")

    # Load provided files.
    arrays = []
    for i, filename in enumerate(files):
        # Append '.npy' if no file extension used.
        if len(filename.split('.')) == 1:
            files[i] = filename + '.npy'
        arrays.append(np.load(files[i]))

    if show_overtake_zones:
        fig, (ax1, ov1, ax2, ov2) = plt.subplots(4, 1, gridspec_kw={'height_ratios': [4, 1, 4, 1]})
    else:
        fig, (ax1, ax2) = plt.subplots(2, 1)

    for ax in [ax1, ax2]:
        ax.set_xlim([0,750])
        ax.set_ylim([-0.1,1.1])
        ax.set_ylabel('Agent Win Probability')
        ax.set_xlabel('Opponent Rider Sprint Point (m)')
    ax1.set_title('Agent Leading', fontsize=15)
    ax2.set_title('Agent Trailing', fontsize=15)

    colors = ['tab:green', 'tab:blue', 'tab:red', 'tab:black']
    if interpolate: new_x = np.linspace(0, 750, 1000)
    for i, array in enumerate(arrays):
        # Get index of win rate data
        lead_index = 1
        trail_index = 1 + int(0.5 * (array.shape[1] - 1))

        # Interpolate data if required.
        if interpolate:
            f1 = scipy.interpolate.interp1d(array[:,0].reshape((len(array[:,0]), )),
                                            array[:,lead_index].reshape((len(array[:,0]), )),
                                            axis=0, kind='slinear')
            f2 = scipy.interpolate.interp1d(array[:,0].reshape((len(array[:,0]), )),
                                            array[:,trail_index].reshape((len(array[:,0]), )),
                                            axis=0, kind='slinear')
            ax1.plot(new_x, f1(new_x), color=colors[i])
            if labels is not None:
                ax2.plot(new_x, f2(new_x), color=colors[i], label=labels[i])
            else:
                ax2.plot(new_x, f2(new_x), color=colors[i])
        else:
            ax1.plot(array[:,0], array[:,lead_index], color=colors[i])
            if labels is not None:
                ax2.plot(array[:,0], array[:,trail_index], color=colors[i], label=labels[i])
            else:
                ax2.plot(array[:,0], array[:,trail_index], color=colors[i])

    # Plot expert sprint zones.
    if show_sprint_zones:
        pol = MsrPolicy()
        trail_min_dist = pol.min_trail_sprint_distance
        trail_max_dist = pol.max_trail_sprint_distance
        lead_min_dist = pol.min_lead_sprint_distance
        lead_max_dist = pol.max_lead_sprint_distance
        ax2.fill_between([lead_min_dist, lead_max_dist],
                         [-0.2, -0.2], [1.2, 1.2],
                         color='tab:red', alpha=0.3, label='Expert Sprint Zone')
        ax1.fill_between([trail_min_dist, trail_max_dist],
                         [-0.2, -0.2], [1.2, 1.2],
                         color='tab:red', alpha=0.3)

    # Plot bands for average overtakes
    if show_overtake_zones:
        array = arrays[0]
        # Check array contains overtake data
        if array.shape[1] < 9:
            raise ValueError('Data array does not contain overtake results.')

        # Plot overtaking zones.
        colors = ['xkcd:cyan', 'xkcd:aqua', 'xkcd:azure', 'xkcd:navy']
        #colors = ['xkcd:yellow', 'xkcd:orange', 'xkcd:red', 'xkcd:maroon']
        colors = ['cyan', 'teal', 'dodgerblue', 'mediumblue']

        # Set arrow style
        style="Simple,tail_width=0.5,head_width=4,head_length=8"
        kw = dict(arrowstyle=style)

        # Add overtake zones for both plots
        indexes = [lead_index, trail_index]
        overtake_icons = False
        for i, ax in enumerate([ov1, ov2]):
            sections = analyse_overtakes(array[:, indexes[i]+3])
            for section in sections:
                start, stop, n_overtakes = section
                x_start = array[start,0]
                x_stop = array[stop,0]
                ax.fill_between([x_start, x_stop], [-0.2, -0.2], [1.2, 1.2],
                                color=colors[n_overtakes], alpha=0.4)

                ## Add overtake demo arrows
                gap = 40

                # Order colors to match plots
                if i == 0:
                    lead_color = 'green'
                    trail_color = 'blue'
                else:
                    lead_color = 'blue'
                    trail_color = 'green'

                # Skip arrows if section is too small.
                if x_stop - x_start < 1.75*(n_overtakes+1)*gap:
                    continue
                else:
                    overtake_icons = True

                m_pos = 0.5*(x_start + x_stop)
                if n_overtakes == 0:
                    ax.plot([m_pos - 0.5*gap], [0.5], 'o', color=trail_color)
                    a2 = patches.FancyArrowPatch((m_pos - 0.50*gap, 0.5),
                                                 (m_pos + 0.4*gap, 0.5),
                                                  color=trail_color, **kw )
                    ax.add_patch(a2)
                    ax.plot([m_pos + 0.5*gap], [0.5], 'o', color=lead_color)
                    a1 = patches.FancyArrowPatch((m_pos + 0.50*gap, 0.5),
                                                 (m_pos + 1.4*gap, 0.5),
                                                  color=lead_color, **kw )
                    ax.add_patch(a1)

                elif n_overtakes == 1:
                    ax.plot([m_pos - gap], [0.5], 'o', color=trail_color)
                    a1 = patches.FancyArrowPatch( (m_pos - gap, 0.5),
                                                    (m_pos + gap, 0.5),
                                                    connectionstyle="arc3,rad=.3",
                                                    color=trail_color, **kw)
                    ax.add_patch(a1)
                    ax.plot([m_pos], [0.5], 'o', color=lead_color)

                elif n_overtakes == 2:
                    ax.plot([m_pos - 1.5*gap], [0.5], 'o', color=trail_color)
                    a1 = patches.FancyArrowPatch( (m_pos - 1.5*gap, 0.5),
                                                    (m_pos + 0.5*gap, 0.5),
                                                    connectionstyle="arc3,rad=.3",
                                                    color=trail_color, **kw)
                    ax.add_patch(a1)
                    ax.plot([m_pos - 0.5*gap], [0.5], 'o', color=lead_color)
                    a2 = patches.FancyArrowPatch( (m_pos - 0.5*gap, 0.5),
                                                    (m_pos + 1.5*gap, 0.5),
                                                    connectionstyle="arc3,rad=-.3",
                                                    color=lead_color, **kw)
                    ax.add_patch(a2)
                    ax.plot([m_pos + 0.5*gap], [0.5], 'o', color=trail_color)

                else:
                    ax.plot([m_pos - 1.5*gap], [0.5], 'o', color=trail_color)
                    a1 = patches.FancyArrowPatch( (m_pos - 1.5*gap, 0.5),
                                                    (m_pos + 0.5*gap, 0.5),
                                                    connectionstyle="arc3,rad=.3",
                                                    color=trail_color, **kw)
                    ax.add_patch(a1)
                    ax.plot([m_pos - 0.5*gap], [0.5], 'o', color=lead_color)
                    a2 = patches.FancyArrowPatch( (m_pos - 0.5*gap, 0.5),
                                                    (m_pos + 1.5*gap, 0.5),
                                                    connectionstyle="arc3,rad=-.3",
                                                    color=lead_color, **kw)
                    ax.add_patch(a2)
                    ax.plot([m_pos + 0.5*gap], [0.5], 'o', color=trail_color)
                    a3 = patches.FancyArrowPatch( (m_pos + 0.5*gap, 0.5),
                                                    (m_pos + 2.5*gap, 0.4),
                                                    connectionstyle="arc3,rad=.3",
                                                    color=trail_color, **kw)
                    ax.add_patch(a3)
                    ax.text(m_pos + 2.5*gap, 0.65, '$\cdots$',
                            horizontalalignment='center',
                            verticalalignment='center',
                            fontsize=20)

            # Set axes props
            ax.set_yticklabels([])
            ax.set_yticks([])
            ax.set_xticklabels([])
            ax.xaxis.tick_top()
            ax.set_xlabel('(Average Number of Overtakes)', fontsize=8)
            ax.set_xticks([])
            ax.set_xlim([0, 750])
            ax.set_ylim([0, 1])

        # Add labels for riders if overtake icons have been added
        if overtake_icons:
            ax1.plot(-100, -100, 'o', color='green', label='Agent')
            ax1.plot(-100, -100, 'o', color='blue', label='Opponent Rider')


    # Add legend + titles if needed
    if labels is not None:
        lines, labels = ax2.get_legend_handles_labels()
        lines2, labels2 = ax1.get_legend_handles_labels()

        # Add overtake zones
        if show_overtake_zones:
            labels += ['0 Overtakes', '1 Overtake', '2 Overtakes', '$>2$ Overtakes']
            patch_0 = patches.Patch(color=colors[0], alpha=0.4)
            patch_1 = patches.Patch(color=colors[1], alpha=0.4)
            patch_2 = patches.Patch(color=colors[2], alpha=0.4)
            patch_3 = patches.Patch(color=colors[3], alpha=0.4)
            lines += [patch_0, patch_1, patch_2, patch_3]

        fig.legend(lines+lines2, labels+labels2, loc=7, prop={'size': 11})

    # Add any labels
    if text_labels is not None:
        for label in text_labels:
            if label[0] == 1:
                ax1.annotate(label[3], xy=(label[1], label[2]),
                            ha='center', va='center', fontsize=10)
            else:
                ax2.annotate(label[3], xy=(label[1], label[2]),
                            ha='center', va='center', fontsize=10)

    fig.set_size_inches(8, 5.33)
    if title is not None:
        plt.suptitle(title)
        # Smaller area to stop title clash.
        fig.tight_layout(rect=[0, 0, 1, 0.975])
    else:
        fig.tight_layout()

    if show_overtake_zones:
        fig.subplots_adjust(right=0.7, hspace=0.6)
    else:
        fig.subplots_adjust(right=0.7)

    if save_name is not None:
        plt.savefig(save_name, dpi=480, bbox_inches="tight")

    if show:
        plt.show()
    else:
        plt.close()

def plot_fatigue_analysis(files, fatigue_range, titles=None, show=True,
                            save_name=None, test_points=None):
    """Plot results of policy fatigue anaylysis.
        Parameters:
        files - List of filepaths for numpy win rate result array.
        title - Plot title.
        fatigue_range - [min, max], fatigue range for file.
        show - If true, plot is show,
        save_name - If given, plot is saved to this location.
        test_points - A list of (x, y, color, label) points to
                      be marked on the graph.
    """

    from matplotlib import rc
    rc('font', **{'family':'CMU Serif'})
    rc('text', usetex=True)

    if len(files) == 1:
        fig, ax = plt.subplots(1, 1)
        ax = [ax]
    else:
        fig, ax = plt.subplots(1, 2)

    if titles is not None:
        if len(titles) != len(files): raise ValueError('Titles must match provided files.')

    for i, file in enumerate(files):
        win_rates = np.load(file)
        (n_rider_a, n_rider_b) = win_rates.shape
        rider_a_fatigues = np.linspace(fatigue_range[0], fatigue_range[1], n_rider_a)
        rider_b_fatigues = np.linspace(fatigue_range[0], fatigue_range[1], n_rider_b)

        X, Y = np.meshgrid(rider_b_fatigues, rider_a_fatigues)
        ax[i].contourf(X, Y, win_rates)
        if i == 0: ax[i].scatter([0.01],[0.01], marker='o', color='tab:red', zorder=1, label='Training Setup')
        else: ax[i].scatter([0.01],[0.01], marker='o', color='tab:red', zorder=1)
        pmesh = ax[i].pcolormesh(X, Y, win_rates, vmin=0, vmax=1, zorder=-1)

        # Add test points if provided
        if test_points is not None:
            for point in test_points:
                ax[i].scatter(point[0],point[1], marker='o',
                              color=point[2], zorder=1, label=point[3])

        ax[i].set(xlim=fatigue_range, ylim=fatigue_range)
        ax[i].xaxis.set_major_locator(plt.MaxNLocator(5))
        ax[i].yaxis.set_major_locator(plt.MaxNLocator(5))
        ax[i].set_aspect('equal', 'box')
        if titles is not None:
            ax[i].set_title(titles[i], fontsize=15)

    ax[0].set_ylabel('Agent Fatigue Rate', fontsize=11)
    ax[0].set_xlabel('Opponent Rider Fatigue Rate', fontsize=11)
    ax[-1].set_xlabel('Opponent Rider Fatigue Rate', fontsize=11)
    if len(ax) == 2:
        ax[-1].yaxis.set_label_position("right")
        ax[-1].yaxis.tick_right()
        ax[-1].set_ylabel('Agent Fatigue Rate', fontsize=11, rotation=270, va='bottom')
    # ax[-1].set_ylabel('Agent Fatigue Rate', fontsize=11)
    # ax[-1].tick_params(right=True, left=False, labelright=True, labelleft=False)


    cbar = fig.colorbar(pmesh, ax=ax, orientation='horizontal', shrink=0.5, anchor=(0.5, 0.5))
    cbar.set_label('Agent Win Probability')
    fig.legend(loc=(0.42,0.01))

    fig.set_size_inches(8, 5)
    fig.subplots_adjust(top=0.99, bottom=0.3, wspace=0.15, left=0.11, right=0.89)

    # fig.tight_layout()

    if save_name is not None:
        plt.savefig(save_name, dpi=480, bbox_inches="tight")
        #plt.savefig(save_name, dpi=480)
    if show:
        plt.show()
    plt.close()

def analyse_overtakes(data):
    """ Calculate the ranges where overtakes lie within
        certain range given a dataset of average overtake data.

        Returns:
        sections - Set of sections described by tuple of (start_index,
                    end_index, reference).
                 - Reference is an integer mapping to number of overtakes.
                 - Reference = 0 is no overtakes, 1 is 1 overtake, 2 = 2 overtakes
                   and 3 is > 2 overtakes.
    """
    data = np.array(data)
    vals = np.zeros(data.shape)
    # Clip to max vals
    for i, clip in enumerate([0.5, 1.5, 2.5]):
        vals = np.where(data > clip , i+1, vals)

    # Convert into sections
    sections = []
    start_index = 0
    for i in range(1, len(vals)):
        if vals[i] != vals[i-1]:
            sections.append( (start_index, i-1, int(vals[i-1])))
            start_index = i-1
        if i == len(vals) - 1:
            sections.append( (start_index, i, int(vals[i])))

    return sections

