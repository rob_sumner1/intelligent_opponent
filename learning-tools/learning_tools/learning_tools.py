"""A number of different tools for investigating learning with the MSR gym.
Classes
-------

Functions
---------
get_last_agent - Return path to last agent and next agent to be saved
                 in a given folder/location.
get_agents - Return paths to all agents in a folder.
run_model - Run a model on a given environment.
plot_action_separation - Plot action separation graph for a history.
plot_fatigue - Plot fatigue data for a given history.
plot_velocities - Plot velocity data for a given history.
draft_step_control_performance - Evaluate the performance of a drafting
                                 controller.
"""

import gym
import gym_msr
import numpy as np
from stable_baselines.common.vec_env import DummyVecEnv
from stable_baselines import DQN
import itertools

from stable_baselines.deepq.policies import MlpPolicy
import glob
import os
import matplotlib.pyplot as plt
import pickle

def get_last_agent(folder, string_base="msr_agent_"):
    """Find the most recent model file in the folder.
        Agent names must be _ separated with the final section being an integer.
        Parameters:
        folder - Folder to check for agent models.
               - Folder in format "path/to/folder"
        Returns:
        last_path - Path to most recent agent file.
        next_path - Path to save next agent to.
    """
    # Check folder exists.
    if not os.path.isdir(folder):
        raise ValueError("Folder does not exist.")

    # Check which .zip files are present in the folder
    all_files =  glob.glob(folder + "/*.zip")

    # Remove any files which dont match string base
    files = []
    for file_path in all_files:
        agent_name = file_path.split('/')[-1]
        if string_base in agent_name:
            files.append(file_path)

    if len(files) > 0:
        # Most recent file.
        last_file = max(files, key=os.path.getctime)

        # Return without .zip ending.
        last_path = last_file.split('.')[0]

        # Find next name.
        agent_name = last_path.split('/')[-1]

        # Check there is an underscore in name
        if '_' not in agent_name:
            raise RuntimeError("Most recent file does not have correct file name.")

        # Get next agent number
        split_name = agent_name.split('_')
        new_num = int(split_name[-1]) + 1
        joint_name = '_'.join(split_name[:-1])
        if folder[-1] == "/":
            next_path = folder + joint_name + '_' + str(new_num)
        else:
            next_path = folder + "/" + joint_name + '_' + str(new_num)
    else:
        last_path = None
        if folder[-1] == "/":
            next_path = folder + string_base + "1"
        else:
            next_path = folder + "/" + string_base + "1"

    return last_path, next_path

def get_agents(folder, string_base=None):
    """Find all agent files in a folder, or all agents matching string_base
        Parameters:
        folder - Folder to check for agent models.
               - Folder in format "path/to/folder"
        string_base - A string to match file names against
        Returns:
        agents - List of agent file paths.
    """
    # Check folder exists.
    if not os.path.isdir(folder):
        raise ValueError("Folder does not exist.")

    # Check which .zip files are present in the folder
    files =  glob.glob(folder + "/*.zip")

    # Remove all .zip extensions.
    for i in range(len(files)):
        files[i] = files[i][:-4]

    # Check against the string base
    if string_base is None:
        return files
    else:
        new_files = []
        for file_name in files:
            sub_string = file_name.split('/')[-1]
            if string_base in sub_string:
                new_files.append(file_name)
        return new_files

def run_model(env, model, obs=None, hist_save_name=None, render=False):
    """Run model on environment.
        Parameters:
        env - Environment to run.
        model - Agent model to use on environment.
              - Must match the environment definition.
        obs - If obs is None, agent is reset.
            - If obs is provided, this is used as the first observation.
        hist_save_name - File path to save history file to.
        render - If true, images are rendered and shown.
        Returns:
        Total reward - Total reward obtained during the run.
    """
    if obs is None:
        obs = env.reset()
    info = {}
    while not env.complete():
        action, _states = model.predict(obs)
        obs, _, _, info = env.step(action)
        if render:
            env.render()
    env.close()

    if hist_save_name is not None:
        print("Saving history")
        env.save_history(name=hist_save_name)

    return info['total_reward']

def plot_action_separation( folder=None, histories=None,
                            titles=None, suptitle=None,
                            save=False, single_althete=True,
                            sep_ylim=None, plot_control_vals=False,
                            image_save_name=None, show=True,
                            plot_fatigue=True, legend_off_plot=False,
                            show_overtake=True, rider_names=None,
                            arrow_labels=None, rider_a_int=1):
    """Plot rider history files for a number of simulations.
        Parameters:
        folder    - location of history files.
        histories - Provided history file names in an array.
                  - History names don't need to end with .hist, but if they
                    do not have an extension, this is appended.
                  - If none provided, all will be plotted.
        titles - List of title strings for plots to be created.
        suptitle - String of the super title to be created for plot.
        save      - Bool, if true the file is saved out in the folder
                    the function is called from.
        single_athlete - Only plot the actions for rider A.
                       - Default is true, if false, rider B actions
                         will also be plotted.
        sep_ylim - Define limit on the separation axes to overide automatic detection.
        plot_control_vals - If true, parameters which define the drafting controller
                            are plotted (overshoot, undershoot, response time).
                          - Not recommended for use unless investigating drafting controller
                            only.
        image_save_name - Filename to save an image to. If provided, image is saved.
        show - If true, image is shown to user.
        plot_fatigue - If true, fatigue plots are overlaid onto the action separation graph.
        legend_off_plot - If true, legend is placed outside axex.
        show_overtake - Add line demonstrating overtake point.
        rider_names - Specific names to used for riders in order of ['Rider A', 'Rider B']
                    - If none provided, these are used.
        arrow_labels - List of labels to add to plot.
                     - Each element is (ax, x_point, y_point, x_text, y_text, text) for an arrow
                     - Or (ax, x_text, y_text, text)
        rider_a_int - Integer for rider A's data in history file.
    """

    from matplotlib import rc
    rc('font', **{'family':'CMU Serif'})
    rc('text', usetex=True)

    if folder is None and histories is None:
        agent_folder = '*hist'
        histories = [file.split('/')[-1] for file in glob.glob(agent_folder)]
    elif histories is None:
        agent_folder = folder + '/*hist'
        histories = glob.glob(agent_folder)

    if rider_a_int == 1:
        rider_b_int = 2
    else:
        rider_a_int = 2
        rider_b_int = 1

    if titles is not None and len(titles) != len(histories):
        raise ValueError("Number of titles does not match number of histories.")

    if len(histories) <= 3:
        rows = len(histories)
        cols = 1
    else:
        rows = int(np.ceil(len(histories)/2))
        cols = 2

    if rider_names is not None and len(rider_names) != 2:
        raise ValueError('Epected two rider names.')
    elif rider_names is None:
        rider_names = ['Rider A', 'Rider B']

    # Append '.hist' if no file extension.
    for i, hist in enumerate(histories):
        if len(hist.split('.')) == 1:
            histories[i] = hist + '.hist'

    # Get axis limits
    if sep_ylim is None:
        ymin = 0
        ymax = 0
        for hist in histories:
            data = pickle.load(open(hist, 'rb'))
            state_dim = len(data[1]['state'][0])
            state_a = np.reshape(np.array(data[rider_a_int]['state']), (-1, state_dim))
            state_b = np.reshape(np.array(data[rider_b_int]['state']), (-1, state_dim))

            ymax_check = max(state_a[:, 0] - state_b[:, 0])
            ymin_check = min(state_a[:, 0] - state_b[:, 0])
            if  ymax_check > ymax:
                ymax = ymax_check
            if ymin_check < ymin:
                ymin = ymin_check
        ymax += 0.2*abs(ymax - ymin)
        ymin -= 0.2*abs(ymax - ymin)
    else:
        ymin = sep_ylim[0]
        ymax = sep_ylim[1]

    if plot_control_vals:
        overshoots, undershoots, response_t = draft_step_control_performance(folder=folder,
            histories=histories)

    fig, axes = plt.subplots(nrows=rows, ncols=cols)

    for i, history in enumerate(histories):
        data = pickle.load(open(history, 'rb'))
        times = data[0]
        control_a = data[rider_a_int]['control']
        control_b = data[rider_b_int]['control']
        state_dim = len(data[1]['state'][0])
        state_a = np.reshape(np.array(data[rider_a_int]['state']), (-1, state_dim))
        state_b = np.reshape(np.array(data[rider_b_int]['state']), (-1, state_dim))

        # Plot the left, control axes
        # ax1 = plt.subplot(rows, cols, i+1)
        if cols == 1 and rows == 1:
            ax1 = axes
        elif cols > 1 and rows > 1:
            ax1 = axes[i // cols][i % cols]
        else:
            ax1 = axes[i]

        ax1.set_xlabel('Time (s)')
        if single_althete:
            ax1.set_ylabel('Control', color='tab:red')
            ax1.plot(times, control_a, color='tab:red')
        if not single_althete:
            ax1.set_ylabel('Control')
            ax1.plot(times, control_a, color='tab:red', linestyle=':', label=rider_names[0] + ' Control')
            ax1.plot(times, control_b, color='tab:green', linestyle=':', label=rider_names[1] + ' Control')
        ax1.set_ylim([-0.1,1.1])

        # Plot fatigue if required
        if plot_fatigue:
            fatigue_a = data[rider_a_int]['fatigue']
            fatigue_b = data[rider_b_int]['fatigue']

            if single_althete:
                ax1.fill_between(times, np.zeros(len(times)), fatigue_a,
                    color='tab:green', alpha=0.2, label=rider_names[0] + ' Fatigue')
            else:
                # Check which rider has least fatigue and fill first
                fat_sum = np.sum(fatigue_a - fatigue_b)
                if fat_sum > 0:
                    ax1.fill_between(times, np.zeros(len(times)), fatigue_b,
                        color='tab:green', alpha=0.2, label=rider_names[1] + ' Fatigue')
                    ax1.fill_between(times, fatigue_a, fatigue_b,
                        color='tab:red', alpha=0.2, label=rider_names[0] + ' Fatigue')
                else:
                    ax1.fill_between(times, np.zeros(len(times)), fatigue_a,
                            color='tab:red', alpha=0.2, label=rider_names[0] + ' Fatigue')
                    ax1.fill_between(times, fatigue_a, fatigue_b,
                        color='tab:green', alpha=0.2, label=rider_names[1] + ' Fatigue')

        # Plot the right, separation axes.
        sep = state_a[:, 0] - state_b[:, 0]
        ax2 = ax1.twinx()
        color = 'tab:blue'
        if ymax - ymin < 400:
            ax2.set_ylim([ymin, ymax])
        ax2.set_ylabel('Separation ($d_{Ag} - d_{Ex}$) (m)', color=color)
        ax2.plot(times, sep, color=color, label='Separation')

        # Plot control performance params
        if plot_control_vals:
            ax2.plot(times[overshoots[i][1]], sep[overshoots[i][1]], 'r*', label='Overshoot')
            ax2.plot(times[undershoots[i][1]], sep[undershoots[i][1]], 'g*', label='Undershoot')
            ax2.plot(times[response_t[i][1]], sep[response_t[i][1]], 'b*', label='Response Time')

        if titles is None:
            # Latex cant deal with underscores
            filename = history.split('/')[-1]
            title = "File: " + filename[:-5].replace("_", " ")
            ax1.set_title(title, fontsize=15)
        elif titles[i] is not None:
            ax1.set_title(titles[i], fontsize=15)

        # Plot dotted line on 0 separation
        if show_overtake:
            ax2.plot([times[0], times[-1]], [0, 0], 'k--', label='Overtake Point')

    # Add any labels
    if arrow_labels is not None:
        for label in arrow_labels:
            if len(label) == 6:
                if rows*cols == 1:
                    axes.annotate(label[5], xy=(label[1], label[2]),
                                  xytext=(label[3], label[4]),
                                  arrowprops=dict(facecolor='black',
                                                  width=1.0,
                                                  headwidth=6.0,
                                                  headlength=6.0),
                                  ha='center', va='center', fontsize=10)
                else:
                    axes[label[0]].annotate(label[5], xy=(label[1], label[2]),
                                            xytext=(label[3], label[4]),
                                            arrowprops=dict(facecolor='black',
                                                            width=1.0,
                                                            headwidth=6.0,
                                                            headlength=6.0),
                                            ha='center', va='center', fontsize=10)
            else:
                if rows*cols == 1:
                    axes.annotate(label[3], xy=(label[1], label[2]),
                                  ha='center', va='center', fontsize=10)
                else:
                    axes[label[0]].annotate(label[3], xy=(label[1], label[2]),
                                            ha='center', va='center', fontsize=10)

    if suptitle is not None:
        fig = plt.gcf()
        fig.suptitle(suptitle)

    if not single_althete:
        lines, labels = ax1.get_legend_handles_labels()
        lines2, labels2 = ax2.get_legend_handles_labels()
        fig.legend(lines + lines2, labels + labels2, loc=7, prop={'size': 11})

    fig.set_size_inches(8, 5.33)
    fig.tight_layout()
    fig.subplots_adjust(right=0.7)
    if save:
        if folder is None and image_save_name is None:
            plt.savefig("action_separation.png", dpi=480, bbox_inches="tight")
        elif folder is not None and image_save_name is None:
            plt.savefig(folder + "action_separation.png", dpi=480, bbox_inches="tight")
        elif image_save_name is not None:
            if len(image_save_name.split('.')) == 1:
                plt.savefig(image_save_name, dpi=480, bbox_inches="tight")
            else:
                plt.savefig(image_save_name + '.png', dpi=480, bbox_inches="tight")
    if show:
        plt.show()
    else:
        plt.close()

def plot_fatigue(folder=None, histories=None, titles=None,
                    suptitle=None, save=False, image_save_name=None,
                    show=True):
    """Plot rider fatigue measures.
        Parameters:
        folder    - location of history files.
        histories - Provided history file names in an array (with the .hist)
                    - If none provided, all will be plotted.
        titles - List of title strings for plots to be created.
        suptitle - String of the super title to be created for plot.
        save      - Bool, if true the file is saved out in the folder
                    the function is called from.
    """
    from matplotlib import rc
    rc('font', **{'family':'CMU Serif'})
    rc('text', usetex=True)

    if folder is None and histories is None:
        agent_folder = '*hist'
        histories = [file.split('/')[-1] for file in glob.glob(agent_folder)]
    elif histories is None:
        agent_folder = folder + '/*hist'
        histories = glob.glob(agent_folder)

    if titles is not None and len(titles) != len(histories):
        raise ValueError("Number of titles does not match number of histories.")

    if len(histories) <= 3:
        rows = len(histories)
        cols = 1
    else:
        rows = int(np.ceil(len(histories)/2))
        cols = 2

    for i, history in enumerate(histories):
        data = pickle.load(open(history, 'rb'))

        ax1 = plt.subplot(rows, cols, i+1)
        times = data[0]
        fatigue_a = data[1]['fatigue']
        fatigue_b = data[2]['fatigue']

        ax1.set_xlabel('Time (s)')
        ax1.set_ylabel('Fatigue Measure')
        ax1.plot(times, fatigue_a, color='tab:green', label='Rider A')
        ax1.plot(times, fatigue_b, color='tab:blue',  label='Rider B')

        if titles is None:
            # Latex cant deal with underscores
            filename = history.split('/')[-1]
            title = "File: " + filename[:-5].replace("_", " ")
        else:
            title = titles[i]
        ax1.set_title(title)

    if suptitle is not None:
        fig = plt.gcf()
        fig.suptitle(suptitle)

    plt.legend()
    plt.tight_layout(rect=[0, 0, 1, 0.975])
    if save:
        if folder is None and image_save_name is None:
            plt.savefig("fatigue.png", dpi=480)
        elif folder is not None and image_save_name is None:
            plt.savefig(folder + "fatigue.png", dpi=480)
        elif image_save_name is not None:
            if len(image_save_name.split('.')) == 1:
                plt.savefig(image_save_name, dpi=480)
            else:
                plt.savefig(image_save_name + '.png', dpi=480)

    if show:
        plt.show()
    else:
        plt.close()

def plot_velocities(folder=None, histories=None, titles=None,
                    suptitle=None, save=False, image_save_name=None,
                    show=True):
    """Plot rider velocities.
        Parameters:
        folder    - location of history files.
        histories - Provided history file names in an array (with the .hist)
                    - If none provided, all will be plotted.
        titles - List of title strings for plots to be created.
        suptitle - String of the super title to be created for plot.
        save      - Bool, if true the file is saved out in the folder
                    the function is called from.
    """
    from matplotlib import rc
    rc('font', **{'family':'CMU Serif'})
    rc('text', usetex=True)

    if folder is None and histories is None:
        agent_folder = '*hist'
        histories = [file.split('/')[-1] for file in glob.glob(agent_folder)]
    elif histories is None:
        agent_folder = folder + '/*hist'
        histories = glob.glob(agent_folder)

    if titles is not None and len(titles) != len(histories):
        raise ValueError("Number of titles does not match number of histories.")

    if len(histories) <= 3:
        rows = len(histories)
        cols = 1
    else:
        rows = int(np.ceil(len(histories)/2))
        cols = 2

    # Get axis limits
    ymin = 0
    ymax = 0
    for hist in histories:
        data = pickle.load(open(hist, 'rb'))
        state_dim = len(data[1]['state'][0])
        state_a = np.reshape(np.array(data[1]['state']), (-1, state_dim))
        state_b = np.reshape(np.array(data[2]['state']), (-1, state_dim))

        ymax_check = np.max(np.concatenate((state_a[:, 1], state_b[:, 1])))
        if  ymax_check > ymax:
            ymax = ymax_check
    ymax += 0.1*abs(ymax - ymin)

    for i, history in enumerate(histories):
        data = pickle.load(open(history, 'rb'))
        ax1 = plt.subplot(rows, cols, i+1)
        times = data[0]
        state_dim = len(data[1]['state'][0])
        state_a = np.reshape(np.array(data[1]['state']), (-1, state_dim))
        state_b = np.reshape(np.array(data[2]['state']), (-1, state_dim))

        ax1.set_xlabel('Time (s)')
        ax1.set_ylabel('Velocity ($ms^{-1}$)')
        ax1.plot(times, state_a[:,1], color='tab:green', label='Rider A')
        ax1.plot(times, state_b[:,1], color='tab:blue',  label='Rider B')
        ax1.set_ylim([ymin, ymax])

        if titles is None:
            # Latex cant deal with underscores
            filename = history.split('/')[-1]
            title = "File: " + filename[:-5].replace("_", " ")
        else:
            title = titles[i]
        ax1.set_title(title)

    if suptitle is not None:
        fig = plt.gcf()
        fig.suptitle(suptitle)

    plt.legend()
    plt.tight_layout(rect=[0, 0, 1, 0.975])
    if save:
        if folder is None and image_save_name is None:
            plt.savefig("velocity.png", dpi=480)
        elif folder is not None and image_save_name is None:
            plt.savefig(folder + "velocity.png", dpi=480)
        elif image_save_name is not None:
            if len(image_save_name.split('.')) == 1:
                plt.savefig(image_save_name, dpi=480)
            else:
                plt.savefig(image_save_name + '.png', dpi=480)

    if show:
        plt.show()
    else:
        plt.close()

def draft_step_control_performance(folder=None, histories=None, set_point=2.5):
    """Evaluate the performance of a controller.
        Parameters:
        folder    - location of history files.
        histories - Provided history file names in an array (with the .hist)
                  - If none provided, all in folder will be used.
        set_point - The separation about
        Returns:
        overshoot - List of maximum overshoots of set point.
        undershoot - List of maximum undershoot of the set point.
        response_t - List of response time of the controller.
    """

    if folder is None and histories is None:
        agent_folder = '*hist'
        histories = [file.split('/')[-1] for file in glob.glob(agent_folder)]
    elif histories is None:
        agent_folder = folder + '/*hist'
        histories = glob.glob(agent_folder)

    overshoots = []
    undershoots = []
    response_t = []

    def moving_av(a, n=20) :
        ret = np.cumsum(a, dtype=float)
        ret[n:] = ret[n:] - ret[:-n]
        return ret[n - 1:] / n

    for history in histories:
        data = pickle.load(open(history, 'rb'))
        times = data[0]
        state_dim = len(data[1]['state'][0])
        state_a = np.reshape(np.array(data[1]['state']), (-1, state_dim))
        state_b = np.reshape(np.array(data[2]['state']), (-1, state_dim))
        sep = state_a[:, 0] - state_b[:, 0]

        # Find max overshoot
        arg_max = np.argmax(sep)
        max_sep = sep[arg_max]
        if max_sep > set_point:
            overshoots.append( (max_sep - set_point, arg_max) )
        else:
            overshoots.append( (0, -1) )

        # Find max undershoot - start searching after max
        if arg_max == len(sep) - 1:
            undershoots.append( (0, -1) )

        arg_min = np.argmin(sep[arg_max:]) + arg_max
        min_sep = sep[arg_min]
        if min_sep < set_point:
            undershoots.append( (set_point - min_sep, arg_min) )
        else:
            undershoots.append( (0, -1))

        # Find response time
        ind = -1
        mov_meas = moving_av(sep)
        for i in range(len(mov_meas)-50):
            vals = np.array([mov_meas[i + j] for j in [0, 10, 20, 30, 40, 50]])
            vals = abs(vals-2.5)
            if all(val < 0.025 for val in vals):
                ind = i
                break
        response_t.append( (times[ind], ind) )

    return overshoots, undershoots, response_t



