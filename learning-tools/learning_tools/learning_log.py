"""Tools for use in logging learning process.
Classes
-------
LearningLog - Class to implement logging.

Functions
---------
"""

from datetime import datetime
import numpy as np

class LearningLog:
    """Class implementing logging during learning process.
        Parameters
        ----------
        Public Methods
        --------------
   """

    def __init__(self, filename="learn_log.txt"):
        """Initialise Learning log.
            Parameters:
            filename - Filename for the log file.
        """
        # Open the log file for writing.
        self.filename = filename

        # Table elements for each run.
        self.tables = {}

        # Specific lines
        self.description = ""
        self.lines = []

        # Store title and starting time.
        now = datetime.now()
        dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
        self.title = "LEARNING LOG \n"
        self.time_string = "Time: " + dt_string + '\n'

    def set_description(self, desciption):
        """Set the title of the log.
            Parameters:
            description - The description line to place at the top
                          of the log file.
        """
        if not isinstance(desciption, str):
            raise ValueError("Descripton must be a string.")
        self.description = desciption + '\n'

    def add_table(self, table_name, column_titles):
        """Set the table definition for the log file.
            Parameters:
            column_titles - List of string keys to be used for column titles.
                          - Any table values added after this must match the ordering
                            in this list.
            table_name - Unique name to access table.
        """
        if table_name in self.tables.keys():
            raise ValueError("Table name already in use.")
        self.tables[table_name] = Table(table_name, column_titles)

    def add_table_line(self, table_name, values):
        """Add a line to the table.
            Parameters:
            values - Array like set of values to be added to the table.
                   - Values can be of any string convertable type.
        """
        if not hasattr(values, '__len__'):
            raise ValueError("Values must be a 1D array or list.")
        if table_name not in self.tables.keys():
            raise ValueError("Unknown table name")

        elif len(values) != self.tables[table_name].num_columns:
            raise ValueError("Values do not match table definition.")

        for val in values:
            try: str(val)
            except: raise ValueError('Values must be string convertable.')
        self.tables[table_name].add_line(values)

    def add_line(self, string):
        """Add a line to the log file.
            Parameters:
            string - The line to be added to log file.
        """
        if not isinstance(string, str):
            raise ValueError("Line must be a string.")
        self.lines.append(string)

    def update_string(self):
        """Create the full string to be reported."""
        # Title, time and description.
        full_string = self.title + '\n' + \
                      self.time_string + \
                      self.description + '\n'

        # Report tables
        for table in self.tables.values():
            full_string += table.table_string()

        # Add lines in order.
        for line in self.lines:
            full_string += line
            full_string += '\n'

        return full_string

    def dump(self, skip_errors=False):
        """Dump contents of the log to log file."""
        # Try opening the file
        try:
            log = open(self.filename,"a")
        except:
            if skip_errors:
                return
            else:
                raise ValueError("Could not write out to log file. Please check file: "
                                + self.filename  + " is not open in another program.")

        # Write the formatted document to file.
        full_string = self.update_string()
        log.write(full_string)

    def report(self):
        """Report the data."""
        print(self.update_string())

    def reset(self):
        """Reset storage."""
        self.description = ""
        for key in self.tables.keys():
            self.tables[key].reset()
        self.lines = []

        # Store new start time
        now = datetime.now()
        dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
        self.time_string = "Time: " + dt_string + '\n'

class Table:
    """Class implementing table like reporting.
        Parameters
        ----------
        Public Methods
        --------------
    """

    def __init__(self, table_name, column_titles):
        """ Parameters:
            columns - List of column names used in table.
        """
        self.table_name = table_name
        self.table_columns = {}
        self.table_lines = []
        self.table_delim = "|"
        self.num_columns = 0

        # Store column titles
        for i, key in enumerate(column_titles):
            if not isinstance(key, str):
                raise ValueError("Column titles must be strings.")
            # Add key in order
            self.table_columns[key] = i
        self.num_columns = len(self.table_columns.keys())

    def add_line(self, values):
        """Add a line to the table.
            Parameters:
            values - Array like set of values to be added to the table.
                   - Values can be of any string convertable type.
        """
        if not hasattr(values, '__len__'):
            raise ValueError("Values must be a 1D array or list.")
        elif len(values) != len(self.table_columns.keys()):
            raise ValueError("Values do not match table definition.")

        for val in values:
            try: str(val)
            except: raise ValueError('Values must be string convertable.')
        self.table_lines.append(values)

    def table_string(self):
        """Create string describing the table."""
        # Find minimum number of tabs needed for columns
        if len(self.table_lines) == 0:
            return ""

        min_space = 0
        for col in self.table_columns.keys():
            if min_space < len(col):
                min_space = len(col)
        min_space += 2

        # Check maximum value
        max_vals = max(self.table_lines)
        for val in max_vals:
            try:
                max_len = np.ceil(np.log10(max_val))
                if max_len  > min_space:
                    max_space = np.ceil(np.log10(max_val)) + 2
            except:
                continue

        # Format the table name line
        full_string = self.table_name + ":\n"
        name_line = " " + self.table_delim
        for col in self.table_columns.keys():
            name_line += col + " "*(min_space - len(col)) + self.table_delim
        full_string += name_line + '\n'

        # Write all table entries.
        for i in range(len(self.table_lines)):
            line = ""
            for val in self.table_lines[i]:
                # Limit width of string value
                str_val = str(val)
                if len(str_val) > min_space - 1:
                    str_val = str_val[0:min_space-1]
                line += str_val + " "*(min_space - len(str_val)) + "|"
            line = " " + self.table_delim + line + '\n'
            full_string += line
        full_string += '\n'

        return full_string

    def reset(self):
        """Reset the table."""
        self.table_lines = []



