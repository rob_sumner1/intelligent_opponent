"""Test functions from deploy.py"""
import os
import pytest
import numpy as np
import tensorflow as tf
from collections import Counter
from stable_baselines import DQN
from stable_baselines.deepq import MlpPolicy
from learning_tools import convert_to_keras_agent

def test_convert_baselines_to_keras_model():
    """Test converting a baselines model."""
    # Should fail on file that doesn't exist
    with pytest.raises(ValueError):
        convert_to_keras_agent('not_a_path')

    # Remove existing .h5 files
    agent_file = os.path.dirname(os.path.abspath(__file__)) + '/agent_folder/msr_agent_trained'
    if os.path.exists(agent_file + '_network.h5'):
        os.remove(agent_file + '_network.h5')
    assert not os.path.exists(agent_file + '_network.h5')

    convert_to_keras_agent(agent_file)
    assert os.path.exists(agent_file + '_network.h5')

    # Check for a given value
    test_points_file = os.path.dirname(os.path.abspath(__file__)) + '/agent_folder/state_test_points.npz'
    test_points = np.load(test_points_file)['obs']

    sb_agent = DQN.load(agent_file + '.zip')
    sb_dists = sb_agent.step_model.sess.run([sb_agent.step_model.q_values,
                                            sb_agent.step_model.policy_proba],
                                            {sb_agent.step_model.obs_ph: test_points})[0]
    tf_agent = tf.keras.models.load_model(agent_file + '_network.h5')
    tf_dists = tf_agent.predict(test_points)

    # Check total number of errors
    sb_prediction = sb_agent.predict(test_points)[0]
    tf_prediction = np.argmax(tf_agent.predict(np.array(test_points)), axis=1)
    errors = np.count_nonzero(sb_prediction - tf_prediction)
    assert errors/test_points.shape[0] < 5e-5

    # Some differenes in prediction dists due to change
    # in architecture.
    for i in range(len(sb_dists)):
        if np.argmax(sb_dists[i]) != np.argmax(tf_dists[i]):
            # Print results if you want more details
            # print('Test = ', i)
            # print('State = ', test_points[i])
            # print('Baselines distribution: ', sb_dists[i])
            # print('Baselines argmax: ', np.argmax(sb_dists[i]))
            # print('Keras distribution: ', tf_dists[i])
            # print('Keras argmas: ', np.argmax(tf_dists[i]))
            # print('')

            # Assert total variation of distributions is small.
            total_variation = np.sum( np.abs(sb_dists[i] - tf_dists[i]) )
            assert total_variation <= 1e-4
