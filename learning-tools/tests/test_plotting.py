"""Test the plotting tools."""

import pytest
from learning_tools import VelodromePlot, distance_index
import numpy as np
import pickle
import os

@pytest.fixture
def velo():
    """Return a new instance of the Velodrome plot."""
    return VelodromePlot()

@pytest.fixture
def velo_filled():
    """Return a new instance of the Velodrome plot."""
    velo = VelodromePlot()
    velo.add_track()
    velo.add_rider(1, [255, 0, 0], distance=0)
    velo.add_rider(2, [0,255,0], distance=10)
    return velo

def test_velo_init(velo):
    """Test the initialisation of the VelodromePlot class"""
    assert velo.render_buffer_x == 0.2
    assert velo.render_buffer_y == 0.3
    assert len(velo.x_survey) == len(velo.y_survey)
    assert len(velo.x_survey) == 1001
    assert len(velo.riders.keys()) == 0
    assert velo.screen_width % 2 == 0
    assert velo.screen_height % 2 == 0
    assert velo.viewer is None

def test_pixel_coord(velo):
    """Test the conversion from local to pixel coordinates."""
    xp, yp = velo.pixel_coord(1, 1)
    assert isinstance(xp, float)
    assert isinstance(yp, float)

    xp, yp = velo.pixel_coord([1,1], [1,1])
    assert isinstance(xp, np.ndarray)
    assert isinstance(yp, np.ndarray)

    with pytest.raises(ValueError):
        velo.pixel_coord('a', 1)
    with pytest.raises(ValueError):
        velo.pixel_coord([1], 'b')

    velo.reset()

def test_add_track(velo):
    """Test adding track to the plot."""
    velo.add_track()
    velo.show(test=True)
    assert velo.viewer is not None
    velo.reset()

def test_add_rider(velo):
    """Test adding riders to the plot."""
    velo.add_rider(1, [255, 0, 0])
    assert 1 in velo.riders.keys()

    with pytest.raises(ValueError):
        velo.add_rider(1, [255, 0, 0])

    velo.add_rider(2, [0,255,0])
    assert len(velo.riders.keys()) == 2
    assert 2 in velo.riders.keys()
    velo.reset()

def test_move_rider(velo):
    """Test moving a rider around the plot."""
    with pytest.raises(ValueError):
        velo.move_rider(1, 0)

    velo.add_rider(1, [255,0,0],0)
    velo.move_rider(1, 10)
    pos = velo.find_rider_points(10)
    xp, yp = velo.pixel_coord(pos[0], pos[1])
    assert velo.riders[1][1].translation[0] == xp
    assert velo.riders[1][1].translation[1] == yp
    velo.reset()

def test_add_label(velo):
    """Test adding a label to the plot."""
    velo.add_label(10, 10, "sss", id=1)
    assert 1 in velo.labels.keys()

    with pytest.raises(ValueError):
        velo.add_label(100, 100, "sss", id=1)

    velo.add_label(10, 10, "sss")
    assert 2 in velo.labels.keys()
    velo.reset()

def test_reset(velo):
    """Test reset function."""
    assert velo.viewer is None
    velo.reset()
    velo.add_rider(1, [255,0,0],0)
    velo.add_label(1, 1, "ssss", id=1)
    assert len(velo.riders.keys()) == 1
    assert len(velo.labels.keys()) == 1
    velo.reset()
    assert velo.viewer is None
    assert len(velo.riders.keys()) == 0
    assert len(velo.labels.keys()) == 0

def test_distance_index():
    """Test the distance index function."""
    data = pickle.load(open("history_test.hist", 'rb'))
    rider_a = data[1]['state']
    rider_b = data[2]['state']

    d1 = max(rider_a[100][0], rider_b[100][0])
    d2 = max(rider_a[200][0], rider_b[200][0])

    i, d = distance_index(data, 0)
    assert i == 0
    assert d <= 0
    i, d = distance_index(data, d1)
    assert i == 100
    assert d <= d1
    i, d = distance_index(data, d2)
    assert i == 200
    assert d <= d2

def test_show_save(velo_filled):
    """Test the save functionality for viewer."""
    # Remove saved image if it exists.
    path = 'test_save_image.png'
    save_name = 'test_save_image'
    if os.path.exists(path):
        os.remove(path)

    assert not os.path.exists(path)
    velo_filled.show(test=True)
    assert not os.path.exists(path)
    velo_filled.show(file_name=save_name, test=True)
    assert os.path.exists(path)

def test_scale_track(velo):
    """Test the track scaling function"""
    new_track = velo.scale_track(0)
    assert new_track[0,0] == velo.x_survey[0]
    assert new_track[0,1] == velo.y_survey[0]

    # Check the straight is presered
    new_track = velo.scale_track(1)
    assert new_track[0,0] == velo.x_survey[0]
    assert new_track[0,1] == velo.y_survey[0] - 1
    assert new_track[10,0] == velo.x_survey[10]
    assert new_track[10,1] == velo.y_survey[10] - 1

