"""Set of tests for the learning tools."""

import os
import pytest
import gym
from stable_baselines import DQN
from stable_baselines.deepq.policies import MlpPolicy

from learning_tools.learning_tools import *

def test_last_trained_model():
    """Test loading of the last trained DQN model."""
    folder =  os.path.dirname(os.path.abspath(__file__)) + "/agent_folder"

    with pytest.raises(ValueError):
        get_last_agent(folder + "/ls", string_base='msr_agent_dqn')

    cur, nex = get_last_agent(folder, string_base='msr_agent_dqn')
    assert cur == folder + "/msr_agent_dqn_1"
    assert nex == folder + "/msr_agent_dqn_2"

def test_get_agents():
    """Test function to find the set of agents."""
    agent_folder = os.path.dirname(os.path.abspath(__file__)) + "/agent_folder"

    files = get_agents(agent_folder, string_base=None)
    assert len(files) == 3

    files = get_agents(agent_folder, string_base="msr_agent_dqn")
    assert len(files) == 1

def test_run_model():
    """Test helper function to run model."""
    file_path = os.path.dirname(os.path.abspath(__file__))
    agent_file = file_path + "/agent_folder/run_model"
    env = gym.make('msr-fixed-draft-v1')
    model = DQN.load(agent_file)

    # Check reward return
    reward = run_model(env, model)
    assert reward == 58

    # Delete history file if it exists.
    hist_file = "run_model_history"
    if os.path.exists(hist_file + ".hist"):
        os.remove(hist_file + ".hist")
    reward = run_model(env, model, hist_save_name=hist_file)
    assert reward == 58
    assert os.path.exists(hist_file + ".hist")