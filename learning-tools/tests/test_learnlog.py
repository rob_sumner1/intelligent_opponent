"""Test learn log class."""

import pytest
from learning_tools import LearningLog
import os

@pytest.fixture
def log():
    """Return a new instance of the Learning Log class."""
    return LearningLog()

@pytest.fixture
def filled_log():
    """Return an instance of the Learning Log class with some values."""
    log = LearningLog()
    log.add_line("This is a test line.")
    log.add_line("This is another test line...")
    log.set_description("Test log file for testing logs...")
    log.add_table("table1", ['val1', 'val2'])
    log.add_table_line("table1", [1.101010101010101010,2])
    log.add_table_line("table1", [3,4])
    return log

@pytest.fixture
def double_filled_log():
    """Return an instance of the Learning Log class with some values."""
    log = LearningLog()
    log.add_line("This is a test line.")
    log.add_line("This is another test line...")
    log.set_description("Test log file for testing logs...")

    log.add_table("table1", ['val1', 'val2'])
    log.add_table_line("table1", [1.101010101010101010, 2])
    log.add_table_line("table1", [3, 4])

    log.add_table("table2", ['val1', 'val2', 'val3'])
    log.add_table_line("table2", [1.101010101010101010, 2, 4])
    log.add_table_line("table2", [3, 4, 444040404040404])
    return log

def test_log_init(log):
    """Test initialisation of learning log."""
    assert log.filename == "learn_log.txt"
    assert log.tables == {}
    assert log.lines == []

def test_log_set_table(log):
    """Test changing the table definition."""
    assert len(log.tables.keys()) == 0

    # Add a table entry
    log.add_table("table1", ["A","B"])
    assert len(log.tables.keys()) == 1
    assert len(log.tables['table1'].table_columns) == 2

    with pytest.raises(ValueError):
        # Reusing table name.
        log.add_table("table1", ["A","B","C"])

    log.add_table("table2", ["A","B","C"])
    assert len(log.tables.keys()) == 2
    assert len(log.tables['table2'].table_columns) == 3

    with pytest.raises(ValueError):
        # Cols must be strings.
        log.add_table("table3", ["A",1])

def test_log_table_line(filled_log):
    """Test adding a line of the table."""
    filled_log.add_table_line("table1", [1,2])
    assert len(filled_log.tables["table1"].table_lines) == 3

    with pytest.raises(ValueError):
        # Non-iterable
        filled_log.add_table_line('table1', 1)

    with pytest.raises(ValueError):
        # Wrong name
        filled_log.add_table_line('tabl', [1])

def test_log_add_line(log):
    """Test adding a line to log."""
    log.add_line("This is a valid line")
    assert len(log.lines) == 1

    with pytest.raises(ValueError):
        log.add_line(["This is not", "a valid line"])

def test_log_dump(double_filled_log):
    """Test the log dump method."""

    path = 'learn_log.txt'
    if os.path.exists(path):
        os.remove(path)

    assert not os.path.exists(path)
    double_filled_log.dump()
    assert os.path.exists(path)

def test_log_reset(filled_log):
    """Test resetting the log."""
    assert len(filled_log.lines) != 0
    assert len(filled_log.tables) != 0

    filled_log.reset()
    # Check storage is empty now
    assert len(filled_log.lines) == 0
    assert len(filled_log.tables) == 1
    assert len(filled_log.tables['table1'].table_lines) == 0