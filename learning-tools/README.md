# Intelligent Opponent

Collection of tools for use with learning approaches. 

### Prerequisites

A virtual environment should be set up following the instructions given in the Lapysim. 

**Note** Trained agents may not generalise to newer versions of Lapysim. Changes including bug fixes may cause agent behaviour to change significantly. Agents in this folder have been validated using Master branch of Lapysim up to 08/03/2020 (commit baad43a). Any changes after this (including the change to the physiology model in committ 9caca9a) could cause poor performance. 

### Installing

Once a Lapysim virtual environment has been created, each module can be installed individually using the command:
```
pip install -e . 
```
inside each of the module folders. 

To use code in this folder as intended, it is recommended you use the version of Lapysim agents were trained with.
This can be achieved with the commands (in the lapysim folder):
```
git checkout -b branch_name baad43a
```
to create a branch at this point. Alternatively, to just try running the code, use:
```
git checkout baad43a
```
which will will detatch the head and move it to this point. 

## Running the tests

Tests for all modules can be found in the *tests* folder inside each module. 

Tests are run using pytest:
```
pytest -v path_to_tests_folder
```
Or, for a specific test file:
```
pytest -v path_to_tests_folder/test_file.py
```

## Authors
* **Rob Sumner** (Final year project work). 

## License
This project is copywrited - see LICENSE.md for more details. 
