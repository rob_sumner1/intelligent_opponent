"""File contains several functions which allow agents to be used as opponents
   in simulations of the match sprint race.

   Classes
   -------

   Functions
   ---------
"""

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
import tensorflow as tf
tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)
import os
os.environ["KMP_WARNINGS"] = "FALSE"

import os
import random
import numpy as np
from gym_msr.envs.msr_policies import MsrPolicy, MsrNetPolicy
from lapysim import  NullPhysiology
from lapysim import VariableFatigueMaximalSubmaximalModel as VFMSM
from lapysim import athlete
import scipy.integrate as integrate
import sympy as sym
import itertools

class MsrOpponent():
    """
        Class implementing a number of controllers for a Lapysim MSR simulation.
        These controllers are based upon idential riders, using the variable fatigue
        model with fatigue rate 0.01.

        Notation - Rider being controlled by this class is known as the opponent.
                 - The rider that this class is racing against is known as the athlete.
                   (following basic idea for virtual training tool).

        Implemented controllers:
        agents - Deep-Q network based agents trained using Duelling, Double DQN.
                 (https://stable-baselines.readthedocs.io/en/master/modules/dqn.html)
               - There are 4 agents of this type to use - 'agent_1',...,'agent_4'.
        policies - Neural network based policy functions trained using supervised learning.
                 - There are 3 agents of this type - 'policy_1',...,'policy_3'
        expert - A hand crafted expert opponent.
               - Each time this class is reset, the policy will be randomised to give
                 different responses.
               - One opponent of this time - 'expert'.

        Parameters:
        event - (Lapysim simulation class) Event for MSR race being run.
        controller_name - (String) Name of controller to be used.
                   - If None provided, a random controller will be used.
                   - See above for implemented controllers.
        rider_name - (String) The name of the rider in the simulation that this
                     class will control.
                   - Needs to match athlete.name class for desired rider.
                   - Default value is 'Rider A'.
        random_on_reset - (Bool) Randomise the opponent controller on reset.
                        - Default is False, the same opponent will be used.
                        - If True, a new controller is selected at every reset.
        force_load_all - (Bool) If True, all opponents are loaded on initialisation.
                       - This will be faster when random_on_reset is True and resets
                         are called frequently (eg. when simulating races with non-human
                         rider).
                       - However, this will require more memory.

        Attributes:
        controller_name - (String) Name of controller being used
        controller_description - (String) Short description of controller being used.

        Public Methods:
        reset - Reset the controller being used.
              - Required to be called before a new race.
              - For the expert opponent, this will randomise the policy.
        predict - Predict the action of the agent for a given state.
                - Note that for the expert controller, predictions need
                  to be called sequentially.
                - For non-expert controllers, predictions do not have to
                  be sequential.

    """

    def __init__(self, simulation, controller_name=None, rider_name='Rider A',
                random_on_reset=False, force_load_all=False):

        self._random_on_reset = random_on_reset
        self._force_load_all = force_load_all

        # Filepaths for specific opponents + description + usage type.
        agent_folder = os.sep.join([os.path.dirname(os.path.abspath(__file__)), 'files'])
        self._opponent_types = {
            "agent_1" : [os.sep.join([agent_folder, 'agent_1.zip']),
                        'DQN Agent 1 - RL trained from scratch against experts. ' + \
                         'This agent sprints initially before employing a drop'],
            "agent_2" : [os.sep.join([agent_folder, 'agent_2.zip']),
                        'DQN Agent 2 - Pretrained agent 1. This agent emulates a ' + \
                        'drafting controller.'],
            "agent_3" : [os.sep.join([agent_folder, 'agent_3.zip']),
                        'DQN Agent 3 - Pretrained agent 2. This agent emulates a ' + \
                        'drafting controller.'],
            "agent_4" : [os.sep.join([agent_folder, 'agent_4.zip']),
                        'DQN Agent 4 - Pretrained agent with RL improvement.'],
            "policy_1": [os.sep.join([agent_folder, 'policy_1.h5']),
                        'Policy Agent 1 - Trained using supervised learning.'],
            "policy_2": [os.sep.join([agent_folder, 'policy_2.h5']),
                        'Policy Agent 2 - Trained using supervised learning.'],
            "policy_3": [os.sep.join([agent_folder, 'policy_3.h5']),
                        'Policy Agent 3 - Trained using supervised learning.'],
            "expert"  : [None, 'Expert Policy']
        }

        # Opponents used in for specific conditions
        self._lead_opponents = ['agent_1', 'agent_4', 'policy_2']
        self._trail_opponents = ['agent_2', 'agent_3', 'policy_1', 'policy_2', 'policy_3']

        # Load all agents if required
        if force_load_all:
            self._lead_opponent_agents = [MsrNetPolicy(self._opponent_types[name][0],
                                                        rider='Rider A') for name in self._lead_opponents]
            self._trail_opponent_agents = [MsrNetPolicy(self._opponent_types[name][0],
                                                        rider='Rider A') for name in self._trail_opponents]

        # Event class of parent simulation
        self._simulation = simulation
        self._match_sprint_event = self._simulation.event

        # opponent is being controlled by this class.
        # athlete is the athlete this class races against.
        athletes = self._match_sprint_event.athletes
        if len(athletes) != 2:
            raise RuntimeError('Expected two athletes in MSR event.')

        if athletes[0].name == rider_name:
            self._eval_first_rider = True
            self._opponent = self._match_sprint_event.athletes[0]
            self._athlete = self._match_sprint_event.athletes[1]
        elif athletes[1].name == rider_name:
            self._eval_first_rider = False
            self._opponent = self._match_sprint_event.athletes[1]
            self._athlete = self._match_sprint_event.athletes[0]
        else:
            raise ValueError('Could not find rider with name ' + str(rider_name))

        # Check opponent has correct physiology model
        if not isinstance(self._opponent.physiology, VFMSM):
            raise RuntimeError('Opponent controlled rider must have Variable ' \
                    + 'fatigue phisiology model.')

        # Infer physiology model for athlete
        if isinstance(self._athlete.physiology, VFMSM):
            self._virtual_athlete = True
        else:
            self._virtual_athlete = False

        # Set type of controller
        self._set_controller(controller_name=controller_name)

        # Create expert & fatigue functions.
        self._build_evaluators()
        self._athlete_fatigue_now = 1
        self._last_control_step = 0

    def reset(self, initial_position=None):
        """ Reset the opponent.
            - This should be called at the end of every Lapysim race simulation.
            Parameters:
            initial_position - (String) Initial starting position for the intelligent
                               opponent, either 'lead' or 'trail'.
                             - If random_on_reset is True, an opponent specific to the
                               initial position is used. If False, this argument is
                               ignored.
        """
        self._athlete_fatigue_now = 1
        self._last_control_step = 0

        if initial_position is not None:
            if initial_position not in ['lead', 'trail']:
                raise ValueError('Unexpected initial position: ', initial_position)

        # Agent is not swapped out.
        if self._random_on_reset:
            self._set_controller(controller_type=initial_position)
        else:
            self._agent.reset()

    def predict(self, opp_state, athlete_state):
        """Get control response for agent.
            Parameters:
            opp_state - State for the rider that is currently being
                        controlled by this opponent class
            athlete_state - State for the human rider this class is
                            racing against.
            Note - State for each rider should be [distance, velocity, fatigue].

            Returns:
            exertion - Value in the range [0, 1] to control rider exertion.
        """
        if self._virtual_athlete:
            if len(opp_state) != 3 or len(athlete_state) != 3:
                raise ValueError('Expected state vector of length 3 - [distance, velocity, fatigue].')
        else:
            warn_string = 'Expected state vector of length'
            if len(opp_state) != 3:
                raise ValueError(warn_string + ' 3 for opponent - [distance, velocity, fatigue].')
            elif len(athlete_state) != 2:
                raise ValueError(warn_string + ' 2 for athlete  - [distance, velocity].')

        # For agents, predictions are based on state with [rider_being_controlled, athlete]
        state = list( itertools.chain( *[opp_state, athlete_state] ) )
        if not self._virtual_athlete:
            state = list( itertools.chain( *[state, [self._get_athlete_fatigue_now()]] ) )

        if isinstance(self._agent, MsrPolicy):
            eq_exertion = self._get_draft_exertion(opp_state, athlete_state)
            return self._agent.predict(state, eq_exertion)
        else:
            return self._agent.predict(state)

    def evaluate(self, opp_state, athlete_state, normalise=False):
        """Use opponent to produce a quality distribution over actions.
            The distribution is a non-negative distribution (which sums to 1)
            which indicates which action the agent believes to be best.
            For agent based opponents, this uses the action value function. For
            policy based opponents, this uses the predictive distribution.

            Note - This will not work for expert opponent.

            Parameters:
            opp_state - State for the rider that is currently being
                        controlled by this opponent class
            athlete_state - State for the human rider this class is
                            racing against.

            Returns:
            value_dist - 1D array over containing distribution over actions.
                       - For agent opponents, distribution is 0:0.2:1 (length 5).
                       - For policy opponents, distribution is 0:0.1:1 (length 10).
        """
        if isinstance(self._agent, MsrPolicy):
            raise NotImplementedError('Cannot evaluate actions for expert opponents.')

        if self._virtual_athlete:
            if len(opp_state) != 3 or len(athlete_state) != 3:
                raise ValueError('Expected state vector of length 3 - [distance, velocity, fatigue].')
        else:
            warn_string = 'Expected state vector of length'
            if len(opp_state) != 3:
                raise ValueError(warn_string + ' 3 for opponent - [distance, velocity, fatigue].')
            elif len(athlete_state) != 2:
                raise ValueError(warn_string + ' 2 for athlete  - [distance, velocity].')

        # For agents, predictions are based on state with [rider_being_controlled, athlete]
        state = list( itertools.chain( *[opp_state, athlete_state] ) )
        if not self._virtual_athlete:
            state = list( itertools.chain( *[state, [self._get_athlete_fatigue_now()]] ) )

        action_dist = self._agent.evaluate(state)

        if normalise:
            if min(action_dist) < 0:
                action_dist -= min(action_dist)
            action_dist /= sum(action_dist)

        return action_dist

    def _set_controller(self, controller_name=None, controller_type=None):
        """Set the controller for the opponent.
            If no arguments are provided, a random controller will be chosen.
            Parameters:
            controller_name - (String) The name of a controller taken from the
                              _opponent_types dict. If provided, this controller is used
                              and will override the controller type argument.
            controller_type - (String) Type of controller to be used, in ['lead', 'trail']
        """
        if controller_name is None and controller_type is None:
            self.controller_name = random.choice( list(self._opponent_types.keys()) )
        # Use specific controller if provided.
        elif controller_name is not None:
            if controller_name not in self._opponent_types.keys():
                raise ValueError('Controller name not recognised: ' + str(controller_name))
            else:
                self.controller_name = controller_name
        # Choose a controller of a given type.
        elif controller_type not in ['lead', 'trail']:
            raise ValueError('Unexpected controller type: ' + str(controller_type))
        elif controller_type == 'lead':
            self.controller_name = random.choice( self._lead_opponents )
        else:
            self.controller_name = random.choice( self._trail_opponents )

        # Use pre-loaded policy if required
        if self._force_load_all:
            if self.controller_name in self._lead_opponents:
                self._agent = self._lead_opponent_agents[self._lead_opponents.index(self.controller_name)]
            else:
                self._agent = self._trail_opponent_agents[self._trail_opponents.index(self.controller_name)]
            return

        # Load type of agent.
        [self.controller_path, self.controller_description] = self._opponent_types[self.controller_name]
        if self.controller_name is "expert":
            # Will always pass state as [opponent, athlete] so Policy
            # controls Rider A (first rider in state).
            self._agent = MsrPolicy(rider='Rider A')
        else:
            self._agent = MsrNetPolicy(self.controller_path, rider='Rider A')

    def _build_evaluators(self):
        """Build evaluators needed to calculate equilibrium control for drafting."""
        # Acceleration of rider A.
        self._eval_opponent_accel = self._match_sprint_event.build_evaluator( ( \
            self._opponent.bike_fit._F_drive_sym - \
            self._opponent.aerodynamics._F_drag_sym - \
            self._opponent.rolling_resistance._F_rr_sym) / \
            self._opponent.anthropometry._mass_sym )
        # Acceleration of rider B.
        self._eval_athlete_accel = self._match_sprint_event.build_evaluator( ( \
            self._athlete.bike_fit._F_drive_sym - \
            self._athlete.aerodynamics._F_drag_sym - \
            self._athlete.rolling_resistance._F_rr_sym) / \
            self._athlete.anthropometry._mass_sym )

        # Mass of rider A, used as a multiplier.
        self._eval_opponent_mass = self._match_sprint_event.build_evaluator( \
            self._opponent.anthropometry._mass_sym )
        # Mass of rider B, used as a multiplier.
        self._eval_athlete_mass = self._match_sprint_event.build_evaluator( \
            self._athlete.anthropometry._mass_sym )

        # Rider A rolling drag / mass rider A.
        self._eval_opponent_norm_rr = self._match_sprint_event.build_evaluator( \
            self._opponent.rolling_resistance._F_rr_sym / \
            self._opponent.anthropometry._mass_sym )
        # Rider B rolling drag / mass rider B.
        self._eval_athlete_norm_rr = self._match_sprint_event.build_evaluator( \
            self._athlete.rolling_resistance._F_rr_sym / \
            self._athlete.anthropometry._mass_sym )

        # Rider A aero drag.
        self._eval_opponent_aero_drag = self._match_sprint_event.build_evaluator( \
            self._opponent.aerodynamics._F_drag_sym )
        # Rider B aero drag.
        self._eval_athlete_aero_drag = self._match_sprint_event.build_evaluator( \
            self._athlete.aerodynamics._F_drag_sym )

        # Rider A multiplier to get crank torque from drive force.
        self._eval_opponent_crank_torque = self._match_sprint_event.build_evaluator( \
            ( self._opponent.bike_fit._G_ratio_sym * self._opponent.bike.rear_wheel._diameter_sym ) / \
            ( 2 * self._opponent.bike._chain_eff_sym ) )

        # Rider A divisor for exertion from crank torque
        # Opponent will always have a Variable Fatigue model.
        self._eval_opponent_cad_max = self._match_sprint_event.build_evaluator( \
            self._opponent.physiology._cad_max_sym )
        self._eval_athlete_cad_now = self._match_sprint_event.build_evaluator( \
            self._athlete.dynamics._cad_sym )
        self._eval_opponent_T_max = self._match_sprint_event.build_evaluator( \
            self._opponent.physiology._T_max_sym )
        self._eval_opponent_T_crit = self._match_sprint_event.build_evaluator( \
            self._opponent.physiology._T_crit_sym )
        self._eval_opponent_fatigue = self._match_sprint_event.build_evaluator( \
            self._opponent.physiology._fatigue_now_sym )

        # Assume that athletes model is the same as opponent when athlete is real rider
        if not self._virtual_athlete:
            self._eval_athlete_cad_max = self._match_sprint_event.build_evaluator( \
                self._opponent.physiology._cad_max_sym )
            self._eval_athlete_T_max = self._match_sprint_event.build_evaluator( \
                self._opponent.physiology._T_max_sym )
            self._eval_athlete_T_crit = self._match_sprint_event.build_evaluator( \
                self._opponent.physiology._T_crit_sym )
            self._eval_athlete_fatigue_rate_max = self._match_sprint_event.build_evaluator( \
                self._opponent.physiology._fatigue_rate_max_sym)

    def _get_athlete_fatigue_now(self):
        """Calculate a fatigue value for a real rider (the athlete).
            This approach fits a Variable Fatigue Maximal Sub-Maximal model
            to a real rider to estimate their fatigue values.
            The updated fatigue value is obtained by solving the fatigue
            rate differential equation given by the Variable Fatigue model.
        """
        # Use this for real Athlete only.
        if self._virtual_athlete:
            raise RuntimeError('Athlete fatigue requires a NullPhysiology Model.')

        def exer_now(T_crank):
            """Back calculate exertion from Athlete's crank torque."""
            T_crit_now = self._eval_athlete_T_crit(*eval_args) * \
                    (1 - self._eval_athlete_cad_now(*eval_args)/ \
                    self._eval_athlete_cad_max(*eval_args) )
            # Note the 1 + self._eval_athlete_cad_now is a bug
            # This has been fixed in Lapysim, but agents trained on model with
            # bug, so require this form to give better performance.
            T_max_now = self._athlete_fatigue_now * \
                    ( self._eval_athlete_T_max(*eval_args) - T_crit_now ) * \
                    ( 1 + self._eval_athlete_cad_now(*eval_args) / \
                      self._eval_athlete_cad_max(*eval_args) )
            return T_crank / (T_crit_now + T_max_now)

        def dfat_dt(t, fatigue_now):
            """Calculate rate of change of fatigue following Variable
                Fatigue Maximal Sub-Maximal model."""
            n_dot = ( self._eval_athlete_cad_now(*eval_args) / 2*sym.pi)
            maximal_fatigue_rate_now = self._eval_athlete_fatigue_rate_max(*eval_args) * \
                                       exer_now(athlete_control[0])
            return n_dot * sym.ln(1 - maximal_fatigue_rate_now) * self._athlete_fatigue_now

        # If no new steps have been been made, return current value.
        completed_steps = len(self._simulation._simulation_histories['time'])
        if self._last_control_step == len(self._simulation._simulation_histories['time']) - 1:
            return self._athlete_fatigue_now
        else:
            # Step fatigue to most recent control
            for _ in range(self._last_control_step, completed_steps - 1):

                # Evaluators take arguments:
                # [state_rider_a, state_rider_b, control_a, control_b]
                eval_args = []
                opp_control = self._simulation._simulation_histories[self._opponent]['control'][self._last_control_step]
                opp_state = self._simulation._simulation_histories[self._opponent]['state'][self._last_control_step]
                athlete_control = self._simulation._simulation_histories[self._athlete]['control'][self._last_control_step]
                athlete_state = self._simulation._simulation_histories[self._athlete]['state'][self._last_control_step]

                # Add opponent & athlete_ in order - following order defined in simulation.
                if self._eval_first_rider:
                    eval_args.extend(opp_state + athlete_state)
                    # Remove Nones at start of sim.
                    exers = [ 0 if exer is None else exer[0] for exer in [opp_control, athlete_control] ]
                    eval_args.extend(exers)
                else:
                    eval_args.extend(athlete_state + opp_state)
                    # Remove Nones at start of sim
                    exers = [ 0 if exer is None else exer[0] for exer in [athlete_control, opp_control] ]
                    eval_args.extend(exers)

                # Step the solution for a single (possibly varying duration) time step
                t_step = self._simulation._simulation_histories['time'][self._last_control_step + 1] - \
                         self._simulation._simulation_histories['time'][self._last_control_step]
                t_span = (0.0, t_step)
                solution = integrate.solve_ivp(dfat_dt, t_span, [self._athlete_fatigue_now])
                self._athlete_fatigue_now = solution.y[0][1]
                self._last_control_step += 1

        return self._athlete_fatigue_now


    def _get_draft_exertion(self, opp_state, athlete_state, eq_sep=2.5):
        """Return the equilibrium exertion required for rider to draft at a
           given point behind opponent.
            Parameters:
            opp_state - State for the rider being controlled by this class.
                      - [rider position, rider_velocity, rider_fatigue].
            athlete_state - State for rider this class is racing against.
                          - [rider position, rider_velocity, rider_fatigue].
            eq_sep - The point about which the equilibrium is calculated.
                   - Default is 2.5 metres.
            Returns:
            exertion - Exertion value required to draft at this point.
                     - Float, in range [0,1].
        """
        # Evaluator take arguments:
        # [state_rider_a, state_rider_b, control_a, control_b]
        # Add opponent & athlete_ in order - following order defined in simulation.
        eval_args = []
        opponent_control = self._simulation._simulation_histories[self._opponent]['control'][-1]
        athlete_control = self._simulation._simulation_histories[self._athlete]['control'][-1]
        if self._eval_first_rider:
            eval_args.extend(opp_state + athlete_state)
            # Remove Nones at start of sim.
            exers = [ 0 if exer is None else exer[0] for exer in [opponent_control, athlete_control] ]
            eval_args.extend(exers)
        else:
            eval_args.extend(athlete_state + opp_state)
            # Remove Nones at start of sim
            exers = [ 0 if exer is None else exer[0] for exer in [athlete_control, opponent_control] ]
            eval_args.extend(exers)

        # Calculate drag factors
        drag_factor_a = 1 - 0.25*np.tanh(-2 * eq_sep) + 0.25*np.tanh(-0.2 * eq_sep - 2)
        drag_factor_b = 1 - 0.25*np.tanh(2 * eq_sep) + 0.25*np.tanh(0.2 * eq_sep - 2)

        # Calculate for rider A drafting behind rider B.
        # Rider B required drive force at given separation.
        athlete_accel = self._eval_athlete_accel(*eval_args)
        opponent_mass = self._eval_opponent_mass(*eval_args)
        athlete_norm_rr = self._eval_athlete_norm_rr(*eval_args)
        opponent_rr = athlete_norm_rr * opponent_mass
        athlete_aero_drag = self._eval_athlete_aero_drag(*eval_args)
        opponent_aero_drag = drag_factor_a * athlete_aero_drag / drag_factor_b
        opponent_f_drive = athlete_accel*opponent_mass + opponent_aero_drag + opponent_rr

        # Convert drive force to crank torque
        opponent_T_crank = opponent_f_drive * self._eval_opponent_crank_torque(*eval_args)

        # Convert T crank to exertion
        opponent_T_crit = self._eval_opponent_T_crit(*eval_args)
        opponent_T_max = self._eval_opponent_T_max(*eval_args)
        cad = self._eval_athlete_cad_now(*eval_args)
        cad_max = self._eval_opponent_cad_max(*eval_args)
        fatigue = self._eval_opponent_fatigue(*eval_args)
        divisor = opponent_T_crit * (1 - cad/cad_max) + fatigue  * \
            (opponent_T_max - opponent_T_crit) * (1 + cad/cad_max)
        eq_exertion = opponent_T_crank / divisor

        return eq_exertion