"""Run a race against an opponent using a policy created using supervised learning.
    This uses a Neural network to classify (state, action) pairs, providing a
    mapping from a race condition to an action.
"""

import lapysim
from intelligent_opponent import MsrOpponent
import numpy as np
import matplotlib.pyplot as plt

# # Setup Lapysim MSR sim for race
match_sprint_track = lapysim.Manchester()
match_sprint_conditions = lapysim.Conditions()
match_sprint_venue = lapysim.Venue(course=match_sprint_track,
                                    conditions=match_sprint_conditions)
match_sprint_rules = lapysim.Rules()

# Note that axial only aero model needs to be used, along with the
# follow-line-no-lean dynamics model as this is the setup used in
# training.
match_sprint_settings = lapysim.settings.Settings(aerodynamics_model='axial-only',
                                    dynamics_model='follow-line-no-lean',
                                    rolling_resistance_model='fitton-symons')

# Identical bike for both riders
vittoria_cl_gold_160_psi = lapysim.Tyre.from_library(
    'vittoria cl gold')
mavic_ellipse_700_cc = lapysim.Wheel.from_library(
    'mavic ellipse', tyre=vittoria_cl_gold_160_psi)
cervelo_T5GB_size_M = lapysim.Bike.from_library('cervelo t5gb',
                                                frame_size='M',
                                                front_wheel=mavic_ellipse_700_cc,
                                                rear_wheel=mavic_ellipse_700_cc)

# Rider A is a real rider and uses a Null Physiology model
# Rider B is virtual, and requires a Variable Fatigue model.
rider_a_anthropometry = lapysim.Anthropometry(mass=80, drag_area=0.26,
                                                height=1.82)
rider_a_physiology = lapysim.NullPhysiology()
rider_a_bike_fit = lapysim.BikeFit(saddle_height=1.1, gear_ratio=3.8)
rider_a = lapysim.Athlete(
    name='Rider A',
    athlete_id='rdrA',
    bike=cervelo_T5GB_size_M,
    anthropometry=rider_a_anthropometry,
    physiology=rider_a_physiology,
    bike_fit=rider_a_bike_fit,
)
rider_b_anthropometry = lapysim.Anthropometry(mass=80, drag_area=0.26,
                                                height=1.82)
rider_b_physiology = lapysim.VariableFatigueMaximalSubmaximalModel(peak_power=1000,
                                                max_torque=260,
                                                max_cadence=250,
                                                max_fatigue_rate=0.01)
rider_b_bike_fit = lapysim.BikeFit(saddle_height=1.1, gear_ratio=3.8)
rider_b = lapysim.Athlete(
    name='Rider B',
    athlete_id='rdrB',
    bike=cervelo_T5GB_size_M,
    anthropometry=rider_b_anthropometry,
    physiology=rider_b_physiology,
    bike_fit=rider_b_bike_fit,
)

# Compile the event.
match_sprint_event = lapysim.Event(
    venue=match_sprint_venue,
    athletes=(rider_a, rider_b),
    rules=match_sprint_rules,
    settings=match_sprint_settings
)
match_sprint_simulation = match_sprint_event.initialise_forward_simulation()

# During training, the initial conditions were created to enforce one rider
# initially taking the lead. Leading rider is 1m infront, moving at 1 m/s.
# Swap opponent and athlete states to change who leads & trials initially.
opponent_state = [0, 0, 1]
athlete_state = [1, 1]
match_sprint_simulation.initialise_rider_states((athlete_state, opponent_state))

# # Setup the opponent
# Will control rider A directly & use intelligent opponent for rider B
# controller_name - Name of controller to be used (see below).
# rider_name - The name of rider we want to control with this class.
#            - Must match name defined for rider above.
#
# Swap contoller name below to vary which controller is used.
# The policy based opponents implemented are:
#   policy_1 - Learned drafting controller.
#   policy_2 - Learned drafting controller with better accuracy when leading.
#   policy_3 - Learned drafting controller.
opponent = MsrOpponent(match_sprint_simulation,
                        controller_name='policy_1',
                        rider_name='Rider B')

# # Simulate race
# Athlete is leading, and initially moves at low power before
# sprinting after 500 m.
time_step = 0.1
ath_max_control = 100
while opponent_state[0] < 750 and athlete_state[0] < 750:
    if athlete_state[0] < 500:
        ath_control = 30
    else:
        ath_control = ath_max_control
    opp_control = opponent.predict(opponent_state, athlete_state)
    array_state = match_sprint_simulation.update_rider_states(
                    timestep=time_step,
                    athletes_control=([ath_control], [opp_control]),
                    )
    athlete_state = array_state[0]
    opponent_state = array_state[1]


# # Plot the race outcome.
from matplotlib import rc
rc('font', **{'family':'CMU Serif'})
rc('text', usetex=True)

t_data = np.array(match_sprint_simulation._simulation_histories['time'])
athlete_data = match_sprint_simulation._simulation_histories[opponent._athlete]
opponent_data = match_sprint_simulation._simulation_histories[opponent._opponent]
athlete_state = np.array(athlete_data['state'])
opponent_state = np.array(opponent_data['state'])
separation = athlete_state[:,0] - opponent_state[:,0]

ax1 = plt.subplot(1, 1, 1)
ax1.set_xlabel('Time (s)')
ax1.set_ylabel('Control')
ax1.set_ylim([0,1])
ax1.plot(t_data, np.array(athlete_data['control']) / ath_max_control,
            color='tab:red', linestyle=':', label='Athlete Control')
ax1.plot(t_data, np.array(opponent_data['control']),
            color='tab:green', linestyle=':', label='Opponent Control')

ax2 = ax1.twinx()
color = 'tab:blue'
ax2.set_ylabel('Separation ($d_{A} - d_{B}$) (m)', color='tab:blue')
ax2.plot(t_data, separation, color=color, label='Separation')
ax2.plot([t_data[0], t_data[-1]], [0, 0], 'k--')
plt.title('Race History for MSR Simulation Using Policy Based Opponent')
lines, labels = ax1.get_legend_handles_labels()
lines2, labels2 = ax2.get_legend_handles_labels()
ax2.legend(lines + lines2, labels + labels2, loc=0)
plt.savefig('policy_opponent', dpi=480)
plt.show()




