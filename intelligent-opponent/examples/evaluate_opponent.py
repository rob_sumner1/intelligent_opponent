"""Run a race between two expert opponents and critique their moves using
   a trained agent policy.
"""

import lapysim
from intelligent_opponent import MsrOpponent
import numpy as np
import matplotlib.pyplot as plt

# # Setup Lapysim MSR sim for race
match_sprint_track = lapysim.Manchester()
match_sprint_conditions = lapysim.Conditions()
match_sprint_venue = lapysim.Venue(course=match_sprint_track,
                                    conditions=match_sprint_conditions)
match_sprint_rules = lapysim.Rules()

# Note that axial only aero model needs to be used, along with the
# follow-line-no-lean dynamics model as this is the setup used in
# training.
match_sprint_settings = lapysim.settings.Settings(aerodynamics_model='axial-only',
                                    dynamics_model='follow-line-no-lean',
                                    rolling_resistance_model='fitton-symons')

# Identical bike for both riders
vittoria_cl_gold_160_psi = lapysim.Tyre.from_library(
    'vittoria cl gold')
mavic_ellipse_700_cc = lapysim.Wheel.from_library(
    'mavic ellipse', tyre=vittoria_cl_gold_160_psi)
cervelo_T5GB_size_M = lapysim.Bike.from_library('cervelo t5gb',
                                                frame_size='M',
                                                front_wheel=mavic_ellipse_700_cc,
                                                rear_wheel=mavic_ellipse_700_cc)

# Rider A & rider B are both identical & use the VariableFatigue physiology model
# with fatigue rate 0.01. This value has been roughly tuned to give ~20s max exertion
# before full fatigue.
rider_a_anthropometry = lapysim.Anthropometry(mass=80, drag_area=0.26,
                                                height=1.82)
rider_a_physiology = lapysim.VariableFatigueMaximalSubmaximalModel(peak_power=1000,
                                                max_torque=260,
                                                max_cadence=250,
                                                max_fatigue_rate=0.01)
rider_a_bike_fit = lapysim.BikeFit(saddle_height=1.1, gear_ratio=3.8)
rider_a = lapysim.Athlete(
    name='Rider A',
    athlete_id='rdrA',
    bike=cervelo_T5GB_size_M,
    anthropometry=rider_a_anthropometry,
    physiology=rider_a_physiology,
    bike_fit=rider_a_bike_fit,
)
rider_b_anthropometry = lapysim.Anthropometry(mass=80, drag_area=0.26,
                                                height=1.82)
rider_b_physiology = lapysim.VariableFatigueMaximalSubmaximalModel(peak_power=1000,
                                                max_torque=260,
                                                max_cadence=250,
                                                max_fatigue_rate=0.01)
rider_b_bike_fit = lapysim.BikeFit(saddle_height=1.1, gear_ratio=3.8)
rider_b = lapysim.Athlete(
    name='Rider B',
    athlete_id='rdrB',
    bike=cervelo_T5GB_size_M,
    anthropometry=rider_b_anthropometry,
    physiology=rider_b_physiology,
    bike_fit=rider_b_bike_fit,
)

# Compile the event.
match_sprint_event = lapysim.Event(
    venue=match_sprint_venue,
    athletes=(rider_a, rider_b),
    rules=match_sprint_rules,
    settings=match_sprint_settings
)
match_sprint_simulation = match_sprint_event.initialise_forward_simulation()

# During training, the initial conditions were created to enforce one rider
# initially taking the lead. Leading rider is 1m infront, moving at 1 m/s.
# Swap opponent and rider_a states to change who leads & trials initially.
rider_a_state = [0, 0, 1]
rider_b_state = [1, 1, 1]
match_sprint_simulation.initialise_rider_states((rider_a_state, rider_b_state))

# # Setup the opponent
# Control both riders using expert policies.
rider_a_cont = MsrOpponent(match_sprint_simulation,
                        controller_name='expert',
                        rider_name='Rider A')
rider_b_cont = MsrOpponent(match_sprint_simulation,
                        controller_name='expert',
                        rider_name='Rider B')


# # Simulate race
# rider_a is leading, and initially moves at low power before
# sprinting after 500 m.
time_step = 0.1
while rider_b_state[0] < 750 and rider_a_state[0] < 750:
    rider_a_control = rider_a_cont.predict(rider_a_state, rider_b_state)
    rider_b_control = rider_b_cont.predict(rider_b_state, rider_a_state)
    array_state = match_sprint_simulation.update_rider_states(
                    timestep=time_step,
                    athletes_control=([rider_a_control],
                                      [rider_b_control]),
                    )
    rider_a_state = array_state[0]
    rider_b_state = array_state[1]


# # Plot the race outcome.
from matplotlib import rc
rc('font', **{'family':'CMU Serif'})
rc('text', usetex=True)

t_data = np.array(match_sprint_simulation._simulation_histories['time'])
rider_a_data = match_sprint_simulation._simulation_histories[rider_a]
rider_b_data = match_sprint_simulation._simulation_histories[rider_b]
rider_a_state = np.array(rider_a_data['state'])
rider_b_state = np.array(rider_b_data['state'])
rider_a_control = np.array(rider_a_data['control'])
rider_b_control = np.array(rider_b_data['control'])
separation = rider_a_state[:,0] - rider_b_state[:,0]

ax1 = plt.subplot(1, 1, 1)
ax1.set_xlabel('Time (s)')
ax1.set_ylabel('Control')
ax1.set_ylim([0,1])
ax1.plot(t_data, rider_a_control, color='tab:red', linestyle=':', label='Rider A Control')
ax1.plot(t_data, rider_b_control,color='tab:green', linestyle=':', label='Rider B Control')

ax2 = ax1.twinx()
color = 'tab:blue'
ax2.set_ylabel('Separation ($d_{A} - d_{B}$) (m)', color='tab:blue')
ax2.plot(t_data, separation, color=color, label='Separation')
ax2.plot([t_data[0], t_data[-1]], [0, 0], 'k--')
plt.title('Race History for MSR Simulation Between Two Expert Opponents')
lines, labels = ax1.get_legend_handles_labels()
lines2, labels2 = ax2.get_legend_handles_labels()
ax2.legend(lines + lines2, labels + labels2, loc=0)
plt.savefig('evaluate_opponent_1', dpi=480)
plt.show()

# # Plot evaluation of a move.
plt.subplot(2, 1, 1)
plt.plot(t_data, rider_a_control, 'r-', label='Rider A')
plt.plot(t_data, rider_b_control, 'g-', label='Rider B')
plt.xlabel('Time (s)')
plt.ylabel('Control')
plt.title('Rider Controls')

eval_policy = MsrOpponent(match_sprint_simulation, controller_name='agent_1')
q_rider_a, q_rider_b = [], []
for i in range(len(t_data)):
    a_dist = eval_policy.evaluate(rider_a_state[i], rider_b_state[i])
    q_rider_a.append(a_dist[int(rider_a_control[i] // 0.2)])
    b_dist = eval_policy.evaluate(rider_b_state[i], rider_a_state[i])
    q_rider_b.append(b_dist[int(rider_b_control[i] // 0.2)])
plt.subplot(2,1,2)
plt.plot(t_data, q_rider_a, 'r-', label='Rider A')
plt.plot(t_data, q_rider_b, 'g-', label='Rider B')
plt.xlabel('Time (s)')
plt.ylabel('Action Quality')
plt.title('Action Quality')
plt.suptitle('Action Evaluation using ' + eval_policy.controller_name.replace('_', ' '))
plt.legend()
plt.tight_layout(rect=[0, 0, 1, 0.975])
plt.savefig('evaluate_opponent_2', dpi=480)
plt.show()


