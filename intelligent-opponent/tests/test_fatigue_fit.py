"""Test fatigue fitting for Athlete vs Virtual rider races."""

import pytest
import lapysim
import tensorflow as tf
import numpy as np
from intelligent_opponent.msr_opponent import MsrOpponent
from stable_baselines import DQN
import matplotlib.pyplot as plt


@pytest.fixture
def sim_real():
    """Return a Lapysim MSR simulation between real athlete & virtual rider."""

    match_sprint_track = lapysim.Manchester()
    match_sprint_conditions = lapysim.Conditions()
    match_sprint_venue = lapysim.Venue(course=match_sprint_track,
                                       conditions=match_sprint_conditions)
    match_sprint_rules = lapysim.Rules()
    match_sprint_settings = lapysim.settings.Settings(aerodynamics_model='axial-only',
                                     dynamics_model='follow-line-no-lean',
                                     rolling_resistance_model='fitton-symons')

    # Bike for both riders.
    vittoria_cl_gold_160_psi = lapysim.Tyre.from_library(
        'vittoria cl gold')
    mavic_ellipse_700_cc = lapysim.Wheel.from_library(
        'mavic ellipse', tyre=vittoria_cl_gold_160_psi)
    cervelo_T5GB_size_M = lapysim.Bike.from_library('cervelo t5gb',
                                                    frame_size='M',
                                                    front_wheel=mavic_ellipse_700_cc,
                                                    rear_wheel=mavic_ellipse_700_cc)

    # Create rider A - Controlled by opponent.
    rider_a_anthropometry = lapysim.Anthropometry(mass=80, drag_area=0.26,
                                                    height=1.82)
    rider_a_physiology = lapysim.VariableFatigueMaximalSubmaximalModel(peak_power=1000,
                                                    max_torque=260,
                                                    max_cadence=250,
                                                    max_fatigue_rate=0.01)
    rider_a_bike_fit = lapysim.BikeFit(saddle_height=1.1, gear_ratio=3.8)
    rider_a = lapysim.Athlete(
        name='Rider A',
        athlete_id='rdrA',
        bike=cervelo_T5GB_size_M,
        anthropometry=rider_a_anthropometry,
        physiology=rider_a_physiology,
        bike_fit=rider_a_bike_fit,
    )

    # Create rider B - real rider.
    rider_b_anthropometry = lapysim.Anthropometry(mass=80, drag_area=0.26,
                                                    height=1.82)
    rider_b_bike_fit = lapysim.BikeFit(saddle_height=1.1, gear_ratio=3.8)
    rider_b_physiology = lapysim.NullPhysiology()
    rider_b = lapysim.Athlete(
        name='Rider B',
        athlete_id='rdrB',
        bike=cervelo_T5GB_size_M,
        anthropometry=rider_b_anthropometry,
        physiology=rider_b_physiology,
        bike_fit=rider_b_bike_fit,
    )

    # Compile the event.
    match_sprint_event = lapysim.Event(
        venue=match_sprint_venue,
        athletes=(rider_a, rider_b),
        rules=match_sprint_rules,
        settings=match_sprint_settings
    )
    match_sprint_simulation = match_sprint_event.initialise_forward_simulation()
    match_sprint_simulation.initialise_rider_states(([0, 0, 1], [1, 1]))

    return match_sprint_simulation

@pytest.fixture
def sim_virtual():
    """Return a Lapysim MSR simulation between virtual athlete & virtual rider."""

    match_sprint_track = lapysim.Manchester()
    match_sprint_conditions = lapysim.Conditions()
    match_sprint_venue = lapysim.Venue(course=match_sprint_track,
                                       conditions=match_sprint_conditions)
    match_sprint_rules = lapysim.Rules()
    match_sprint_settings = lapysim.settings.Settings(aerodynamics_model='axial-only',
                                     dynamics_model='follow-line-no-lean',
                                     rolling_resistance_model='fitton-symons')

    # Bike for both riders.
    vittoria_cl_gold_160_psi = lapysim.Tyre.from_library(
        'vittoria cl gold')
    mavic_ellipse_700_cc = lapysim.Wheel.from_library(
        'mavic ellipse', tyre=vittoria_cl_gold_160_psi)
    cervelo_T5GB_size_M = lapysim.Bike.from_library('cervelo t5gb',
                                                    frame_size='M',
                                                    front_wheel=mavic_ellipse_700_cc,
                                                    rear_wheel=mavic_ellipse_700_cc)

    # Create rider A - Controlled by opponent.
    rider_a_anthropometry = lapysim.Anthropometry(mass=80, drag_area=0.26,
                                                    height=1.82)
    rider_a_physiology = lapysim.VariableFatigueMaximalSubmaximalModel(peak_power=1000,
                                                    max_torque=260,
                                                    max_cadence=250,
                                                    max_fatigue_rate=0.01)
    rider_a_bike_fit = lapysim.BikeFit(saddle_height=1.1, gear_ratio=3.8)
    rider_a = lapysim.Athlete(
        name='Rider A',
        athlete_id='rdrA',
        bike=cervelo_T5GB_size_M,
        anthropometry=rider_a_anthropometry,
        physiology=rider_a_physiology,
        bike_fit=rider_a_bike_fit,
    )

    # Create rider B - real rider.
    rider_b_anthropometry = lapysim.Anthropometry(mass=80, drag_area=0.26,
                                                    height=1.82)
    rider_b_bike_fit = lapysim.BikeFit(saddle_height=1.1, gear_ratio=3.8)
    rider_b_physiology = lapysim.VariableFatigueMaximalSubmaximalModel(peak_power=1000,
                                                    max_torque=260,
                                                    max_cadence=250,
                                                    max_fatigue_rate=0.01)
    rider_b = lapysim.Athlete(
        name='Rider B',
        athlete_id='rdrB',
        bike=cervelo_T5GB_size_M,
        anthropometry=rider_b_anthropometry,
        physiology=rider_b_physiology,
        bike_fit=rider_b_bike_fit,
    )

    # Compile the event.
    match_sprint_event = lapysim.Event(
        venue=match_sprint_venue,
        athletes=(rider_a, rider_b),
        rules=match_sprint_rules,
        settings=match_sprint_settings
    )
    match_sprint_simulation = match_sprint_event.initialise_forward_simulation()
    match_sprint_simulation.initialise_rider_states(([0, 0, 1], [1, 1, 1]))

    return match_sprint_simulation

def get_eval_args(pol):
    """Create eval args."""
    eval_args = []
    opp_control = pol._simulation._simulation_histories[pol._opponent]['control'][pol._last_control_step]
    opp_state = pol._simulation._simulation_histories[pol._opponent]['state'][pol._last_control_step]
    athlete_control = pol._simulation._simulation_histories[pol._athlete]['control'][pol._last_control_step]
    athlete_state = pol._simulation._simulation_histories[pol._athlete]['state'][pol._last_control_step]

    # Add opponent & athlete_ in order - following order defined in simulation.
    if pol._eval_first_rider:
        eval_args.extend(opp_state + athlete_state)
        # Remove Nones at start of sim.
        exers = [ 0 if exer is None else exer[0] for exer in [opp_control, athlete_control] ]
        eval_args.extend(exers)
    else:
        eval_args.extend(athlete_state + opp_state)
        # Remove Nones at start of sim
        exers = [ 0 if exer is None else exer[0] for exer in [athlete_control, opp_control] ]
        eval_args.extend(exers)

    return eval_args

def T_crank(exer_now, pol):
    """Convert exer_now to T_crank."""
    eval_args = get_eval_args(pol)
    T_crit_now = pol._eval_athlete_T_crit(*eval_args) * \
                (1 - pol._eval_athlete_cad_now(*eval_args)/ \
                pol._eval_athlete_cad_max(*eval_args) )
    T_max_now = pol._athlete_fatigue_now * \
                ( pol._eval_athlete_T_max(*eval_args) - T_crit_now ) * \
                ( 1 + pol._eval_athlete_cad_now(*eval_args) / \
                  pol._eval_athlete_cad_max(*eval_args) )
    return exer_now * (T_crit_now + T_max_now)

def test_fatigue_comp_sprint(sim_real, sim_virtual):
    """Test fatigue values returned by two values."""
    # Virtual vs virtual at 1 exertion
    for _ in range(250):
        sim_virtual.update_rider_states(
            timestep=0.1,
            athletes_control=([1], [1]),
            )
    virt_fatigue = np.array(sim_virtual._simulation_histories[sim_virtual.event.athletes[1]]['state'])[:,2]

    # Real vs virtual at equivalent Crank Torque.
    pol = MsrOpponent(sim_real)
    real_fatigue = []
    for _ in range(250):
        real_fatigue.append(pol._get_athlete_fatigue_now())
        athlete_T_crank = T_crank(1, pol)
        sim_real.update_rider_states(
            timestep=0.1,
            athletes_control=([1], [athlete_T_crank]),
            )
    real_fatigue.append(pol._get_athlete_fatigue_now())

    assert all([pytest.approx(a, abs=5e-3) == b for a, b in zip(real_fatigue, virt_fatigue)])

def test_fatigue_comp_step(sim_real, sim_virtual):
    """Test fatigue values returned by two values."""
    # Virtual vs virtual at 1 exertion
    exer = 0.75
    for i in range(250):
        if i % 10 == 0:
            exer = 1 - exer
        sim_virtual.update_rider_states(
            timestep=0.1,
            athletes_control=([1], [exer]),
            )
    virt_fatigue = np.array(sim_virtual._simulation_histories[sim_virtual.event.athletes[1]]['state'])[:,2]

    # Real vs virtual at equivalent Crank Torque.
    pol = MsrOpponent(sim_real)
    real_fatigue = []
    exer = 0.75
    for i in range(250):
        if i % 10 == 0:
            exer = 1 - exer
        real_fatigue.append(pol._get_athlete_fatigue_now())
        athlete_T_crank = T_crank(exer, pol)
        sim_real.update_rider_states(
            timestep=0.1,
            athletes_control=([1], [athlete_T_crank]),
            )
    real_fatigue.append(pol._get_athlete_fatigue_now())

    assert all([pytest.approx(a, abs=5e-3) == b for a, b in zip(real_fatigue, virt_fatigue)])

