""" Test the opponent class in races between two virtual opponents."""

import pytest
import lapysim
import tensorflow as tf
import numpy as np
from intelligent_opponent.msr_opponent import MsrOpponent
from stable_baselines import DQN

@pytest.fixture
def sim():
    """Return a Lapysim MSR simulation."""

    match_sprint_track = lapysim.Manchester()
    match_sprint_conditions = lapysim.Conditions()
    match_sprint_venue = lapysim.Venue(course=match_sprint_track,
                                       conditions=match_sprint_conditions)
    match_sprint_rules = lapysim.Rules()
    match_sprint_settings = lapysim.settings.Settings(aerodynamics_model='axial-only',
                                     dynamics_model='follow-line-no-lean',
                                     rolling_resistance_model='fitton-symons')

    # Bike for both riders.
    vittoria_cl_gold_160_psi = lapysim.Tyre.from_library(
        'vittoria cl gold')
    mavic_ellipse_700_cc = lapysim.Wheel.from_library(
        'mavic ellipse', tyre=vittoria_cl_gold_160_psi)
    cervelo_T5GB_size_M = lapysim.Bike.from_library('cervelo t5gb',
                                                    frame_size='M',
                                                    front_wheel=mavic_ellipse_700_cc,
                                                    rear_wheel=mavic_ellipse_700_cc)

    # Create rider A
    rider_a_anthropometry = lapysim.Anthropometry(mass=80, drag_area=0.26,
                                                    height=1.82)
    rider_a_physiology = lapysim.VariableFatigueMaximalSubmaximalModel(peak_power=1000,
                                                    max_torque=260,
                                                    max_cadence=250,
                                                    max_fatigue_rate=0.01)
    rider_a_bike_fit = lapysim.BikeFit(saddle_height=1.1, gear_ratio=3.8)
    rider_a = lapysim.Athlete(
        name='Rider A',
        athlete_id='rdrA',
        bike=cervelo_T5GB_size_M,
        anthropometry=rider_a_anthropometry,
        physiology=rider_a_physiology,
        bike_fit=rider_a_bike_fit,
    )

    # Create rider B.
    rider_b_anthropometry = lapysim.Anthropometry(mass=80, drag_area=0.26,
                                                    height=1.82)
    rider_b_physiology = lapysim.VariableFatigueMaximalSubmaximalModel(peak_power=1000,
                                                    max_torque=260,
                                                    max_cadence=250,
                                                    max_fatigue_rate=0.01)
    rider_b_bike_fit = lapysim.BikeFit(saddle_height=1.1, gear_ratio=3.8)
    rider_b = lapysim.Athlete(
        name='Rider B',
        athlete_id='rdrB',
        bike=cervelo_T5GB_size_M,
        anthropometry=rider_b_anthropometry,
        physiology=rider_b_physiology,
        bike_fit=rider_b_bike_fit,
    )

    # Compile the event.
    match_sprint_event = lapysim.Event(
        venue=match_sprint_venue,
        athletes=(rider_a, rider_b),
        rules=match_sprint_rules,
        settings=match_sprint_settings
    )
    match_sprint_simulation = match_sprint_event.initialise_forward_simulation()
    match_sprint_simulation.initialise_rider_states(([0, 0, 1], [1, 1, 1]))

    return match_sprint_simulation


def test_opponent_init(sim):
    """Test initialisation of opponent class."""
    pol = MsrOpponent(sim)
    assert pol._match_sprint_event is not None
    assert len(pol._opponent_types.keys() ) == 8

    # Check ID's & names match
    assert pol._opponent.id == 'rdrA'
    assert pol._opponent.name == 'Rider A'
    assert pol._athlete.id == 'rdrB'
    assert pol._athlete.name == 'Rider B'
    assert pol.controller_name in pol._opponent_types.keys()
    assert pol._athlete_fatigue_now == 1
    assert pol._last_control_step == 0

    # Incorrect controller name
    with pytest.raises(ValueError):
        MsrOpponent(sim, controller_name='not_a_name')

def test_get_athlete_fatigue(sim):
    """Test trying to get athlete fatigue."""
    pol = MsrOpponent(sim)
    with pytest.raises(RuntimeError):
        pol._get_athlete_fatigue_now()

def test_dqn_opponent(sim):
    """Test trained DQN opponents."""
    # 4 DQN agents in total.
    for controller_name in ['agent_1', 'agent_2', 'agent_3', 'agent_4']:
        pol = MsrOpponent(sim, controller_name=controller_name)
        assert pol.controller_name == controller_name
        assert isinstance(pol.controller_description, str)

        # Predict should throw error for wrong size state
        with pytest.raises(ValueError):
            pol.predict([0, 0], [1, 1.5, 1])
        with pytest.raises(ValueError):
            pol.predict([0, 0, 1], [1.5, 1])

        # Test states which return control of 1.
        states = [ [0, 0, 1, 1, 1.5, 1],
                [300, 10, 0.9, 315, 10, 0.9],
                [400, 15, 0.5, 385, 15, 0.5] ]
        for state in states:
            assert pol.predict(state[:3], state[3:]) == 1

        # Test states predicted by agent.
        states = [ [10, 5, 1, 11, 5, 1],
                [300, 10, 0.9, 302, 10.1, 0.9],
                [700, 15, 0.5, 695, 15, 0.5] ]
        [agent_file, _] = pol._opponent_types[controller_name]
        model = DQN.load(agent_file)
        for state in states:
            norm_state = [state[0]/760, state[1]/30, state[2],
                        state[3]/760, state[4]/30, state[5]]
            pol_control = pol.predict(state[:3], state[3:])
            agent_control = model.predict(norm_state)[0]/4
            assert pol_control == agent_control

        # Test running the sim
        athlete_state, opponent_state = [ [0, 0, 1], [1, 1, 1] ]
        while opponent_state[0] < 250 and athlete_state[0] < 250:
            opp_control = pol.predict(opponent_state, athlete_state)
            array_state = sim.update_rider_states(
                            timestep=0.1,
                            athletes_control=([1], [opp_control]),
                            )
            athlete_state = array_state[0]
            opponent_state = array_state[1]

def test_policy_opponent(sim):
    """Test trained policy opponents."""
    # 3 Policy based agents in total.
    for controller_name in ['policy_1', 'policy_2', 'policy_3']:
        pol = MsrOpponent(sim, controller_name=controller_name)
        assert pol.controller_name == controller_name
        assert isinstance(pol.controller_description, str)

        # Predict should throw error for wrong size state
        with pytest.raises(ValueError):
            pol.predict([0, 0], [1, 1.5, 1])
        with pytest.raises(ValueError):
            pol.predict([0, 0, 1], [1, 1.5])

        # Test states which return control of 1.
        states = [ [0, 0, 1, 1, 1.5, 1],
                [300, 10, 0.9, 315, 10, 0.9],
                [400, 15, 0.5, 385, 15, 0.5] ]
        for state in states:
            assert pol.predict(state[:3], state[3:]) == 1

        # Test states predicted by agent.
        states = [ [10, 5, 1, 11, 5, 1],
                [300, 10, 0.9, 302, 10.1, 0.9],
                [700, 15, 0.5, 695, 15, 0.5] ]
        [agent_file, _] = pol._opponent_types[controller_name]
        model = tf.keras.models.load_model(agent_file)
        for state in states:
            norm_state = [state[0]/760, state[1]/30, state[2],
                        state[3]/760, state[4]/30, state[5] ]
            norm_state = np.array(norm_state).reshape( (1, 6) )
            pol_control = pol.predict(state[:3], state[3:])
            agent_dist = model.predict(norm_state)[0]
            agent_control = np.argmax(agent_dist) / (len(agent_dist) - 1)
            assert pol_control == agent_control

        # Test running the sim
        athlete_state, opponent_state = [ [0, 0, 1], [1, 1, 1] ]
        while opponent_state[0] < 250 and athlete_state[0] < 250:
            opp_control = pol.predict(opponent_state, athlete_state)
            array_state = sim.update_rider_states(
                            timestep=0.1,
                            athletes_control=([1], [opp_control]),
                            )
            athlete_state = array_state[0]
            opponent_state = array_state[1]

def test_expert_opponent(sim):
    """Test expert opponent class."""
    # Expert controlling rider A
    pol = MsrOpponent(sim, controller_name="expert", rider_name='Rider A')
    assert pol.controller_name == "expert"
    assert isinstance(pol.controller_description, str)

    # Predict should throw error for wrong size state
    with pytest.raises(ValueError):
        pol.predict([300, 10], [312, 10, 0.9])
    with pytest.raises(ValueError):
        pol.predict([300, 10, 0.9], [312, 10])

    # More in draft spot - should be 0
    assert pol.predict([300, 10, 0.9], [312, 10, 0.9]) == pytest.approx(0.2887, 1e-3)
    assert pol.predict([300, 10, 0.9], [310, 10, 0.9]) == pytest.approx(0.3762, 1e-3)
    assert pol.predict([300, 10, 0.9], [330, 15, 0.9]) == pytest.approx(0.6881, 1e-3)
    assert pol.predict([1, 0.1, 1], [0, 0.05, 1.0]) == 0.5

    # Expert controlling rider B
    pol = MsrOpponent(sim, controller_name="expert", rider_name='Rider B')

    # More in draft spot - should be 0
    assert pol.predict([300, 10, 0.9], [312, 10, 0.9]) == pytest.approx(0.2887, 1e-3)
    assert pol.predict([300, 10, 0.9], [310, 10, 0.9]) == pytest.approx(0.3762, 1e-3)
    assert pol.predict([300, 10, 0.9], [330, 15, 0.9]) == pytest.approx(0.6881, 1e-3)
    assert pol.predict([1, 0.1, 1], [0, 0.05, 1.0]) == 0.5

    # Test running the sim
    athlete_state, opponent_state = [ [0, 0, 1], [1, 1, 1] ]
    while opponent_state[0] < 250 and athlete_state[0] < 250:
        opp_control = pol.predict(opponent_state, athlete_state)
        array_state = sim.update_rider_states(
                        timestep=0.1,
                        athletes_control=([1], [opp_control]),
                        )
        athlete_state = array_state[0]
        opponent_state = array_state[1]

def test_evaluate(sim):
    """Test the evaluate function."""
    # Fails for expert policies
    with pytest.raises(NotImplementedError):
        pol = MsrOpponent(sim, controller_name="expert")
        pol.evaluate([0,0,0],[0,0,0])

    # Agent
    pol = MsrOpponent(sim, controller_name="agent_1")
    dist = pol.evaluate([300, 10, 0.9], [299, 10, 0.9], normalise=True)
    assert len(dist) == 5
    assert pytest.approx(sum(dist), 1e-3) == 1

    # Policy
    pol = MsrOpponent(sim, controller_name="policy_1")
    dist = pol.evaluate([300, 10, 0.9], [299, 10, 0.9])
    assert len(dist) == 10
    assert pytest.approx(sum(dist), 1e-3) == 1

    # Evaluate should throw error for wrong size state
    with pytest.raises(ValueError):
        pol.evaluate([0,0],[0,0,0])
    with pytest.raises(ValueError):
        pol.evaluate([0,0,0],[0,0])




