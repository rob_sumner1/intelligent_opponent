""" Test the general opponent class functions."""

import pytest
import lapysim
import tensorflow as tf
import numpy as np
from intelligent_opponent.msr_opponent import MsrOpponent, MsrNetPolicy
from stable_baselines import DQN

@pytest.fixture
def sim():
    """Return a Lapysim MSR simulation."""

    match_sprint_track = lapysim.Manchester()
    match_sprint_conditions = lapysim.Conditions()
    match_sprint_venue = lapysim.Venue(course=match_sprint_track,
                                       conditions=match_sprint_conditions)
    match_sprint_rules = lapysim.Rules()
    match_sprint_settings = lapysim.settings.Settings(aerodynamics_model='axial-only',
                                     dynamics_model='follow-line-no-lean',
                                     rolling_resistance_model='fitton-symons')

    # Bike for both riders.
    vittoria_cl_gold_160_psi = lapysim.Tyre.from_library(
        'vittoria cl gold')
    mavic_ellipse_700_cc = lapysim.Wheel.from_library(
        'mavic ellipse', tyre=vittoria_cl_gold_160_psi)
    cervelo_T5GB_size_M = lapysim.Bike.from_library('cervelo t5gb',
                                                    frame_size='M',
                                                    front_wheel=mavic_ellipse_700_cc,
                                                    rear_wheel=mavic_ellipse_700_cc)

    # Create rider A - Controlled by opponent.
    rider_a_anthropometry = lapysim.Anthropometry(mass=80, drag_area=0.26,
                                                    height=1.82)
    rider_a_physiology = lapysim.VariableFatigueMaximalSubmaximalModel(peak_power=1000,
                                                    max_torque=260,
                                                    max_cadence=250,
                                                    max_fatigue_rate=0.01)
    rider_a_bike_fit = lapysim.BikeFit(saddle_height=1.1, gear_ratio=3.8)
    rider_a = lapysim.Athlete(
        name='Rider A',
        athlete_id='rdrA',
        bike=cervelo_T5GB_size_M,
        anthropometry=rider_a_anthropometry,
        physiology=rider_a_physiology,
        bike_fit=rider_a_bike_fit,
    )

    # Create rider B - real rider.
    rider_b_anthropometry = lapysim.Anthropometry(mass=80, drag_area=0.26,
                                                    height=1.82)
    rider_b_bike_fit = lapysim.BikeFit(saddle_height=1.1, gear_ratio=3.8)
    rider_b_physiology = lapysim.NullPhysiology()
    rider_b = lapysim.Athlete(
        name='Rider B',
        athlete_id='rdrB',
        bike=cervelo_T5GB_size_M,
        anthropometry=rider_b_anthropometry,
        physiology=rider_b_physiology,
        bike_fit=rider_b_bike_fit,
    )

    # Compile the event.
    match_sprint_event = lapysim.Event(
        venue=match_sprint_venue,
        athletes=(rider_a, rider_b),
        rules=match_sprint_rules,
        settings=match_sprint_settings
    )
    match_sprint_simulation = match_sprint_event.initialise_forward_simulation()
    match_sprint_simulation.initialise_rider_states(([0, 0, 1], [1, 1]))

    return match_sprint_simulation


def test_reset(sim):
    """Test resetting of opponent class."""
    # Check throws error with wrong position
    pol = MsrOpponent(sim)
    with pytest.raises(ValueError):
        pol.reset(initial_position='wrong')

    # Shouldn't change controller
    cur_controller = pol.controller_name
    pol.reset()
    assert cur_controller == pol.controller_name

    # Reset on init - multiple runs to allow for probability
    # that agent is picked multiple times by random choice.
    pol = MsrOpponent(sim, random_on_reset=True)
    cur_controller = pol.controller_name
    passed = False
    for _ in range(50):
        pol.reset()
        if cur_controller != pol.controller_name:
            passed = True
            break
    assert passed

    # Should fail with wrong type
    with pytest.raises(ValueError):
        pol.reset(initial_position='none')

    # Check leading reset
    pol.reset(initial_position='lead')
    assert pol.controller_name in pol._lead_opponents
    assert isinstance(pol._agent, MsrNetPolicy)
    pol.reset(initial_position='trail')
    assert pol.controller_name in pol._trail_opponents
    assert isinstance(pol._agent, MsrNetPolicy)