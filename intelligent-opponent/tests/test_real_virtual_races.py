""" Test the opponent class in races between real rider and virtual opponent."""

import pytest
import lapysim
import tensorflow as tf
import numpy as np
from intelligent_opponent.msr_opponent import MsrOpponent
from stable_baselines import DQN

@pytest.fixture
def sim():
    """Return a Lapysim MSR simulation."""

    match_sprint_track = lapysim.Manchester()
    match_sprint_conditions = lapysim.Conditions()
    match_sprint_venue = lapysim.Venue(course=match_sprint_track,
                                       conditions=match_sprint_conditions)
    match_sprint_rules = lapysim.Rules()
    match_sprint_settings = lapysim.settings.Settings(aerodynamics_model='axial-only',
                                     dynamics_model='follow-line-no-lean',
                                     rolling_resistance_model='fitton-symons')

    # Bike for both riders.
    vittoria_cl_gold_160_psi = lapysim.Tyre.from_library(
        'vittoria cl gold')
    mavic_ellipse_700_cc = lapysim.Wheel.from_library(
        'mavic ellipse', tyre=vittoria_cl_gold_160_psi)
    cervelo_T5GB_size_M = lapysim.Bike.from_library('cervelo t5gb',
                                                    frame_size='M',
                                                    front_wheel=mavic_ellipse_700_cc,
                                                    rear_wheel=mavic_ellipse_700_cc)

    # Create rider A - Controlled by opponent.
    rider_a_anthropometry = lapysim.Anthropometry(mass=80, drag_area=0.26,
                                                    height=1.82)
    rider_a_physiology = lapysim.VariableFatigueMaximalSubmaximalModel(peak_power=1000,
                                                    max_torque=260,
                                                    max_cadence=250,
                                                    max_fatigue_rate=0.01)
    rider_a_bike_fit = lapysim.BikeFit(saddle_height=1.1, gear_ratio=3.8)
    rider_a = lapysim.Athlete(
        name='Rider A',
        athlete_id='rdrA',
        bike=cervelo_T5GB_size_M,
        anthropometry=rider_a_anthropometry,
        physiology=rider_a_physiology,
        bike_fit=rider_a_bike_fit,
    )

    # Create rider B - real rider.
    rider_b_anthropometry = lapysim.Anthropometry(mass=80, drag_area=0.26,
                                                    height=1.82)
    rider_b_bike_fit = lapysim.BikeFit(saddle_height=1.1, gear_ratio=3.8)
    rider_b_physiology = lapysim.NullPhysiology()
    rider_b = lapysim.Athlete(
        name='Rider B',
        athlete_id='rdrB',
        bike=cervelo_T5GB_size_M,
        anthropometry=rider_b_anthropometry,
        physiology=rider_b_physiology,
        bike_fit=rider_b_bike_fit,
    )

    # Compile the event.
    match_sprint_event = lapysim.Event(
        venue=match_sprint_venue,
        athletes=(rider_a, rider_b),
        rules=match_sprint_rules,
        settings=match_sprint_settings
    )
    match_sprint_simulation = match_sprint_event.initialise_forward_simulation()
    match_sprint_simulation.initialise_rider_states(([0, 0, 1], [1, 1]))

    return match_sprint_simulation


def test_opponent_init(sim):
    """Test initialisation of opponent class."""
    pol = MsrOpponent(sim)
    assert pol._match_sprint_event is not None
    assert len(pol._opponent_types.keys() ) == 8
    assert not pol._virtual_athlete

    # Check ID's & names match
    assert pol._opponent.id == 'rdrA'
    assert pol._opponent.name == 'Rider A'
    assert pol._athlete.id == 'rdrB'
    assert pol._athlete.name == 'Rider B'
    assert pol.controller_name in pol._opponent_types.keys()
    assert pol._athlete_fatigue_now == 1
    assert pol._last_control_step == 0

    # Extra evaluators have been created for athlete fatigue
    for attr in ['_eval_athlete_cad_max', '_eval_athlete_T_max',
                 '_eval_athlete_T_crit', '_eval_athlete_fatigue_rate_max']:
        assert hasattr(pol, attr) and callable(getattr(pol, attr))

    # Incorrect controller name
    with pytest.raises(ValueError):
        MsrOpponent(sim, controller_name='not_a_name')

def test_get_athlete_fatigue_sequential_steps(sim):
    """Test getting athlete fatigue values with alternate
        simulation step & fatigue model calls."""
    pol = MsrOpponent(sim)

    # Before a step, should be 1
    assert pol._last_control_step == 0
    assert pol._athlete_fatigue_now == 1
    assert pol._get_athlete_fatigue_now() == 1

    # Step multipe times and check values
    fat_vals = [0.99952812, 0.99901560,
                0.99846271, 0.99786967,
                0.99723671]
    for i, fat_val in enumerate(fat_vals):
        # Step simulation once
        sim.update_rider_states(
            timestep=0.1,
            athletes_control=([1], [100]),
            )

        # Sim step should give new value
        assert pytest.approx(pol._get_athlete_fatigue_now(), 1e-5) == fat_val
        assert pytest.approx(pol._athlete_fatigue_now, 1e-5) == fat_val
        assert pol._last_control_step == i + 1

        # Recalling should not update without sim step.
        assert pytest.approx(pol._get_athlete_fatigue_now(), 1e-5) == fat_val
        assert pol._last_control_step == i + 1

    # Stepping with 0 torque for athlete should not change value.
    for i in range(5):
        # Step simulation with 0 power.
        sim.update_rider_states(
            timestep=0.1,
            athletes_control=([1], [0]),
            )

        # Sim step should give new value
        assert pytest.approx(pol._get_athlete_fatigue_now(), 1e-5) == fat_vals[-1]
        assert pytest.approx(pol._athlete_fatigue_now, 1e-5) == fat_vals[-1]
        assert pol._last_control_step == 6 + i

    # Resetting and recalling get fatigue should give same values
    pol.reset()
    assert pytest.approx(pol._get_athlete_fatigue_now(), 1e-5) == fat_vals[-1]

def test_get_athlete_fatigue_asynchronous_steps(sim):
    """Test getting athlete fatigue values with asynchronous
        simulation step & fatigue model calls."""
    pol = MsrOpponent(sim)

    # Before a step, should be 1
    assert pol._last_control_step == 0
    assert pol._athlete_fatigue_now == 1
    assert pol._get_athlete_fatigue_now() == 1

    # Step simulation twice
    for _ in range(2):
        sim.update_rider_states(
            timestep=0.1,
            athletes_control=([1], [100]),
            )

    # Values should match for two alternate step/fatigue calls.
    assert pytest.approx(pol._get_athlete_fatigue_now(), 1e-5) == 0.9990156
    assert pytest.approx(pol._athlete_fatigue_now, 1e-5) == 0.9990156
    assert pol._last_control_step == 2

    # Step simulation twice
    for _ in range(2):
        sim.update_rider_states(
            timestep=0.1,
            athletes_control=([1], [100]),
            )

    # Values should match for two alternate step/fatigue calls.
    assert pytest.approx(pol._get_athlete_fatigue_now(), 1e-5) == 0.99786967
    assert pytest.approx(pol._athlete_fatigue_now, 1e-5) == 0.99786967
    assert pol._last_control_step == 4

    # Step twice with zero power
    for _ in range(2):
        # Step simulation with 0 power.
        sim.update_rider_states(
            timestep=0.1,
            athletes_control=([1], [0]),
            )
    assert pytest.approx(pol._get_athlete_fatigue_now(), 1e-5) == 0.99786967
    assert pytest.approx(pol._athlete_fatigue_now, 1e-5) == 0.99786967
    assert pol._last_control_step == 6

    # Resetting and recalling get fatigue should give same values
    pol.reset()
    assert pytest.approx(pol._get_athlete_fatigue_now(), 1e-5) == 0.99786967

def test_dqn_opponent(sim):
    """Test trained DQN opponents."""
    # 4 DQN agents in total.
    for controller_name in ['agent_1', 'agent_2', 'agent_3', 'agent_4']:
        pol = MsrOpponent(sim, controller_name=controller_name)
        assert pol.controller_name == controller_name
        assert isinstance(pol.controller_description, str)

        # Predict should throw error for wrong size state
        with pytest.raises(ValueError):
            pol.predict([0, 0], [1, 1.5])
        with pytest.raises(ValueError):
            pol.predict([0, 0, 1], [1.5, 1, 1])

        # Test states which return control of 1.
        states = [ [0, 0, 1, 1, 1.5],
                [300, 10, 0.9, 315, 10],
                [400, 15, 0.5, 385, 15] ]
        for state in states:
            assert pol.predict(state[:3], state[3:]) == 1

        # Test running the sim
        athlete_state, opponent_state = [ [0, 0], [1, 1, 1] ]
        while opponent_state[0] < 250 and athlete_state[0] < 250:
            opp_control = pol.predict(opponent_state, athlete_state)
            array_state = sim.update_rider_states(
                            timestep=0.1,
                            athletes_control=([1], [opp_control]),
                            )
            opponent_state = array_state[0]
            athlete_state = array_state[1]

def test_policy_opponent(sim):
    """Test trained policy opponents."""
    # 3 Policy based agents in total.
    for controller_name in ['policy_1', 'policy_2', 'policy_3']:
        pol = MsrOpponent(sim, controller_name=controller_name)
        assert pol.controller_name == controller_name
        assert isinstance(pol.controller_description, str)

        # Predict should throw error for wrong size state
        with pytest.raises(ValueError):
            pol.predict([0, 0, 1], [1, 1.5, 1])
        with pytest.raises(ValueError):
            pol.predict([0, 0], [1, 1.5])

        # Test states which return control of 1.
        states = [ [0, 0, 1, 1, 1.5],
                [300, 10, 0.9, 315, 10],
                [400, 15, 0.5, 385, 15] ]
        for state in states:
            assert pol.predict(state[:3], state[3:]) == 1

        # Test running the sim
        athlete_state, opponent_state = [ [0, 0], [1, 1, 1] ]
        while opponent_state[0] < 250 and athlete_state[0] < 250:
            opp_control = pol.predict(opponent_state, athlete_state)
            array_state = sim.update_rider_states(
                            timestep=0.1,
                            athletes_control=([1], [opp_control]),
                            )
            opponent_state = array_state[0]
            athlete_state = array_state[1]

def test_expert_opponent(sim):
    """Test expert opponent class."""
    # Expert controlling rider A
    pol = MsrOpponent(sim, controller_name="expert", rider_name='Rider A')
    assert pol.controller_name == "expert"
    assert isinstance(pol.controller_description, str)

    # Predict should throw error for wrong size state
    with pytest.raises(ValueError):
        pol.predict([0, 0], [1, 1.5])
    with pytest.raises(ValueError):
        pol.predict([0, 0, 1], [1.5, 1, 1])

    # More in draft spot - should be 0
    assert pol.predict([300, 10, 0.9], [312, 10]) == pytest.approx(0.2887, 1e-3)
    assert pol.predict([300, 10, 0.9], [310, 10]) == pytest.approx(0.3762, 1e-3)
    assert pol.predict([300, 10, 0.9], [330, 15]) == pytest.approx(0.6881, 1e-3)
    assert pol.predict([1, 0.1, 1], [0, 0.05]) == 0.5

    # Can't control rider B without Variable Fatigue model.
    with pytest.raises(RuntimeError):
        pol = MsrOpponent(sim, controller_name="expert", rider_name='Rider B')

    # Test running the sim
    athlete_state, opponent_state = [ [0, 0], [1, 1, 1] ]
    while opponent_state[0] < 250 and athlete_state[0] < 250:
        opp_control = pol.predict(opponent_state, athlete_state)
        array_state = sim.update_rider_states(
                        timestep=0.1,
                        athletes_control=([1], [opp_control]),
                        )
        opponent_state = array_state[0]
        athlete_state = array_state[1]

def test_evaluate(sim):
    """Test the evaluate function."""
    # Fails for expert policies
    with pytest.raises(NotImplementedError):
        pol = MsrOpponent(sim, controller_name="expert")
        pol.evaluate([0,0,0],[0,0])

    # Agent
    pol = MsrOpponent(sim, controller_name="agent_1")
    dist = pol.evaluate([300, 10, 1.0], [299, 10], normalise=True)
    assert len(dist) == 5
    assert pytest.approx(sum(dist), 1e-3) == 1

    # Policy
    pol = MsrOpponent(sim, controller_name="policy_1")
    dist = pol.evaluate([300, 10, 1.0], [299, 10])
    assert len(dist) == 10
    assert pytest.approx(sum(dist), 1e-3) == 1

    # Evaluate should throw error for wrong size state
    with pytest.raises(ValueError):
        pol.evaluate([0,0],[0,0])
    with pytest.raises(ValueError):
        pol.evaluate([0,0,0],[0,0,0])
