#!/usr/bin/env python

from setuptools import setup, find_packages

setup(
	name='gym_msr',
	version='0.0.1',
	author='Rob Sumner',
	author_email='rjcs3@cam.ac.uk',
	packages=find_packages(),
	license='All Rights Reserved',
	description=('Gym implementation of MSR sprint race.'),
	python_requires='>=3.6',
	install_requires=['gym>=0.15.6',
		'tensorflow==1.15.0',
		'stable_baselines==2.9.0',
		'Pillow>=7.1.1',
		'numpy>=1.18.1',
		'matplotlib>=2.2.3'],
	tests_require=['pytest>=4.0']
)
