"""Match Sprint Race Policies

This file contains a number of  policies which provide
a deterministic method for controlling MSR riders.

author: Rob Sumner

Classes
-------
MsrNetPolicy - Class implementing policy defined by a neural network type function
                approximator.
MsrPolicy - General policy for controlling an MSR rider.
TestPoint - Fixed description of policy definition used for evaluation.
LeadJockey - Policy implementing a policy for initially leading rider.
TrailDraft - Policy implementing a policy for initially trailing rider.
"""

import numpy as np
import os
import tensorflow as tf
from stable_baselines import DQN

class MsrNetPolicy():
    """Class implementing policy defined by a neural network or similar function
        approximator from a saved Tensorflow/Stable Baslines model.

        This class applies basic edge case bounds on separation and velocity
        difference (to control rider action). Inside these bounds, the power
        of the rider is controlled by a supplied policy.

        Parameters
        ----------
        path - Path to file defining policy function.
             - This can either be a .h5 Keras model file or a .zip stable
               baselines model, but the extension must be included.
        rider - Defines which rider is being controlled by this policy.
              - Default value is Rider B.
        max_distance - A maximum distance value used to normalise the state.
                     - This must match the values used in training.
                     - Default value is 760.
        max_speed - A maximum velocity value used to normalise state.
                  - Value must match training.
                  - Default is 30.

        Public methods
        --------------
        reset - Reset the agent.
              - Call this function between races if agent is being used sequentially.
        predict - Predict the action for the agent from a given state.
        evaluate - Return a distribution over actions for a given state.
                 - For policy agents, this distribution will be a valid
                   probability distribution.
                 - For agents, this gives the action-value function for each valid
                   action (which will not be a valid probability distribution).
    """

    def __init__(self, path, rider='Rider B', max_distance=760, max_speed=30):
        # Check path to agent.
        if not isinstance(path, str):
            raise ValueError('Invalid path. Please provide a string.')
        elif not os.path.exists(path):
            raise ValueError('Could not find path, ' + path)
        self.path = path

        # Try to load the agent
        if path.endswith ('.h5'):
            try:
                self.agent = tf.keras.models.load_model(self.path)
                self.agent_type = 'Keras'
            except:
                raise ValueError('Could not load file ' + self.path)
        elif path.endswith('.zip'):
            try:
                self.agent = DQN.load(path, custom_objects={'verbose':0})
                self.agent_type = 'Baselines'
            except:
                raise ValueError('Could not load file ' + self.path)
        else:
            raise ValueError('Could not load file. Please provide path with extension')

        # Rider that policy is controlling.
        if rider not in ['Rider A', 'Rider B']:
            raise ValueError('Rider must match MSR gym race definition ' + \
                                '(i.e. Rider A or Rider B).')
        self.rider = rider
        self.max_distance = max_distance
        self.max_speed = max_speed

    def reset(self):
        """Reset the class."""
        pass

    def predict(self, state):
        """Predict action for given state.
            Parameters:
            state - The unnormalised state of the envrionment, a vector of length
                    6 which.
            Returns:
            action - A value in the range [0, 1] to control the power output of
                     the rider.
        """

        # Split state based upon which rider is being controlled
        if self.rider == 'Rider B':
            rider_state = state[3:]
            opponent_state = state[0:3]
        else:
            rider_state = state[0:3]
            opponent_state = state[3:]

        # Sprint if moving slower than opponent.
        if opponent_state[1] - rider_state[1] >= 1.2:
            return 1
        # Sprint if gap of +15m opens up (leading)
        elif rider_state[0] - opponent_state[0] >= 15:
            return 1
        # Sprint if gap of -12m opens up (trailing)
        elif opponent_state[0] - rider_state[0] >= 10:
            return 1

        # Agent requires [rider, opponent] state values
        # normalised in range [0,1] for each element.
        norm_state = [rider_state[0]/self.max_distance,
                      rider_state[1]/self.max_speed,
                      rider_state[2],
                      opponent_state[0]/self.max_distance,
                      opponent_state[1]/self.max_speed,
                      opponent_state[2]]
        obs = np.array(norm_state).reshape((1,6))

        # Get prediction distribution over actions.
        if self.agent_type is 'Keras':
            action_dist = self.agent.predict(obs)[0]
        elif self.agent_type is 'Baselines':
            # Sess.run returns [[action_dist], [act_probs]]
            # we don't care about action probabilities, only distribution.
            action_dist = self.agent.step_model.sess.run([self.agent.step_model.q_values,
                                self.agent.step_model.policy_proba],
                                {self.agent.step_model.obs_ph: obs})[0][0]

        return np.argmax(action_dist) / (len(action_dist) - 1)

    def evaluate(self, state):
        """Return value distribution over actions for analysis.
            Parameters:
            state - The unnormalised state of the envrionment, a vector of length
                    6 which.
            Returns:
            dist - Distribution over action space of agent. Each entry is the
                   predicted action value for action in current state.
        """

        # Split state based upon which rider is being controlled
        if self.rider == 'Rider B':
            rider_state = state[3:]
            opponent_state = state[0:3]
        else:
            rider_state = state[0:3]
            opponent_state = state[3:]

        # Agent requires [rider, opponent] state values
        # normalised in range [0,1] for each element.
        norm_state = [rider_state[0]/self.max_distance,
                      rider_state[1]/self.max_speed,
                      rider_state[2],
                      opponent_state[0]/self.max_distance,
                      opponent_state[1]/self.max_speed,
                      opponent_state[2]]
        obs = np.array(norm_state).reshape((1,6))

        # Get prediction distribution over actions.
        if self.agent_type is 'Keras':
            return self.agent.predict(obs)[0]
        elif self.agent_type is 'Baselines':
            # Sess.run returns [[action_dist], [act_probs]]
            # we don't care about action probabilities, only distribution.
            return self.agent.step_model.sess.run([self.agent.step_model.q_values,
                            self.agent.step_model.policy_proba],
                            {self.agent.step_model.obs_ph: obs})[0][0]


class MsrPolicy():
    """General pre-determined policy which implements both trailing and
        leading policies.

        When leading, policy implments drops and increases in power to try and
        force the trailing rider to move in front. When trailing, the policy
        uses a drafting controller to remain behind the leading rider.

        Sprinting distances are decided based upon whether the rider is leading
        or trailing, and distances are set to lie within regions where the policy
        is sure to win (independent of fatigue).

        This represents a 'risk-averse' policy which is sub-optimal - a better
        policy would include consideration of differences in fatigue between
        both riders.

        Parameters
        ----------
        rider - Defines which rider is being controlled by this policy.
              - Default value is Rider B.

        Public Methods
        --------------
        reset - Reset the policy definition.

        predict - Return a control value for a given state and equilibrium
                  exertion value.
    """
    def __init__(self, rider='Rider B'):
        # Rider that policy is controlling.
        if rider not in ['Rider A', 'Rider B']:
            raise ValueError('Rider must match MSR gym race definition ' + \
                                '(i.e. Rider A or Rider B).')
        self.rider = rider

        # Allowable sprinting distances
        self.min_trail_sprint_distance = 480
        self.max_trail_sprint_distance = 655
        self.min_lead_sprint_distance = 705
        self.max_lead_sprint_distance = 750
        self.max_sep_before_sprint = 15

        # Implementation of events
        self.event_steps_completed = 0 # Number of timesteps of event completed.
        self.current_event_index = -1   # Index in lookup/description of current event.
        self.n_events = 0   # Total number of events.
        self.sprinting = False  # True when rider has begun sprinting.
        self.event_descriptions = [] # Descriptions of all events.
        self.event_distance_lookup = [] # Distance ranges for events.

        # Drafting controller
        self.draft_controller = DraftController(rider=self.rider)

        # Randomise starting point
        self.reset()

        # Fixed policies for testing
        # Each policy is (sprint_starting_distance)
        self.test_points = [TestPoint(200, 600, [], [], [], []),
                            TestPoint(263, 650, [100],[25], [50], [15]),
                            TestPoint(325, 700, [50], [15], [100], [50]),
                            TestPoint(389, 600, [100, 290], [40, 10], [200], [30]),
                            TestPoint(450, 650, [50, 350, 400], [50, 20, 10], [250], [25]),
                            TestPoint(513, 700, [150], [15], [50, 90], [30, 15]),
                            TestPoint(575, 600, [225], [25], [75, 150], [25, 25]),
                            TestPoint(638, 650, [],  [], [100, 250], [20, 30]),
                            TestPoint(700, 700, [100, 425],  [30, 15], [250, 350], [50, 10]),
                            TestPoint(550, 705, [400], [30], [150], [30])]

    def reset(self, random_seed=None, sprint_distance=None):
        """Reset the policy. When no arguments provided, this means the policy is
            randomised.
            Parameters:
            random_seed - Int corresponding to a test point.
            sprint_distance - Distance override for policy randomisation.
        """
        # Reset trackers for producing the events
        self.event_steps_completed = 0
        self.current_event_index = -1
        self.sprinting = False

        # For no provided policy, randomise fully.
        if random_seed is None:
            self.randomise_policy(sprint_distance=sprint_distance)
        # Valid random seed provided
        elif type(random_seed) is int and 0 <= random_seed < len(self.test_points):
                self.lead_sprint_start_distance = self.test_points[random_seed].lead_sprint_start_distance
                self.trail_sprint_start_distance = self.test_points[random_seed].trail_sprint_start_distance
                self.event_descriptions, self.event_distance_lookup = \
                    self.test_points[random_seed].get_description()
                self.n_events = len(self.event_descriptions)
        else:
            raise ValueError('Random seed does not match a defined policy.')

    def randomise_policy(self, sprint_distance=None):
        """Randomise policy parameters.
            Parameters:
            sprint_distance - Override of random sprint distance.
        """
        # Random number of drops and jumps (max 3 each).
        n_drops = np.random.randint(3)
        n_jumps = np.random.randint(3)
        self.n_events = n_drops + n_jumps

        # Set sprint parameters
        self.sprinting = False
        if sprint_distance is None:
            self.lead_sprint_start_distance = np.random.rand() * \
                                                ( self.max_lead_sprint_distance - \
                                                self.min_lead_sprint_distance ) + \
                                                self.min_lead_sprint_distance
            self.trail_sprint_start_distance = np.random.rand() * \
                                                ( self.max_trail_sprint_distance - \
                                                self.min_trail_sprint_distance ) + \
                                                self.min_trail_sprint_distance
        else:
            if sprint_distance > 750 or sprint_distance < 0:
                raise ValueError('Sprint distance out of range.')
            self.lead_sprint_start_distance = sprint_distance
            self.trail_sprint_start_distance = sprint_distance

        # Scale maximum duration by number of events.
        if self.n_events > 0:
            max_duration = int(50*(3/self.n_events))
        else:
            max_duration = 0

        self.event_descriptions = []
        self.event_distance_lookup = []
        n_drops_set = 0
        for i in range(self.n_events):
            # Use a fixed bin length to ensure no overlap of drops/jumps
            event_bin = self.lead_sprint_start_distance/self.n_events

            # Randomly select jump or drop without replacement.
            event_selector = np.random.randint(n_drops + n_jumps - i)
            if event_selector < n_drops - n_drops_set:
                # Event is a drop
                drop_distance = np.random.rand()*event_bin + i*event_bin
                drop_duration = np.random.randint(10, max_duration)
                self.event_descriptions.append( ('drop', drop_distance, drop_duration) )

                # Store bounds for the event
                self.event_distance_lookup.append( (drop_distance, (i+1)*event_bin) )
            else:
                # Event is a jump
                jump_distance = np.random.rand()*event_bin + i*event_bin
                jump_duration = np.random.randint(10, max_duration)
                self.event_descriptions.append( ('jump', jump_distance, jump_duration) )

                # Store bounds for the event
                self.event_distance_lookup.append( (jump_distance, (i+1)*event_bin) )

    def predict(self, state, equilibrium_exertion):
        """Return an exertion value controlling power.
            Parameters:
            state - Unnormalised state of the race (6,)
            equilibrium_exertion - Calculated equilirbium exertion to hold
                                   drafting position.
            Returns:
            control - Power control value in range [0,1]
        """
        # Always sprint once jump has occured
        if self.sprinting:
            return 1

        # Split state based upon which rider is being controlled
        if self.rider == 'Rider B':
            rider_state = state[3:]
            opponent_state = state[0:3]
        else:
            rider_state = state[0:3]
            opponent_state = state[3:]

        # Rider is leading
        if rider_state[0] > opponent_state[0]:
            # Sprint if after lead sprint distance
            if rider_state[0] >= self.lead_sprint_start_distance:
                self.sprinting = True
                return 1
            # Sprint if there is a large gap.
            elif rider_state[0] >= opponent_state[0] + self.max_sep_before_sprint:
                self.sprinting = True
                return 1
            # Before sprint point - employ drops & jumps.
            else:
                # Find nearest event & get duration + type.
                event_duration, event_type = self.get_current_event(rider_state)

                # If there is no event to run.
                if event_type is None:
                    return 0.5

                # Perform event if inside the step range of current event.
                elif self.event_steps_completed < event_duration:
                    self.event_steps_completed += 1
                    if event_type == 'jump':
                        return 1
                    else:
                        return 0.1

                # Carry on at 0.5 if event has been completed.
                else:
                    return 0.5

        # Rider is trailing
        else:
            # Sprint if after trail sprint distance
            if rider_state[0] >= self.trail_sprint_start_distance:
                self.sprinting = True
                return 1
            # If opponent is stationary, overtake slowly
            elif opponent_state[1] < 0.1:
                return 0.5
            # Draft if we haven't become stationary
            elif rider_state[1] > 0.2:
                return self.draft_controller.control(state, equilibrium_exertion)
            # Prevent riders becoming permanently stopped.
            else:
                return 0.5

    def get_current_event(self, rider_state):
        """Return the event currently occuring.
            Parameters:
            rider_state - The state of the rider at the current time.
            Returns:
            event_duration - The number of time steps the event will last for.
            event_type - Either 'jump' or 'drop', describing the type of event.
        """
        # When event changes, reset self.event_steps_completed
        event_type = None
        event_duration = 0

        # Return if no events planned.
        if self.n_events == 0:
            return event_duration, event_type

        # Find event currently occuring
        event_index = -1
        for ind, distance_range in enumerate(self.event_distance_lookup):
            if distance_range[0] <= rider_state[0] and rider_state[0] < distance_range[1]:
                event_index = ind
                break

        # Check on not yet reaching an event.
        if event_index == -1:
            return event_duration, event_type

        # Reset number of steps if i changes.
        if event_index != self.current_event_index:
            self.event_steps_completed = 0
            self.current_event_index = event_index

        return self.event_descriptions[event_index][2], self.event_descriptions[event_index][0]



class DraftController():
    """Class to implement drafting controller.

        Parameters
        ----------
        None

        Public Methods
        --------------
        control - Returns the control value that should be used.
    """

    def __init__(self, rider='Rider B'):
        """Initialise the controller.
            Values of k_p and k_v are the result of a tuning investigation which
            balanced response time, overshoot and undershoot.
            k_p - Proportional gain for the separation error (tuned value = 1.5).
            k_v - Proportional gain for the velocity error (tuned value = 5.0).

            Parameters:
            rider - The rider being controlled by the drafting controller.
                  - Default value is rider B.
        """
        self.k_p = 1.5
        self.k_v = 5.0
        self.last_exertion_value = 0
        if rider not in ['Rider A', 'Rider B']:
            raise ValueError('Unknown rider.')
        self.rider = rider

    def control(self, state, equilibrium_exertion, last_exertion_value=None):
        """Applies control about an equilibrium exertion (calculated to be at 2.5m
            behind the leading rider).
            Control is proportional to the error signal (separation error and velocity
            error). I.e. it is a PD controller on the separation error signal.
            Parameters:
            state - The current state of the system.
            equilibrium_exertion - The exertion required to hold rider B 2.5m behind
                                    rider A at the current time-step.
            last_action - The last control action used.
                        - If none provided, the previous value is used.
        """
        # Set correct direction for rider being controlled
        if self.rider == 'Rider B':
            separation = state[0] - state[3] - 2.5
            velocity = state[1] - state[4]
        else:
            separation = state[3] - state[0] - 2.5
            velocity = state[4] - state[1]

        # Use PD controller about equilibrium exertion
        exertion_val =  (1 + self.k_p*separation + self.k_v*velocity) * equilibrium_exertion

        # Clip exertion to lie in [0,1]
        exertion_val = max(0, exertion_val)
        exertion_val = min(exertion_val, 1)

        # Smooth exertion value to retain stability
        if last_exertion_value is None:
            new_action = 0.5 * (exertion_val + self.last_exertion_value)
        else:
            new_action = 0.5 * (exertion_val + last_exertion_value)
        self.last_exertion_value = new_action
        return new_action



class TestPoint():
    """Class describing a fixed policy test point.

        Parameters
        ----------
        trail_sprint_start_distance - Point at which rider will sprint from trailing position.
        lead_sprint_start_distance - Point at which rider will sprint form leading position.
        jump_distances - List of distances at which jumps occur.
        jump_durations - List of durations (in # timesteps) matching number of jumps.
        drop_distances - List of distances at which drops occur.
        drop_durations - List of drop durations (in # timesteps) matching number of drops.

        Public Methods
        --------------
        get_description - Return a description of the event in distance order.
    """
    def __init__(self, trail_sprint_start_distance,
                    lead_sprint_start_distance,
                    jump_distances=[], jump_durations=[],
                    drop_distances=[], drop_durations=[]):
        # Sprint distance limits.
        if trail_sprint_start_distance < 0 or trail_sprint_start_distance > 750:
            raise ValueError('Sprint start distance must lie in range [0,750]')
        self.trail_sprint_start_distance = trail_sprint_start_distance
        if lead_sprint_start_distance < 0 or lead_sprint_start_distance > 750:
            raise ValueError('Sprint start distance must lie in range [0,750]')
        self.lead_sprint_start_distance = lead_sprint_start_distance

        # Check jump distances and durations.
        if len(jump_distances) != len(jump_durations):
            raise ValueError('Number of jump distances and jump durations must match.')
        for i in range(len(jump_distances)):
            if jump_distances[i] < 0 or jump_distances[i] > lead_sprint_start_distance:
                raise ValueError('Jump distance must lie in range [0,sprint_start_distance]')
            if jump_durations[i] <=0 or jump_durations[i] > 50:
                raise ValueError('Jump duration must lie in range (0, 50]')
        self.jump_distances = jump_distances
        self.jump_durations = jump_durations
        self.n_jumps = len(jump_distances)

        # Check drop distances and durations.
        if len(drop_distances) != len(drop_durations):
            raise ValueError('Number of drop distances and drop durations must match.')
        for i in range(len(drop_distances)):
            if drop_distances[i] < 0 or drop_distances[i] > lead_sprint_start_distance:
                raise ValueError('Drop distance must lie in range [0,sprint_start_distance]')
            if drop_durations[i] <=0 or drop_durations[i] > 50:
                raise ValueError('Drop duration must lie in range (0, 50]')
        self.drop_distances = drop_distances
        self.drop_durations = drop_durations
        self.n_drops = len(drop_distances)
        self.n_events = self.n_drops + self.n_jumps

    def get_description(self):
        """Convert test point into a distance based tuple for use by policy.
            Parameters:
            None
            Returns:
            descriptions - [sprint_start_distance, [(event_descriptions)] ] where
                            each event description is a list of tuples which are
                            ('jump'/'drop', distance, duration).
            distance_lookup - List of distance ranges for each event, in the form
                              [ (start_distance, max_distance) ]
                            - max_distance defines the largest distance in which
                              this event can occur. This is used by the policy
                              to decide which event to implement.
        """
        events = []
        distances = []
        for i in range(self.n_jumps):
            events.append( ('jump', self.jump_distances[i], self.jump_durations[i]) )
            distances.append( self.jump_distances[i] )
        for j in range(self.n_drops):
            events.append( ('drop', self.drop_distances[j], self.drop_durations[j]) )
            distances.append( self.drop_distances[j] )

        # Sort events in ascending distance order
        event_descriptions = [x for _,x in sorted(zip(distances, events))]

        # Create distance lookup
        event_distance_lookup = []
        for i, event in enumerate(event_descriptions):
            # If this is the last event
            if i == len(event_descriptions) - 1:
                event_distance_lookup.append( (event[1], 750) )
            else:
                event_distance_lookup.append( (event[1], event_descriptions[i+1][1]) )

        return event_descriptions, event_distance_lookup




# Note that the policies below are obsolete - they are retained here
# for their use in investigations, but the most recent environment now uses the
# general MSR policy described above.
class LeadJockey():
    """Class implementing policy for rider B leading.
        Rider B leads and uses reduction & increase in power to force rider A
        to overtake early on (jockeying).

        Rider B will employ a random number (0, 1 or 2) of drops and increases in power
        before a random sprint distance in range (100, 550) (at which point Rider B moves
        at maximum exertion).

        Parameters
        ----------
        None

        Public Methods
        --------------
        randomise - Randomise the policy about a specific point.

        predict - Return response from this policy under specific state.
    """

    def __init__(self):
        """Initalise the policy."""
        # Drafting controller value
        self.draft_controller = DraftController()
        self.last_exertion_value = 0

        # Randomise starting point
        self.randomise()

        # Fixed policies for testing
        # Each policy is (sprint_starting_distance, [drop_distances], [drop_duration],
        #                 [jump_distances], [jump_durations])
        self.test_points = [(200, []),
                            (300, []),
                            (400, []),
                            (500, []),
                            (500, [('jump', 50,  45),
                                   ('drop', 250, 40)]),
                            (300, [('jump', 50,  15),
                                   ('drop', 100, 50)]),
                            (400, [('jump', 100, 40),
                                   ('drop', 200, 30),
                                   ('jump', 290, 10)]),
                            (500, [('jump', 50,  50),
                                   ('drop', 250, 25),
                                   ('jump', 350, 20),
                                   ('jump', 400, 10)]),
                            (200, [('drop', 50,  30),
                                   ('drop', 90,  15),
                                   ('jump', 150, 15)]),
                            (300, [('drop', 75,  25),
                                   ('drop', 150, 25),
                                   ('jump', 225, 25)]),
                            (400, [('drop', 100, 20),
                                   ('drop', 250, 30)]),
                            (500, [('jump', 50, 40),
                                   ('drop', 150, 30),
                                   ('drop', 250, 40),
                                   ('jump', 300, 20)]) ]

    def randomise(self, random_seed=None):
        """Randomise the policy about a given point.
            The number of drops in power is randomised to be 0,1 or 2.
            The distance at which rider B will sprint is randomised to lie in
            the range (100, 550).
            Parameters:
            random_seed - int corresponding to a specific random form of the
                          policy (given in self.test_points).
                        - If None, the policy is automatically.
        """
        # Reset trackers for producing the drop.
        self.events_completed = 0
        self.event_steps_completed = 0

        if random_seed is None:
            # Sprints start between 100 & 550 m
            n_drops = np.random.randint(3)
            n_jumps = np.random.randint(3)
            self.n_events = n_drops + n_jumps
            self.sprint_start_distance = np.random.rand()*449 + 101

            # Randomise drop/jump points
            if self.n_events > 0:
                max_duration = int(50*(3/self.n_events))
            else:
                max_duration = 0

            self.event_descriptions = []
            n_drops_set = 0
            for i in range(self.n_events):
                # Use a fixed bin length to ensure no overlap of drops/jumps
                event_bin = self.sprint_start_distance/self.n_events

                # Randomly select jump or drop without replacement.
                event_selector = np.random.randint(n_drops + n_jumps - i)
                if event_selector < n_drops - n_drops_set:
                    # Event is a drop
                    drop_point = np.random.rand()*event_bin + i*event_bin
                    drop_duration = np.random.randint(10, max_duration)
                    self.event_descriptions.append( ('drop', drop_point, drop_duration) )
                else:
                    # Event is a jump
                    jump_point = np.random.rand()*event_bin + i*event_bin
                    jump_duration = np.random.randint(10, max_duration)
                    self.event_descriptions.append( ('jump', jump_point, jump_duration) )

        elif type(random_seed) is int and 0 <= random_seed < len(self.test_points):
                self.sprint_start_distance = self.test_points[random_seed][0]
                self.event_descriptions = self.test_points[random_seed][1]
                self.n_events = len(self.event_descriptions)

        else:
            raise ValueError('Random seed does not match a defined policy.')

    def predict(self, state, equilibrium_exertion):
        """Return an action based upon the current state.
            Parameters:
            state - The current state of the environment.
            rider_a_action - Last action taken by rider A.
        """
        rider_a_distance = state[0]
        rider_b_distance = state[3]

        # Rider B is still leading
        if rider_b_distance > rider_a_distance:
            # Before sprint started
            if rider_b_distance < self.sprint_start_distance:
                # All drops and jumps completed
                if self.events_completed == self.n_events:
                    self.last_exertion_value = 0.5
                    return 0.5
                # Still have events to run
                elif rider_b_distance >= self.event_descriptions[self.events_completed][1]:
                    event_type =  self.event_descriptions[self.events_completed][0]
                    event_duration =  self.event_descriptions[self.events_completed][2]
                    if self.event_steps_completed < event_duration:
                        self.event_steps_completed += 1
                        if event_type == 'drop':
                            self.last_exertion_value = 0.1
                            return 0.1
                        else:
                            self.last_exertion_value = 1
                            return 1
                    else:
                        self.event_steps_completed = 0
                        self.events_completed += 1
                        self.last_exertion_value = 0.5
                        return 0.5
                else:
                    self.last_exertion_value = 0.5
                    return 0.5
            # Full sprint after rider sprint distance
            else:
                self.last_exertion_value = 1
                return 1
        # Rider A in front.
        else:
            # Draft until sprint distance point
            if rider_b_distance < self.sprint_start_distance:
                # Drafting control logic
                draft_exer_val = self.draft_controller.control(state, equilibrium_exertion,
                                    last_exertion_value=self.last_exertion_value)
                self.last_exertion_value = draft_exer_val
                return draft_exer_val
            # Full sprint
            else:
                self.last_exertion_value = 1
                return 1


class TrailDraft():
    """Class implementing policy for rider B trailing.
        Rider B trails and attempts to draft behind rider A before sprinting.

        Rider B will sprint at a random point in the range [100, 550].
        If Rider A sprints, there will be a delay of 5 steps (roughly half a second)
        before Rider B reacts.

        Parameters
        ----------
        k_p, k_v - Parameters for drafting controller.

        Public Methods
        --------------
        randomise - Randomise the policy about a specific point.

        predict - Return response from this policy under specific state.
    """

    def __init__(self):
        """Initalise the policy.
            Parameters:
            None
        """
        # Sprinting parameters
        self.min_sprint_distance = 480
        self.max_sprint_distance = 655

        # Drafting controller value
        self.draft_controller = DraftController()

        # Randomise starting point
        self.randomise()

        # Fixed policies for testing
        # Each policy is (sprint_starting_distance)
        self.test_points = [(480,), (500,), (519,), (540,), (558,),
                            (577,), (597,), (616,), (635,), (655,)]

    def randomise(self, random_seed=None):
        """Randomise the policy about a given point.
            The number of drops in power is randomised to be 0,1 or 2.
            The distance at which rider B will sprint is randomised to lie in
            the range (100, 550).
            Parameters:
            random_seed - int corresponding to a specific random form of the
                          policy (given in self.test_points).
                        - If None, the policy is automatically.
        """
        if random_seed is None:
            # Sprints start between 480 & 655 m
            self.sprint_start_distance = np.random.rand() * (self.max_sprint_distance - \
                self.min_sprint_distance) + self.min_sprint_distance
        elif type(random_seed) is int and 0 <= random_seed < len(self.test_points):
                self.sprint_start_distance = self.test_points[random_seed][0]
        else:
            raise ValueError('Random seed does not match a defined policy.')

    def predict(self, state, equilibrium_exertion):
        """Return an action based upon the current state.
            Parameters:
            state - The current state of the environment.
            rider_a_action - Last action taken by rider A.
        """
        rider_a_distance = state[0]
        rider_b_distance = state[3]
        rider_b_velocity = state[1]

        # # Pure drafting response
        # return draft_controller(state, equilibrium_exertion, k_p=self.k_p, k_v=self.k_v)

        # If reached sprinting point
        if rider_b_distance >= self.sprint_start_distance:
            return 1
        # If rider A is infront and we have not become stationary.
        elif rider_a_distance > rider_b_distance and rider_b_velocity > 0.1:
            return self.draft_controller.control(state, equilibrium_exertion)
        # We have moved in front or stopped - low power
        else:
            # Move at low power until sprint
            return 0.5