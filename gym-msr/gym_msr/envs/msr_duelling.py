"""Match Sprint Race Gym Environment
Creates a MSR simulation in which the riders follow the black line.
The user is able to control the power output of rider A, while rider B has an
identical physiology model but may follow different control strategies.

Control strategies in this set of environments are more realisting than in
previous forms.

Riders in these environments follow the Variable Maximal Sub-Maximal
physiology model. This is an adapted form of the Torque-Cadence model which
allows for sub-maximal actions (with reduced fatigue rate).

author: Rob Sumner
Classes
-------
MsrDuelFixed - Environment implementing pre-determined & fixed policies for Rider B.
"""

# Suppress warnings due to prescribed package versions
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
import tensorflow as tf
tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)
import os
os.environ["KMP_WARNINGS"] = "FALSE"

# Gym based imports.
import gym
from gym import error, spaces, utils
from gym.utils import seeding
import copy

# Lapysim imports.
import numpy as np
import lapysim
from lapysim.simulation import ForwardDynamics

# Base Gym
from gym_msr.envs.msr_ident_submax_base import MsrSubMaxBase
from gym_msr.envs.msr_policies import TrailDraft, LeadJockey, MsrPolicy, MsrNetPolicy


class MsrDuelFixed3(MsrSubMaxBase):
    """Implement MSR simulation as Gym.

    VERSION 3 - msr-duel-fixed-v3
    Updates from version 2 switches the Rider B control policy from hand-crafted
    'expert' policy to one run by a randomly selected, pre-trained agent.

    This gym requires version of Lapysim with aero modelling implemented.
    Currently, this uses a single dimension aero model.
    Opponents in this gym are identical and follow the sub-max physiology model.

    This gym implements full MSR race simulation, where Rider A's power output
    is controlled externally (by an agent) while Rider B's power output is controlled
    by pre-trained, stationary agents.

    Starting positions (i.e. leading/trailing) and Rider B's policy is randomised
    at each reset.

    The Reward function employed is the 'scaled-distance' function used previously.

    Observation:
        All observation spaces are normalised.
        Type: Box(6)
        Num	Observation                 Min         Max
        0	Rider A Distance            0            1
        1	Rider A Velocity            0            1
        2   Rider A Fatigue             0            1
        3	Rider B Distance            0            1
        4	Rider B Velocity            0            1
        5   Rider B Fatigue             0            1

    Actions:
        Type: Discrete(n)
        Num	    Action
        0:4   [0:0.25:1] - Power value in range [0,1] mapping to exertion.

    Parameters
    ----------
    MsrIdenticalBase - The parent class for variable opponent.

    verbose - Bool flag controlling data which is printed to console.

    Public methods:
    ---------------
    __init__(self):
        Initialise MSR gym.
        verbose - Information reporting flag for the environment.

    complete(self):
        Return True if race is finished, False if not.

    riders_stationary(self):
        Return True if both riders have stopped moving (defined by a velocity
        threshold value in function).

    step(self, action):
        Step the gym forward by a time step.
        Action is an array [value] where 0 <= value <= 1
        Returns [state, reward, done, additional_info]

    reset(self):
        Reset the gym to its initial state.

    reward(self):
        Returns reward for given state.
            0 - If race is incomplete.
            0.75*(rider_distance/race_distance) - If race complete and rider A loses.
            1 + separation - If race complete and rider A wins.

    normalised_state(self):
        Returns a state value normalised to match observation space.

    random_init(self, leading_rider=None, policy_random_seed=None):
        Returns initialisations state for a random sprint.

    build_evaluators(self):
        Build evaluators needed to calculate simulation properties.

    get_draft_exertion(self, eq_sep=2.5):
        Return the equilibrium exertion required for Rider B to draft at a
           given point behind Rider A.
    """

    metadata = {'render.modes': ['human', 'rgb_array']}

    def __init__(self, verbose=True):
        """Initialise the MSR gym.
            Parameters:
            verbose - Flag for limiting the amount of reporting during learning.
                    - True allows statements to be printed, False does not.
        """

        # Env params
        super(MsrDuelFixed3, self).__init__()
        self.verbose = verbose
        self.action_space = spaces.Discrete(5)
        self.viewer = None
        self.time_step = 0.1

        # Observation spaces.
        self.race_distance = 750
        self.max_distance = 760
        self.max_speed = 30
        obs_low  = np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0])
        obs_high = np.array([1.0, 1.0, 1.0, 1.0, 1.0, 1.0])
        self.observation_space = spaces.Box(obs_low, obs_high, dtype=np.float32)

        # Find location of agent opponent folder.
        this_folder = os.path.dirname(os.path.abspath(__file__))
        self.agent_opponents_folder = os.sep.join([this_folder, 'data', 'agent_opponents'])
        self.agent_opponent_files = []
        for file in os.listdir(self.agent_opponents_folder):
            if file.endswith(".h5"):
                self.agent_opponent_files.append( os.sep.join([self.agent_opponents_folder, file]) )
        self.agent_opponents = [ MsrNetPolicy(file,
                                            rider='Rider B',
                                            max_distance=self.max_distance,
                                            max_speed=self.max_speed)
                                            for file in self.agent_opponent_files]

        # Initialise policy & starting point state
        self.rider_b_policy = None
        self.rider_a_init, self.rider_b_init = self.random_init()
        self.state = self.rider_a_init + self.rider_b_init

        # Environment info used externally.
        self.done = False
        self.info = {'repetitions':0, 'total_reward':0}
        self.reward = 0
        self.control_sum = 0
        self.env_steps = 0
        self.overtakes = 0
        self.leading_rider_now = None

        # Initialise the forward solution.
        # Master dynamics is stored to avoid multiple initialisations - much faster.
        self.match_sprint_simulation = self.match_sprint_event.initialise_forward_simulation()
        self.master_dynamics_lambdas = copy.deepcopy(self.match_sprint_simulation._dyn_eqns_lambdas)
        self.match_sprint_simulation.initialise_rider_states(
            (self.rider_a_init, self.rider_b_init))

    def complete(self):
        """Check if the simulation has completed.
        Return 1 (true) if race is finished.
        Return 0 (false) if race is not finished.
        """
        if (self.state[0] >= self.race_distance or self.state[3] >= self.race_distance):
            self.done = True
            return True
        elif self.riders_stationary():
            self.done = True
            return True
        else:
            return False

    def riders_stationary(self, v_thresh=0.025):
        """Check if riders have stopped moving.
            Parameters:
            v_thresh - Minimum velocity, below which riders are
                       deemed to be stationary.
            Returns:
            Bool - True if riders are still moving, false if not.
        """
        if self.state[1] < v_thresh and self.state[4] < v_thresh:
            return True
        else:
            return False

    def step(self, rider_a_action):
        """Step the gym forward by a time step.
        rider_a_action  - An integer value 0:1:5 which is converted into 0:0.2:1
                        - This maps to maximum possible output power in the
                            torque-cadence model.
                        - 1 is full power, 0 is no power.
                        - If no action is provided, the simulation will step
                            with previous action.
        Returns [state, reward, done, additional_info]
        """

        assert self.action_space.contains(rider_a_action), "%r (%s) invalid"%(rider_a_action, type(rider_a_action))
        assert self.rider_b_policy is not None

        if self.complete():
            self.reward = self.reward_function()
            self.info['total_reward'] = self.info['total_reward'] + self.reward
            self.close()
            return self.normalised_state(), self.reward, self.done, self.info

        # Normalise action value in rance [0,1]
        rider_a_control = rider_a_action / (self.action_space.n - 1)
        self.control_sum += rider_a_control

        # Get rider B's control in [0,1]
        rider_b_control = self.rider_b_policy.predict(self.state)

        # array_state is [ [rider_a_state], [rider_b_state] ]
        # rider_state is [ distance, velocity, fatigue ]
        array_state = self.match_sprint_simulation.update_rider_states(
            timestep=self.time_step,
            athletes_control=([rider_a_control], [rider_b_control]),
            )

        # State needs to match observation space [a_s, a_v, a_f, b_s, b_v, b_f]
        self.state = array_state[0] + array_state[1]

        # Track overtakes
        if self.state[0] > self.state[3] and self.leading_rider_now == 'Rider B':
            self.overtakes += 1
            self.leading_rider_now = 'Rider A'
        elif self.state[0] < self.state[3] and self.leading_rider_now == 'Rider A':
            self.overtakes += 1
            self.leading_rider_now = 'Rider B'

        # Get reward function
        self.reward = self.reward_function()
        self.info['total_reward'] = self.info['total_reward'] + self.reward
        self.env_steps += 1

        return self.normalised_state(), self.reward, self.done, self.info

    def reset(self, leading_rider=None, policy_random_seed=None, verbose=True,
             policy_file_path=None):
        """Reset the gym to its initial state.
            Parameters:
            leading_rider - Rider beginning in the leading position.
                          - If None, this is randomly selected (from Rider A or B).
            policy_random_seed- Specific randomisation point to be used in
                                Rider B's policy.
            verbose - Flag for reporting during reset.
            policy_file_path - The filepath to a trained agent to use as policy.
                             - This policy is used instead of the policies stored by
                               the agent.
        """
        # Current distance and velocity for two riders
        if verbose and self.verbose:
            print("Total reward = " + str(self.info['total_reward']))
            print("Reset #" + str(self.info['repetitions']))
        self.info['repetitions'] = self.info['repetitions'] + 1
        self.done = False
        self.info['total_reward'] = 0
        self.reward = 0
        self.control_sum = 0
        self.env_steps = 0
        self.overtakes = 0

        # Reset state randomly
        self.rider_a_init, self.rider_b_init = self.random_init(
            leading_rider=leading_rider,
            policy_random_seed=policy_random_seed,
            policy_file_path=policy_file_path)
        self.state = self.rider_a_init + self.rider_b_init

        # Reset the simulation.
        self.match_sprint_simulation = ForwardDynamics(
            self.match_sprint_event,
            self.master_dynamics_lambdas)
        self.match_sprint_simulation.initialise_rider_states(
            (self.rider_a_init,
            self.rider_b_init))
        return self.normalised_state()

    def reward_function(self):
        """ Return the reward.
            0 - If race has not been finished.
            1 + sep - If rider A wins.
            0.75*distance - If rider B wins.
                          - This is designed to encourage forward movement.
        """
        if not self.complete():
            return 0
        elif self.riders_stationary():
            return -1
        elif self.state[0] >= self.state[3]:
            return 1 + (self.state[0] - self.state[3])
        else:
            return 0.75*(self.state[0] - self.rider_a_init[0]) / \
                    (self.race_distance - self.rider_a_init[0])

    def normalised_state(self):
        """Return a normalised state value.
            This normalises values to lie in the range [0,1] as it was found
            to speed up the learning process.
        """
        state = self.state
        norm_state = [state[0]/self.max_distance,
                      state[1]/self.max_speed, state[2],
                      state[3]/self.max_distance,
                      state[4]/self.max_speed, state[5]]
        return np.array(norm_state)

    def random_init(self, leading_rider=None, policy_random_seed=None,
                    policy_file_path=None):
        """Return initialisations state for a random sprint.
            The trailing/leading rider is set randomly here, as is the policy
            for rider B (dependent on starting position).
            Parameters:
            leading_rider - Either 'Rider A' or 'Rider B', the rider to take the lead.
            policy_random_seed - Randomised point to be used within Rider B's
                                policy.
            policy_file_path - Path to a specific trained agent to be used for policy.
            Returns:
            rider_a_init - Initial state for rider A.
            rider_b_init - Initial state for rider B.
        """
        # Pick one rider to lead/follow.
        riders = ['Rider A', 'Rider B']
        if leading_rider is not None:
            if leading_rider not in riders:
                raise ValueError('Unknown leading rider.')
        else:
            leading_rider = riders[np.random.randint(2)]
        self.leading_rider_now = leading_rider

        # Initialise state
        if leading_rider == 'Rider A':
            rider_a_init = [1, 1, 1]
            rider_b_init = [0, 0, 1]
        else:
            rider_a_init = [0, 0, 1]
            rider_b_init = [1, 1, 1]

        # Randomise rider B's policy
        if policy_random_seed is None and policy_file_path is None:
            self.rider_b_policy = self.agent_opponents[np.random.randint(len(self.agent_opponents))]
        elif policy_random_seed is not None:
            if policy_random_seed > len(self.agent_opponents) - 1 or \
                policy_random_seed < 0:
                raise ValueError('Policy random seed out of range.')
            self.rider_b_policy = self.agent_opponents[policy_random_seed]
        else:
            try:
                self.rider_b_policy = MsrNetPolicy(policy_file_path, rider='Rider B',
                                                    max_distance=self.max_distance,
                                                    max_speed=self.max_speed)
            except:
                raise ValueError('Could not load agent file: ' + policy_file_path)

        return rider_a_init, rider_b_init


class MsrDuelFixed2(MsrSubMaxBase):
    """Implement MSR simulation as Gym.

    VERSION 2 - msr-duel-fixed-v2
    Updates from version 1 include the use of a unified policy for rider B.

    This gym requires version of Lapysim with aero modelling implemented.
    Currently, this uses a single dimension aero model.
    Opponents in this gym are identical, and both follow the sub-max phys model.

    This gym implements full MSR race simulation, where Rider A's power output
    is controlled externally (by an agent) while Rider B's power output is controlled
    following a pre-defined, 'expert' policy.

    Starting positions (i.e. leading/trailing) and Rider B's policy is randomised
    at each reset.

    The Reward function employed is the 'scaled-distance' function.

    Observation:
        All observation spaces are normalised.
        Type: Box(6)
        Num	Observation                 Min         Max
        0	Rider A Distance            0            1
        1	Rider A Velocity            0            1
        2   Rider A Fatigue             0            1
        3	Rider B Distance            0            1
        4	Rider B Velocity            0            1
        5   Rider B Fatigue             0            1

    Actions:
        Type: Discrete(n)
        Num	    Action
        0:4   [0:0.25:1] - Power value in range [0,1] mapping to exertion.

    Parameters
    ----------
    MsrIdenticalBase - The parent class for variable opponent.

    action_dim - Dimension of action space to be used.

    verbose - Bool flag controlling data which is printed to console.

    Public methods:
    ---------------
    __init__(self):
        Initialise MSR gym.
        verbose - Information reporting flag for the environment.

    complete(self):
        Return True if race is finished, False if not.

    step(self, action):
        Step the gym forward by a time step.
        Action is an array [value] where 0 <= value <= 1
        Returns [state, reward, done, additional_info]

    reset(self):
        Reset the gym to its initial state.

    reward(self):
        Returns reward for given state.
            0 - If race is incomplete.
            0.75*(rider_distance/race_distance) - If race complete and rider A loses.
            1 + separation - If race complete and rider A wins.

    normalised_state(self):
        Returns a state value normalised to match observation space.

    random_init(self, leading_rider=None, policy_random_seed=None):
        Returns initialisations state for a random sprint.

    build_evaluators(self):
        Build evaluators needed to calculate simulation properties.

    get_draft_exertion(self, eq_sep=2.5):
        Return the equilibrium exertion required for Rider B to draft at a
           given point behind Rider A.
    """

    metadata = {'render.modes': ['human', 'rgb_array']}

    def __init__(self, action_dim=5, verbose=True, rider_a_fatigue_rate=0.01,
                rider_b_fatigue_rate=0.01):
        """Initialise the MSR gym.
            Parameters:
            action_dim - Dimension of the action space.
                       - Needs to be non-zero integer.
            verbose - Flag for limiting the amount of reporting during learning.
                    - True allows statements to be printed, False does not.
            rider_a_fatigue_rate - Fatigue rate to use with rider A physiology model.
            rider_b_fatigue_rate - Fatigue rate to use with rider B physiology model.
        """

        # Env params
        super(MsrDuelFixed2, self).__init__(rider_a_fatigue_rate=rider_a_fatigue_rate,
                                            rider_b_fatigue_rate=rider_b_fatigue_rate)
        self.verbose = verbose
        self.action_space = spaces.Discrete(action_dim)
        self.viewer = None
        self.time_step = 0.1

        # Observation spaces.
        self.race_distance = 750
        self.max_distance = 760
        self.max_speed = 30
        obs_low  = np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0])
        obs_high = np.array([1.0, 1.0, 1.0, 1.0, 1.0, 1.0])
        self.observation_space = spaces.Box(obs_low, obs_high, dtype=np.float32)

        # Initialise policy & starting point state
        self.rider_b_policy = MsrPolicy(rider='Rider B')
        self.last_controls = [0, 0]
        self.last_eq_exertion = 0
        self.rider_a_init, self.rider_b_init = self.random_init()
        self.state = self.rider_a_init + self.rider_b_init

        # Environment info used externally.
        self.done = False
        self.info = {'repetitions':0, 'total_reward':0}
        self.reward = 0
        self.env_steps = 0
        self.overtakes = 0
        self.leading_rider_now = None

        # Initialise the forward solution.
        # Master dynamics is stored to avoid multiple initialisations - much faster.
        self.match_sprint_simulation = self.match_sprint_event.initialise_forward_simulation()
        self.master_dynamics_lambdas = copy.deepcopy(self.match_sprint_simulation._dyn_eqns_lambdas)
        self.match_sprint_simulation.initialise_rider_states(
            (self.rider_a_init, self.rider_b_init))

        # Build evaluators for drafting exertion
        self.build_evaluators()

    def complete(self):
        """Check if the simulation has completed.
        Return 1 (true) if race is finished.
        Return 0 (false) if race is not finished."""
        if (self.state[0] >= self.race_distance or self.state[3] >= self.race_distance):
            self.done = True
            return True
        else:
            return False

    def step(self, action):
        """Step the gym forward by a time step.
        Action  - An integer value 0:1:action_dim which is converted into control in [0,1]
                - This maps to maximum possible output power in the torque-cadence model.
                - 1 is full power, 0 is no power.
        Returns [state, reward, done, additional_info]"""

        assert self.action_space.contains(action), "%r (%s) invalid"%(action, type(action))
        assert self.rider_b_policy is not None

        if self.complete():
            self.reward = self.reward_function()
            self.info['total_reward'] = self.info['total_reward'] + self.reward

            self.close()
            return self.normalised_state(), self.reward, self.done, self.info

        # Normalise action value in rance [0,1]
        action /= (self.action_space.n - 1)

        # Get rider B's action.
        rider_b_control = self.rider_b_policy.predict(self.state, self.get_draft_exertion())

        # array_state is [ [rider_a_state], [rider_b_state] ]
        # rider_state is [ distance, velocity, fatigue ]
        array_state = self.match_sprint_simulation.update_rider_states(
            timestep=self.time_step,
            athletes_control=([action], [rider_b_control]),
            )

        # State needs to match observation space [a_s, a_v, a_f, b_s, b_v, b_f]
        self.state = array_state[0] + array_state[1]

        # Store rider A's last action
        self.last_controls = [action, rider_b_control]

        # Track overtakes
        if self.state[0] > self.state[3] and self.leading_rider_now == 'Rider B':
            self.overtakes += 1
            self.leading_rider_now = 'Rider A'
        elif self.state[0] < self.state[3] and self.leading_rider_now == 'Rider A':
            self.overtakes += 1
            self.leading_rider_now = 'Rider B'

        # Get reward function
        self.reward = self.reward_function()
        self.info['total_reward'] = self.info['total_reward'] + self.reward
        self.env_steps += 1

        return self.normalised_state(), self.reward, self.done, self.info

    def reset(self, leading_rider=None, policy_random_seed=None, verbose=True,
              rider_b_sprint_distance=None):
        """Reset the gym to its initial state.
            Parameters:
            leading_rider - Rider beginning in the leading position.
                          - If None, this is randomly selected (from Rider A or B).
            policy_rand_point - Specific randomisation point to be used in
                                Rider B's policy.
            verbose - Flag for reporting during reset.
            rider_b_sprint_distance - Distance at which rider B sprints.
                                    - This is used for policy investigations.
        """
        # Current distance and velocity for two riders
        if verbose and self.verbose:
            print("Total reward = " + str(self.info['total_reward']))
            print("Reset #" + str(self.info['repetitions']))
        self.info['repetitions'] = self.info['repetitions'] + 1
        self.done = False
        self.info['total_reward'] = 0
        self.reward = 0
        self.env_steps = 0
        self.overtakes = 0
        self.last_controls = [0, 0]

        # Reset state randomly
        self.rider_a_init, self.rider_b_init = self.random_init(
            leading_rider=leading_rider,
            policy_random_seed=policy_random_seed,
            rider_b_sprint_distance=rider_b_sprint_distance)
        self.state = self.rider_a_init + self.rider_b_init

        # Reset the simulation.
        self.match_sprint_simulation = ForwardDynamics(
            self.match_sprint_event,
            self.master_dynamics_lambdas)
        self.match_sprint_simulation.initialise_rider_states(
            (self.rider_a_init,
            self.rider_b_init))
        return self.normalised_state()

    def reward_function(self):
        """ Return the reward.
            0 - If race has not been finished.
            1 - If rider A wins.
            0.75*distance - If rider B wins.
                          - This is designed to encourage forward movement.
        """
        if not self.complete():
            return 0
        elif self.state[0] >= self.state[3]:
            return 1 + (self.state[0] - self.state[3])
        else:
            return 0.75*(self.state[0] - self.rider_a_init[0]) / \
                    (self.race_distance - self.rider_a_init[0])

    def normalised_state(self):
        """Return a normalised state value.
            This normalises values to lie in the range [0,1] as it was found
            to speed up the learning process.
        """
        state = self.state
        norm_state = [state[0]/self.max_distance,
                      state[1]/self.max_speed, state[2],
                      state[3]/self.max_distance,
                      state[4]/self.max_speed, state[5]]
        return np.array(norm_state)

    def random_init(self, leading_rider=None, policy_random_seed=None,
                    rider_b_sprint_distance=None):
        """Return initialisations state for a random sprint.
            The trailing/leading rider is set randomly here, as is the policy
            for rider B (dependent on starting position).
            Parameters:
            leading_rider - Either 'Rider A' or 'Rider B', the rider to take the lead.
            policy_random_seed - Randomised point to be used within Rider B's
                                policy.
            rider_b_sprint_distance - Sprint distance for rider B.
                                    - Used when evaluating a policy.
            Returns:
            rider_a_init - Initial state for rider A.
            rider_b_init - Initial state for rider B.
            rider_b_policy - Policy function for rider B.
        """
        # Pick one rider to lead/follow.
        riders = ['Rider A', 'Rider B']
        if leading_rider is not None:
            if leading_rider not in riders:
                raise ValueError('Unknown leading rider.')
        else:
            leading_rider = riders[np.random.randint(2)]
        self.leading_rider_now = leading_rider

        # Initialise state
        if leading_rider == 'Rider A':
            rider_a_init = [1, 1, 1]
            rider_b_init = [0, 0, 1]
        else:
            rider_a_init = [0, 0, 1]
            rider_b_init = [1, 1, 1]

        # Randomise rider B's policy
        self.rider_b_policy.reset(random_seed=policy_random_seed,
                                  sprint_distance=rider_b_sprint_distance)

        return rider_a_init, rider_b_init

    def build_evaluators(self):
        """Build evaluators needed to calculate simulation properties."""
        # Acceleration of rider A.
        self._eval_rider_a_accel = self.match_sprint_event.build_evaluator( ( \
            self.rider_a.bike_fit._F_drive_sym - \
            self.rider_a.aerodynamics._F_drag_sym - \
            self.rider_a.rolling_resistance._F_rr_sym) / \
            self.rider_a.anthropometry._mass_sym )
        # Acceleration of rider B.
        self._eval_rider_b_accel = self.match_sprint_event.build_evaluator( ( \
            self.rider_b.bike_fit._F_drive_sym - \
            self.rider_b.aerodynamics._F_drag_sym - \
            self.rider_b.rolling_resistance._F_rr_sym) / \
            self.rider_b.anthropometry._mass_sym )

        # Mass of rider A, used as a multiplier.
        self._eval_rider_a_mass = self.match_sprint_event.build_evaluator( \
            self.rider_a.anthropometry._mass_sym )
        # Mass of rider B, used as a multiplier.
        self._eval_rider_b_mass = self.match_sprint_event.build_evaluator( \
            self.rider_b.anthropometry._mass_sym )

        # Rider A rolling drag / mass rider A.
        self._eval_rider_a_norm_rr = self.match_sprint_event.build_evaluator( \
            self.rider_a.rolling_resistance._F_rr_sym / \
            self.rider_a.anthropometry._mass_sym )
        # Rider B rolling drag / mass rider B.
        self._eval_rider_b_norm_rr = self.match_sprint_event.build_evaluator( \
            self.rider_b.rolling_resistance._F_rr_sym / \
            self.rider_b.anthropometry._mass_sym )

        # Rider A aero drag.
        self._eval_rider_a_aero_drag = self.match_sprint_event.build_evaluator( \
            self.rider_a.aerodynamics._F_drag_sym )
        # Rider B aero drag.
        self._eval_rider_b_aero_drag = self.match_sprint_event.build_evaluator( \
            self.rider_b.aerodynamics._F_drag_sym )

        # Rider A multiplier to get crank torque from drive force.
        self._eval_rider_a_crank_torque = self.match_sprint_event.build_evaluator( \
            ( self.rider_a.bike_fit._G_ratio_sym * self.rider_a.bike.rear_wheel._diameter_sym ) / \
            ( 2 * self.rider_a.bike._chain_eff_sym ) )
        # Rider B multiplier to get crank torque from drive force.
        self._eval_rider_b_crank_torque = self.match_sprint_event.build_evaluator( \
            ( self.rider_b.bike_fit._G_ratio_sym * self.rider_b.bike.rear_wheel._diameter_sym ) / \
            ( 2 * self.rider_b.bike._chain_eff_sym ) )

        # Rider A divisor for exertion from crank torque
        self._eval_rider_a_cad_max = self.match_sprint_event.build_evaluator( \
            self.rider_a.physiology._cad_max_sym )
        self._eval_rider_a_cad_at_draft = self.match_sprint_event.build_evaluator( \
            self.rider_b.dynamics._cad_sym )
        self._eval_rider_a_T_max = self.match_sprint_event.build_evaluator( \
            self.rider_a.physiology._T_max_sym )
        self._eval_rider_a_T_crit = self.match_sprint_event.build_evaluator( \
            self.rider_a.physiology._T_crit_sym )
        self._eval_rider_a_fatigue = self.match_sprint_event.build_evaluator( \
            self.rider_a.physiology._fatigue_now_sym )

        # Rider B divisor for exertion from crank torque
        self._eval_rider_b_cad_max = self.match_sprint_event.build_evaluator( \
            self.rider_b.physiology._cad_max_sym )
        self._eval_rider_b_cad_at_draft = self.match_sprint_event.build_evaluator( \
            self.rider_a.dynamics._cad_sym )
        self._eval_rider_b_T_max = self.match_sprint_event.build_evaluator( \
            self.rider_b.physiology._T_max_sym )
        self._eval_rider_b_T_crit = self.match_sprint_event.build_evaluator( \
            self.rider_b.physiology._T_crit_sym )
        self._eval_rider_b_fatigue = self.match_sprint_event.build_evaluator( \
            self.rider_b.physiology._fatigue_now_sym )

    def get_draft_exertion(self, eq_sep=2.5, drafting_rider='Rider B'):
        """Return the equilibrium exertion required for Rider B to draft at a
           given point behind Rider A.
            Parameters:
            eq_sep - The point about which the equilibrium is calculated.
            drafting_rider - Define which rider we are controlling.
            Returns:
            exertion - Exertion value required to draft at this point.
        """
        # Form eval args from states & controls
        eval_args = []
        eval_args.extend(self.state)
        eval_args.extend(self.last_controls)

        # Calculate drag factors
        drag_factor_a = 1 - 0.25*np.tanh(-2 * eq_sep) + 0.25*np.tanh(-0.2 * eq_sep - 2)
        drag_factor_b = 1 - 0.25*np.tanh(2 * eq_sep) + 0.25*np.tanh(0.2 * eq_sep - 2)

        if drafting_rider == 'Rider B':
            # Calculate for rider B drafting behind rider A
            # Rider B required drive force at given separation.
            rider_a_accel = self._eval_rider_a_accel(*eval_args)
            rider_b_mass = self._eval_rider_b_mass(*eval_args)
            rider_a_norm_rr = self._eval_rider_a_norm_rr(*eval_args)
            rider_b_rr = rider_a_norm_rr * rider_b_mass
            rider_a_aero_drag = self._eval_rider_a_aero_drag(*eval_args)
            rider_b_aero_drag = drag_factor_b * rider_a_aero_drag / drag_factor_a
            rider_b_f_drive = rider_a_accel*rider_b_mass + rider_b_aero_drag + rider_b_rr

            # Convert drive force to crank torque
            rider_b_T_crank = rider_b_f_drive * self._eval_rider_b_crank_torque(*eval_args)

            # Convert T crank to exertion
            rider_b_T_crit = self._eval_rider_b_T_crit(*eval_args)
            rider_b_T_max = self._eval_rider_b_T_max(*eval_args)
            cad = self._eval_rider_b_cad_at_draft(*eval_args)
            cad_max = self._eval_rider_b_cad_max(*eval_args)
            fatigue = self._eval_rider_b_fatigue(*eval_args)
            divisor = rider_b_T_crit * (1 - cad/cad_max) + fatigue  * \
                (rider_b_T_max - rider_b_T_crit) * (1 + cad/cad_max)
            eq_exertion = rider_b_T_crank / divisor
        else:
            # Calculate for rider A drafting behind rider B.
            # Rider B required drive force at given separation.
            rider_b_accel = self._eval_rider_b_accel(*eval_args)
            rider_a_mass = self._eval_rider_a_mass(*eval_args)
            rider_b_norm_rr = self._eval_rider_b_norm_rr(*eval_args)
            rider_a_rr = rider_b_norm_rr * rider_a_mass
            rider_b_aero_drag = self._eval_rider_b_aero_drag(*eval_args)
            rider_a_aero_drag = drag_factor_a * rider_b_aero_drag / drag_factor_b
            rider_a_f_drive = rider_b_accel*rider_a_mass + rider_a_aero_drag + rider_a_rr

            # Convert drive force to crank torque
            rider_a_T_crank = rider_a_f_drive * self._eval_rider_a_crank_torque(*eval_args)

            # Convert T crank to exertion
            rider_a_T_crit = self._eval_rider_a_T_crit(*eval_args)
            rider_a_T_max = self._eval_rider_a_T_max(*eval_args)
            cad = self._eval_rider_a_cad_at_draft(*eval_args)
            cad_max = self._eval_rider_a_cad_max(*eval_args)
            fatigue = self._eval_rider_a_fatigue(*eval_args)
            divisor = rider_a_T_crit * (1 - cad/cad_max) + fatigue  * \
                (rider_a_T_max - rider_a_T_crit) * (1 + cad/cad_max)
            eq_exertion = rider_a_T_crank / divisor

        return eq_exertion



class MsrDuelFixed1(MsrSubMaxBase):
    """Implement MSR simulation as Gym.

    VERSION 1 - msr-duel-fixed-v1

    This gym requires version of Lapysim with aero modelling implemented.
    Currently, this uses a single dimension aero model.
    Opponents in this gym are identical, and both follow the sub-max phys model.

    This gym implements full MSR race simulation, where Rider A's power output
    is controlled externally (by an agent) while Rider B's power output is controlled
    following a defined policy. For this environment, separate policies are used for
    the trailing and leading rider conditions.

    Starting positions (i.e. leading/trailing) and Rider B's policy is randomised
    at each reset.

    The Reward function employed is the 'scaled-distance' function employed previously.

    Observation:
        All observation spaces are normalised.
        Type: Box(6)
        Num	Observation                 Min         Max
        0	Rider A Distance            0            1
        1	Rider A Velocity            0            1
        2   Rider A Fatigue             0            1
        3	Rider B Distance            0            1
        4	Rider B Velocity            0            1
        5   Rider B Fatigue             0            1

    Actions:
        Type: Discrete(n)
        Num	    Action
        0:4   [0:0.33:1] - Power value in range [0,1] mapping to exertion.

    Parameters
    ----------
    MsrIdenticalBase - The parent class for variable opponent.

    action_dim - Dimension of action space to be used.

    verbose - Bool flag controlling data which is printed to console.

    Public methods:
    ---------------
    __init__(self):
        Initialise MSR gym.
        verbose - Information reporting flag for the environment.

    complete(self):
        Return True if race is finished, False if not.

    step(self, action):
        Step the gym forward by a time step.
        Action is an array [value] where 0 <= value <= 1
        Returns [state, reward, done, additional_info]

    reset(self):
        Reset the gym to its initial state.

    reward(self):
        Returns reward for given state.
            0 - If race is incomplete.
            0.75*(rider_distance/race_distance) - If race complete and rider A loses.
            1 + separation - If race complete and rider A wins.

    normalised_state(self):
        Returns a state value normalised to match observation space.

    random_init(self, leading_rider=None, policy=None, policy_random_seed=None):
        Returns initialisations state for a random sprint.

    policy_test_points(self):
        Helper function for evaluation on the environment.

    build_evaluators(self):
        Build evaluators needed to calculate simulation properties.

    get_draft_exertion(self, eq_sep=2.5):
        Return the equilibrium exertion required for Rider B to draft at a
           given point behind Rider A.

    """

    metadata = {'render.modes': ['human', 'rgb_array']}

    def __init__(self, action_dim=5, verbose=True):
        """Initialise the MSR gym.
            Parameters:
            action_dim - Dimension of the action space.
                       - Needs to be non-zero integer.
            verbose - Flag for limiting the amount of reporting during learning.
                    - True allows statements to be printed, False does not.
        """

        # Env params
        super(MsrDuelFixed1, self).__init__()
        self.verbose = verbose
        self.action_space = spaces.Discrete(action_dim)
        self.viewer = None
        self.time_step = 0.1

        # Observation spaces.
        self.race_distance = 750
        self.max_distance = 760
        self.max_speed = 30
        obs_low  = np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0])
        obs_high = np.array([1.0, 1.0, 1.0, 1.0, 1.0, 1.0])
        self.observation_space = spaces.Box(obs_low, obs_high, dtype=np.float32)

        # Initialise policy & starting point state
        self.rider_b_policies = {'trail':[TrailDraft()],
                                 'lead':[LeadJockey()]}
        self.last_controls = [0, 0]
        self.last_eq_exertion = 0
        self.rider_a_init, self.rider_b_init, self.rider_b_policy = self.random_init()
        self.state = self.rider_a_init + self.rider_b_init

        # Environment info used externally.
        self.done = False
        self.info = {'repetitions':0, 'total_reward':0}
        self.reward = 0
        self.env_steps = 0

        # Initialise the forward solution.
        # Master dynamics is stored to avoid multiple initialisations - much faster.
        self.match_sprint_simulation = self.match_sprint_event.initialise_forward_simulation()
        self.master_dynamics_lambdas = copy.deepcopy(self.match_sprint_simulation._dyn_eqns_lambdas)
        self.match_sprint_simulation.initialise_rider_states(
            (self.rider_a_init, self.rider_b_init))

        # Build evaluators for drafting exertion
        self.build_evaluators()

    def complete(self):
        """Check if the simulation has completed.
        Return 1 (true) if race is finished.
        Return 0 (false) if race is not finished."""
        if (self.state[0] >= self.race_distance or self.state[3] >= self.race_distance):
            self.done = True
            return True
        else:
            return False

    def step(self, action):
        """Step the gym forward by a time step.
        Action  - An integer value 0:1:5 which is converted into 0:0.2:1
                - This maps to maximum possible output power in the torque-cadence model.
                - 1 is full power, 0 is no power.
                - If no action is provided, the simulation will step with previous action.
        Returns [state, reward, done, additional_info]"""

        assert self.action_space.contains(action), "%r (%s) invalid"%(action, type(action))
        assert self.rider_b_policy is not None

        if self.complete():
            self.reward = self.reward_function()
            self.info['total_reward'] = self.info['total_reward'] + self.reward

            self.close()
            return self.normalised_state(), self.reward, self.done, self.info

        # Normalise action value in rance [0,1]
        action /= (self.action_space.n - 1)

        # Get rider B action
        rider_b_control = self.rider_b_policy.predict(self.state, self.get_draft_exertion())

        # array_state is [ [rider_a_state], [rider_b_state] ]
        # rider_state is [ distance, velocity, fatigue ]
        array_state = self.match_sprint_simulation.update_rider_states(
            timestep=self.time_step,
            athletes_control=([action], [rider_b_control]),
            )

        # State needs to match observation space [a_s, a_v, a_f, b_s, b_v, b_f]
        self.state = array_state[0] + array_state[1]

        # Store rider A's last action
        self.last_controls = [action, rider_b_control]

        # Get reward function
        self.reward = self.reward_function()
        self.info['total_reward'] = self.info['total_reward'] + self.reward
        self.env_steps += 1

        return self.normalised_state(), self.reward, self.done, self.info

    def reset(self, leading_rider=None, policy=None, policy_random_seed=None, verbose=True):
        """Reset the gym to its initial state.
            Parameters:
            leading_rider - Rider beginning in the leading position.
                          - If None, this is randomly selected (from Rider A or B).
            policy - Specific policy to be used for Rider B.
            policy_rand_point - Specific randomisation point to be used in
                                Rider B's policy.
            verbose - Flag for reporting during reset.
            """
        # Current distance and velocity for two riders
        if verbose and self.verbose:
            print("Total reward = " + str(self.info['total_reward']))
            print("Reset #" + str(self.info['repetitions']))
        self.info['repetitions'] = self.info['repetitions'] + 1
        self.done = False
        self.info['total_reward'] = 0
        self.reward = 0
        self.env_steps = 0
        self.last_controls = [0, 0]

        # Reset state randomly
        self.rider_a_init, self.rider_b_init, self.rider_b_policy = self.random_init(
            leading_rider=leading_rider,
            policy=policy,
            policy_random_seed=policy_random_seed)
        self.state = self.rider_a_init + self.rider_b_init

        # Reset the simulation.
        self.match_sprint_simulation = ForwardDynamics(
            self.match_sprint_event,
            self.master_dynamics_lambdas)
        self.match_sprint_simulation.initialise_rider_states(
            (self.rider_a_init,
            self.rider_b_init))
        return self.normalised_state()

    def reward_function(self):
        """ Return the reward.
            0 - If race has not been finished.
            1 - If rider A wins.
            0.75*distance - If rider B wins.
                          - This is designed to encourage forward movement.
        """
        if not self.complete():
            return 0
        elif self.state[0] >= self.state[3]:
            return 1 + (self.state[0] - self.state[3])
        else:
            return 0.75*(self.state[0] - self.rider_a_init[0]) / \
                    (self.race_distance - self.rider_a_init[0])

    def normalised_state(self):
        """Return a normalised state value.
            This normalises values to lie in the range [0,1] as it was found
            to speed up the learning process.
        """
        state = self.state
        norm_state = [state[0]/self.max_distance,
                      state[1]/self.max_speed, state[2],
                      state[3]/self.max_distance,
                      state[4]/self.max_speed, state[5]]
        return np.array(norm_state)

    def random_init(self, leading_rider=None, policy=None, policy_random_seed=None):
        """Returns initialisations state for a random sprint.
            The trailing/leading rider is set randomly here, as is the policy
            for rider B (dependent on starting position).
            Parameters:
            leading_rider - Either 'Rider A' or 'Rider B', the rider to take the lead.
            policy - Integer referencing specific policy to be used for rider B.
            policy_random_seed - Randomised point to be used within Rider B's
                                policy.
            Returns:
            rider_a_init - Initial state for rider A.
            rider_b_init - Initial state for rider B.
            rider_b_policy - Policy function for rider B.
        """
        # Pick one rider to lead/follow.
        riders = ['Rider A', 'Rider B']
        n_trail_policies = len(self.rider_b_policies['trail'])
        n_lead_policies = len(self.rider_b_policies['lead'])
        if leading_rider is not None:
            if leading_rider not in riders:
                raise ValueError('Unknown leading rider.')
        else:
            leading_rider = riders[np.random.randint(2)]

        # Initialise state & policy based on leading rider.
        if leading_rider == 'Rider A':
            rider_a_init = [1, 1, 1]
            rider_b_init = [0, 0, 1]
            if policy is None:
                rider_b_policy = self.rider_b_policies['trail'][np.random.randint(n_trail_policies)]
            elif policy >= 0 and policy < len(self.rider_b_policies['trail']):
                rider_b_policy = self.rider_b_policies['trail'][policy]
            else:
                raise ValueError('Invalid policy requirement for Rider A leading.')
        else:
            rider_a_init = [0, 0, 1]
            rider_b_init = [1, 1, 1]
            if policy is None:
                rider_b_policy = self.rider_b_policies['lead'][np.random.randint(n_lead_policies)]
            elif policy >=0 and policy < len(self.rider_b_policies['lead']):
                rider_b_policy = self.rider_b_policies['lead'][policy]
            else:
                raise ValueError('Invalid policy requirement for Rider B leading.')

        # Randomise rider B's policy
        rider_b_policy.randomise(random_seed=policy_random_seed)

        return rider_a_init, rider_b_init, rider_b_policy

    def policy_test_points(self):
        """Helper function for evaluation on the environment.
            This function returns a list of policy test parameters for the preset
            rider B policies.
            These are defined from the perspective of rider A - i.e. leading means
            rider A will lead out the race.
            Returns:
            rider_a_lead_tests  - [(policy_int, policy_seed)] for all trailing test
                                    policies and seeds.
            rider_a_trail_tests - [(policy_int, policy_seed)] for all leading test
                                    policies and seeds.
        """
        # Rider A leading means rider B trailing.
        rider_a_lead_tests = []
        for policy_int, policy in enumerate(self.rider_b_policies['trail']):
            rider_a_lead_tests += [(policy_int, seed) for seed in range(len(policy.test_points))]

        # Rider A trailing means rider B leading.
        rider_a_trail_tests = []
        for policy_int, policy in enumerate(self.rider_b_policies['lead']):
            rider_a_trail_tests += [(policy_int, seed) for seed in range(len(policy.test_points))]
        return rider_a_lead_tests, rider_a_trail_tests

    def build_evaluators(self):
        """Build evaluators needed to calculate simulation properties."""
        # Acceleration of rider A.
        self._eval_rider_a_accel = self.match_sprint_event.build_evaluator( ( \
            self.rider_a.bike_fit._F_drive_sym - \
            self.rider_a.aerodynamics._F_drag_sym - \
            self.rider_a.rolling_resistance._F_rr_sym) / \
            self.rider_a.anthropometry._mass_sym )
        # Mass of rider B, used as a multiplier.
        self._eval_rider_b_mass = self.match_sprint_event.build_evaluator( \
            self.rider_b.anthropometry._mass_sym )
        # Rider A rolling drag / mass rider A.
        self._eval_rider_a_norm_rr = self.match_sprint_event.build_evaluator( \
            self.rider_a.rolling_resistance._F_rr_sym / \
            self.rider_a.anthropometry._mass_sym )
        # Rider A aero drag.
        self._eval_rider_a_aero_drag = self.match_sprint_event.build_evaluator( \
            self.rider_a.aerodynamics._F_drag_sym )
        # Rider B multiplier to get crank torque from drive force.
        self._eval_rider_b_crank_torque = self.match_sprint_event.build_evaluator( \
            ( self.rider_b.bike_fit._G_ratio_sym * self.rider_b.bike.rear_wheel._diameter_sym ) / \
            ( 2 * self.rider_b.bike._chain_eff_sym ) )
        # Rider be divisor for exertion from crank torque
        self._eval_rider_b_cad_max = self.match_sprint_event.build_evaluator( \
            self.rider_b.physiology._cad_max_sym )
        self._eval_rider_b_cad_at_draft = self.match_sprint_event.build_evaluator( \
            self.rider_a.dynamics._cad_sym )
        self._eval_rider_b_T_max = self.match_sprint_event.build_evaluator( \
            self.rider_b.physiology._T_max_sym )
        self._eval_rider_b_T_crit = self.match_sprint_event.build_evaluator( \
            self.rider_b.physiology._T_crit_sym )
        self._eval_rider_b_fatigue = self.match_sprint_event.build_evaluator( \
            self.rider_b.physiology._fatigue_now_sym )

    def get_draft_exertion(self, eq_sep=2.5):
        """Return the equilibrium exertion required for Rider B to draft at a
           given point behind Rider A.
            Parameters:
            eq_sep - The point about which the equilibrium is calculated.
            Returns:
            exertion - Exertion value required to draft at this point.
        """
        # Form eval args from states & controls
        eval_args = []
        eval_args.extend(self.state)
        eval_args.extend(self.last_controls)

        # Calculate drag factors
        drag_factor_a = 1 - 0.25*np.tanh(-2 * eq_sep) + 0.25*np.tanh(-0.2 * eq_sep - 2)
        drag_factor_b = 1 - 0.25*np.tanh(2 * eq_sep) + 0.25*np.tanh(0.2 * eq_sep - 2)

        # Build up calls from evaluator results.
        # Rider B required drive force at given separation.
        rider_a_accel = self._eval_rider_a_accel(*eval_args)
        rider_b_mass = self._eval_rider_b_mass(*eval_args)
        rider_a_norm_rr = self._eval_rider_a_norm_rr(*eval_args)
        rider_b_rr = rider_a_norm_rr * rider_b_mass
        rider_a_aero_drag = self._eval_rider_a_aero_drag(*eval_args)
        rider_b_aero_drag = drag_factor_b * rider_a_aero_drag / drag_factor_a
        rider_b_f_drive = rider_a_accel*rider_b_mass + rider_b_aero_drag + rider_b_rr

        # Convert drive force to crank torque
        rider_b_T_crank = rider_b_f_drive * self._eval_rider_b_crank_torque(*eval_args)

        # Convert T crank to exertion
        rider_b_T_crit = self._eval_rider_b_T_crit(*eval_args)
        rider_b_T_max = self._eval_rider_b_T_max(*eval_args)
        cad = self._eval_rider_b_cad_at_draft(*eval_args)
        cad_max = self._eval_rider_b_cad_max(*eval_args)
        fatigue = self._eval_rider_b_fatigue(*eval_args)
        divisor = rider_b_T_crit * (1 - cad/cad_max) + fatigue  * \
            (rider_b_T_max - rider_b_T_crit) * (1 + cad/cad_max)
        eq_exertion = rider_b_T_crank / divisor

        return eq_exertion