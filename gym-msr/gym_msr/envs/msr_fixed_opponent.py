"""Match Sprint Race Gym Environment
Creates a MSR simulation in which the riders follow the black line.
The user is able to control the power output of rider A, while rider B follows a constant
power output.
All opponents in this module follow fixed physiology models, not torque cadence.
The agent controlled rider follows the torque cadence model.

author: Rob Sumner
Classes
-------
MsrFixedDraft_1 - Fixed opponent working at 100 W, reward for drafting within 5m.

MsrFixedDraft_2 - Fixed opponent working at random power in range [50,150].
                - Power is randomised at each reset.
                - Pulse reward for being within 5m.

MsrFixedDraft_3 - Fixed opponent working at 100 W.
                - Reward is a pulse shape peaking at -1.5 m separation, and 0 at -3,0.

MsrFixedWin_1   - Reward function for both drafting and winning.
                - Gym was never really used successfully.

"""

# Gym based imports.
import gym
from gym import error, spaces, utils
from gym.utils import seeding
from gym.envs.classic_control import rendering
from PIL import Image
import pickle
from datetime import datetime

# Lapysim imports.
import numpy as np
import sympy as sym
import sympy.physics.mechanics as me
import lapysim

# Base Gym
from gym_msr.envs.msr_fixed_opponent_base import MsrFixedBase


class MsrFixedDraft_1(MsrFixedBase):
    """Implement MSR simulation as Gym.
    Drafing using a step reward function within 3 meters.
    Opponent power is fixed at 100 watts.

    Observation:
        Type: Box(4)
        Num	Observation                 Min         Max
        0	Rider A Distance            0            800
        1	Rider A Velocity            0            50
        2	Rider B Distance            0            800
        3	Rider B Velocity            0            50

    Actions:
        Type: Discrete(6)
        Num	    Action
        0-10    Num/10 - Power value in range [0,1] for Torque-Cadence model.

    Parameters
    ----------
    MsrFixedBase    - Parent class for fixed opponent.

    Public methods:
    ---------------
    __init__(self):
        Initialise MSR gym.

    complete(self):
        Return True if race is finished, False if not.

    step(self, action):
        Step the gym forward by a time step.
        Action is an array [value] where 0 <= value <= 1
        Returns [state, reward, done, additional_info]

    reset(self):
        Reset the gym to its initial state.

    """

    metadata = {'render.modes': ['human', 'rgb_array']}

    def __init__(self):
        """Initialise the MSR gym."""

        # Initialise parent
        super(MsrFixedDraft_1, self).__init__()

        # Action and observation spaces.
        self.race_distance = 250
        self.action_space = spaces.Discrete(11)
        obs_low = np.array([0, 0, 0, 0])
        obs_high = np.array([self.race_distance*1.1, 50.0, self.race_distance*1.1, 50.0])
        self.observation_space = spaces.Box(obs_low, obs_high, dtype=np.float32)

        # Current distance and velocity for two rider
        self.rider_a_init = [0, 0]
        self.rider_b_init = [15, 0]
        self.state = self.rider_a_init + self.rider_b_init
        self.done = False
        self.info = {'repetitions':0, 'total_reward':0}
        self.reward = 0
        self.viewer = None

        # Create rider B.
        self.rider_b_control = [100]

        # Initialise the forward solution.
        self.match_sprint_simulation = self.match_sprint_event.initialise_forward_simulation()
        self.match_sprint_simulation.initialise_rider_states(
            (self.rider_a_init, self.rider_b_init))

    def complete(self):
        """Check if the simulation has completed.
        Return 1 (true) if race is finished.
        Return 0 (false) if race is not finished."""

        if (self.state[0] >= self.race_distance or self.state[2] >= self.race_distance):
            self.done = True
            return True
        else:
            return False

    def step(self, action):
        """Step the gym forward by a time step.
        Action  - An integer value 0:1:5 which is converted into 0:0.2:1
                - This maps to maximum possible output power in the torque-cadence model.
                - 1 is full power, 0 is no power.
                - If no action is provided, the simulation will step with previous action.
        Returns [state, reward, done, additional_info]"""

        assert self.action_space.contains(action), "%r (%s) invalid"%(action, type(action))

        if self.complete():
            self.close()
            return np.array(self.state), self.reward, self.done, self.info

        action /= (self.action_space.n - 1)

        # [ [rider_a_state], [rider_b_state]] & state = [distance, velocity]
        array_state = self.match_sprint_simulation.update_rider_states(
            timestep=0.1,
            athletes_control=([action], self.rider_b_control),)

        # State needs to match observation space [a_s, a_v, b_s, b_v]
        self.state = array_state[0] + array_state[1]

        # Reward for drafting
        gap = (self.state[0] - self.state[2])
        if (gap < 0 and gap > -5):
            self.reward = 1
            self.info['total_reward'] = self.info['total_reward']+ 1
        else:
            self.reward = 0

        return np.array(self.state), self.reward, self.done, self.info

    def reset(self):
        """Reset the gym to its initial state."""

        # Current distance and velocity for two riders
        print("Reset #" + str(self.info['repetitions']))
        self.info['repetitions'] = self.info['repetitions'] + 1
        self.state = self.rider_a_init + self.rider_b_init
        self.done = False
        self.info['total_reward'] = 0
        self.reward = 0

        # Reset the simulation.
        self.match_sprint_simulation = self.match_sprint_event.initialise_forward_simulation()
        self.match_sprint_simulation.initialise_rider_states(
            (self.rider_a_init, self.rider_b_init))
        return np.array(self.state)


class MsrFixedDraft_2(MsrFixedBase):
    """Implement MSR simulation as Gym.
    Drafing using a step reward function within 5 meters
    and a (fixed)random opponent power in the range [50:20:150].
    Power is resampled at every reset.

    Observation:
        Type: Box(4)
        Num	Observation                 Min         Max
        0	Rider A Distance            0            800
        1	Rider A Velocity            0            50
        2	Rider B Distance            0            800
        3	Rider B Velocity            0            50

    Actions:
        Type: Discrete(6)
        Num	    Action
        0-10    Num/10 - Power value in range [0,1] for Torque-Cadence model.

    Parameters
    ----------
    MsrFixedBase    - Parent class for fixed opponent.

    Public methods:
    ---------------
    __init__(self):
        Initialise MSR gym.

    complete(self):
        Return True if race is finished, False if not.

    step(self, action):
        Step the gym forward by a time step.
        Action is an array [value] where 0 <= value <= 1
        Returns [state, reward, done, additional_info]

    reset(self):
        Reset the gym to its initial state.

    """

    metadata = {'render.modes': ['human', 'rgb_array']}

    def __init__(self):
        """Initialise the MSR gym."""

        # Initialise parent
        super(MsrFixedDraft_2, self).__init__()

        # Action and observation spaces.
        self.race_distance = 250
        self.action_space = spaces.Discrete(11)
        obs_low = np.array([0, 0, 0, 0])
        obs_high = np.array([self.race_distance*1.1, 50.0, self.race_distance*1.1, 50.0])
        self.observation_space = spaces.Box(obs_low, obs_high, dtype=np.float32)

        # Current distance and velocity for two rider
        self.rider_a_init = [0, 0]
        self.rider_b_init = [15, 0]
        self.state = self.rider_a_init + self.rider_b_init
        self.done = False
        self.info = {'repetitions':0, 'total_reward':0}
        self.reward = 0
        self.viewer = None

        # Create rider B.
        self.rider_b_control = [100]

        # Initialise the forward solution.
        self.match_sprint_simulation = self.match_sprint_event.initialise_forward_simulation()
        self.match_sprint_simulation.initialise_rider_states(
            (self.rider_a_init, self.rider_b_init))

    def complete(self):
        """Check if the simulation has completed.
        Return 1 (true) if race is finished.
        Return 0 (false) if race is not finished."""

        if (self.state[0] >= self.race_distance or self.state[2] >= self.race_distance):
            self.done = True
            return True
        else:
            return False

    def step(self, action):
        """Step the gym forward by a time step.
        Action  - An integer value 0:1:5 which is converted into 0:0.2:1
                - This maps to maximum possible output power in the torque-cadence model.
                - 1 is full power, 0 is no power.
                - If no action is provided, the simulation will step with previous action.
        Returns [state, reward, done, additional_info]"""

        assert self.action_space.contains(action), "%r (%s) invalid"%(action, type(action))

        if self.complete():
            self.close()
            return np.array(self.state), self.reward, self.done, self.info

        action /= (self.action_space.n - 1)

        # [ [rider_a_state], [rider_b_state]] & state = [distance, velocity]
        array_state = self.match_sprint_simulation.update_rider_states(
            timestep=0.1,
            athletes_control=([action], self.rider_b_control),)

        # State needs to match observation space [a_s, a_v, b_s, b_v]
        self.state = array_state[0] + array_state[1]

        # Reward for drafting
        gap = (self.state[0] - self.state[2])
        if (gap < 0 and gap > -5):
            # print('Drafting')
            self.reward = 1
            self.info['total_reward'] = self.info['total_reward']+ 1
        else:
            self.reward = 0

        return np.array(self.state), self.reward, self.done, self.info

    def reset(self):
        """Reset the gym to its initial state."""
        # Randomise the opponent power.
        self.rider_b_control = [np.random.randint(0,5)*20 + 50]

        print("Reset #" + str(self.info['repetitions']))
        print("New rider power = " + str(self.rider_b_control))

        # Current distance and velocity for two riders
        self.info['repetitions'] = self.info['repetitions'] + 1
        self.state = self.rider_a_init + self.rider_b_init
        self.done = False
        self.info['total_reward'] = 0
        self.reward = 0

        # Reset the simulation.
        self.match_sprint_simulation = self.match_sprint_event.initialise_forward_simulation()
        self.match_sprint_simulation.initialise_rider_states(
            (self.rider_a_init, self.rider_b_init))
        return np.array(self.state)


class MsrFixedDraft_3(MsrFixedBase):
    """Implement MSR simulation as Gym.
    Drafing using a triangular pulse reward function over 5 m.
    Power is constant at 100 w.

    Observation:
        Type: Box(4)
        Num	Observation                 Min         Max
        0	Rider A Distance            0            800
        1	Rider A Velocity            0            50
        2	Rider B Distance            0            800
        3	Rider B Velocity            0            50

    Actions:
        Type: Discrete(6)
        Num	    Action
        0-10    Num/10 - Power value in range [0,1] for Torque-Cadence model.

    Parameters
    ----------
    MsrFixedBase    - Parent class for fixed opponent.

    Public methods:
    ---------------
    __init__(self):
        Initialise MSR gym.

    complete(self):
        Return True if race is finished, False if not.

    step(self, action):
        Step the gym forward by a time step.
        Action is an array [value] where 0 <= value <= 1
        Returns [state, reward, done, additional_info]

    reset(self):
        Reset the gym to its initial state.

    reward_function(self:
        Return the step reward for current state.
        Reward function is more complicated for this function so this is used.

    """

    metadata = {'render.modes': ['human', 'rgb_array']}

    def __init__(self):
        """Initialise the MSR gym."""

        # Initialise parent
        super(MsrFixedDraft_3, self).__init__()

        # Action and observation spaces.
        self.race_distance = 250
        self.action_space = spaces.Discrete(11)
        obs_low = np.array([0, 0, 0, 0])
        obs_high = np.array([self.race_distance*1.1, 50.0, self.race_distance*1.1, 50.0])
        self.observation_space = spaces.Box(obs_low, obs_high, dtype=np.float32)

        # Current distance and velocity for two rider
        self.rider_a_init = [0, 0]
        self.rider_b_init = [15, 0]
        self.state = self.rider_a_init + self.rider_b_init
        self.done = False
        self.info = {'repetitions':0, 'total_reward':0}
        self.reward = 0
        self.viewer = None

        # Create rider B.
        self.rider_b_control = [100]

        # Initialise the forward solution.
        self.match_sprint_simulation = self.match_sprint_event.initialise_forward_simulation()
        self.match_sprint_simulation.initialise_rider_states(
            (self.rider_a_init, self.rider_b_init))

        # Reward data
        self.sep_width = 3
        self.draft_reward = 3

    def complete(self):
        """Check if the simulation has completed.
        Return 1 (true) if race is finished.
        Return 0 (false) if race is not finished."""

        if (self.state[0] >= self.race_distance or self.state[2] >= self.race_distance):
            self.done = True
            return True
        else:
            return False

    def step(self, action):
        """Step the gym forward by a time step.
        Action  - An integer value 0:1:5 which is converted into 0:0.2:1
                - This maps to maximum possible output power in the torque-cadence model.
                - 1 is full power, 0 is no power.
                - If no action is provided, the simulation will step with previous action.
        Returns [state, reward, done, additional_info]"""

        assert self.action_space.contains(action), "%r (%s) invalid"%(action, type(action))

        if self.complete():
            self.close()
            return np.array(self.state), self.reward, self.done, self.info

        action /= (self.action_space.n - 1)

        # [ [rider_a_state], [rider_b_state]] & state = [distance, velocity]
        array_state = self.match_sprint_simulation.update_rider_states(
            timestep=0.1,
            athletes_control=([action], self.rider_b_control),)

        # State needs to match observation space [a_s, a_v, b_s, b_v]
        self.state = array_state[0] + array_state[1]

        # Reward for drafting
        self.reward = self.reward_function()
        self.info['total_reward'] = self.info['total_reward'] + self.reward

        return np.array(self.state), self.reward, self.done, self.info

    def reset(self):
        """Reset the gym to its initial state."""
        # Current distance and velocity for two riders
        print("Total reward = " + str(self.info['total_reward']))
        print("Reset #" + str(self.info['repetitions']))
        self.info['repetitions'] = self.info['repetitions'] + 1
        self.state = self.rider_a_init + self.rider_b_init
        self.done = False
        self.info['total_reward'] = 0
        self.reward = 0

        # Reset the simulation.
        self.match_sprint_simulation = self.match_sprint_event.initialise_forward_simulation()
        self.match_sprint_simulation.initialise_rider_states(
            (self.rider_a_init, self.rider_b_init))
        return np.array(self.state)

    def reward_function(self):
        """Return the reward for a given rider state."""
        separation = self.state[0] - self.state[2]

        # Reward for drafting that fits a triangular pulse function.
        if (separation > -self.sep_width and separation < 0):
            if (separation >= -self.sep_width*0.5):
                return (-2*self.draft_reward/self.sep_width)*separation
            elif (separation < -self.sep_width*0.5):
                return (2*self.draft_reward/self.sep_width)*separation +2*self.draft_reward
            else:
                return 0
        else:
            return 0


class MsrFixedWin(MsrFixedBase):
    """Implement MSR simulation as Gym.

    Observation:
        Type: Box(4)
        Num	Observation                 Min         Max
        0	Rider A Distance            0            800
        1	Rider A Velocity            0            50
        2	Rider B Distance            0            800
        3	Rider B Velocity            0            50

    Actions:
        Type: Discrete(6)
        Num	Action
        0	Power = 0
        1	Power = 0.2
        2   Power = 0.4
        3   Power = 0.6
        4   Power = 0.8
        5   Power = 1.0

    Parameters
    ----------
    gym.Env - The environment module.
            - Create this gym using gym.make('msr-v0')

    Public methods:
    ---------------
    __init__(self):
        Initialise MSR gym.

    passed_syntax(self):
        Return True if no syntax errors have been found.

    complete(self):
        Return True if race is finished, False if not.

    step(self, action):
        Step the gym forward by a time step.
        Action is an array [value] where 0 <= value <= 1
        Returns [state, reward, done, additional_info]

    reset(self):
        Reset the gym to its initial state.

    """

    metadata = {'render.modes': ['human', 'rgb_array']}

    def __init__(self):
        """Initialise the MSR gym."""

        super(MsrFixedWin, self).__init__()

        # Action and observation spaces.
        self.race_distance = 250
        self.action_space = spaces.Discrete(11)
        obs_low = np.array([0, 0, 0, 0])
        obs_high = np.array([self.race_distance*1.1, 50.0,
                            self.race_distance*1.1, 50.0])
        self.observation_space = spaces.Box(
            obs_low, obs_high, dtype=np.float32)

        # Current distance and velocity for two rider
        self.rider_a_init = [0, 0]
        self.rider_b_init = [15, 0]
        self.state = self.rider_a_init + self.rider_b_init
        self.done = False
        self.info = {'repetitions':0, 'total_reward':0}
        self.reward = 0
        self.viewer = None

        # Create rider B.
        self.rider_b_control = [100]

        # Initialise the forward solution.
        self.match_sprint_simulation = self.match_sprint_event.initialise_forward_simulation()
        self.match_sprint_simulation.initialise_rider_states(
            (self.rider_a_init, self.rider_b_init))

    def complete(self):
        """Check if the simulation has completed.
        Return 1 (true) if race is finished.
        Return 0 (false) if race is not finished."""

        if (self.state[0] >= self.race_distance or self.state[2] >= self.race_distance):
            self.done = True
            return True
        else:
            return False

    def step(self, action):
        """Step the gym forward by a time step.
        Action  - An integer value 0:1:5 which is converted into 0:0.2:1
                - This maps to maximum possible output power in the torque-cadence model.
                - 1 is full power, 0 is no power.
                - If no action is provided, the simulation will step with previous action.
        Returns [state, reward, done, additional_info]"""

        assert self.action_space.contains(
            action), "%r (%s) invalid" % (action, type(action))

        if self.complete():
            self.close()
            return np.array(self.state), self.reward, self.done, self.info

        action /= (self.action_space.n - 1)

        # [ [rider_a_state], [rider_b_state]] & state = [distance, velocity]
        array_state = self.match_sprint_simulation.update_rider_states(
            timestep=0.1,
            athletes_control=([action], self.rider_b_control),)

        # State needs to match observation space [a_s, a_v, b_s, b_v]
        self.state = array_state[0] + array_state[1]

        # Combined reward for drafting and win
        self.reward = self.reward_function()
        self.info['total_reward'] = self.info['total_reward'] + self.reward

        return np.array(self.state), self.reward, self.done, self.info

    def reset(self):
        """Reset the gym to its initial state."""

        # Randomise the opponent power.
        self.rider_b_control = np.random.rand(1)*100 + 50

        # Current distance and velocity for two riders
        print("Reset #" + str(self.info['repetitions']))
        self.info['repetitions'] = self.info['repetitions'] + 1
        self.state = self.rider_a_init + self.rider_b_init
        self.done = False
        self.info['total_reward'] = 0
        self.reward = 0

        # Reset the simulation.
        self.match_sprint_simulation = self.match_sprint_event.initialise_forward_simulation()
        self.match_sprint_simulation.initialise_rider_states(
            (self.rider_a_init, self.rider_b_init))
        return np.array(self.state)

    def reward_function(self):
        """Return the reward for a given rider state."""
        separation = self.state[2] - self.state[0]
        sep_width = 5
        draft_reward = 1
        win_reward = 2*self.info['total_reward']

        # Reward for drafting if less than 200 meters
        # Return final reward if you win.
        if (self.state[0] < 200 and separation > -sep_width and separation < 0):
            if (separation >= -sep_width*0.5):
                return (-2*draft_reward/sep_width)*separation
            elif (separation < -sep_width*0.5):
                return (2*draft_reward/sep_width)*separation +2*draft_reward
            else:
                return 0
        elif (self.complete() and separation > 0):
            return win_reward
        else:
            return 0
