"""Match Sprint Race Gym Environment
Base class for MSR gym with identical opponents following the sub-maximal
physiology model.

author: Rob Sumner
Classes
-------
MsrSubMaxBase - The gym class for the MSR simulation with two identical
                   opponents, both using a variable physiology model.
"""

# Gym based imports.
import gym
import os
from gym import error, spaces, utils
from gym.utils import seeding
import pickle
from datetime import datetime
from gym_msr.envs.msr_base import MsrBase

# Lapysim imports.
import numpy as np
import sympy as sym
import sympy.physics.mechanics as me
import lapysim

class MsrSubMaxBase(MsrBase):
    """Implement MSR simulation as Gym - create base class for all simulations
        using identical opponents.

    Parameters
    ----------
    MsrBase - Base parent class for the MSR gym.

    Public methods:
    ---------------
    __init__():
        Initialise the rider physiology models and bikes for use in the simulation.
        Also initialises the simulation for further work.
    """

    metadata = {'render.modes': ['human', 'rgb_array']}

    def __init__(self, rider_a_fatigue_rate=0.01, rider_b_fatigue_rate=0.01):
        # Initialise parent
        super(MsrSubMaxBase, self).__init__()
        self.rider_a_fatigue_rate = rider_a_fatigue_rate
        self.rider_b_fatigue_rate = rider_b_fatigue_rate
        self.setup_sim()

    def setup_sim(self):
        """Set up the simulation parameters and initialise sim.
            Simulation uses two riders with torque-cadence model.
            The physiology models are identical in this case"""
        # Bike for both riders.
        vittoria_cl_gold_160_psi = lapysim.Tyre.from_library(
            'vittoria cl gold')
        mavic_ellipse_700_cc = lapysim.Wheel.from_library(
            'mavic ellipse', tyre=vittoria_cl_gold_160_psi)
        cervelo_T5GB_size_M = lapysim.Bike.from_library('cervelo t5gb',
                                                        frame_size='M',
                                                        front_wheel=mavic_ellipse_700_cc,
                                                        rear_wheel=mavic_ellipse_700_cc)

        # Create rider A
        rider_a_anthropometry = lapysim.Anthropometry(mass=80, drag_area=0.26,
                                                      height=1.82)
        rider_a_physiology = lapysim.VariableFatigueMaximalSubmaximalModel(peak_power=1000,
                                                        max_torque=260,
                                                        max_cadence=250,
                                                        max_fatigue_rate=self.rider_a_fatigue_rate)
        rider_a_bike_fit = lapysim.BikeFit(saddle_height=1.1, gear_ratio=3.8)
        self.rider_a = lapysim.Athlete(
            name='Rider A',
            athlete_id='rdrA',
            bike=cervelo_T5GB_size_M,
            anthropometry=rider_a_anthropometry,
            physiology=rider_a_physiology,
            bike_fit=rider_a_bike_fit,
        )

        # Create rider B.
        rider_b_anthropometry = lapysim.Anthropometry(mass=80, drag_area=0.26,
                                                      height=1.82)
        rider_b_physiology = lapysim.VariableFatigueMaximalSubmaximalModel(peak_power=1000,
                                                        max_torque=260,
                                                        max_cadence=250,
                                                        max_fatigue_rate=self.rider_b_fatigue_rate)
        rider_b_bike_fit = lapysim.BikeFit(saddle_height=1.1, gear_ratio=3.8)
        self.rider_b = lapysim.Athlete(
            name='Rider B',
            athlete_id='rdrB',
            bike=cervelo_T5GB_size_M,
            anthropometry=rider_b_anthropometry,
            physiology=rider_b_physiology,
            bike_fit=rider_b_bike_fit,
        )

        # Compile the event.
        self.match_sprint_event = lapysim.Event(
            venue=self.match_sprint_venue,
            athletes=(self.rider_a, self.rider_b),
            rules=self.match_sprint_rules,
            settings=self.match_sprint_settings
        )

        # Simulation is initialised in child classes.
        self.match_sprint_simulation = None

    def save_history(self, folder=None, name=None):
        """Save the race history for experience replay."""
        # Build history structure
        times = self.match_sprint_simulation._simulation_histories['time']
        rider_a_dict = self.match_sprint_simulation._simulation_histories[self.rider_a]
        rider_a_dict['fatigue'] = np.array(rider_a_dict['state'])[:,2]
        rider_b_dict = self.match_sprint_simulation._simulation_histories[self.rider_b]
        rider_b_dict['fatigue'] = np.array(rider_b_dict['state'])[:,2]
        data = [times, rider_a_dict, rider_b_dict]

        # Build file name
        if name is None:
            now = datetime.now()
            dt_string = now.strftime("%d_%m_%Y_%H_%M_%S")
            filename = "msr_" + dt_string + ".hist"
        else:
            filename = name + ".hist"
        # Save in folder
        if folder is not None:
            if not os.path.exists(folder):
                raise ValueError("Provided folder does not exist.")
            if folder[-1] != '/':
                folder += '/'
            filename = folder + filename
        filehandler = open(filename, 'wb')
        pickle.dump(data, filehandler)
