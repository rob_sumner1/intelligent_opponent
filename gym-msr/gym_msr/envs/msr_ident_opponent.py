"""Match Sprint Race Gym Environment
Creates a MSR simulation in which the riders follow the black line.
The user is able to control the power output of rider A, while rider B has an
identical physiology model but may follow different control strategies.

author: Rob Sumner
Classes
-------
MsrIdenticalWin_1   - Gym for two identical opponents following torque-cadence model.
                    - Race distance is 250m (ie one lap).
                    - Reward is fully delayed until the end of the race.
                        - +1 reward for winning race.
                        - 0.75*elapsed distance for losing.
MsrIdenticalWin_2   - Gym for two identical opponents following torque-cadence model.
                    - Race distance is increased to 750m (full race).
                    - Reward delayed to end of race.
                        - 0 if race is incomplete.
                        - +1 reward given for winning race.
                        - 0.75*elapsed distance for losing.
MsrIdenticalWin_3   - Gym for two identical opponents following torque-cadence model.
                    - Random sprint race distance in range [100, 500]
                    - Riders initially moving at 3 m/s and 1m apart (rider A behind).
                    - Reward delayed to end of race.
                        - Varying reward function used, see reward function.
"""

# Gym based imports.
import gym
from gym import error, spaces, utils
from gym.utils import seeding
from gym.envs.classic_control import rendering
from PIL import Image
import pickle
from datetime import datetime

# Lapysim imports.
import numpy as np
import sympy as sym
import sympy.physics.mechanics as me
import lapysim

# Base Gym
from gym_msr.envs.msr_ident_opponent_base import MsrIdenticalBase

class MsrIdenticalWin_1(MsrIdenticalBase):
    """Implement MSR simulation as Gym.
    This gym requires version of Lapysim with aero modelling implemented.
    Currently, this is used as a single dimension aero model.
    Opponents in this gym are identical, and both follow the torque-cadence model.
    Race distance is set at 250 meters.

    Observation:
        Type: Box(6)
        Num	Observation                 Min         Max
        0	Rider A Distance            0            1000
        1	Rider A Velocity            0            50
        2   Rider A Fatigue             0            1
        3	Rider B Distance            0            1000
        4	Rider B Velocity            0            50
        5   Rider B Fatigue             0            1

    Actions:
        Type: Discrete(11)
        Num	    Action
        0-10    Num/10 - Power value in range [0,1] for Torque-Cadence model.

    Parameters
    ----------
    MsrIdenticalBase - The parent class for variable opponent.

    Public methods:
    ---------------
    __init__(self):
        Initialise MSR gym.

    complete(self):
        Return True if race is finished, False if not.

    step(self, action):
        Step the gym forward by a time step.
        Action is an array [value] where 0 <= value <= 1
        Returns [state, reward, done, additional_info]

    reset(self):
        Reset the gym to its initial state.

    reward(self):
        Returns reward for given state.
            0 - If race is incomplete.
            0.75*(rider_distance/race_distance) - If race complete and rider A loses.
            1 + separation - If race complete and rider A wins.

    fatigue(self):
        Returns a fatigue measure based upon the torque cadence model.


    """

    metadata = {'render.modes': ['human', 'rgb_array']}

    def __init__(self):
        """Initialise the MSR gym."""

        # Initialise parent
        super(MsrIdenticalWin_1, self).__init__()

        # Action and observation spaces.
        self.race_distance = 250
        self.action_space = spaces.Discrete(11)
        obs_low = np.array([0, 0, 0, 0, 0, 0])
        obs_high = np.array([1000.0, 50.0, 1.0, 1000.0, 50.0, 1.0])
        self.observation_space = spaces.Box(obs_low, obs_high, dtype=np.float32)

        # Initial state: [distance, velocityl, fatigue]
        self.rider_a_init = [0, 0, 1.0]
        self.rider_b_init = [1, 0, 1.0]
        self.state = self.rider_a_init + self.rider_b_init
        self.done = False
        self.info = {'repetitions':0, 'total_reward':0}
        self.reward = 0
        self.viewer = None
        self.time_step = 0.1

        # Rider B moves at max power.
        self.rider_b_control = [1]

        # Initialise the forward solution.
        self.match_sprint_simulation = self.match_sprint_event.initialise_forward_simulation()
        self.match_sprint_simulation.initialise_rider_states(
            (self.rider_a_init[0:2], self.rider_b_init[0:2]))

    def complete(self):
        """Check if the simulation has completed.
        Return 1 (true) if race is finished.
        Return 0 (false) if race is not finished."""
        if (self.state[0] >= self.race_distance or self.state[3] >= self.race_distance):
            self.done = True
            return True
        else:
            return False

    def step(self, action):
        """Step the gym forward by a time step.
        Action  - An integer value 0:1:5 which is converted into 0:0.2:1
                - This maps to maximum possible output power in the torque-cadence model.
                - 1 is full power, 0 is no power.
                - If no action is provided, the simulation will step with previous action.
        Returns [state, reward, done, additional_info]"""

        assert self.action_space.contains(action), "%r (%s) invalid"%(action, type(action))

        if self.complete():
            self.reward = self.reward_function()
            self.info['total_reward'] = self.info['total_reward'] + self.reward

            self.close()
            return np.array(self.state), self.reward, self.done, self.info

        action /= (self.action_space.n - 1)

        # [ [rider_a_state], [rider_b_state]] & state = [distance, velocity]
        array_state = self.match_sprint_simulation.update_rider_states(
            timestep=self.time_step,
            athletes_control=([action], self.rider_b_control),
            )

        # State needs to match observation space [a_s, a_v, b_s, b_v]
        fatigue_a, fatigue_b = self.fatigue()
        self.fatigue_data.append([fatigue_a, fatigue_b])
        self.state = array_state[0] + [fatigue_a] + array_state[1] + [fatigue_b]

        # Reward for drafting
        self.reward = self.reward_function()
        self.info['total_reward'] = self.info['total_reward'] + self.reward

        return np.array(self.state), self.reward, self.done, self.info

    def reset(self):
        """Reset the gym to its initial state."""
        # Current distance and velocity for two riders
        print("Total reward = " + str(self.info['total_reward']))
        print("Reset #" + str(self.info['repetitions']))
        self.info['repetitions'] = self.info['repetitions'] + 1
        self.state = self.rider_a_init + self.rider_b_init
        self.done = False
        self.info['total_reward'] = 0
        self.reward = 0
        self.fatigue_data = [[1,1]]

        # Reset the simulation.
        self.match_sprint_simulation = self.match_sprint_event.initialise_forward_simulation()
        self.match_sprint_simulation.initialise_rider_states(
            (self.rider_a_init[0:2], self.rider_b_init[0:2]))
        return np.array(self.state)

    def reward_function(self):
        """ Return the reward.
            0 - If race has not been finished.
            1 - If rider A wins.
            0.75*distance - If rider B wins.
                          - This is designed to encourage forward movement.
        """
        if not self.complete():
            return 0
        elif self.state[0] >= self.state[3]:
            return 1
        else:
            return 0.75*self.state[0] / self.race_distance

    def fatigue(self):
        """Return a fatigue measure for both athletes.
        Fatigue measure follows the torque cadence model.
        The measure will lie in the range [0,1]
            - 0 is fully fatigued.
            - 1 is no fatigue.
        Each athlete has a fatigue decay value alpha
        And number of pedal revolutions n:
            Fatigue measure = alpha^n
        """
        diam = 0.7
        n = np.array([self.state[0] - self.rider_a_init[0],
             self.state[3] - self.rider_b_init[0] ]) / (np.pi * diam)
        rider_a_fatigue = (1 - self.rider_a_fatigue_rate) ** n[0]
        rider_b_fatigue = (1 - self.rider_b_fatigue_rate) ** n[1]
        return rider_a_fatigue, rider_b_fatigue


# Gym used for experiments on normalisation and run method.
class MsrIdenticalWin_2(MsrIdenticalBase):
    """Implement MSR simulation as Gym.
    This gym requires version of Lapysim with aero modelling implemented.
    Currently, this is used as a single dimension aero model.
    Opponents in this gym are identical, and both follow the torque-cadence model.

    This model uses full race distance of 750 meters with a reward function
    that encourages winning. Desired behaviour is an agent which sprints and wins.
    The enrivonment is deterministic from the perspective of rider A, which is
    relatively simplistic.

    This gym was mainly used for gym tuning, such as investigating normalisation
    and training methods.

    Observation:
        All observation spaces are normalised.
        Type: Box(6)
        Num	Observation                 Min         Max
        0	Rider A Distance            0            1
        1	Rider A Velocity            0            1
        2   Rider A Fatigue             0            1
        3	Rider B Distance            0            1
        4	Rider B Velocity            0            1
        5   Rider B Fatigue             0            1

    Actions:
        Type: Discrete(2)
        Num	    Action
        0-1     0 or 1 - Power value of 0 or 1 for Torque-Cadence model.

    Parameters
    ----------
    MsrIdenticalBase - The parent class for variable opponent.

    Public methods:
    ---------------
    __init__(self):
        Initialise MSR gym.

    complete(self):
        Return True if race is finished, False if not.

    step(self, action):
        Step the gym forward by a time step.
        Action is an array [value] where 0 <= value <= 1
        Returns [state, reward, done, additional_info]

    reset(self):
        Reset the gym to its initial state.

    reward(self):
        Returns reward for given state.
            0 - If race is incomplete.
            0.75*(rider_distance/race_distance) - If race complete and rider A loses.
            1 + separation - If race complete and rider A wins.

    fatigue(self):
        Returns a fatigue measure based upon the torque cadence model.

    normalised_state(self):
        Returns a state value normalised to match observation space.

    """

    metadata = {'render.modes': ['human', 'rgb_array']}

    def __init__(self):
        """Initialise the MSR gym."""

        # Initialise parent
        super(MsrIdenticalWin_2, self).__init__()

        # Action and observation spaces.
        self.race_distance = 750
        self.action_space = spaces.Discrete(2)
        self.max_distance = 760
        self.max_speed = 30
        obs_low  = np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0])
        obs_high = np.array([1.0, 1.0, 1.0, 1.0, 1.0, 1.0])
        # obs_high = np.array([800.0, 50.0, 1.0, 800.0, 50.0, 1.0])
        self.observation_space = spaces.Box(obs_low, obs_high, dtype=np.float32)

        # Initial state: [distance, velocityl, fatigue]
        self.rider_a_init = [0, 0, 1.0]
        self.rider_b_init = [1, 0, 1.0]
        self.state = self.rider_a_init + self.rider_b_init
        self.done = False
        self.info = {'repetitions':0, 'total_reward':0}
        self.reward = 0
        self.viewer = None
        self.time_step = 0.3

        # Rider B moves at max power.
        self.rider_b_control = [1]

        # Initialise the forward solution.
        self.match_sprint_simulation = self.match_sprint_event.initialise_forward_simulation()
        self.match_sprint_simulation.initialise_rider_states(
            (self.rider_a_init[0:2], self.rider_b_init[0:2]))

    def complete(self):
        """Check if the simulation has completed.
        Return 1 (true) if race is finished.
        Return 0 (false) if race is not finished."""
        if (self.state[0] >= self.race_distance or self.state[3] >= self.race_distance):
            self.done = True
            return True
        else:
            return False

    def step(self, action):
        """Step the gym forward by a time step.
        Action  - An integer value 0:1:5 which is converted into 0:0.2:1
                - This maps to maximum possible output power in the torque-cadence model.
                - 1 is full power, 0 is no power.
                - If no action is provided, the simulation will step with previous action.
        Returns [state, reward, done, additional_info]"""

        assert self.action_space.contains(action), "%r (%s) invalid"%(action, type(action))

        if self.complete():
            self.reward = self.reward_function()
            self.info['total_reward'] = self.info['total_reward'] + self.reward

            self.close()
            return self.normalised_state(), self.reward, self.done, self.info

        # Normalise action value in rance [0,1]
        action /= (self.action_space.n - 1)

        # [ [rider_a_state], [rider_b_state]] & state = [distance, velocity]
        array_state = self.match_sprint_simulation.update_rider_states(
            timestep=self.time_step,
            athletes_control=([action], self.rider_b_control),
            )

        # State needs to match observation space [a_s, a_v, b_s, b_v]
        fatigue_a, fatigue_b = self.fatigue()
        self.fatigue_data.append([fatigue_a, fatigue_b])
        self.state = array_state[0] + [fatigue_a] + array_state[1] + [fatigue_b]

        # Reward for drafting
        self.reward = self.reward_function()
        self.info['total_reward'] = self.info['total_reward'] + self.reward

        # # Print out states and actions for debugging
        # print("Action value         = ", action)
        # print("New state            = ", self.state)
        # print("New normalised state = ", self.normalised_state())

        return self.normalised_state(), self.reward, self.done, self.info

    def reset(self):
        """Reset the gym to its initial state."""
        # Current distance and velocity for two riders
        print("Total reward = " + str(self.info['total_reward']))
        print("Reset #" + str(self.info['repetitions']))
        self.info['repetitions'] = self.info['repetitions'] + 1
        self.state = self.rider_a_init + self.rider_b_init
        self.done = False
        self.info['total_reward'] = 0
        self.reward = 0
        self.fatigue_data = [[1,1]]

        # Reset the simulation.
        self.match_sprint_simulation = self.match_sprint_event.initialise_forward_simulation()
        self.match_sprint_simulation.initialise_rider_states(
            (self.rider_a_init[0:2], self.rider_b_init[0:2]))
        return self.normalised_state()

    def reward_function(self):
        """ Return the reward.
            0 - If race has not been finished.
            1 - If rider A wins.
            0.75*distance - If rider B wins.
                          - This is designed to encourage forward movement.
        """
        if not self.complete():
            return 0
        elif self.state[0] >= self.state[3]:
            return 1 + (self.state[0] - self.state[3])
        else:
            return 0.75*self.state[0] / self.race_distance

    def fatigue(self):
        """Return a fatigue measure for both athletes.
        Fatigue measure follows the torque cadence model.
        The measure will lie in the range [0,1]
            - 0 is fully fatigued.
            - 1 is no fatigue.
        Each athlete has a fatigue decay value alpha
        And number of pedal revolutions n:
            Fatigue measure = alpha^n
        """
        diam = 0.7
        n = np.array([self.state[0] - self.rider_a_init[0],
             self.state[3] - self.rider_b_init[0] ]) / (np.pi * diam)
        rider_a_fatigue = (1 - self.rider_a_fatigue_rate) ** n[0]
        rider_b_fatigue = (1 - self.rider_b_fatigue_rate) ** n[1]
        return rider_a_fatigue, rider_b_fatigue

    def normalised_state(self):
        """Return a normalised state value."""
        state = self.state
        norm_state = [state[0]/self.max_distance,
                      state[1]/self.max_speed, state[2],
                      state[3]/self.max_distance,
                      state[4]/self.max_speed, state[5]]
        return np.array(norm_state)


class MsrIdenticalWin_3(MsrIdenticalBase):
    """Implement MSR simulation as Gym.
    This gym requires version of Lapysim with aero modelling implemented.
    Currently, this uses a single dimension aero model.
    Opponents in this gym are identical, and both follow the torque-cadence model.

    This gym randomises the sprint distance at each reset to lie in the range
    [250, 650]. This is achieved by setting the starting distance to match this value.
    Riders are spawned at 1 m apart, both travelling at 3 m/s.

    The Reward function encourages riders to win, and the desired behaviour
    is a rider which can sprint and win across a range of sprint distances.

    Observation:
        All observation spaces are normalised.
        Type: Box(6)
        Num	Observation                 Min         Max
        0	Rider A Distance            0            1
        1	Rider A Velocity            0            1
        2   Rider A Fatigue             0            1
        3	Rider B Distance            0            1
        4	Rider B Velocity            0            1
        5   Rider B Fatigue             0            1

    Actions:
        Type: Discrete(2)
        Num	    Action
        0-1     0 or 1 - Power value of 0 or 1 for Torque-Cadence model.

    Parameters
    ----------
    MsrIdenticalBase - The parent class for variable opponent.

    Public methods:
    ---------------
    __init__(self):
        Initialise MSR gym.

    complete(self):
        Return True if race is finished, False if not.

    step(self, action):
        Step the gym forward by a time step.
        Action is an array [value] where 0 <= value <= 1
        Returns [state, reward, done, additional_info]

    reset(self):
        Reset the gym to its initial state.

    reward(self):
        Returns reward for given state.
            0 - If race is incomplete.
            0.75*(rider_distance/race_distance) - If race complete and rider A loses.
            1 + separation - If race complete and rider A wins.

    fatigue(self):
        Returns a fatigue measure based upon the torque cadence model.

    normalised_state(self):
        Returns a state value normalised to match observation space.

    """

    metadata = {'render.modes': ['human', 'rgb_array']}

    def __init__(self):
        """Initialise the MSR gym."""

        # Initialise parent
        super(MsrIdenticalWin_3, self).__init__()

        # Action and observation spaces.
        self.race_distance = 750
        self.action_space = spaces.Discrete(11)
        self.max_distance = 760
        self.max_speed = 30
        obs_low  = np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0])
        obs_high = np.array([1.0, 1.0, 1.0, 1.0, 1.0, 1.0])
        # obs_high = np.array([800.0, 50.0, 1.0, 800.0, 50.0, 1.0])
        self.observation_space = spaces.Box(obs_low, obs_high, dtype=np.float32)

        # Initial state: [distance, velocity, fatigue]
        self.rider_a_init, self.rider_b_init = self.random_init()
        self.state = self.rider_a_init + self.rider_b_init
        self.done = False
        self.info = {'repetitions':0, 'total_reward':0}
        self.reward = 0
        self.viewer = None
        self.time_step = 0.3

        # Rider B moves at max power.
        self.rider_b_control = [1]

        # Initialise the forward solution.
        self.match_sprint_simulation = self.match_sprint_event.initialise_forward_simulation()
        self.match_sprint_simulation.initialise_rider_states(
            (self.rider_a_init[0:2], self.rider_b_init[0:2]))

    def complete(self):
        """Check if the simulation has completed.
        Return 1 (true) if race is finished.
        Return 0 (false) if race is not finished."""
        if (self.state[0] >= self.race_distance or self.state[3] >= self.race_distance):
            self.done = True
            return True
        else:
            return False

    def step(self, action):
        """Step the gym forward by a time step.
        Action  - An integer value 0:1:5 which is converted into 0:0.2:1
                - This maps to maximum possible output power in the torque-cadence model.
                - 1 is full power, 0 is no power.
                - If no action is provided, the simulation will step with previous action.
        Returns [state, reward, done, additional_info]"""

        assert self.action_space.contains(action), "%r (%s) invalid"%(action, type(action))

        if self.complete():
            self.reward = self.reward_function()
            self.info['total_reward'] = self.info['total_reward'] + self.reward

            self.close()
            return self.normalised_state(), self.reward, self.done, self.info

        # Normalise action value in rance [0,1]
        action /= (self.action_space.n - 1)

        # [ [rider_a_state], [rider_b_state]] & state = [distance, velocity]
        array_state = self.match_sprint_simulation.update_rider_states(
            timestep=self.time_step,
            athletes_control=([action], self.rider_b_control),
            )

        # State needs to match observation space [a_s, a_v, b_s, b_v]
        fatigue_a, fatigue_b = self.fatigue(array_state[0][0], array_state[1][0])
        self.fatigue_data.append([fatigue_a, fatigue_b])
        self.state = array_state[0] + [fatigue_a] + array_state[1] + [fatigue_b]

        # Reward for drafting
        self.reward = self.reward_function()
        self.info['total_reward'] = self.info['total_reward'] + self.reward

        # # Print out states and actions for debugging
        # print("Action value         = ", action)
        # print("New state            = ", self.state)
        # print("New normalised state = ", self.normalised_state())

        return self.normalised_state(), self.reward, self.done, self.info

    def reset(self, distance=None):
        """Reset the gym to its initial state."""
        # Current distance and velocity for two riders
        print("Total reward = " + str(self.info['total_reward']))
        print("Reset #" + str(self.info['repetitions']))
        self.info['repetitions'] = self.info['repetitions'] + 1
        self.done = False
        self.info['total_reward'] = 0
        self.reward = 0
        self.fatigue_data = [[1,1]]

        # Reset state randomly
        self.rider_a_init, self.rider_b_init = self.random_init(distance=distance)
        self.state = self.rider_a_init + self.rider_b_init

        # Reset the simulation.
        self.match_sprint_simulation = self.match_sprint_event.initialise_forward_simulation()
        self.match_sprint_simulation.initialise_rider_states(
            (self.rider_a_init[0:2], self.rider_b_init[0:2]))
        return self.normalised_state()

    def reward_function(self):
        """ Return the reward.
            0 - If race has not been finished.
            1 - If rider A wins.
            0.75*distance - If rider B wins.
                          - This is designed to encourage forward movement.
        """
        if not self.complete():
            return 0
        elif self.state[0] >= self.state[3]:
            return 1 + (self.state[0] - self.state[3])
        else:
            return 0.75*(self.state[0] - self.rider_a_init[0]) / \
                    (self.race_distance - self.rider_a_init[0])
        #     # return 0.5*(self.state[0] - self.rider_a_init[0]) / \
        #     #         (self.race_distance - self.rider_a_init[0])
        # else:
        #     return self.state[0] - self.state[3]
        # else:
        #     return (self.state[0] - self.state[3])/(self.race_distance - self.rider_a_init[0])

    def fatigue(self, distance_a, distance_b):
            """Return a fatigue measure for both athletes.
            Fatigue measure follows the torque cadence model.
            The measure will lie in the range [0,1]
                - 0 is fully fatigued.
                - 1 is no fatigue.
            Each athlete has a fatigue decay value alpha
            And number of pedal revolutions n:
                Fatigue measure = alpha^n
            """
            diam = 0.7
            n = np.array([distance_a - self.rider_a_init[0],
                          distance_b - self.rider_b_init[0] ]) / (np.pi * diam)
            rider_a_fatigue = (1 - self.rider_a_fatigue_rate) ** n[0]
            rider_b_fatigue = (1 - self.rider_b_fatigue_rate) ** n[1]
            return rider_a_fatigue, rider_b_fatigue

    def normalised_state(self):
        """Return a normalised state value."""
        state = self.state
        norm_state = [state[0]/self.max_distance,
                      state[1]/self.max_speed, state[2],
                      state[3]/self.max_distance,
                      state[4]/self.max_speed, state[5]]
        return np.array(norm_state)

    def random_init(self, distance=None):
        """Returns initialisations state for a random sprint.
            Parameters:
            distance - Provided distance value used.
                     - This is used for agent evalation.
        """
        # Initialise race between [100,500] for sprint of [250,650].
        d_max = 500
        d_min = 100
        if distance is None:
            distance = np.random.rand(1)[0]*(d_max - d_min) + d_min
        else:
            if distance > d_max or distance < d_min:
                raise ValueError("Distance value does not lie in range.")
            print("Using initial distance = ", distance)

        # Return initial state. [distance, velocity, fatigue]
        gap = 1
        velocity = 3
        rider_a_init = [distance, velocity, 1]
        rider_b_init = [distance + gap, velocity, 1]
        return rider_a_init, rider_b_init

