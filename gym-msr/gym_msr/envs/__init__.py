from gym_msr.envs.msr_fixed_opponent import MsrFixedWin
from gym_msr.envs.msr_fixed_opponent import MsrFixedDraft_1, MsrFixedDraft_2, MsrFixedDraft_3
from gym_msr.envs.msr_ident_opponent import MsrIdenticalWin_1, MsrIdenticalWin_2, MsrIdenticalWin_3
from gym_msr.envs.msr_ident_submax import MsrSubMaxSprint
from gym_msr.envs.msr_duelling import MsrDuelFixed1, MsrDuelFixed2, MsrDuelFixed3
from gym_msr.envs.msr_policies import LeadJockey, TrailDraft, MsrPolicy
