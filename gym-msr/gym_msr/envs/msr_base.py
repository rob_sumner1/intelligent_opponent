"""Match Sprint Race Gym Environment
Base class for MSR gym.

author: Rob Sumner
Classes
-------
MsrBase     - The base gym class for the MSR gym.
            - This implements shared functions such as rendering
                which are used by all child gyms.
TextLabel   - Label class for use with rendering.
            - Implements text rendering in Pyglet through gym.render
"""

# Gym based imports.
import gym
from gym import error, spaces, utils
from gym.utils import seeding
from gym.envs.classic_control import rendering
from PIL import Image
import pyglet

# Lapysim imports.
import numpy as np
import sympy as sym
import sympy.physics.mechanics as me
import lapysim
from lapysim.settings import Settings

class MsrBase(gym.Env):
    """Implement the basic functionality of a MSR simulation as a gym.

    Parameters
    ----------
    gym.Env - The environment module.

    Public methods:
    ---------------
    render(self, mode='human'):
        Render the rider simulation results for the current state.

    close(self):
        Close the viewer.

    find_rider_points(self):
        Return (x,y) positions of riders in velodrome coordinate system.

    image_render(self, file_name=None, index=None, file_path=None)
        Render environment as image and save an image.
        file_name - If prodided individually, will be saved as provided ie no added filetype
                  - If index is also suppled, will be saved as file_name_index.png
        close - Bool, if True the render is immediately closed, otherwise viewer is kept open.
        If no file_name supplied, saves in current working folder.

    """

    metadata = {'render.modes': ['human', 'rgb_array']}

    def __init__(self):
        # Initialise parent
        super(MsrBase, self).__init__()

        # Current distance and velocity for two rider
        self.rider_a_init = [0, 0]
        self.rider_b_init = [0, 0]
        self.state = self.rider_a_init + self.rider_b_init
        self.done = False
        self.info = {}
        self.reward = 0
        self.viewer = None

        # Setup the match sprint race.
        self.match_sprint_track = lapysim.Manchester()
        self.match_sprint_conditions = lapysim.Conditions()
        self.match_sprint_venue = lapysim.Venue(course=self.match_sprint_track,
                                           conditions=self.match_sprint_conditions)
        self.match_sprint_rules = lapysim.Rules()
        self.match_sprint_settings = Settings(aerodynamics_model='axial-only',
                                              dynamics_model='follow-line-no-lean',
                                              rolling_resistance_model='fitton-symons')

        # Rendering parameters
        self.x_survey = self.match_sprint_track._track_data_from_file[1]
        self.y_survey = self.match_sprint_track._track_data_from_file[2]
        self.x_range = np.max(self.x_survey) - np.min(self.x_survey)
        self.y_range = np.max(self.y_survey) - np.min(self.y_survey)
        self.x_min = np.min(self.x_survey)
        self.y_min = np.min(self.y_survey)
        self.render_buffer = 0.15

    def render(self, mode='human', close=False):
        """Plot the rider histories in the race."""
        if self.done:
            return None

        # self.match_sprint_simulation.plot_rider_histories()
        # Note that width + height need to be divisible by 2.
        track_screen_width = 2000
        stats_screen_width = 500
        height_scale = int(track_screen_width * self.y_range / self.x_range) // 2
        screen_height = height_scale*2

        # Plot the track
        if self.viewer is None:
            self.viewer = rendering.Viewer(track_screen_width + stats_screen_width, screen_height)

            self.x_scale = track_screen_width*(1-self.render_buffer)/self.x_range
            self.y_scale = screen_height*(1-self.render_buffer)/self.y_range

            self.x_offset = track_screen_width*self.render_buffer*0.5
            self.y_offset = screen_height*self.render_buffer*0.5

            v = []
            for i in range(len(self.x_survey)):
                vx = (self.x_survey[i]- self.x_min)*self.x_scale + self.x_offset
                vy = (self.y_survey[i] - self.y_min)*self.y_scale + self.y_offset
                v.append([vx, vy])

            self.track = rendering.PolyLine(v, close=True)
            self.track.set_color(0, 0, 0)
            self.track.set_linewidth(20)
            self.viewer.add_geom(self.track)

            self.start_line = rendering.Line((v[0][0],v[0][1]-25), (v[0][0],v[0][1]+25))
            self.start_line.set_color(255,0,0)
            self.start_line.linewidth.stroke = 30
            self.viewer.add_geom(self.start_line)

            rider_a_icon = rendering.make_circle(radius=20, res=30, filled=True)
            rider_a_icon.set_color(0,255,0)
            self.rider_a_trans = rendering.Transform()
            rider_a_icon.add_attr(self.rider_a_trans)
            self.viewer.add_geom(rider_a_icon)

            rider_b_icon = rendering.make_circle(radius=20, res=30, filled=True)
            rider_b_icon.set_color(0,0,255)
            self.rider_b_trans = rendering.Transform()
            rider_b_icon.add_attr(self.rider_b_trans)
            self.viewer.add_geom(rider_b_icon)

            rider_a_label = TextLabel(track_screen_width + 150, 0.5*screen_height + 75)
            rider_a_label.set_text("Rider A")
            self.viewer.add_geom(rider_a_label)

            rider_a_legend = rendering.make_circle(radius=20, res=30, filled=True)
            rider_a_legend.set_color(0,255,0)
            self.rider_a_legend = rendering.Transform()
            rider_a_legend.add_attr(self.rider_a_legend)
            self.viewer.add_geom(rider_a_legend)
            self.rider_a_legend.set_translation(track_screen_width + 300, 0.5*screen_height + 80)

            rider_b_label = TextLabel(track_screen_width + 150, 0.5*screen_height + 25)
            rider_b_label.set_text("Rider B")
            self.viewer.add_geom(rider_b_label)

            rider_b_legend = rendering.make_circle(radius=20, res=30, filled=True)
            rider_b_legend.set_color(0,0,255)
            self.rider_b_legend = rendering.Transform()
            rider_b_legend.add_attr(self.rider_b_legend)
            self.viewer.add_geom(rider_b_legend)
            self.rider_b_legend.set_translation(track_screen_width + 300, 0.5*screen_height + 30)

            self.sep_label = TextLabel(track_screen_width + 175, 0.5*screen_height - 25)
            rider_dist = self.rider_distances()
            self.sep_label.set_text("Separation = " + str(round((rider_dist[0] - rider_dist[1])*10)/10))
            self.viewer.add_geom(self.sep_label)

            self.reward_label = TextLabel(track_screen_width + 175, 0.5*screen_height - 75)
            self.reward_label.set_text("Total Reward = " + str(round(self.info["total_reward"]*100)/100))
            self.viewer.add_geom(self.reward_label)

        # Set rider positions
        rider_a_pos, rider_b_pos = self.find_rider_points()

        x_a = (rider_a_pos[0] - self.x_min)*self.x_scale + self.x_offset
        y_a = (rider_a_pos[1] - self.y_min)*self.y_scale + self.y_offset
        self.rider_a_trans.set_translation(x_a, y_a)

        x_b = (rider_b_pos[0] - self.x_min)*self.x_scale + self.x_offset
        y_b = (rider_b_pos[1] - self.y_min)*self.y_scale + self.y_offset
        self.rider_b_trans.set_translation(x_b, y_b)

        # Set label texts
        self.reward_label.set_text("Total Reward = " + str(round(self.info["total_reward"]*100)/100))
        rider_dist = self.rider_distances()
        self.sep_label.set_text("Separation = " + str(round((rider_dist[0] - rider_dist[1])*10)/10))

        return self.viewer.render(return_rgb_array=(mode == 'rgb_array'))

    def close(self):
        """Close the viewer."""
        if self.viewer:
            self.viewer.close()
            self.viewer = None

    def find_rider_points(self):
        """Return (x,y) positions of riders in velodrome coordinate system."""
        distance_covered = 0
        rider_a_position = [self.x_survey[0], self.y_survey[0]]
        rider_b_position = [self.x_survey[0], self.y_survey[0]]
        i = 1

        while (distance_covered <= np.max(self.rider_distances())):
            x_distance = (self.x_survey[i] - self.x_survey[i-1])
            y_distance = (self.y_survey[i] - self.y_survey[i-1])
            distance_covered +=  np.sqrt(x_distance**2 + y_distance**2)

            [rider_a_distance, rider_b_distance] = self.rider_distances()
            if (distance_covered <= rider_a_distance):
                rider_a_position = [self.x_survey[i], self.y_survey[i]]

            if (distance_covered <= rider_b_distance):
                rider_b_position = [self.x_survey[i], self.y_survey[i]]

            i += 1
            i = i % len(self.x_survey)

        return rider_a_position, rider_b_position

    def rider_distances(self):
        """Return the rider distances based upon the state definition.
           State will always be length 2n (equal for both riders)
           Rider state will always be [distance, ...]
           This function finds the appropriate rider b distance.
        """
        n_states = len(self.state)
        dist_a = self.state[0]
        dist_b = self.state[n_states // 2]
        return [dist_a, dist_b]

    def image_render(self, file_name=None, index=None, close=True):
        """Render environment as image and save an image.
        file_name - If prodided individually, will be saved as provided ie no added filetype
                  - If index is also suppled, will be saved as file_name_index.png
        close - Bool, if True the render is immediately closed, otherwise viewer is kept open.
        If no file_name supplied, saves in current working folder."""
        image_array = self.render(mode='rgb_array')

        if image_array is None:
            return

        im = Image.fromarray(image_array)

        if (file_name is None):
            im.save("env_render.png")
        elif (file_name is not None and index is None):
            im.save(file_name + '.png')
        else:
            im.save(file_name + '_' + str('{0:05}'.format(index)) + '.png')

        # Close testing function.
        if close:
            self.close()


class TextLabel(rendering.Geom):
    """Class to plot a text label in a gym rendering.

    This class follows the structure used in the Gym rendering module.
    """
    def __init__(self, x, y):
        rendering.Geom.__init__(self)
        self.x = x
        self.y = y
        self.label = None
    def render1(self):
        if self.label is not None:
            self.label.draw()
    def set_text(self, text):
        self.label = pyglet.text.Label(text,
                          font_size=40,
                          x=self.x, y=self.y,
                          anchor_x='center', anchor_y='center',
                          color=(0, 0, 0, 255))