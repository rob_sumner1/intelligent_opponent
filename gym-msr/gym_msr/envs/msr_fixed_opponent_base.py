"""Match Sprint Race Gym Environment
Base class for MSR gym with fixed opponent.

author: Rob Sumner
Classes
-------
MsrFixedBase - The gym class for the MSR simulation with fixed opponent.
"""

# Gym based imports.
import gym
from gym import error, spaces, utils
from gym.utils import seeding
import pickle
from datetime import datetime
from gym_msr.envs.msr_base import MsrBase

# Lapysim imports.
import numpy as np
import sympy as sym
import sympy.physics.mechanics as me
import lapysim

class MsrFixedBase(MsrBase):
    """Implement MSR simulation as Gym - create base class for common methods.

    Parameters
    ----------
    gym.Env - The environment module.

    Public methods:
    ---------------
    __init__():
        Initialise the rider physiology models and bikes for use in the simulation.
        Also initialises the simulation for further work.
    """

    metadata = {'render.modes': ['human', 'rgb_array']}

    def __init__(self):
        # Initialise parent
        super(MsrFixedBase, self).__init__()

        self.setup_sim()

    def setup_sim(self):
        """Set up the simulation parameters and initialise sim."""
        # Bike for both riders.
        vittoria_cl_gold_160_psi = lapysim.Tyre.from_library(
            'vittoria cl gold')
        mavic_ellipse_700_cc = lapysim.Wheel.from_library(
            'mavic ellipse', tyre=vittoria_cl_gold_160_psi)
        cervelo_T5GB_size_M = lapysim.Bike.from_library('cervelo t5gb',
                                                        frame_size='M',
                                                        front_wheel=mavic_ellipse_700_cc,
                                                        rear_wheel=mavic_ellipse_700_cc)

        # Create rider A
        rider_a_anthropometry = lapysim.Anthropometry(mass=80, drag_area=0.26,
                                                      height=1.82)
        rider_a_physiology = lapysim.TorqueCadenceModel(peak_power_seated=1000,
                                                        peak_power_standing=1500,
                                                        max_torque_seated=260,
                                                        max_torque_standing=300,
                                                        max_cadence_seated=250,
                                                        max_cadence_standing=200,
                                                        fatigue_rate=0.02)
        rider_a_bike_fit = lapysim.BikeFit(saddle_height=1.1, gear_ratio=3.8)
        self.rider_a = lapysim.Athlete(
            name='Rider A',
            athlete_id='rdrA',
            bike=cervelo_T5GB_size_M,
            anthropometry=rider_a_anthropometry,
            physiology=rider_a_physiology,
            bike_fit=rider_a_bike_fit,
        )

        # Create rider B.
        rider_b_anthropometry = lapysim.Anthropometry(mass=75, drag_area=0.24,
                                                      height=1.79)
        rider_b_physiology = lapysim.NullPhysiology()
        rider_b_bike_fit = lapysim.BikeFit(saddle_height=1.0, gear_ratio=3.6)
        self.rider_b = lapysim.Athlete(
            name='Rider B',
            athlete_id='rdrB',
            bike=cervelo_T5GB_size_M,
            anthropometry=rider_b_anthropometry,
            physiology=rider_b_physiology,
            bike_fit=rider_b_bike_fit,
        )

        # Compile the event.
        self.match_sprint_event = lapysim.Event(
            venue=self.match_sprint_venue,
            athletes=(self.rider_a, self.rider_b),
            rules=self.match_sprint_rules,
        )

    def reset(self):
        """Reset the gym to its initial state."""
        # Current distance and velocity for two riders
        print("Reset #" + str(self.info['repetitions']))
        self.info['repetitions'] = self.info['repetitions'] + 1
        self.state = self.rider_a_init + self.rider_b_init
        self.done = False
        self.info['total_reward'] = 0
        self.reward = 0

        # Reset the simulation.
        self.match_sprint_simulation = self.match_sprint_event.initialise_forward_simulation()
        self.match_sprint_simulation.initialise_rider_states(
            (self.rider_a_init, self.rider_b_init))
        return np.array(self.state)

    def save_history(self, folder=None, name=None):
        """Save the race history for experience replay."""
        # Build history structure
        times = self.match_sprint_simulation._simulation_histories['time']
        rider_a_dict = self.match_sprint_simulation._simulation_histories[self.rider_a]
        rider_b_dict = self.match_sprint_simulation._simulation_histories[self.rider_b]
        data = [times, rider_a_dict, rider_b_dict]

        # Build file name
        if name is None:
            now = datetime.now()
            dt_string = now.strftime("%d_%m_%Y_%H_%M_%S")
            filename = "msr_" + dt_string + ".hist"
        else:
            filename = name + ".hist"
        # Save in folder
        if folder is not None:
            filename = folder + '/' + filename
        filehandler = open(filename, 'wb')
        pickle.dump(data, filehandler)
