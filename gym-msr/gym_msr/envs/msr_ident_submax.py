"""Match Sprint Race Gym Environment
Creates a MSR simulation in which the riders follow the black line.
The user is able to control the power output of rider A, while rider B has an
identical physiology model but may follow different control strategies.

Riders in these environments follow the Variable Maximal Sub-Maximal
physiology model. This is an adapted form of the Torque-Cadence model which
allows for sub-maximal actions (with reduced fatigue rate).

author: Rob Sumner
Classes
-------
MsrSubMaxSprint - Gym for two identical opponents following sub-max physiology.
                - Race distance is randomly initialised in range [100,500].
                - Riders initially moving at 3 m/s and 1m apart (rider A behind).
                - Reward delayed to end of race.
"""

# Gym based imports.
import gym
from gym import error, spaces, utils
from gym.utils import seeding
from gym.envs.classic_control import rendering
from PIL import Image
import pickle
from datetime import datetime
import copy

# Lapysim imports.
import numpy as np
import sympy as sym
import sympy.physics.mechanics as me
import lapysim
from lapysim.simulation import ForwardDynamics

# Base Gym
from gym_msr.envs.msr_ident_submax_base import MsrSubMaxBase


class MsrSubMaxSprint(MsrSubMaxBase):
    """Implement MSR simulation as Gym.
    This gym requires version of Lapysim with aero modelling implemented.
    Currently, this uses a single dimension aero model.
    Opponents in this gym are identical, and both follow the sub-max phys model.

    This gym randomises the sprint distance at each reset to lie in the range
    [250, 650]. This is achieved by setting the starting distance to match this value.
    Riders are spawned at 1 m apart, both travelling at 3 m/s.

    The Reward function encourages riders to win, and the desired behaviour
    is a rider which can sprint and win across a range of sprint distances.

    Observation:
        All observation spaces are normalised.
        Type: Box(6)
        Num	Observation                 Min         Max
        0	Rider A Distance            0            1
        1	Rider A Velocity            0            1
        2   Rider A Fatigue             0            1
        3	Rider B Distance            0            1
        4	Rider B Velocity            0            1
        5   Rider B Fatigue             0            1

    Actions:
        Type: Discrete(n)
        Num	    Action
        0:(n-1) [0:(1/n-1):1] - Power value in range [0,1] mapping to exertion.

    Parameters
    ----------
    MsrIdenticalBase - The parent class for variable opponent.

    Public methods:
    ---------------
    __init__(self, action_dim=2):
        Initialise MSR gym.
        Parameters:
        action_dim - The dimension of the action space.
                   - Result is always normalised to lie in [0,1].
        reward_mode - The form of the reward given by the environment.
        time_step - Time step used in dynamics.
        verbose - Reporting flag for environment.

    complete(self):
        Return True if race is finished, False if not.

    step(self, action):
        Step the gym forward by a time step.
        Action is an array [value] where 0 <= value <= 1
        Returns [state, reward, done, additional_info]

    reset(self):
        Reset the gym to its initial state.

    reward(self):
        Returns reward for given state.
            0 - If race is incomplete.
            0.75*(rider_distance/race_distance) - If race complete and rider A loses.
            1 + separation - If race complete and rider A wins.

    normalised_state(self):
        Returns a state value normalised to match observation space.

    """

    metadata = {'render.modes': ['human', 'rgb_array']}

    def __init__(self, action_dim=2, reward_mode='scaled_distance', time_step=0.3, verbose=True):
        """Initialise the MSR gym.
            Parameters:
            action_dim - Dimension of the action space.
                       - Needs to be non-zero integer.
            reward_mode - Type of reward function.
                        - Options are: 'scaled_distance', 'separation'.
            time_step - Step used in dynamics simulation.
            verbose - If true, information is printed to sys.stdout during learning.
                    - False will remove all reporting from within the env. 
        """

        # Initialise parent
        super(MsrSubMaxSprint, self).__init__()
        self.verbose = verbose

        # Reward state
        if reward_mode not in ['separation', 'scaled_distance']:
            raise ValueError("Unrecognised reward mode.")
        self.reward_mode = reward_mode

        # Action space
        if type(action_dim) is not int or action_dim <= 0:
            raise ValueError("Action dimension must be non-zero int.")
        self.action_space = spaces.Discrete(action_dim)

        # Observation spaces.
        self.race_distance = 750
        self.max_distance = 760
        self.max_speed = 30
        obs_low  = np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0])
        obs_high = np.array([1.0, 1.0, 1.0, 1.0, 1.0, 1.0])
        self.observation_space = spaces.Box(obs_low, obs_high, dtype=np.float32)

        # Initial state: [distance, velocity, fatigue]
        self.rider_a_init, self.rider_b_init = self.random_init()
        self.state = self.rider_a_init + self.rider_b_init
        self.done = False
        self.info = {'repetitions':0, 'total_reward':0}
        self.reward = 0
        self.env_steps = 0
        self.viewer = None
        self.time_step = time_step

        # Rider B moves at max power.
        self.rider_b_control = [1]

        # Initialise the forward solution.
        self.match_sprint_simulation = self.match_sprint_event.initialise_forward_simulation()
        self.master_dynamics_lambdas = copy.deepcopy(self.match_sprint_simulation._dyn_eqns_lambdas)
        self.match_sprint_simulation.initialise_rider_states(
            (self.rider_a_init, self.rider_b_init))

    def complete(self):
        """Check if the simulation has completed.
        Return 1 (true) if race is finished.
        Return 0 (false) if race is not finished."""
        if (self.state[0] >= self.race_distance or self.state[3] >= self.race_distance):
            self.done = True
            return True
        else:
            return False

    def step(self, action):
        """Step the gym forward by a time step.
        Action  - An integer value 0:1:5 which is converted into 0:0.2:1
                - This maps to maximum possible output power in the torque-cadence model.
                - 1 is full power, 0 is no power.
                - If no action is provided, the simulation will step with previous action.
        Returns [state, reward, done, additional_info]"""

        assert self.action_space.contains(action), "%r (%s) invalid"%(action, type(action))

        if self.complete():
            self.reward = self.reward_function()
            self.info['total_reward'] = self.info['total_reward'] + self.reward

            self.close()
            return self.normalised_state(), self.reward, self.done, self.info

        # Normalise action value in rance [0,1]
        action /= (self.action_space.n - 1)

        # array_state is [ [rider_a_state], [rider_b_state] ]
        # rider_state is [ distance, velocity, fatigue ]
        array_state = self.match_sprint_simulation.update_rider_states(
            timestep=self.time_step,
            athletes_control=([action], self.rider_b_control),
            )

        # State needs to match observation space [a_s, a_v, a_f, b_s, b_v, b_f]
        self.state = array_state[0] + array_state[1]

        # Get reward function
        self.reward = self.reward_function()
        self.info['total_reward'] = self.info['total_reward'] + self.reward
        self.env_steps += 1

        return self.normalised_state(), self.reward, self.done, self.info

    def reset(self, distance=None, verbose=True):
        """Reset the gym to its initial state."""
        # Current distance and velocity for two riders
        if verbose and self.verbose:
            print("Total reward = " + str(self.info['total_reward']))
            print("Reset #" + str(self.info['repetitions']))
        self.info['repetitions'] = self.info['repetitions'] + 1
        self.done = False
        self.info['total_reward'] = 0
        self.reward = 0
        self.env_steps = 0

        # Reset state randomly
        self.rider_a_init, self.rider_b_init = self.random_init(distance=distance)
        self.state = self.rider_a_init + self.rider_b_init

        # Reset the simulation.
        self.match_sprint_simulation = ForwardDynamics(self.match_sprint_event, self.master_dynamics_lambdas)
        self.match_sprint_simulation.initialise_rider_states(
            (self.rider_a_init, self.rider_b_init))
        return self.normalised_state()

    def reward_function(self):
        """ Return the reward.
            0 - If race has not been finished.
            1 - If rider A wins.
            0.75*distance - If rider B wins.
                          - This is designed to encourage forward movement.
        """
        if self.reward_mode == 'scaled_distance':
            if not self.complete():
                return 0
            elif self.state[0] >= self.state[3]:
                return 1 + (self.state[0] - self.state[3])
            else:
                return 0.75*(self.state[0] - self.rider_a_init[0]) / \
                        (self.race_distance - self.rider_a_init[0])
        elif self.reward_mode == 'separation':
            if not self.complete():
                return 0
            else:
                return self.state[0] - self.state[3]
        else:
            raise ValueError("Unknown reward mode")

    def normalised_state(self):
        """Return a normalised state value."""
        state = self.state
        norm_state = [state[0]/self.max_distance,
                      state[1]/self.max_speed, state[2],
                      state[3]/self.max_distance,
                      state[4]/self.max_speed, state[5]]
        return np.array(norm_state)

    def random_init(self, distance=None, gap=1, initial_v=3):
        """Returns initialisations state for a random sprint.
            Parameters:
            distance - Provided distance value used.
                     - This is used for agent evalation.
        """
        # Initialise race between [100,500] for sprint of [250,650].
        d_max = 500
        d_min = 100
        if distance is None:
            distance = np.random.rand(1)[0]*(d_max - d_min) + d_min
        else:
            if distance > d_max or distance < d_min:
                raise ValueError("Distance value does not lie in range.")

        # Return initial state. [distance, velocity, fatigue]
        rider_a_init = [distance, initial_v, 1]
        rider_b_init = [distance + gap, initial_v, 1]
        return rider_a_init, rider_b_init