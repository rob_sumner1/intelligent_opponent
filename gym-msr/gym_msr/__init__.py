from gym.envs.registration import register

register(
    id='msr-fixed-win-v1',
    entry_point='gym_msr.envs:MsrFixedWin',
)

register(
    id='msr-fixed-draft-v1',
    entry_point='gym_msr.envs:MsrFixedDraft_1',
)

register(
    id='msr-fixed-draft-v2',
    entry_point='gym_msr.envs:MsrFixedDraft_2',
)

register(
    id='msr-fixed-draft-v3',
    entry_point='gym_msr.envs:MsrFixedDraft_3',
)

register(
    id='msr-ident-win-v1',
    entry_point='gym_msr.envs:MsrIdenticalWin_1',
)

register(
    id='msr-ident-win-v2',
    entry_point='gym_msr.envs:MsrIdenticalWin_2',
)

register(
    id='msr-ident-win-v3',
    entry_point='gym_msr.envs:MsrIdenticalWin_3',
)

register(
    id='msr-submax-sprint-v1',
    entry_point='gym_msr.envs:MsrSubMaxSprint',
)

register(
    id='msr-duel-fixed-v1',
    entry_point='gym_msr.envs:MsrDuelFixed1',
)

register(
    id='msr-duel-fixed-v2',
    entry_point='gym_msr.envs:MsrDuelFixed2',
)

register(
    id='msr-duel-fixed-v3',
    entry_point='gym_msr.envs:MsrDuelFixed3',
)