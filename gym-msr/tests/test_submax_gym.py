"""Test the sub-maximal physiology gyms."""
import gym
import gym_msr
import pytest
import timeit

@pytest.fixture
def env():
    """Env with 0,1 action space."""
    return gym.make('msr-submax-sprint-v1', action_dim=2)

@pytest.fixture
def env5():
    """Env with 0,1 action space."""
    return gym.make('msr-submax-sprint-v1', action_dim=5)

@pytest.fixture
def envt():
    """Env with 0,1 action space."""
    return gym.make('msr-submax-sprint-v1', time_step=0.1)

def test_init(env, envt):
    """Test the initialised values of the environment."""
    # Check on input assertions
    with pytest.raises(ValueError):
        gym.make('msr-submax-sprint-v1', action_dim=0)
    with pytest.raises(ValueError):
        gym.make('msr-submax-sprint-v1', action_dim=1.1)
    with pytest.raises(ValueError):
        gym.make('msr-submax-sprint-v1', reward_mode='not_a_mode')

    assert env.state == env.rider_a_init + env.rider_b_init
    assert not env.complete()
    assert env.action_space.n == 2

    assert len(env.rider_a_init) == 3
    assert len(env.rider_b_init) == 3
    assert env.rider_a_init[1] == 3
    assert env.rider_a_init[2] == 1
    assert env.rider_b_init[0] == env.rider_a_init[0] + 1
    assert env.rider_b_init[1] == 3
    assert env.rider_b_init[2] == 1

    # Check timestep values
    assert env.time_step == 0.3 # Default.
    assert envt.time_step == 0.1


def test_random_init(env):
    """Test random race position iniitialisation."""
    av_d = 0
    for _ in range(1000):
        state_a, state_b = env.random_init()
        av_d += state_a[0]
        assert state_a[0] + 1 == state_b[0]
        assert state_a[1] == 3
        assert state_a[2] == 1
        assert state_b[1] == 3
        assert state_b[2] == 1

    # Test mean of new distance each time.
    av_d /= 1000
    assert pytest.approx(av_d, 0.1) == 300

def test_normalised_state(env):
    """Test the normalisation state function."""
    state = env.normalised_state()
    for elem in state:
        # Should lie in range [0,1]
        assert elem <= 1
        assert elem >= 0

def test_step_reset(env, envt):
    """Test the stepping value for the environment."""
    with pytest.raises(AssertionError):
        env.step(0.5)

    init_d = env.state[0]
    envt.reset(init_d)
    for _ in range(10):
        env.step(1)
        envt.step(1)
    assert env.state[0] > init_d
    assert env.state[0] > envt.state[0]

    # Check rider A has fatigued.
    assert env.env_steps == 10
    assert env.state[2] < 1

    # Reset env
    env.reset()
    assert env.env_steps == 0
    assert env.reward == 0
    assert env.state[0] != init_d
    assert env.state[1] == 3
    assert env.state[2] == 1
    assert env.state[3] == env.state[0] + 1
    assert env.state[4] == 3
    assert env.state[5] == 1

def test_reward():
    """Test the reward functions."""
    env = gym.make('msr-submax-sprint-v1', reward_mode='scaled_distance')
    assert env.reward_function() == 0
    env.state = [751, 0, 1, 750, 0, 1]
    assert env.reward_function() == 2
    env.state = [500, 0, 1, 751, 0, 1]
    val = 0.75*(500 - env.rider_a_init[0])/(env.race_distance - env.rider_a_init[0])
    assert pytest.approx(env.reward_function()) == val

    env = gym.make('msr-submax-sprint-v1', reward_mode='separation')
    assert env.reward_function() == 0
    env.state = [751, 0, 1, 750, 0, 1]
    assert env.reward_function() == 1
    env.state = [500, 0, 1, 751, 0, 1]
    assert pytest.approx(env.reward_function()) == -251

def test_fatigue(env, env5):
    """Test the fatigue measure."""
    env.reset(distance=500)
    for _ in range(10):
        env.step(1)
        env5.step(3)
    assert pytest.approx(env.state[2],0.1) == 0.79
    assert env5.state[2] > env.state[5]
    # env5.match_sprint_simulation.plot_rider_histories(velocity=False)

def test_env_time(env):
    """Test the time required to complete a set number of runs"""
    start_t = timeit.default_timer()
    N = 100
    for _ in range (N):
        env.reset(verbose=False)
        while not env.complete():
            env.step(1)
    av_run_t = (timeit.default_timer() - start_t) / N
    assert av_run_t <= 1