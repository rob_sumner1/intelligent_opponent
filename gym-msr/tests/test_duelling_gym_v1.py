import gym
import gym_msr
import pytest
import timeit
from gym_msr.envs.msr_duelling import LeadJockey as LJPolicy
from gym_msr.envs.msr_duelling import TrailDraft as TrDraft

@pytest.fixture(scope="module")
def env():
    """Full MSR env_v1."""
    return gym.make('msr-duel-fixed-v1')

def test_gym_init(env):
    """Test initialisation of the gym."""
    assert env.action_space.n == 5
    assert env.time_step == 0.1
    assert env.state == env.rider_a_init + env.rider_b_init
    assert not env.complete()
    assert len(env.rider_a_init) == 3
    assert len(env.rider_b_init) == 3

def test_env_complete(env):
    """Test the check on race completion."""
    # Should not be complete at initialisation
    assert not env.complete()
    # Check incomplete with given values
    env.state = [100, 3, 1, 749, 3, 1]
    assert not env.complete()
    # Check complete with given values
    env.state = [750, 3, 1, 749, 3, 1]
    assert env.complete()

def test_env_step(env):
    """Test stepping the environment."""
    env.reset()
    # No steps, so last actions should be [0,0].
    assert all([a == b for a, b in zip(env.last_controls, [0, 0])])
    start_state = env.state

    env.step(2)
    env.step(2)
    assert env.last_controls[0] == 0.5
    # all state variables should have changed
    assert all([a != b for a, b in zip(env.state, start_state)])

def test_env_reset(env):
    """Test the reset function for the environment."""
    env.reset()
    start_state = env.state
    # Step the env_v1ironment
    for _ in range(10):
        env.step(2)
    assert all([a != b for a, b in zip(env.state, start_state)])

    # Reset and check that state is valid
    env.reset()
    assert env.state[0] in [0,1]
    assert env.state[1] in [0,1]
    assert env.state[2] == 1
    assert env.state[3] in [0,1]
    assert env.state[4] in [0,1]
    assert env.state[5] == 1

def test_env_reward(env):
    """Test the reward function - scaled distance."""
    env.reset()
    # Never complete at init.
    assert env.reward_function() == 0

    # Rider A loses
    env.state = [750, 0, 0, 751, 0, 0]
    assert pytest.approx(env.reward_function(), 0.75)

    # Rider A wins
    env.state = [751, 0, 0, 750, 0, 0]
    assert env.reward_function() == 2

def test_normalised_state(env):
    """Test the normalisation state function."""
    state = env.normalised_state()
    for elem in state:
        # Should lie in range [0,1]
        assert elem <= 1
        assert elem >= 0

def test_env_v1_random_init(env):
    """Test the random init method."""
    # Check init with params.
    with pytest.raises(ValueError):
        env.random_init(leading_rider='Not a rider')

    # Rider A in front produces trailing policy
    rider_a, rider_b, policy = env.random_init(leading_rider='Rider A')
    assert rider_a + rider_b == [1, 1, 1, 0, 0, 1]
    assert isinstance(policy, TrDraft)

    # Rider A with specific policy
    rider_a, rider_b, policy = env.random_init(leading_rider='Rider A', policy=0)
    assert rider_a + rider_b == [1, 1, 1, 0, 0, 1]
    assert isinstance(policy, TrDraft)

    # Rider A with specific policy and random seed
    rider_a, rider_b, policy = env.random_init(leading_rider='Rider A',
                                               policy=0,
                                               policy_random_seed=0)
    assert rider_a + rider_b == [1, 1, 1, 0, 0, 1]
    assert isinstance(policy, TrDraft)
    assert policy.sprint_start_distance == policy.test_points[0][0]

    # Rider B in front produces leading policy
    rider_a, rider_b, policy = env.random_init(leading_rider='Rider B')
    assert rider_a + rider_b == [0, 0, 1, 1, 1, 1]
    assert isinstance(policy, LJPolicy)

    # Rider B in front with specific policy.
    rider_a, rider_b, policy = env.random_init(leading_rider='Rider B', policy=0)
    assert rider_a + rider_b == [0, 0, 1, 1, 1, 1]
    assert isinstance(policy, LJPolicy)

    # Rider B in front with specific policy and random seed
    rider_a, rider_b, policy = env.random_init(leading_rider='Rider B',
                                               policy=0,
                                               policy_random_seed=0)
    assert rider_a + rider_b == [0, 0, 1, 1, 1, 1]
    assert isinstance(policy, LJPolicy)
    assert policy.sprint_start_distance == policy.test_points[0][0]

    # Test some fully random inits
    rider_a_lead_prob = 0
    samples = 500
    for _ in range(samples):
        rider_a, rider_b, policy = env.random_init()
        if rider_a[0] > rider_b[0]:
            rider_a_lead_prob += 1
    rider_a_lead_prob /= samples
    assert rider_a_lead_prob == pytest.approx(0.5, 0.1)

def test_policy_evaluation_data(env):
    """Test the helper function for the evaluation data."""
    lead, trail = env.policy_test_points()
    assert len(lead) == 10
    for i in range(5):
        assert lead[i][0] == 0
        assert lead[i][1] == i

    assert len(trail) == 12
    for i in range(12):
        assert trail[i][0] == 0
        assert trail[i][1] == i

def test_env_draft_exertion(env):
    """Test function returning equilibrium draft exertion."""
    # Point test for evaluator.
    env.state = [10, 10, 1, 11, 10, 1]
    env.last_controls = [0.1, 0.5]
    assert env.get_draft_exertion() ==  pytest.approx(0.090877, 0.01)

def test_env_run_time(env):
    """Test the time required to complete a set number of runs"""
    start_t = timeit.default_timer()
    N = 25
    for _ in range (N):
        env.reset(verbose=False)
        while not env.complete():
            env.step(4)
    av_run_t = (timeit.default_timer() - start_t) / N
    assert pytest.approx(av_run_t, 0.25) == 0.3