import gym
import gym_msr
from gym_msr.envs.msr_policies import LeadJockey as LJPolicy
from learning_tools import plot_action_separation, plot_fatigue, plot_velocities

# Trailing draft policy
env = gym.make('msr-duel-fixed-v1')

for i in range(10):
    env.reset(leading_rider='Rider A', policy=0, policy_random_seed=i)

    while not env.complete():
        env.step(2)

    env.save_history(name='td_policy')
    plot_action_separation(histories=['td_policy.hist'],
                        suptitle='Trailing Draft: Action-Separation',
                        titles=['Rider A Leads, Drafting Policy'],
                        single_althete=False,
                        save=True,
                        image_save_name='action_separation_policy_' + str(i),
                        show=False)
    plot_fatigue(histories=['td_policy.hist'],
                        suptitle='Trailing Draft: Fatigue',
                        titles=['Rider A Leads, Drafting Policy'],
                        save=True,
                        image_save_name='fatigue_policy_' + str(i),
                        show=False)
    plot_velocities(histories=['td_policy.hist'],
                        suptitle='Trailing Draft: Velocity',
                        titles=['Rider A Leads, Drafting Policy'],
                        save=True,
                        image_save_name='velocity_policy_' + str(i),
                        show=False)


# Memory profile run
env = gym.make('msr-duel-fixed-v1')
env.reset(leading_rider='Rider A', policy=0, policy_random_seed=4)
while not env.complete():
    env.step(3)