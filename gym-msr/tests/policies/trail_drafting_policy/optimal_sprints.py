"""Script to investigate which sprints will lose a race and which will win."""

import gym
import gym_msr
import numpy as np
import matplotlib.pyplot as plt
from gym_msr.envs.msr_policies import LeadJockey as LJPolicy
from learning_tools import plot_action_separation, plot_fatigue, plot_velocities

# Trailing draft policy
action_dim = 5
env = gym.make('msr-duel-fixed-v1', action_dim=action_dim)

# Test points
points = 50
sprint_distances = np.linspace(200, 745, points)
actions = [1, action_dim-1]
separations = np.zeros((len(sprint_distances), len(actions)))
overtakes = np.zeros((len(sprint_distances), len(actions)))

for j, action in enumerate(actions):
    for i, distance in enumerate(sprint_distances):
        # Manually set sprint distance
        env.reset(leading_rider='Rider A')
        env.rider_b_policy.sprint_start_distance = distance

        # Run environment and count number of position swaps.
        leading_index = 0 # Rider A leads initially
        trailing_index = 3
        overtake_count = 0
        while not env.complete():
            # Check if riders have swapped positions
            if env.state[leading_index] < env.state[trailing_index]:
                overtake_count += 1
                leading_index = 3
                trailing_index = 0
            if env.state[3] > env.rider_b_policy.sprint_start_distance:
                env.step(action_dim-1)
            else:
                env.step(action)

        # Store results
        separations[i,j] = env.state[0] - env.state[3]
        overtakes[i,j] = overtake_count

# Store results
results = np.concatenate( (sprint_distances.reshape(len(sprint_distances), 1), separations, overtakes), axis=1 )
header = 'Sprint Initiation Distance,Min Action (' + str(1/(action_dim-1)) + ') Separation,' + \
          'Max Action (' + str(1) + ') Separation,Min Action Overtakes,Max Action Overtakes'
np.savetxt('policy_tuning/sprint_results_space' + str(action_dim) + '.csv', results, delimiter=',', header=header)

# Store and save results
y_max = np.max(separations)
y_min = np.min(separations)

# Set tex intepreter
from matplotlib import rc
rc('font', **{'family':'CMU Serif'})
rc('text', usetex=True)

# Plot results
for i,action in enumerate(actions):
    plt.plot(results[:,0], results[:,i+1], label='Exertion = ' + str(action/(action_dim-1)) )
plt.fill_between(results[:,0], 0, 2*y_max, facecolor='green', alpha=0.3, label='Rider A Wins')
plt.fill_between(results[:,0], 0, 2*y_min, facecolor='red', alpha=0.3, label='Rider A Loses')
plt.xlim([300,740])
plt.ylim([1.1 * y_min, 1.1 * y_max])
plt.suptitle('Final Separation Against Sprint Initiation Distance')
plt.title('Rider B following TD policy, Rider A moving at varying exertion')
plt.xlabel('Sprint Initiation Distance')
plt.ylabel('Final Separation $d_{A}-d_{B}$')
plt.legend()
plt.savefig('policy_tuning/varying_exer_sprint_plot_space' + str(action_dim)+ '.png', dpi=480)
plt.show()