import os
import gym
from learning_tools import analyse_trained_policies, plot_policy_analysis, plot_action_separation, measure_win_rate
import matplotlib.pyplot as plt
import numpy as np
from gym_msr.envs.msr_policies import MsrPolicy

# from learning_tools import analyse_expert_policies, plot_policy_analysis, plot_action_separation, calculate_win_rate
# import matplotlib.pyplot as plt
# import numpy as np

# rider_a_policy = MsrPolicy(rider='Rider A')
# # results = analyse_expert_policies(rider_a_policy, n_steps=100, n_step_runs=100, action_dim=200)
# # np.save('results', results)

# files = ['policy_analysis_files/policy_analysis_long_size_5', 'policy_analysis_files/policy_analysis_long_size_200']
# labels = ['$|Action Space|=5$', '$|Action Space|=200$']
# save_name = 'example'
# plot_policy_analysis(files, labels=labels, save_name=save_name)
# plot_action_separation(histories=['msr_policy'], single_althete=False, plot_fatigue=True)

# # Analyse win rates
# overall, lead, trail = calculate_win_rate(files[0])
# print('Win rates size 5 = ', overall, ', ', lead, ', ', trail)
# overall, lead, trail = calculate_win_rate(files[1])
# print('Win rates size 5 = ', overall, ', ', lead, ', ', trail)

# import warnings
# warnings.simplefilter(action='ignore', category=FutureWarning)
# import tensorflow as tf
# tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)
# import os
# os.environ["KMP_WARNINGS"] = "FALSE"


# # Investigate fatigue
agent_folder = os.path.dirname(os.path.abspath(__file__))
rider_a_agent = MsrPolicy(rider='Rider A')

# # Run analysis of policy.
save_folder = agent_folder + '/policy_analysis_files/'
os.makedirs(save_folder, exist_ok=True)

# Fatigue rate investigation
n_steps = 10
rider_a_fatigue_rates = np.linspace(0.009,0.0115, n_steps)
rider_b_fatigue_rates = np.linspace(0.009,0.0115, n_steps)
overall = np.zeros( (len(rider_b_fatigue_rates), len(rider_a_fatigue_rates)) )
lead =  np.zeros( (len(rider_b_fatigue_rates), len(rider_a_fatigue_rates)) )
trail =  np.zeros( (len(rider_b_fatigue_rates), len(rider_a_fatigue_rates)) )

# Vary fatigue and measure result
action_dim = 5
n_runs = 75
for i, rider_a_fatigue in enumerate(rider_a_fatigue_rates):
    for j, rider_b_fatigue in enumerate(rider_b_fatigue_rates):
        env = gym.make('msr-duel-fixed-v2',
            rider_a_fatigue_rate=rider_a_fatigue,
            rider_b_fatigue_rate=rider_b_fatigue)

        lead_wins, trail_wins = 0, 0

        # Rider A leading
        for _ in range(n_runs):
            info = {}
            state = env.reset(leading_rider='Rider A', verbose=False)
            while not env.complete():
                rider_a_eq_exer = env.get_draft_exertion(drafting_rider='Rider A')
                action = int(rider_a_agent.predict(env.state, rider_a_eq_exer)*(action_dim-1))
                env.step(action)

            # Store results
            if state[0] > state[3]:
                lead_wins += 1

        # Rider B leading
        for _ in range(n_runs):
            info = {}
            state = env.reset(leading_rider='Rider B', verbose=False)
            while not env.complete():
                rider_a_eq_exer = env.get_draft_exertion(drafting_rider='Rider A')
                action = int(rider_a_agent.predict(env.state, rider_a_eq_exer)*(action_dim-1))
                env.step(action)

            # Store results
            if state[0] > state[3]:
                trail_wins += 1

        # Results
        overall_win_rate = ( lead_wins + trail_wins ) / (2 * n_runs)
        lead_win_rate = lead_wins / n_runs
        trail_win_rate = trail_wins / n_runs

        # Store results
        overall[i,j] = overall_win_rate
        lead[i,j] = lead_win_rate
        trail[i,j] = trail_win_rate

# Save out raw arrays.
np.save(save_folder + 'overall_win_rate.npy', overall)
np.save(save_folder + 'trail_win_rate', trail)
np.save(save_folder + 'lead_win_rate', lead)

print('Overall: ', overall)
print('Trail: ', trail)
print('Lead: ', lead)
