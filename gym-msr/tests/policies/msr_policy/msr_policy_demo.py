import gym
import gym_msr
import timeit
from gym_msr.envs.msr_policies import MsrPolicy
from learning_tools import plot_action_separation, plot_fatigue, plot_velocities

# Demonstrate policies by racing two riders controlled by MsrPolicy
action_dim = 100 # Large to make roughly continuous
env = gym.make('msr-duel-fixed-v2', action_dim=action_dim)

# # Rider A controlled by policy rather than agent.
# rider_a_agent = MsrPolicy(rider='Rider A')

# n_races = 500
# races_won = 0
# for i in range(n_races):
#     # Rider B is leading
#     env.reset(leading_rider='Rider A', verbose=False)
#     rider_a_agent.reset()

#     # Step with rider A making no action.
#     while not env.complete():
#         rider_a_eq_exer = env.get_draft_exertion(drafting_rider='Rider A')
#         action = int(rider_a_agent.predict(env.state, rider_a_eq_exer)*(action_dim-1))
#         env.step(action)

#     # Save every 10th plot.
#     if i % 50 == 0:
#         env.save_history(name='msr_policy')
#         plot_action_separation(histories=['msr_policy.hist'],
#                             suptitle='MSR Policy: Action-Separation',
#                             titles=['Rider A Leads'],
#                             single_althete=False,
#                             save=True,
#                             image_save_name='space_' + str(action_dim) + '/action_separation_policy_' + str(i),
#                             show=False)

#     # Check if rider A has won.
#     if env.state[0] > env.state[3]:
#         races_won += 1
#     # else:
#         print('\nRACE WON!')
#         print('Rider B Policy:')
#         print('Events: ', env.rider_b_policy.event_descriptions)
#         print('Trail sprint: ', env.rider_b_policy.trail_sprint_start_distance)
#         print('Lead sprint: ', env.rider_b_policy.lead_sprint_start_distance)
#         print('Rider A Policy:')
#         print('Events: ', rider_a_agent.event_descriptions)
#         print('Trail sprint: ', rider_a_agent.trail_sprint_start_distance)
#         print('Lead sprint: ', rider_a_agent.lead_sprint_start_distance)
#         print('\n')
#         env.save_history(name='msr_policy')
#         plot_action_separation(histories=['msr_policy.hist'],
#                             suptitle='MSR Policy: Action-Separation',
#                             titles=['Rider A Leads'],
#                             single_althete=False,
#                             save=True,
#                             image_save_name='space_' + str(action_dim) + '/won_action_separation_policy_' + str(i),
#                             show=False)

# print('Win rate = ', races_won/n_races)


# # All 1's response
# # Rider A controlled by policy rather than agent.
# n_races = 500
# races_won = 0
# for i in range(n_races):
#     # Rider B is leading
#     env.reset(leading_rider='Rider A', verbose=False)

#     # Step with rider A making no action.
#     while not env.complete():
#         env.step( (action_dim - 1) )

#     # Save every 10th plot.
#     if i % 50 == 0:
#         env.save_history(name='msr_policy')
#         plot_action_separation(histories=['msr_policy.hist'],
#                             suptitle='MSR Policy: Action-Separation',
#                             titles=['Rider A Leads'],
#                             single_althete=False,
#                             save=True,
#                             image_save_name='all_ones/action_separation_policy_' + str(i),
#                             show=False)

#     # Check if rider A has won.
#     if env.state[0] > env.state[3]:
#         races_won += 1
# print('Win rate = ', races_won/n_races)


# # # Sub-optimal agent policies
# # Rider A controlled by policy rather than agent.
# rider_a_agent = MsrPolicy(rider='Rider A')
# rider_a_agent.min_lead_sprint_distance = 0
# rider_a_agent.max_lead_sprint_distance = 705

# n_races = 500
# races_won = 0
# for i in range(n_races):
#     # Rider B is leading
#     env.reset(leading_rider='Rider A', verbose=False)
#     rider_a_agent.reset()

#     # Step with rider A making no action.
#     while not env.complete():
#         rider_a_eq_exer = env.get_draft_exertion(drafting_rider='Rider A')
#         action = int(rider_a_agent.predict(env.state, rider_a_eq_exer)*(action_dim-1))
#         env.step(action)

#     # Save every 10th plot.
#     if i % 100 == 0:
#         env.save_history(name='msr_policy')
#         plot_action_separation(histories=['msr_policy.hist'],
#                             suptitle='MSR Policy: Action-Separation',
#                             titles=['Rider A Leads'],
#                             single_althete=False,
#                             save=True,
#                             image_save_name='sub_optimal/action_separation_policy_' + str(i),
#                             show=False)

#     # Check if rider A has won.
#     if env.state[0] > env.state[3]:
#         races_won += 1

# print('Win rate = ', races_won/n_races)


# # # Average win rate
# # Rider A controlled by policy rather than agent.
# rider_a_agent = MsrPolicy(rider='Rider A')
# n_races = 500
# races_won = 0
# for i in range(n_races):
#     # Fully random races
#     env.reset(verbose=False)
#     rider_a_agent.reset()

#     # Step with rider A making no action.
#     while not env.complete():
#         rider_a_eq_exer = env.get_draft_exertion(drafting_rider='Rider A')
#         action = int(rider_a_agent.predict(env.state, rider_a_eq_exer)*(action_dim-1))
#         env.step(action)

#     # Check if rider A has won.
#     if env.state[0] > env.state[3]:
#         races_won += 1

# print('Win rate = ', races_won/n_races)


# # # Test zero action
# # Rider A controlled by policy rather than agent.
# rider_a_agent = MsrPolicy(rider='Rider A')
# for leading_rider in ['A', 'B']:
#     print('Leading rider = ', leading_rider)

#     # Fully random races
#     env.reset(verbose=False, leading_rider='Rider ' + leading_rider)
#     rider_a_agent.reset()

#     # Step with rider A making no action.
#     start = timeit.default_timer()
#     while not env.complete():
#         env.step(0)
#     run_time = timeit.default_timer() - start
#     print('Run time = ', run_time)

#     env.save_history(name='msr_policy')
#     plot_action_separation(histories=['msr_policy.hist'],
#                         suptitle='MSR Policy: Action-Separation',
#                         titles=['Rider ' + leading_rider + ' Leads'],
#                         single_althete=False,
#                         save=True,
#                         image_save_name='zero_action/act_sep_leading_rider_' + leading_rider,
#                         show=False)

#     # Check if rider A has won.
#     if env.state[0] > env.state[3]:
#         races_won += 1

# # # Memory test
# # Demonstrate policies by racing two riders controlled by MsrPolicy
# action_dim = 100 # Large to make roughly continuous
# env = gym.make('msr-duel-fixed-v2', action_dim=action_dim)

# # Rider A controlled by policy rather than agent.
# rider_a_agent = MsrPolicy(rider='Rider A')

# n_races = 500
# for i in range(n_races):
#     env.reset(verbose=True)
#     rider_a_agent.reset()

#     # Step with rider A making no action.
#     while not env.complete():
#         rider_a_eq_exer = env.get_draft_exertion(drafting_rider='Rider A')
#         action = int(rider_a_agent.predict(env.state, rider_a_eq_exer)*(action_dim-1))
#         env.step(action)


# # Check custom environment
from stable_baselines.common.env_checker import check_env

env = gym.make('msr-duel-fixed-v2')
check_env(env)