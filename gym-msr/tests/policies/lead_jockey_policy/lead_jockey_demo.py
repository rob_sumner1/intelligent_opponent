import gym
import gym_msr
from gym_msr.envs.msr_policies import LeadJockey as LJPolicy
from learning_tools import plot_action_separation, plot_fatigue, plot_velocities

# Lead Jockey policy:
# Sprinting at 200m with 2 drops at 50 & 90m, of 50 & 15 steps respectively.
env = gym.make('msr-duel-fixed-v1')

for i in range(12):
    env.reset(leading_rider='Rider B', policy=0, policy_random_seed=i)

    # Step with rider A making no action.
    while not env.complete():
        env.step(3)

    env.save_history(name='lj_policy')
    plot_action_separation(histories=['lj_policy.hist'],
                        suptitle='Leading Jockey: Action-Separation',
                        titles=['Rider B Leads, Jockeying Policy'],
                        single_althete=False,
                        save=True,
                        image_save_name='action_separation_policy_' + str(i),
                        show=False)

# # Memory run
# env = gym.make('msr-duel-fixed-v1')
# env.reset(leading_rider='Rider B', policy=0, policy_random_seed=11)
# while not env.complete():
#     env.step(3)