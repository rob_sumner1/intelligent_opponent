"""Script to investigate which sprints will lose a race and which will win."""

import gym
import gym_msr
import numpy as np
from matplotlib import colors
import matplotlib.pyplot as plt
from gym_msr.envs.msr_policies import LeadJockey as LJPolicy
from learning_tools import plot_action_separation

# Lead jockeying investigation
action_dim = 50
env = gym.make('msr-duel-fixed-v1', action_dim=action_dim)

# Define sampling
sample_points = 20
# trail_sprint_distances = np.linspace(480, 745, sample_points)
trail_sprint_distances = np.linspace(650, 745, sample_points)
lead_sprint_distances = np.linspace(480, 745, sample_points)

# Storage
separations = np.zeros((sample_points, sample_points))
overtakes = np.zeros(separations.shape)

for i, lead_sprint_distance in enumerate(lead_sprint_distances):
    # Set points that drafting rider will sprint at

    for j, trail_sprint_distance in enumerate(trail_sprint_distances):
        # Trackers
        leading_index = 0 # Rider A leads initially
        trailing_index = 3
        overtake_count = 0

        # Manually set rider B to draft behind rider A
        env.reset(leading_rider='Rider A')
        env.rider_b_policy.sprint_start_distance = trail_sprint_distance

        # Create rider A policy object
        rider_a_policy = LJPolicy()
        rider_a_policy.randomise()
        rider_a_policy.sprint_start_distance = lead_sprint_distance
        rider_a_policy.event_descriptions = [('jump', 0.33*lead_sprint_distance, 50),
                                             ('jump', 0.66*lead_sprint_distance, 50)]
        rider_a_policy.n_events = 2

        # Simulate at each timestep.
        rider_a_last_action = 0
        while not env.complete():
            # Check if riders have swapped positions
            if env.state[leading_index] < env.state[trailing_index]:
                overtake_count += 1
                leading_index = 3
                trailing_index = 0
            rider_eq_action = env.get_draft_exertion()

            # Swap state to perspective of rider A
            rider_a_action = rider_a_policy.predict(env.state[3:] + env.state[:3], rider_eq_action)

            # Step environment using this action
            rider_a_action = int( rider_a_action * (action_dim - 1) )
            env.step(rider_a_action)
        # env.save_history(name='policy_tuning/test_' + str(i) + '_' + str(j) )

        # Store results
        separations[i,j] = env.state[0] - env.state[3]
        overtakes[i,j] = overtake_count


# Store results
sep_results = np.concatenate( (lead_sprint_distances.reshape(len(lead_sprint_distances), 1), separations), axis=1 )
overtake_results = np.concatenate( (lead_sprint_distances.reshape(len(lead_sprint_distances), 1), overtakes), axis=1 )
header = 'Lead Sprint Initiation Distance,' + \
         ''.join(['Draft Sprint Distance = ' + str(dist) +',' for dist in trail_sprint_distances])
csv = open('policy_tuning/sprint_results_space' + str(action_dim) + '.csv','w')
np.savetxt(csv, sep_results, delimiter=',', header=header)
csv.close()
header = 'Lead Sprint Initiation Distance,' + \
         ''.join(['Overtakes for Draft Sprint Distance = ' + str(dist) +',' for dist in trail_sprint_distances])
csv = open('policy_tuning/sprint_results_space' + str(action_dim) + '.csv','a')
np.savetxt(csv, overtake_results, delimiter=',', header=header)
csv.close()

# plot_action_separation(folder='policy_tuning/', single_althete=False, save=True)

# make a color map of fixed colors
from matplotlib import rc
rc('font', **{'family':'CMU Serif'})
rc('text', usetex=True)
cmap = colors.ListedColormap(['red', 'green'])
bounds=[0.5]
norm = colors.BoundaryNorm(bounds, cmap.N)

# Plot results as tile
tile_data = np.zeros(separations.shape)
tile_data[separations > 0] = 1
cax = plt.imshow(np.flip(tile_data, 0), interpolation='nearest', cmap=cmap)
plt.suptitle('Rider A Leading')
plt.title('Outcome of Race for Varying Leading and Drafting Sprint Distance')

# Create colorbar
cbar = plt.colorbar(cax, ticks=[0, 1])
cbar.ax.set_yticklabels(['Rider A Loses', 'Rider A Wins'])  # vertically oriented colorbar

# Set axis labels.
plt.xticks(np.linspace(0, sample_points-1, 5), [str(int(dist*10)/10) for dist in np.linspace(650, 745, 5)])
plt.xlabel('Drafting Sprint Distance')
plt.yticks(np.linspace(0, sample_points-1, 5), [str(int(dist*10)/10) for dist in np.linspace(745, 480, 5)])
plt.ylabel('Leading Sprint Distance')

# Save image
plt.savefig('policy_tuning/win_loss_plot.png', dpi=480)
plt.show()