import os
import gym
import gym_msr
from gym_msr.envs.msr_policies import LeadJockey as LJPolicy
from learning_tools import plot_action_separation, plot_fatigue, plot_velocities
from learning_tools import LearningLog, draft_step_control_performance


def analyse_control_step(env, k_p, k_v, rider_a_action, folder, run_index=0):
    env.rider_b_policies['trail'][0].k_p = k_p
    env.rider_b_policies['trail'][0].k_v = k_v

    # Run with specific policy
    env.reset(leading_rider='Rider A', policy=0, policy_random_seed=4)
    while not env.complete():
        env.step(rider_a_action)

    # Plot the results
    env.save_history(name='draft_policy')
    history = ['draft_policy.hist']
    title = 'Rider B Drafting: kp = ' + str(k_p) + ', kv = ' + str(k_v)
    plot_action_separation(histories=history, titles=[title], save=True,
                        single_althete=False, sep_ylim=[-2,10],
                        suptitle='Action Separation Plot',
                        plot_control_vals=False,
                        image_save_name=folder + 'action_sep_' + str(run_index),
                        show=True)
    over, under, res_t = draft_step_control_performance(histories=history)
    return over, under, res_t

def analyse_control_freq(env, k_p, k_v, rider_a_action, folder, half_period, run_index=0):
    env.rider_b_policies['trail'][0].k_p = k_p
    env.rider_b_policies['trail'][0].k_v = k_v

    # Run with specific policy
    env.reset(leading_rider='Rider A', policy=0, policy_random_seed=4)
    step_count = 1
    current_action = rider_a_action
    while not env.complete():
        # Pulse every half_period actions
        if step_count % half_period == 0:
            if current_action == rider_a_action:
                current_action = 0
            else:
                current_action = rider_a_action

        # Step and increment step counter
        env.step(current_action)
        step_count += 1


    # Plot the results
    env.save_history(name='draft_policy')
    history = ['draft_policy.hist']
    title = 'Rider B Drafting: kp = ' + str(k_p) + ', kv = ' + str(k_v) + ', half period = ' + str(half_period)
    plot_action_separation(histories=history, titles=[title], save=True,
                        single_althete=False, sep_ylim=[-2,10],
                        suptitle='Action Separation Plot',
                        plot_control_vals=False,
                        image_save_name=folder + 'action_sep_' + str(run_index),
                        show=True)

# The following code was used to tune the drafting controller parameters.
# Note that to reproduce results, the msr_duelling environment will need to be edited
# back to form used in here:
# 1) Change the drafting policy class back to allowing passable parameters.
# 2) Change drafting policy to be drafting only (i.e. no sprint).

# # # Investigate step responses
# # Parameters
# rider_a_action = 1
# k_ps = [0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5, 5.0]
# k_vs = [0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5, 5.0]
# env = gym.make('msr-duel-fixed-v1')

# # Setup folder for log file.
# folder = os.path.dirname(os.path.abspath(__file__)) + '/run/'
# os.makedirs(folder, exist_ok=True)

# # Create the Learning log file to maintain run counting.
# log_filepath = folder + "results.txt"
# with open(log_filepath, 'a'):
#     os.utime(log_filepath, None)

# # Learning log tracks objective function call data.
# log = LearningLog(log_filepath)
# log.set_description("Optimizing drafting controller parameters: Step response for rider A action of " + \
#                     str(rider_a_action))
# log.add_table('Overshoot',
#             ['-', 'kp=0.5', 'kp=1.0', 'kp=1.5',
#             'kp=2.0', 'kp=2.5', 'kp=3.0', 'kp=3.5',
#             'kp=4.0', 'kp=4.5', 'kp=5.0'])
# log.add_table('Undershoot',
#             ['-', 'kp=0.5', 'kp=1.0', 'kp=1.5',
#             'kp=2.0', 'kp=2.5', 'kp=3.0', 'kp=3.5',
#             'kp=4.0', 'kp=4.5', 'kp=5.0'])
# log.add_table('Response time',
#             ['-', 'kp=0.5', 'kp=1.0', 'kp=1.5',
#             'kp=2.0', 'kp=2.5', 'kp=3.0', 'kp=3.5',
#             'kp=4.0', 'kp=4.5', 'kp=5.0'])

# # Perform all runs
# for i, k_v in enumerate(k_vs):
#     over_line_vals = []
#     under_line_vals = []
#     res_t_line_vals = []
#     for j, k_p in enumerate(k_ps):
#         run_index = i*len(k_ps) + j
#         over, under, res_t = analyse_control_step(env, k_p, k_v, rider_a_action, folder, run_index=run_index)
#         over_line_vals.append(over[0][0])
#         under_line_vals.append(under[0][0])
#         res_t_line_vals.append(res_t[0][0])
#     log.add_table_line('Overshoot',['kv=' + str(k_v)] + over_line_vals)
#     log.add_table_line('Undershoot',['kv=' + str(k_v)] + under_line_vals)
#     log.add_table_line('Response time',['kv=' + str(k_v)] + res_t_line_vals)
# log.report()
# log.dump()

# # # Investigate periodic response
# # Parameters
# k_p = 1.5
# k_v = 5.0
# half_periods = [1, 2, 4, 8, 16, 32]
# magnitudes = [4, 3, 2]
# env = gym.make('msr-duel-fixed-v1')

# # Setup folder for log file.
# folder = os.path.dirname(os.path.abspath(__file__)) + '/run/'
# os.makedirs(folder, exist_ok=True)

# # Create the Learning log file to maintain run counting.
# log_filepath = folder + "results.txt"
# with open(log_filepath, 'a'):
#     os.utime(log_filepath, None)

# # Learning log tracks objective function call data.
# log = LearningLog(log_filepath)
# log.set_description("Optimizing drafting controller parameters: Frequency response for rider A " + \
#                     "using varying frequencies and step sizes. Uisng values of kp = " + str(k_p) + \
#                     " and kv = " + str(k_v) + ".")
# log.add_table('Frequency response', ['Magnitude', '5 Hz', '2.5 Hz', '1.25 Hz', '0.625 Hz', '0.3125 Hz', '0.15625 Hz'])

# # Perform all runs
# for i, mag in enumerate(magnitudes):
#     for j, t in enumerate(half_periods):
#         run_index = i*len(half_periods) + j
#         analyse_control_freq(env, k_p, k_v, mag, folder, run_index=run_index, half_period=t)
#     # log.add_table_line('Overshoot',['kv=' + str(k_v)] + over_line_vals)
# # log.report()
# # log.dump()

# # # Final check on the values we want
# # Parameters
# k_p = 1.5
# k_v = 5.0
# half_periods = [1, 2, 4, 8, 16, 32]
# magnitudes = [4, 3, 2]
# env = gym.make('msr-duel-fixed-v1')

# # Setup folder for log file.
# folder = os.path.dirname(os.path.abspath(__file__)) + '/run/'
# os.makedirs(folder, exist_ok=True)

# # Step response check
# run_index=0
# for mag in magnitudes:
#     analyse_control_step(env, k_p, k_v, mag, folder, run_index=run_index)
#     run_index += 1

# # Frequency response checks
# for mag in magnitudes:
#     for period in half_periods:
#         analyse_control_freq(env, k_p, k_v, mag, folder, run_index=run_index, half_period=period)
#         run_index += 1
