import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
import gym
from learning_tools import plot_action_separation, plot_fatigue, plot_velocities
from stable_baselines import DQN

# Trained agent opponent environment
env = gym.make('msr-duel-fixed-v3')

for i in range(7):
    rider_a = DQN.load(env.agent_opponent_files[i])
    env.reset(leading_rider='Rider A', policy_random_seed=i)

    while not env.complete():
        rider_a_control = rider_a.predict(env.normalised_state())[0]
        env.step(rider_a_control)

    env.save_history(name='agent_policy')
    plot_action_separation(histories=['agent_policy.hist'],
                        suptitle='Agent Opponent: Action-Separation',
                        titles=['Rider B Leads, Agent Opponent'],
                        single_althete=False,
                        save=True,
                        image_save_name='action_separation_oppo_' + str(i),
                        show=False)

# # Memory run
# env = gym.make('msr-duel-fixed-v1')
# env.reset(leading_rider='Rider B', policy=0, policy_random_seed=11)
# while not env.complete():
#     env.step(3)