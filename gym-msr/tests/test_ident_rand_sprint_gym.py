"""Test the random sprint gym."""
import gym
import gym_msr
import pytest
import numpy as np

@pytest.fixture
def env():
    """Return a new instance of the Random Sprint Env."""
    return gym.make('msr-ident-win-v3')

def test_gym_init(env):
    """Test the initialisation of the gym."""
    assert env.race_distance == 750
    assert env.max_distance == 760
    assert env.max_speed == 30
    assert env.info == {'repetitions':0, 'total_reward':0}
    assert env.reward == 0

def test_random_sprint_init(env):
    """Test the function which randomises the start state."""
    # Check a number of times for random numbers
    rand_starts = []
    for _ in range(10):
        rider_a, rider_b = env.random_init()
        rand_starts.append(rider_a[0])

        # Check rider a
        assert rider_a[0] <= 500
        assert rider_a[0] >= 100
        assert rider_a[1] == 3
        assert rider_a[2] == 1
        # Check rider b
        assert rider_b[0] == rider_a[0] + 1
        assert rider_b[1] == 3
        assert rider_b[2] == 1

    # Check values are actually random
    assert np.var(rand_starts) > 0

    # Check initialisation of env
    assert env.state[0] <= 500
    assert env.state[0] >= 100
    assert env.state[1] == 3
    assert env.state[2] == 1
    assert env.state[3] == env.state[0] + 1
    assert env.state[4] == 3
    assert env.state[5] == 1

    # Test evaluation with set distances
    rider_a, rider_b = env.random_init(distance=150)
    assert rider_a[0] == 150
    assert rider_b[0] == 151
    with pytest.raises(ValueError):
        rider_a, rider_b = env.random_init(distance=99)

def test_complete(env):
    """Test the check on race completion."""
    # Should not be complete at initialisation
    assert not env.complete()

    # Check incomplete with given values
    env.state = [100, 3, 1, 749, 3, 1]
    assert not env.complete()

    # Check complete with given values
    env.state = [750, 3, 1, 749, 3, 1]
    assert env.complete()

def test_step(env):
    """Test the step of the simulator."""
    init_state = env.state
    norm_state, reward, done, info = env.step(1)

    # Check state is properly normalised
    for val in norm_state:
        assert val >= 0
        assert val <= 1
        # Check for array errors.
        assert not isinstance(val, np.ndarray)

    # Check reward and completion
    assert reward == 0
    assert not done

    # Check the distance values increase
    new_state = env.state
    print(new_state)
    assert new_state[0] > init_state[0]
    assert new_state[3] > init_state[3]

    # Check fatigue decreases
    assert new_state[2] < init_state[2]
    assert new_state[5] < init_state[5]
    assert len(env.fatigue_data) > 1

def test_reset(env):
    """Test the reset function."""
    init_state = env.state
    env.step(1)
    norm_state = env.reset()

    # Check values reset
    assert len(env.fatigue_data) == 1
    assert env.reward == 0

    # Check initial state has changed
    assert env.state[0] != init_state[0]
    assert env.state[3] != init_state[3]

    # Check normalised state
    assert norm_state[0] == env.state[0]/env.max_distance
    assert norm_state[3] == env.state[3]/env.max_distance

    # Test reset with given value
    env.reset(distance=200)
    assert env.state[0] == 200
    assert env.state[3] == 201

def test_reward(env):
    """Test the reward function"""
    # Never complete at init.
    assert env.reward_function() == 0

    # Rider A loses
    env.state = [750, 0, 0, 751, 0, 0]
    assert pytest.approx(env.reward_function(), 0.75)

    # Rider A wins
    env.state = [751, 0, 0, 750, 0, 0]
    assert env.reward_function() == 2

    # Test similarity of rewards between inits.
    for _ in range(10):
        env.reset()
        d_a_init = env.state[0]
        d_a = d_a_init + 1/3*(env.race_distance - d_a_init)
        env.state = [d_a, 0, 1, 751, 0, 1]

        reward = env.reward_function()
        assert pytest.approx(reward, 0.25)

def test_fatigue(env):
    """Test the fatigue measure."""
    d_a = env.state[0]
    d_b = env.state[3]
    fat_a, fat_b = env.fatigue(d_a, d_b)

    # Values should both be 1.0
    assert isinstance(fat_a, float)
    assert isinstance(fat_b, float)
    assert fat_a == 1.0
    assert fat_b == 1.0

def test_normalised_state(env):
    """Test the state normalisation."""
    state = env.state
    norm_state = env.normalised_state()

    # Normalised state needs to be numpy array
    assert isinstance(norm_state, np.ndarray)

    # Check distance values
    assert norm_state[0] == state[0]/env.max_distance
    assert norm_state[3] == state[3]/env.max_distance

    # Check speed values
    assert norm_state[1] == state[1]/env.max_speed
    assert norm_state[4] == state[4]/env.max_speed

    # Check fatigue
    assert norm_state[2] == state[2]
    assert norm_state[5] == state[5]






