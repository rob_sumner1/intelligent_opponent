"""Test pre-determined policies."""

import gym
import gym_msr
import pytest
import timeit
from gym_msr.envs.msr_policies import LeadJockey as LJPolicy
from gym_msr.envs.msr_policies import TrailDraft as TrDraft
from gym_msr.envs.msr_policies import MsrPolicy, MsrNetPolicy
from gym_msr.envs.msr_policies import TestPoint
import os
import numpy as np

# Keras policy
@pytest.fixture
def net_pol(scope="module"):
    """An unused Keras policy."""
    this_folder = os.path.dirname(os.path.abspath(__file__))
    agent_file = '/'.join(this_folder.split('/')[:-1]) +  \
        '/gym_msr/envs/data/agent_opponents/msr_net_policy_b.h5'
    return MsrNetPolicy(path=agent_file, rider='Rider B')

# Agent policy
@pytest.fixture
def net_pol2(scope="module"):
    """An unused agent policy."""
    this_folder = os.path.dirname(os.path.abspath(__file__))
    agent_file = this_folder + '/agents/msr_agent.zip'
    return MsrNetPolicy(path=agent_file, rider='Rider B')

def test_msr_net_init():
    """Test init of net policy."""
    # Fails on path error
    with pytest.raises(ValueError):
        MsrNetPolicy(path='not_a_real_path')

    # Fails on folder that isn't TF model
    this_folder = os.path.dirname(os.path.abspath(__file__))
    data_folder = '/'.join(this_folder.split('/')[:-1]) + '/gym_msr/envs/data/'
    agent_file = data_folder + 'agent_opponents/'
    with pytest.raises(ValueError):
        MsrNetPolicy(path=agent_file)

    # Opens TF model
    pol = MsrNetPolicy(path=agent_file+'msr_net_policy_a.h5')
    assert pol.rider == 'Rider B'
    assert pol.agent_type == 'Keras'

    # Fails on something that hasn't got extension
    this_folder = os.path.dirname(os.path.abspath(__file__))
    agent_file = this_folder + '/agents/msr_agent'
    with pytest.raises(ValueError):
        MsrNetPolicy(path=agent_file)

    # Opens .zip model
    pol = MsrNetPolicy(path=agent_file+'.zip')
    assert pol.rider == 'Rider B'
    assert pol.agent_type == 'Baselines'

    # Throw error with wrong rider
    with pytest.raises(ValueError):
        MsrNetPolicy(path=agent_file+'msr_net_policy_a.h5', rider='NOTARIDER')

def test_msr_net_predict(net_pol, net_pol2):
    """Test predict method for NN policy."""
    # # Test keras policy
    # Sprint when velocity difference >= 1.25
    assert net_pol.predict([0.0, 1.5, 1.0, 0.0, 0.0, 1.0]) == 1

    # Sprint when gap >= 15m
    assert net_pol.predict([0.0, 0, 1.0, 15.0, 0.0, 1.0]) == 1

    # Sprint at end of race
    assert net_pol.predict([730.0, 15, 1.0, 735.0, 15.0, 1.0]) == 1

    # Check random points for range of actions
    for _ in range(100):
        rand = np.random.rand(4)
        state = [rand[0]*750, rand[1]*30, rand[2],
                rand[0]*750 - 5, rand[1]*29.9, rand[3]]
        control = net_pol.predict(state)
        assert control <= 1
        assert control >= 0

    # # Test baselines policy
    # Sprint when velocity difference >= 1.25
    assert net_pol2.predict([0.0, 1.5, 1.0, 0.0, 0.0, 1.0]) == 1

    # Sprint when gap >= 15m
    assert net_pol2.predict([0.0, 0, 1.0, 15.0, 0.0, 1.0]) == 1

    # Sprint at end of race
    assert net_pol2.predict([730.0, 15, 1.0, 714.0, 15.0, 1.0]) == 1

    # Check random points for range of actions
    for _ in range(100):
        rand = np.random.rand(4)
        state = [rand[0]*750, rand[1]*30, rand[2],
                rand[0]*750 - 5, rand[1]*29.9, rand[3]]
        control = net_pol2.predict(state)
        assert control <= 1
        assert control >= 0

def test_evaluate(net_pol, net_pol2):
    """Test evaluation function for policy."""

    # Check random points for range of actions:
    rand = np.random.rand(4)
    state = [rand[0]*750, rand[1]*30, rand[2],
            rand[0]*750 - 5, rand[1]*29.9, rand[3]]
    dist = net_pol.evaluate(state)
    assert len(dist) == 10

    # Check random points for range of actions
    rand = np.random.rand(4)
    state = [rand[0]*750, rand[1]*30, rand[2],
            rand[0]*750 - 5, rand[1]*29.9, rand[3]]
    dist = net_pol2.evaluate(state)
    assert len(dist) == 5


# Test the MsrPolicy class.
@pytest.fixture
def pol():
    """An unused policy."""
    return MsrPolicy(rider='Rider B')

@pytest.fixture
def old_pol():
    """A policy which has changed parameters."""
    pol = MsrPolicy(rider='Rider B')
    # Reset to TestPoint('lead', 200, [100],[25], [50], [15])
    pol.reset(random_seed=4)

    # Predict two steps into drop
    pol.predict([49, 1, 1, 50, 1, 1], 1)
    pol.predict([49, 1, 1, 51, 1, 1], 1)
    return pol

def test_msrpol_init(pol):
    """Test initialisation of policy."""
    with pytest.raises(ValueError):
        MsrPolicy(rider='RiDeR B')
    assert pol.event_steps_completed == 0
    assert pol.current_event_index == -1
    assert not pol.sprinting

    # Check on randomised policy points
    assert pol.n_events == len(pol.event_descriptions)
    assert pol.n_events == len(pol.event_distance_lookup)
    assert pol.n_events in [0, 1, 2, 3, 4, 5, 6]

def test_msrpol_reset(old_pol):
    """Test the reset function for msr policy."""
    assert old_pol.event_steps_completed != 0
    assert old_pol.current_event_index == 0

    # Reset and recheck.
    old_pol.reset()
    assert old_pol.event_steps_completed == 0
    assert old_pol.current_event_index == -1

    # Should reject non-int values
    with pytest.raises(ValueError):
        old_pol.reset('int')
    # Should reject out of range
    with pytest.raises(ValueError):
        old_pol.reset(random_seed=len(old_pol.test_points)+1)

    # Test reset with sprint distance
    old_pol.reset(sprint_distance=100)
    assert old_pol.lead_sprint_start_distance == 100
    assert old_pol.trail_sprint_start_distance == 100

    with pytest.raises(ValueError):
        old_pol.reset(sprint_distance=760)

def test_msrpol_randomise(pol):
    """Test randomise function for msr policy."""
    # Test seeded reset
    for i in range(len(pol.test_points)):
        test_point = pol.test_points[i]
        desc, lookup = test_point.get_description()
        pol.reset(random_seed=i)

        # Trackers should be reset
        assert pol.event_steps_completed == 0
        assert pol.current_event_index == -1

        # Events should match test point
        assert pol.n_events == len(desc)
        assert pol.trail_sprint_start_distance == test_point.trail_sprint_start_distance
        assert pol.lead_sprint_start_distance == test_point.lead_sprint_start_distance
        assert all([a == b for a, b in zip(pol.event_distance_lookup, lookup)])
        assert all([a == b for a, b in zip(pol.event_descriptions, desc)])

    # Test truly random inits.
    for i in range(100):
        pol.reset()
        assert pol.event_steps_completed == 0
        assert pol.current_event_index == -1
        assert pol.n_events in [0, 1, 2, 3, 4, 5, 6]
        assert pol.lead_sprint_start_distance <= pol.max_lead_sprint_distance
        assert pol.lead_sprint_start_distance >= pol.min_lead_sprint_distance
        assert pol.trail_sprint_start_distance <= pol.max_trail_sprint_distance
        assert pol.trail_sprint_start_distance >= pol.min_trail_sprint_distance

        # Check the drop positions.
        for j in range(pol.n_events):
            event_bin = pol.lead_sprint_start_distance/pol.n_events
            assert pol.event_descriptions[j][1] <= event_bin + j*event_bin

def test_msrpol_get_current_event(pol):
    """Test function which returns the current event."""
    # Reset to TestPoint(389, 600, [100, 290], [40, 10], [200], [30])
    pol.reset(random_seed=3)

    # Before event happens
    event_duration, event_type = pol.get_current_event([1,1,1])
    assert event_duration == 0
    assert event_type == None

    # At first event
    pol.event_steps_completed = 10
    event_duration, event_type = pol.get_current_event([100,1,1])
    assert event_duration == 40
    assert event_type == 'jump'
    assert pol.event_steps_completed == 0 # Event steps reset
    pol.event_steps_completed = 10 # Before event complete
    event_duration, event_type = pol.get_current_event([100,1,1])
    assert event_duration == 40
    assert event_type == 'jump'
    assert pol.event_steps_completed == 10 # Event steps not reset
    pol.event_steps_completed = 40
    event_duration, event_type = pol.get_current_event([100,1,1])
    assert event_duration == 40
    assert event_type == 'jump'
    assert pol.event_steps_completed == 40 # Event steps not reset

    # At second event
    event_duration, event_type = pol.get_current_event([200,1,1])
    assert event_duration == 30
    assert event_type == 'drop'
    assert pol.event_steps_completed == 0 # Event steps reset
    pol.event_steps_completed = 20 # Before event complete
    event_duration, event_type = pol.get_current_event([200,1,1])
    assert event_duration == 30
    assert event_type == 'drop'
    assert pol.event_steps_completed == 20 # Event steps not reset
    pol.event_steps_completed = 30
    event_duration, event_type = pol.get_current_event([200,1,1])
    assert event_duration == 30
    assert event_type == 'drop'
    assert pol.event_steps_completed == 30 # Event steps not reset

    # Reset to another policy test point
    # TestPoint(700, 700, [100, 425],  [30, 15], [250, 350], [50, 10])
    pol.reset(random_seed=8)

    # Check jumping straight to 3rd event
    event_duration, event_type = pol.get_current_event([350,1,1])
    assert event_duration == 10
    assert event_type == 'drop'
    assert pol.event_steps_completed == 0 # Event steps reset
    pol.event_steps_completed = 10

    # Now jump back to second
    event_duration, event_type = pol.get_current_event([100,1,1])
    assert event_duration == 30
    assert event_type == 'jump'
    assert pol.event_steps_completed == 0 # Event steps reset

    # Reset to drafting policy point
    # TestPoint('trail', 480, 700, [],  [], [], [])
    pol.reset(random_seed=0)
    event_duration, event_type = pol.get_current_event([350,1,1])
    assert event_duration == 0
    assert event_type == None

def test_msrpol_lead_predict(pol):
    """Test the policy predict method."""
    # Test fixed policy points
    for test in range(len(pol.test_points)):
        pol.reset(random_seed=test)
        test_point = pol.test_points[test]
        lead_sprint_dist = test_point.lead_sprint_start_distance
        trail_sprint_dist = test_point.trail_sprint_start_distance
        event_descriptions, _ = test_point.get_description()

        # Initial value should be 0.5
        assert pol.predict([1, 1, 1, 2, 1, 1], 0.1) == 0.5

        for event in range(pol.n_events):
            # Event parameters
            event_type = event_descriptions[event][0]
            event_distance = event_descriptions[event][1]
            event_duration = event_descriptions[event][2]

            # Before the event, should return 0.5
            assert pol.current_event_index != event
            assert pol.predict([0.9*event_distance-10, 1, 1, 0.9*event_distance, 1, 1], 0.1) == 0.5
            assert pol.current_event_index != event

            # First step for event should reset step number
            exer = pol.predict([event_distance-10, 1, 1, event_distance, 1, 1], 0.1)
            assert pol.event_steps_completed == 1

            # During event, should return correct value
            for _ in range(event_duration-1):
                exer = pol.predict([event_distance-10, 1, 1, event_distance, 1, 1], 0.1)
                if event_type == 'jump':
                    assert exer == 1
                else:
                    assert exer == 0.1

            # After event, should again return 0.5
            assert pol.predict([event_distance-10, 1, 1, event_distance, 1, 1], 0.1) == 0.5
            assert pol.current_event_index == event
            assert pol.event_steps_completed == event_duration

            # Rider A in front, should return drafting controller value
            # Clipped control values returned
            # Reset last values for draft controller and policy to negate smoothing.
            pol.draft_controller.last_exertion_value = 0
            pol.last_exertion_value = 0
            assert pol.predict([2, 1, 1, 1, 1, 1], 0.5) == max(0, (1 - 1.5 * 1)) * 0.5
            pol.draft_controller.last_exertion_value = 0
            pol.last_exertion_value = 0
            assert pol.predict([5, 1, 1, 1, 1, 1], 0.5) == min(1, (1 + 1.5 * 1)) * 0.5
            pol.draft_controller.last_exertion_value = 0
            pol.last_exertion_value = 0
            assert pol.predict([3.5, 1, 1, 1, 1.5, 1], 0.5) == max(0, (1 - 5 * 0.5)) * 0.5
            pol.draft_controller.last_exertion_value = 0
            pol.last_exertion_value = 0
            assert pol.predict([3.5, 1, 1, 1, 0.5, 1], 0.5) == min(1, (1 + 5 * 0.5)) * 0.5
            pol.draft_controller.last_exertion_value = 0
            pol.last_exertion_value = 0
            assert pol.predict([3.5, 1, 1, 1, 1, 1], 0.5) == 0.5 * 0.5

            # Rider A in front, after sprint distance should return sprint
            assert pol.predict([1.1*trail_sprint_dist, 1, 1, trail_sprint_dist, 1, 1], 0.5) == 1.0
            assert pol.sprinting
            # Should continue sprinting now
            assert pol.predict([1.1*trail_sprint_dist, 1, 1, 1.2*trail_sprint_dist, 1, 1], 0.5) == 1.0

            # Manually Reset sprinting variable
            pol.sprinting = False

        # Just before sprint is 0.5
        assert pol.predict([0.98*lead_sprint_dist, 1, 1, 0.99*lead_sprint_dist, 1, 1], 0.1) == 0.5

        # At sprint should be 1
        assert pol.predict([0.98*lead_sprint_dist, 1, 1, lead_sprint_dist, 1, 1], 0.1) == 1.0
        assert pol.sprinting

        # Test sprint when gap becomes large
        pol.sprinting = False
        assert pol.predict([0.99*lead_sprint_dist-14, 1, 1, 0.99*lead_sprint_dist, 1, 1], 0.5) != 1.0
        assert pol.predict([0.99*lead_sprint_dist-16, 1, 1, 0.99*lead_sprint_dist, 1, 1], 0.5) == 1.0
        assert pol.sprinting

    # Test random points
    for test in range(1000):
        pol.reset()
        lead_sprint_dist = pol.lead_sprint_start_distance
        trail_sprint_dist = pol.trail_sprint_start_distance
        event_descriptions = pol.event_descriptions

        # Initial value should be 0.5 (or 0.1 if its a v. early drop)
        # Early events can occur, but the tests below will fail, so we
        # skip the edge cases where event distance < 1 here.
        if pol.n_events > 0 and pol.event_descriptions[0][1] <= 1:
            if pol.event_descriptions[0][0] == 'drop':
                assert pol.predict([0, 1, 1, 1, 1, 1], 0.1) == 0.1
            else:
                assert pol.predict([0, 1, 1, 1, 1, 1], 0.1) == 1.0
            # Skip this edge case for simplicity.
            continue
        else:
            assert pol.predict([0, 1, 1, 1, 1, 1], 0.1) == 0.5

        for event in range(pol.n_events):
            # Event parameters
            event_type = event_descriptions[event][0]
            event_distance = event_descriptions[event][1]
            event_duration = event_descriptions[event][2]

            # Before the event, should return 0.5
            assert pol.current_event_index != event
            assert pol.predict([0.9*event_distance-10, 1, 1, 0.9*event_distance, 1, 1], 0.1) == 0.5
            assert pol.current_event_index != event

            # First step for event should reset step number
            exer = pol.predict([event_distance-10, 1, 1, event_distance, 1, 1], 0.1)
            assert pol.event_steps_completed == 1

            # During event, should return correct value
            for _ in range(event_duration-1):
                exer = pol.predict([event_distance-10, 1, 1, event_distance, 1, 1], 0.1)
                if event_type == 'jump':
                    assert exer == 1
                else:
                    assert exer == 0.1

            # After event, should again return 0.5
            assert pol.predict([event_distance-10, 1, 1, event_distance, 1, 1], 0.1) == 0.5
            assert pol.current_event_index == event
            assert pol.event_steps_completed == event_duration

            # Rider A in front, should return drafting controller value
            # Clipped controle values returned
            # Reset last values for draft controller and policy to negate smoothing.
            pol.draft_controller.last_exertion_value = 0
            pol.last_exertion_value = 0
            assert pol.predict([2, 1, 1, 1, 1, 1], 0.5) == max(0, (1 - 1.5 * 1)) * 0.5
            pol.draft_controller.last_exertion_value = 0
            pol.last_exertion_value = 0
            assert pol.predict([5, 1, 1, 1, 1, 1], 0.5) == min(1, (1 + 1.5 * 1)) * 0.5
            pol.draft_controller.last_exertion_value = 0
            pol.last_exertion_value = 0
            assert pol.predict([3.5, 1, 1, 1, 1.5, 1], 0.5) == max(0, (1 - 5 * 0.5)) * 0.5
            pol.draft_controller.last_exertion_value = 0
            pol.last_exertion_value = 0
            assert pol.predict([3.5, 1, 1, 1, 0.5, 1], 0.5) == min(1, (1 + 5 * 0.5)) * 0.5
            pol.draft_controller.last_exertion_value = 0
            pol.last_exertion_value = 0
            assert pol.predict([3.5, 1, 1, 1, 1, 1], 0.5) == 0.5 * 0.5

            # Rider A in front, after sprint distance should return sprint
            assert pol.predict([trail_sprint_dist+10, 1, 1, trail_sprint_dist, 1, 1], 0.5) == 1.0
            assert pol.sprinting
            # Should continue sprinting now
            assert pol.predict([trail_sprint_dist, 1, 1, trail_sprint_dist+10, 1, 1], 0.5) == 1.0

            # Manually Reset sprinting variable
            pol.sprinting = False

        # Just before sprint is 0.5
        assert pol.predict([0.98*lead_sprint_dist, 1, 1, 0.99*lead_sprint_dist, 1, 1], 0.1) == 0.5

        # At sprint should be 1
        assert pol.predict([0.98*lead_sprint_dist, 1, 1, lead_sprint_dist, 1, 1], 0.1) == 1.0
        assert pol.sprinting

        # Test sprint when gap becomes large
        pol.sprinting = False
        assert pol.predict([0.99*lead_sprint_dist-14, 1, 1, 0.99*lead_sprint_dist, 1, 1], 0.5) != 1.0
        assert pol.predict([0.99*lead_sprint_dist-16, 1, 1, 0.99*lead_sprint_dist, 1, 1], 0.5) == 1.0
        assert pol.sprinting

def test_msr_draft_predict(pol):
    """Test drafting implementation for msr policy."""

    # Test fixed points.
    for test in range(len(pol.test_points)):
        # Reset policy
        pol.reset(random_seed=test)
        trail_sprint_dist = pol.trail_sprint_start_distance

        # Before sprint & at perfect draft point, should return eq. exertion
        pol.draft_controller.last_exertion_value = 0
        assert pol.predict([0.9*trail_sprint_dist, 1, 1, 0.9*trail_sprint_dist-2.5, 1, 1], 0.1) == 0.1*0.5
        assert pol.draft_controller.last_exertion_value == 0.1*0.5

        # Before sprint, rider B trailing & error in position
        # Returns clipped controller value (max value of 1)
        pol.draft_controller.last_exertion_value = 0
        assert pol.predict([0.9*trail_sprint_dist, 1, 1, 0.9*trail_sprint_dist-4, 1, 1], 0.5) == \
            min(1, (1 + 1.5*1.5))*0.5

        # Before sprint, rider B trailing & error in velocity
        # Returns clipped controller value (max value of 1)
        pol.draft_controller.last_exertion_value = 0
        assert pol.predict([0.9*trail_sprint_dist, 1.5, 1, 0.9*trail_sprint_dist-2.5, 1, 1], 0.5) == \
            min(1, (1 + 5*0.5))*0.5

        # Before sprint, rider B trailing & errors in velocity & position
        # Returns clipped controller value (max value of 1)
        pol.draft_controller.last_exertion_value = 0
        assert pol.predict([0.9*trail_sprint_dist, 1.5, 1, 0.9*trail_sprint_dist-3.5, 1, 1], 0.5) == \
            min(1, (1 + 1.5*1 + 5*0.5))*0.5

        # Before sprint, rider B trailling & errors in velocity & position
        # Returns clipped controller value (min value of 0)
        pol.draft_controller.last_exertion_value = 0
        assert pol.predict([0.9*trail_sprint_dist, 1, 1, 0.9*trail_sprint_dist-1.5, 1.5, 1], 0.5) == \
            max(0, (1 - 1.5*1 - 5*0.5))*0.5

        # Before sprint, rider B trailing & stationary
        # Should return 0.5
        pol.draft_controller.last_exertion_value = 0
        assert pol.predict([0.9*trail_sprint_dist, 0.1, 1, 0.9*trail_sprint_dist-1.5, 0.1, 1], 0.5) == 0.5

        # At sprint distance, should sprint
        pol.draft_controller.last_exertion_value = 0
        assert pol.predict([1.1*trail_sprint_dist, 1, 1, trail_sprint_dist, 1, 1], 0.5) == 1.0
        assert pol.sprinting

    # Test random inits
    for _ in range(1000):
        # Reset policy
        pol.reset()
        trail_sprint_dist = pol.trail_sprint_start_distance

        # Before sprint & at perfect draft point, should return eq. exertion
        pol.draft_controller.last_exertion_value = 0
        assert pol.predict([0.9*trail_sprint_dist, 1, 1, 0.9*trail_sprint_dist-2.5, 1, 1], 0.1) == 0.1*0.5
        assert pol.draft_controller.last_exertion_value == 0.1*0.5

        # Before sprint, rider B trailing & error in position
        # Returns clipped controller value (max value of 1)
        pol.draft_controller.last_exertion_value = 0
        assert pol.predict([0.9*trail_sprint_dist, 1, 1, 0.9*trail_sprint_dist-4, 1, 1], 0.5) == \
            min(1, (1 + 1.5*1.5))*0.5

        # Before sprint, rider B trailing & error in velocity
        # Returns clipped controller value (max value of 1)
        pol.draft_controller.last_exertion_value = 0
        assert pol.predict([0.9*trail_sprint_dist, 1.5, 1, 0.9*trail_sprint_dist-2.5, 1, 1], 0.5) == \
            min(1, (1 + 5*0.5))*0.5

        # Before sprint, rider B trailing & errors in velocity & position
        # Returns clipped controller value (max value of 1)
        pol.draft_controller.last_exertion_value = 0
        assert pol.predict([0.9*trail_sprint_dist, 1.5, 1, 0.9*trail_sprint_dist-3.5, 1, 1], 0.5) == \
            min(1, (1 + 1.5*1 + 5*0.5))*0.5

        # Before sprint, rider B trailling & errors in velocity & position
        # Returns clipped controller value (min value of 0)
        pol.draft_controller.last_exertion_value = 0
        assert pol.predict([0.9*trail_sprint_dist, 1, 1, 0.9*trail_sprint_dist-1.5, 1.5, 1], 0.5) == \
            max(0, (1 - 1.5*1 - 5*0.5))*0.5

        # Before sprint, rider B trailing & stationary
        # Should return 0.5
        pol.draft_controller.last_exertion_value = 0
        assert pol.predict([0.9*trail_sprint_dist, 0.1, 1, 0.9*trail_sprint_dist-1.5, 0.1, 1], 0.5) == 0.5

        # At sprint distance, should sprint
        pol.draft_controller.last_exertion_value = 0
        assert pol.predict([1.1*trail_sprint_dist, 1, 1, trail_sprint_dist, 1, 1], 0.5) == 1.0
        assert pol.sprinting


# Test the test point class
@pytest.fixture
def trail_test():
    return TestPoint(500, 700)

@pytest.fixture
def lead_test():
    return TestPoint(500, 700, [200, 300], [50, 50], [100, 400], [25, 25])

def test_point_init():
    """Test initialisation of test point"""
    with pytest.raises(ValueError):
        # Wrong sprint distance
        TestPoint(755, 700)

    with pytest.raises(ValueError):
        # Wrong jump distance
        TestPoint(500, 700, [-10])

    with pytest.raises(ValueError):
        # Incorrect duration
        TestPoint(500, 700, [100], [51])

    with pytest.raises(ValueError):
        # Mismatch in distance/duration
        TestPoint(500, 700, [100], [])

    with pytest.raises(ValueError):
        # Wrong drop distance
        TestPoint(500, 700, [], [], [-10])

    with pytest.raises(ValueError):
        # Wrong drop duration
        TestPoint(500, 700, [], [], [100], [-10])

    with pytest.raises(ValueError):
        # Mismatch in distance/duration
        TestPoint(500, 700, [], [], [100], [])

    # Check values set
    tp = TestPoint(500, 700, [100], [50], [200, 300], [20, 30])
    assert tp.trail_sprint_start_distance == 500
    assert tp.lead_sprint_start_distance == 700
    assert tp.n_jumps == 1
    assert tp.jump_distances[0] == 100
    assert tp.jump_durations[0] == 50
    assert tp.n_drops == 2

def test_point_descriptions(lead_test, trail_test):
    """Test function which returns the event descriptions."""
    # Leading test point
    description, lookup = lead_test.get_description()
    # Event descriptions
    event_description = [('drop', 100, 25), ('jump', 200, 50),
                        ('jump', 300, 50), ('drop', 400, 25)]
    assert all([a == b for a, b in zip(event_description, description)])
    # Event distance lookups
    event_lookup = [ (100, 200), (200, 300), (300, 400), (400, 750)]
    assert all([a == b for a, b in zip(event_lookup, lookup)])

    # Trailing policy
    description, lookup = trail_test.get_description()
    # Event descriptions - should be empty
    event_description = []
    assert all([a == b for a, b in zip(event_description, description)])
    # Event distance lookups
    assert all([a == b for a, b in zip([(0,750)], lookup)])

# Test the lead / jockeying policy.
@pytest.fixture
def LJ():
    return LJPolicy()

def test_lead_jockey_init(LJ):
    """Test the LeadJockey policy."""
    assert len(LJ.test_points) == 12
    assert LJ.n_events in [0, 1, 2, 3, 4, 5, 6]
    assert LJ.sprint_start_distance >= 100
    assert LJ.sprint_start_distance <= 550

def test_lead_jockey_randomise(LJ):
    """Test the LeadJocket randomise function."""
    for i in range(len(LJ.test_points)):
        test_point = LJ.test_points[i]
        LJ.randomise(random_seed=i)
        assert LJ.events_completed == 0
        assert LJ.event_steps_completed == 0
        assert LJ.n_events == len(test_point[1])
        assert LJ.sprint_start_distance == test_point[0]

    # Should reject non-int values
    with pytest.raises(ValueError):
        LJ.randomise('int')
    # Should reject out of range
    with pytest.raises(ValueError):
        LJ.randomise(random_seed=len(LJ.test_points)+1)

    # Test truly random inits.
    for i in range(100):
        LJ.randomise()
        assert LJ.events_completed == 0
        assert LJ.event_steps_completed == 0
        assert LJ.n_events in [0, 1, 2, 3, 4, 5, 6]
        assert LJ.sprint_start_distance >= 100
        assert LJ.sprint_start_distance <= 550
        # Check the drop positions.
        for j in range(LJ.n_events):
            event_bin = LJ.sprint_start_distance/LJ.n_events
            assert LJ.event_descriptions[j][1] <= event_bin + j*event_bin

def test_lead_jockey_predict(LJ):
    """Test the policy predict method."""
    # Test fixed policy points.
    for test in range(len(LJ.test_points)):
        LJ.randomise(random_seed=test)
        test_point = LJ.test_points[test]
        sprint_dist = test_point[0]
        event_descriptions = test_point[1]

        # Initial value should be 0.5
        assert LJ.predict([1, 1, 1, 2, 1, 1], 0.1) == 0.5

        for event in range(LJ.n_events):
            # Event parameters
            event_type = event_descriptions[event][0]
            event_distance = event_descriptions[event][1]
            event_duration = event_descriptions[event][2]

            # Rider B in front, before drop, should return 0.5
            assert LJ.predict([0.8*event_distance, 1, 1, 0.9*event_distance, 1, 1], 0.1) == 0.5

            # Rider A in front, should return drafting controller value
            # Clipped controle values returned
            # Reset last values for draft controller and policy to negate smoothing.
            LJ.draft_controller.last_exertion_value = 0
            LJ.last_exertion_value = 0
            assert LJ.predict([2, 1, 1, 1, 1, 1], 0.5) == max(0, (1 - 1.5 * 1)) * 0.5
            LJ.draft_controller.last_exertion_value = 0
            LJ.last_exertion_value = 0
            assert LJ.predict([5, 1, 1, 1, 1, 1], 0.5) == min(1, (1 + 1.5 * 1)) * 0.5
            LJ.draft_controller.last_exertion_value = 0
            LJ.last_exertion_value = 0
            assert LJ.predict([3.5, 1, 1, 1, 1.5, 1], 0.5) == max(0, (1 - 5 * 0.5)) * 0.5
            LJ.draft_controller.last_exertion_value = 0
            LJ.last_exertion_value = 0
            assert LJ.predict([3.5, 1, 1, 1, 0.5, 1], 0.5) == min(1, (1 + 5 * 0.5)) * 0.5
            LJ.draft_controller.last_exertion_value = 0
            LJ.last_exertion_value = 0
            assert LJ.predict([3.5, 1, 1, 1, 1, 1], 0.5) == 0.5 * 0.5

            # At event, should return appropriate value
            for step in range(event_duration):
                if event_type == 'drop':
                    assert LJ.predict([0.99*event_distance, 1, 1, event_distance, 1, 1], 0.1) == 0.1
                else:
                    assert LJ.predict([0.99*event_distance, 1, 1, event_distance, 1, 1], 0.1) == 1.0
                assert LJ.event_steps_completed == step+1

            # Then moves on (returns 0.5)
            assert LJ.predict([0.99*event_distance, 1, 1, event_distance, 1, 1], 0.1) == 0.5
            assert LJ.events_completed == event + 1
            assert LJ.event_steps_completed == 0

        # Just before sprint is 0.5
        assert LJ.predict([0.98*sprint_dist, 1, 1, 0.99*sprint_dist, 1, 1], 0.1) == 0.5

        # At sprint should be 1
        assert LJ.predict([0.98*sprint_dist, 1, 1, sprint_dist, 1, 1], 0.1) == 1.0

    # Test random points
    for test in range(1000):
        LJ.randomise()
        sprint_dist = LJ.sprint_start_distance

        # Initial value should be 0.5 (or 0.1 if its a v. early drop)
        # Early events can occur, but the tests below will fail, so we
        # skip the edge cases where event distance < 1 here.
        if LJ.n_events > 0 and LJ.event_descriptions[0][1] <= 1:
            if LJ.event_descriptions[0][0] == 'drop':
                assert LJ.predict([0, 1, 1, 1, 1, 1], 0.1) == 0.1
            else:
                assert LJ.predict([0, 1, 1, 1, 1, 1], 0.1) == 1.0
            # Skip this edge case for simplicity.
            continue
        else:
            assert LJ.predict([0, 1, 1, 1, 1, 1], 0.1) == 0.5

        for event in range(LJ.n_events):
            # Event parameters
            event_type = LJ.event_descriptions[event][0]
            event_distance = LJ.event_descriptions[event][1]
            event_duration = LJ.event_descriptions[event][2]

            # Rider B in front, before drop, should return 0.5
            assert LJ.predict([0.8*event_distance, 1, 1, 0.9*event_distance, 1, 1], 0.1) == 0.5

            # Rider A in front, should return drafting controller value
            # Clipped controle values returned
            # Reset last values for draft controller and policy to negate smoothing.
            LJ.draft_controller.last_exertion_value = 0
            LJ.last_exertion_value = 0
            assert LJ.predict([2, 1, 1, 1, 1, 1], 0.5) == max(0, (1 - 1.5 * 1)) * 0.5
            LJ.draft_controller.last_exertion_value = 0
            LJ.last_exertion_value = 0
            assert LJ.predict([5, 1, 1, 1, 1, 1], 0.5) == min(1, (1 + 1.5 * 1)) * 0.5
            LJ.draft_controller.last_exertion_value = 0
            LJ.last_exertion_value = 0
            assert LJ.predict([3.5, 1, 1, 1, 1.5, 1], 0.5) == max(0, (1 - 5 * 0.5)) * 0.5
            LJ.draft_controller.last_exertion_value = 0
            LJ.last_exertion_value = 0
            assert LJ.predict([3.5, 1, 1, 1, 0.5, 1], 0.5) == min(1, (1 + 5 * 0.5)) * 0.5
            LJ.draft_controller.last_exertion_value = 0
            LJ.last_exertion_value = 0
            assert LJ.predict([3.5, 1, 1, 1, 1, 1], 0.5) == 0.5 * 0.5

            # At event, should return appropriate value
            for step in range(event_duration):
                if event_type == 'drop':
                    assert LJ.predict([0.99*event_distance, 1, 1, event_distance, 1, 1], 0.1) == 0.1
                else:
                    assert LJ.predict([0.99*event_distance, 1, 1, event_distance, 1, 1], 0.1) == 1.0
                assert LJ.event_steps_completed == step+1

            # Then moves on (returns 0.5)
            assert LJ.predict([0.99*event_distance, 1, 1, event_distance, 1, 1], 0.1) == 0.5
            assert LJ.events_completed == event + 1
            assert LJ.event_steps_completed == 0

        # Just before sprint is 0.5
        assert LJ.predict([0.98*sprint_dist, 1, 1, 0.99*sprint_dist, 1, 1], 0.1) == 0.5

        # At sprint should be 1
        assert LJ.predict([0.98*sprint_dist, 1, 1, sprint_dist, 1, 1], 0.1) == 1.0


# Test the trailing/Draft policy.
@pytest.fixture
def TD():
    return TrDraft()

def test_trail_draft_init(TD):
    """Test the LeadJockey policy."""
    assert len(TD.test_points) == 10
    assert TD.sprint_start_distance > TD.min_sprint_distance
    assert TD.sprint_start_distance <= TD.max_sprint_distance

def test_trail_draft_randomise(TD):
    """Test the LeadJocket randomise function."""
    for i in range(len(TD.test_points)):
        test_point = TD.test_points[i]
        TD.randomise(random_seed=i)
        assert TD.sprint_start_distance == test_point[0]

    # Should reject non-int values
    with pytest.raises(ValueError):
        TD.randomise('int')
    # Should reject out of range
    with pytest.raises(ValueError):
        TD.randomise(random_seed=len(TD.test_points)+1)

    # Test truly random inits.
    for i in range(10):
        TD.randomise()
        assert TD.sprint_start_distance >= TD.min_sprint_distance
        assert TD.sprint_start_distance <= TD.max_sprint_distance

def test_trail_draft_predictTD(TD):
    """Test the policy predict method."""
    # Test fixed policy points.
    for test in range(len(TD.test_points)):
        TD.randomise(random_seed=test)
        sprint_dist = TD.test_points[test][0]

        # Note for all exertion values the vals returned are smoothed
        # We reset last value to 0 manually, so value will be 0.5*controller equation result

        # Before sprint & at perfect draft point, should return eq. exertion
        TD.draft_controller.last_exertion_value = 0
        assert TD.predict([0.9*sprint_dist, 1, 1, 0.9*sprint_dist-2.5, 1, 1], 0.1) == 0.1*0.5
        assert TD.draft_controller.last_exertion_value == 0.1*0.5

        # Before sprint, rider B trailing & error in position
        # Returns clipped controller value (max value of 1)
        TD.draft_controller.last_exertion_value = 0
        assert TD.predict([0.9*sprint_dist, 1, 1, 0.9*sprint_dist-4, 1, 1], 0.5) == \
            min(1, (1 + 1.5*1.5))*0.5

        # Before sprint, rider B trailing & error in velocity
        # Returns clipped controller value (max value of 1)
        TD.draft_controller.last_exertion_value = 0
        assert TD.predict([0.9*sprint_dist, 1.5, 1, 0.9*sprint_dist-2.5, 1, 1], 0.5) == \
            min(1, (1 + 5*0.5))*0.5

        # Before sprint, rider B trailing & errors in velocity & position
        # Returns clipped controller value (max value of 1)
        TD.draft_controller.last_exertion_value = 0
        assert TD.predict([0.9*sprint_dist, 1.5, 1, 0.9*sprint_dist-3.5, 1, 1], 0.5) == \
            min(1, (1 + 1.5*1 + 5*0.5))*0.5

        # Before sprint, rider B trailling & errors in velocity & position
        # Returns clipped controller value (min value of 0)
        TD.draft_controller.last_exertion_value = 0
        assert TD.predict([0.9*sprint_dist, 1, 1, 0.9*sprint_dist-1.5, 1.5, 1], 0.5) == \
            max(0, (1 - 1.5*1 - 5*0.5))*0.5

        # Before sprint, rider B trailing & stationary
        # Should return 0.5
        TD.draft_controller.last_exertion_value = 0
        assert TD.predict([0.9*sprint_dist, 0.1, 1, 0.9*sprint_dist-1.5, 1.5, 1], 0.5) == 0.5

        # At sprint distance, should sprint
        TD.draft_controller.last_exertion_value = 0
        assert TD.predict([1.1*sprint_dist, 1, 1, sprint_dist, 1, 1], 0.5) == 1.0

        # Before sprint, rider B leading - Should go low power
        TD.draft_controller.last_exertion_value = 0.1
        assert TD.predict([0.8*sprint_dist, 1, 1, 0.9*sprint_dist, 1, 1], 0.1) == 0.5

    # Test random inits
    for test in range(10):
        TD.randomise()
        sprint_dist = TD.sprint_start_distance

        # Before sprint & at perfect draft point, should return eq. exertion
        TD.draft_controller.last_exertion_value = 0
        assert TD.predict([0.9*sprint_dist, 1, 1, 0.9*sprint_dist-2.5, 1, 1], 0.1) == 0.1*0.5

        # Before sprint, rider B trailing & error in position
        # Returns clipped controller value (max value of 1)
        TD.draft_controller.last_exertion_value = 0
        assert TD.predict([0.9*sprint_dist, 1, 1, 0.9*sprint_dist-4, 1, 1], 0.5) == \
            min(1, (1 + 1.5*1.5))*0.5

        # Before sprint, rider B trailing & error in velocity
        # Returns clipped controller value (max value of 1)
        TD.draft_controller.last_exertion_value = 0
        assert TD.predict([0.9*sprint_dist, 1.5, 1, 0.9*sprint_dist-2.5, 1, 1], 0.5) == \
            min(1, (1 + 5*0.5))*0.5

        # Before sprint, rider B trailing & errors in velocity & position
        # Returns clipped controller value (max value of 1)
        TD.draft_controller.last_exertion_value = 0
        assert TD.predict([0.9*sprint_dist, 1.5, 1, 0.9*sprint_dist-3.5, 1, 1], 0.5) == \
            min(1, (1 + 1.5*1 + 5*0.5))*0.5

        # Before sprint, rider B trailling & errors in velocity & position
        # Returns clipped controller value (min value of 0)
        TD.draft_controller.last_exertion_value = 0
        assert TD.predict([0.9*sprint_dist, 1, 1, 0.9*sprint_dist-1.5, 1.5, 1], 0.5) == \
            max(0, (1 - 1.5*1 - 5*0.5))*0.5

        # Before sprint, rider B trailing & stationary
        # Should return 0.5
        TD.draft_controller.last_exertion_value = 0
        assert TD.predict([0.9*sprint_dist, 0.1, 1, 0.9*sprint_dist-1.5, 1.5, 1], 0.5) == 0.5

        # At sprint distance, should sprint
        TD.draft_controller.last_exertion_value = 0
        assert TD.predict([1.1*sprint_dist, 1, 1, sprint_dist, 1, 1], 0.5) == 1.0

        # Before sprint, rider B leading - Should go low power
        TD.draft_controller.last_exertion_value = 0
        assert TD.predict([0.8*sprint_dist, 1, 1, 0.9*sprint_dist, 1, 1], 0.1) == 0.5



