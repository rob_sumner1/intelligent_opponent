"""Test the 250 m sprint and win gym."""
import gym
import gym_msr


def test_init():
    """Test the initialised values of the environment."""
    env = gym.make('msr-ident-win-v1')
    assert env.state == env.rider_a_init + env.rider_b_init
    assert not env.complete()

def test_reset():
    """Test the reset function."""
    env = gym.make('msr-ident-win-v1')
    for _ in range(10):
        env.step(1)

    # Check step has worked
    assert env.state != env.rider_a_init + env.rider_b_init
    assert len(env.fatigue_data) > 1
    env.reset()
    assert env.state == env.rider_a_init + env.rider_b_init
    assert env.fatigue_data == [[1,1]]

def test_complete():
    """Test the function which checks if race is complete."""
    env = gym.make('msr-ident-win-v1')
    assert not env.complete()
    env.state = [0, 0, 1, 249, 0, 1]
    assert not env.complete()
    env.state = [0, 0, 1, 250, 0, 1]
    assert env.complete()

    env.state = [249, 0, 1, 0, 0, 1]
    assert not env.complete()
    env.state = [250, 0, 1, 0, 0, 1]
    assert env.complete()

def test_fatigue():
    """Test the fatigue measures for both riders."""
    env = gym.make('msr-ident-win-v1')

    f_a, f_b = env.fatigue()
    assert f_a == 1
    assert f_b == 1

def test_reward():
    """Test the reward function."""
    env = gym.make('msr-ident-win-v1')
    assert env.reward_function() == 0

    env.state = [251, 0, 1, 15, 0, 1]
    assert env.reward_function() == 1

    env.state = [249, 0, 1, 251, 0, 1]
    assert env.reward_function() == 0.747


