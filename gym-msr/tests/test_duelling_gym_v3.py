import gym
import gym_msr
import pytest
import timeit

@pytest.fixture(scope="module")
def env():
    """Full MSR env_v3."""
    return gym.make('msr-duel-fixed-v3')


def test_gym_init(env):
    """Test initialisation of the gym."""
    assert env.action_space.n == 5
    assert env.time_step == 0.1
    assert env.state == env.rider_a_init + env.rider_b_init
    assert not env.complete()
    assert len(env.rider_a_init) == 3
    assert len(env.rider_b_init) == 3

    # Check there are 3 opponent agents.
    assert len(env.agent_opponents) == 3

def test_env_complete(env):
    """Test the check on race completion."""
    # Should not be complete at initialisation
    assert not env.complete()
    # Check incomplete with given values
    env.state = [100, 3, 1, 749, 3, 1]
    assert not env.complete()
    # Check complete with given values
    env.state = [750, 3, 1, 749, 3, 1]
    assert env.complete()
    # Should be complete when 0 velocity
    env.state = [740, 0.0009, 1, 740, 0.0009, 1]
    assert env.complete()

def test_env_step(env):
    """Test stepping the environment."""
    env.reset()
    start_state = env.state
    env.step(2)
    env.step(2)
    # Rider A start state should have changed.
    assert all([a != b for a, b in zip(env.state[0:3], start_state[0:3])])

def test_env_reset(env):
    """Test the reset function for the environment."""
    # Reset and check that state is valid
    env.reset()
    assert env.state[0] in [0,1]
    assert env.state[1] in [0,1]
    assert env.state[2] == 1
    assert env.state[3] in [0,1]
    assert env.state[4] in [0,1]
    assert env.state[5] == 1

def test_env_reward(env):
    """Test the reward function - scaled distance."""
    # Check control sum is zero
    assert env.control_sum == 0

    # Never complete at init.
    assert env.reward_function() == 0

    # Rider A loses
    env.state = [750, 1, 0, 751, 1, 0]
    assert pytest.approx(env.reward_function(), 0.75)

    # Rider A wins
    env.state = [751, 1, 0, 750, 1, 0]
    assert env.reward_function() == 2

    # -1 if riders stop
    env.state = [740, 0.0009, 0, 740, 0.0009, 0]
    assert env.reward_function() == -1

    # Check response against all ones.
    for i in range(len(env.agent_opponents)):
        env.reset(policy_random_seed=i, leading_rider='Rider A', verbose=False)
        while not env.complete():
            env.step(4)
        assert round(env.reward*100)/100 == 0.75

def test_normalised_state(env):
    """Test the normalisation state function."""
    state = env.normalised_state()
    for elem in state:
        # Should lie in range [0,1]
        assert elem <= 1
        assert elem >= 0

def test_env_random_init(env):
    """Test the random init method."""
    # Check init with params.
    with pytest.raises(ValueError):
        env.random_init(leading_rider='Not a rider')

    # Rider A in front produces trailing policy
    rider_a, rider_b = env.random_init(leading_rider='Rider A')
    assert rider_a + rider_b == [1, 1, 1, 0, 0, 1]

    # Rider A with specific random seed
    rider_a, rider_b = env.random_init(leading_rider='Rider A',
                                        policy_random_seed=0)
    assert rider_a + rider_b == [1, 1, 1, 0, 0, 1]

    # Rider A with wrong random
    with pytest.raises(ValueError):
        rider_a, rider_b = env.random_init(leading_rider='Rider A',
                                            policy_random_seed=10)

    # Rider B in front produces leading policy
    rider_a, rider_b = env.random_init(leading_rider='Rider B')
    assert rider_a + rider_b == [0, 0, 1, 1, 1, 1]

    # Rider B in front with specific policy and random seed
    rider_a, rider_b = env.random_init(leading_rider='Rider B',
                                        policy_random_seed=0)
    assert rider_a + rider_b == [0, 0, 1, 1, 1, 1]

    # Rider B in front with wrong random seed
    with pytest.raises(ValueError):
        rider_a, rider_b = env.random_init(leading_rider='Rider B',
                                            policy_random_seed=-1)

    # Test init with specific filepath
    with pytest.raises(ValueError):
        env.random_init(policy_file_path='Not_a_true_file')

    # Test reset and reset time
    start_t = timeit.default_timer()
    rider_a_lead_prob = 0
    samples = 1000
    for _ in range(samples):
        state_a, state_b = env.random_init()
        if state_a[0] > state_b[0]:
            rider_a_lead_prob += 1
    av_reset_t = (timeit.default_timer() - start_t) / samples
    assert av_reset_t <= 4e-5

    # Check lead probability
    rider_a_lead_prob /= samples
    assert rider_a_lead_prob == pytest.approx(0.5, 0.1)


def test_env_run_time(env):
    """Test the time required to complete a set number of runs,
        plus the lead probability being uniform.
        Have bundled the random runs together for improved speed.
    """
    start_t = timeit.default_timer()
    N = 500
    for _ in range (N):
        while not env.complete():
            env.step(4)

    # Average run time
    av_run_t = (timeit.default_timer() - start_t) / N
    assert av_run_t <= 7.5e-7