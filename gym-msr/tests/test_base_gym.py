import gym
import gym_msr
import os
import time

def test_find_distance():
    """Test the initialised x,y positions."""
    import time
    env = gym.make('msr-fixed-win-v1')
    rider_a_pos, rider_b_pos = env.find_rider_points()
    assert round(rider_a_pos[0],2) == 0.00
    assert round(rider_a_pos[1],2) == -24.37
    assert round(rider_b_pos[0],2) == 15.00
    assert round(rider_b_pos[1],2) == -24.37

    env = gym.make('msr-fixed-draft-v1')
    rider_a_pos, rider_b_pos = env.find_rider_points()
    assert round(rider_a_pos[0],2) == 0.00
    assert round(rider_a_pos[1],2) == -24.37
    assert round(rider_b_pos[0],2) == 15.00
    assert round(rider_b_pos[1],2) == -24.37

def test_save_histories():
    """Test the save histories function."""
    # Delete any remaining history files
    folder = os.path.dirname(os.path.abspath(__file__)) + '/save_folders'
    if os.path.isfile(folder + '/test_name.hist'):
        os.remove(folder + '/test_name.hist')

    # Try to save file
    env = gym.make('msr-fixed-win-v1')
    env.step(1)
    env.save_history(folder=folder,name='test_name')

    # Check files exist
    assert os.path.isfile(folder + '/test_name.hist')

def test_image_render():
    """Test the save render function."""
    # Delete any image render file
    folder = os.path.dirname(os.path.abspath(__file__)) + '/save_folders'
    if os.path.isfile(folder + '/test_image.png'):
        os.remove(folder + '/test_image.png')
    if os.path.isfile(folder + '/test_image_00001.png'):
        os.remove(folder + '/test_image_00001.png')

    # Try to save file
    env = gym.make('msr-fixed-win-v1')
    env.image_render(file_name=folder + '/test_image', close=True)
    env.image_render(file_name=folder + '/test_image', index=1, close=True)

    # Check files exist
    assert os.path.isfile(folder + '/test_image.png')
    assert os.path.isfile(folder + '/test_image_00001.png')



