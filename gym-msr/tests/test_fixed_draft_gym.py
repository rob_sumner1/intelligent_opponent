import gym
import gym_msr


def test_init():
    """Test the initialised values of the environment."""
    env = gym.make('msr-fixed-draft-v1')
    assert env.state == [0, 0, 15, 0]
    assert not env.complete()

    env = gym.make('msr-fixed-draft-v2')
    assert env.state == [0, 0, 15, 0]
    assert not env.complete()

    env = gym.make('msr-fixed-draft-v3')
    assert env.state == [0, 0, 15, 0]
    assert not env.complete()

def test_reset():
    """Test the reset function."""
    # Gym 1
    env = gym.make('msr-fixed-draft-v1')
    for _ in range(10):
        env.step(1)
    assert env.state != [0, 0, 15, 0]
    env.reset()
    assert env.state == [0, 0, 15, 0]

    # Gym 2
    env = gym.make('msr-fixed-draft-v2')
    for _ in range(10):
        env.step(1)
    assert env.state != [0, 0, 15, 0]
    env.rider_b_control = [-1]
    env.reset()
    assert env.state == [0, 0, 15, 0]
    assert env.rider_b_control != [-1]
    assert env.rider_b_control[0] <= 150
    assert env.rider_b_control[0] >= 50

    # Gym 3
    env = gym.make('msr-fixed-draft-v3')
    for _ in range(10):
        env.step(1)
    assert env.state != [0, 0, 15, 0]
    env.reset()
    assert env.state == [0, 0, 15, 0]


def test_complete():
    """Test the function which checks if race is complete."""
    # Gym 1
    env = gym.make('msr-fixed-draft-v1')
    assert not env.complete()
    env.state = [0, 0, 200, 0]
    assert not env.complete()
    env.state = [0, 0, 250, 0]
    assert env.complete()

    # Gym 2
    env = gym.make('msr-fixed-draft-v2')
    assert not env.complete()
    env.state = [0, 0, 200, 0]
    assert not env.complete()
    env.state = [0, 0, 250, 0]
    assert env.complete()

    # Gym 3
    env = gym.make('msr-fixed-draft-v3')
    assert not env.complete()
    env.state = [0, 0, 200, 0]
    assert not env.complete()
    env.state = [0, 0, 250, 0]
    assert env.complete()

def test_reward():
    """Test the reward function."""

    # Gym 1 - step reward within 5 meters.
    env = gym.make('msr-fixed-draft-v1')
    assert env.reward == 0

    while not env.complete():
        env.step(10)
        state = env.state
        diff = state[2] - state[0]
        if (diff <= 5 and diff > 0):
            assert env.reward == 1
        else:
            assert env.reward == 0

    # Gym 3 - Reward for pulse.
    env = gym.make('msr-fixed-draft-v3')
    assert env.reward == 0
    env.state = [15 - env.sep_width, 0, 15, 0]
    assert env.reward_function() == 0
    env.state = [15 - 0.5*env.sep_width, 0, 15, 0]
    assert env.reward_function() == env.draft_reward
    env.state = [15, 0, 15, 0]
    assert env.reward_function() == 0