import gym
import gym_msr
import pytest
import timeit

@pytest.fixture(scope="module")
def env():
    """Full MSR env_v2."""
    return gym.make('msr-duel-fixed-v2')


def test_gym_init(env):
    """Test initialisation of the gym."""
    assert env.action_space.n == 5
    assert env.time_step == 0.1
    assert env.state == env.rider_a_init + env.rider_b_init
    assert not env.complete()
    assert len(env.rider_a_init) == 3
    assert len(env.rider_b_init) == 3

    # Test fatigue rate changes
    env2 = gym.make('msr-duel-fixed-v2', rider_a_fatigue_rate=0.005,
                    rider_b_fatigue_rate=0.2)
    assert env2.rider_a_fatigue_rate == 0.005
    assert env2.rider_b_fatigue_rate == 0.2

def test_env_complete(env):
    """Test the check on race completion."""
    # Should not be complete at initialisation
    assert not env.complete()
    # Check incomplete with given values
    env.state = [100, 3, 1, 749, 3, 1]
    assert not env.complete()
    # Check complete with given values
    env.state = [750, 3, 1, 749, 3, 1]
    assert env.complete()

def test_env_step(env):
    """Test stepping the environment."""
    env.reset()
    # No steps, so last actions should be [0,0].
    assert all([a == b for a, b in zip(env.last_controls, [0, 0])])
    start_state = env.state

    env.step(2)
    env.step(2)
    assert env.last_controls[0] == 0.5
    # all state variables should have changed
    assert all([a != b for a, b in zip(env.state, start_state)])

def test_env_overtakes(env):
    """Test counting of overtakes in the environment."""
    # Test when rider B leads.
    env.reset(leading_rider='Rider B')
    assert env.overtakes == 0
    while not env.complete():
        env.step(4)
    # Expert policy will let A overtake, then overtake for win.
    assert env.overtakes == 2

    # Test when rider A leads.
    env.reset(leading_rider='Rider A')
    assert env.overtakes == 0
    while not env.complete():
        env.step(2)
    # Expert policy will always trail, then overtake for win.
    assert env.overtakes == 1

def test_env_reset(env):
    """Test the reset function for the environment."""
    env.reset()
    start_state = env.state
    # Step the environment
    for _ in range(10):
        env.step(2)
    assert all([a != b for a, b in zip(env.state, start_state)])

    # Reset and check that state is valid
    env.reset()
    assert env.state[0] in [0,1]
    assert env.state[1] in [0,1]
    assert env.state[2] == 1
    assert env.state[3] in [0,1]
    assert env.state[4] in [0,1]
    assert env.state[5] == 1

    # Check leading rider values
    if env.state[0] > env.state[3]:
        assert env.leading_rider_now == 'Rider A'
    else:
        assert env.leading_rider_now == 'Rider B'

    # Test reset with sprint distance
    env.reset(rider_b_sprint_distance=100)
    assert env.rider_b_policy.trail_sprint_start_distance == 100
    assert env.rider_b_policy.lead_sprint_start_distance == 100

def test_env_reward(env):
    """Test the reward function - scaled distance."""
    env.reset()
    # Never complete at init.
    assert env.reward_function() == 0

    # Rider A loses
    env.state = [750, 0, 0, 751, 0, 0]
    assert pytest.approx(env.reward_function(), 0.75)

    # Rider A wins
    env.state = [751, 0, 0, 750, 0, 0]
    assert env.reward_function() == 2

def test_normalised_state(env):
    """Test the normalisation state function."""
    state = env.normalised_state()
    for elem in state:
        # Should lie in range [0,1]
        assert elem <= 1
        assert elem >= 0

def test_env_random_init(env):
    """Test the random init method."""
    # Check init with params.
    with pytest.raises(ValueError):
        env.random_init(leading_rider='Not a rider')

    # Rider B in front
    rider_a, rider_b = env.random_init(leading_rider='Rider B')
    assert rider_a + rider_b == [0, 0, 1, 1, 1, 1]
    assert env.leading_rider_now == 'Rider B'

    # Rider B in front with specific policy and random seed
    rider_a, rider_b = env.random_init(leading_rider='Rider B',
                                            policy_random_seed=5)
    assert rider_a + rider_b == [0, 0, 1, 1, 1, 1]
    assert env.leading_rider_now == 'Rider B'
    assert env.rider_b_policy.trail_sprint_start_distance ==  \
        env.rider_b_policy.test_points[5].trail_sprint_start_distance
    assert env.rider_b_policy.lead_sprint_start_distance ==  \
        env.rider_b_policy.test_points[5].lead_sprint_start_distance

    # Rider A in front
    rider_a, rider_b = env.random_init(leading_rider='Rider A')
    assert rider_a + rider_b == [1, 1, 1, 0, 0, 1]
    assert env.leading_rider_now == 'Rider A'

    # Rider A with specific random seed
    rider_a, rider_b = env.random_init(leading_rider='Rider A',
                                            policy_random_seed=5)
    assert rider_a + rider_b == [1, 1, 1, 0, 0, 1]
    assert env.leading_rider_now == 'Rider A'
    assert env.rider_b_policy.trail_sprint_start_distance == \
        env.rider_b_policy.test_points[5].trail_sprint_start_distance
    assert env.rider_b_policy.lead_sprint_start_distance ==  \
        env.rider_b_policy.test_points[5].lead_sprint_start_distance

    # Test some fully random inits
    start_t = timeit.default_timer()
    rider_a_lead_prob = 0
    samples = 500
    for _ in range(samples):
        rider_a, rider_b = env.random_init()
        if rider_a[0] > rider_b[0]:
            rider_a_lead_prob += 1
    rider_a_lead_prob /= samples
    assert rider_a_lead_prob == pytest.approx(0.5, 0.2)
    av_reset_t = (timeit.default_timer() - start_t) / samples
    assert av_reset_t <= 7.5e-5

def test_env_draft_exertion(env):
    """Test function returning equilibrium draft exertion."""
    # Point test for evaluator.
    env.state = [10, 10, 1, 11, 10, 1]
    env.last_controls = [0.1, 0.5]
    assert env.get_draft_exertion() == pytest.approx(0.090877, 0.01)

def test_env_run_time(env):
    """Test the time required to complete a set number of runs"""
    start_t = timeit.default_timer()
    N = 25
    for _ in range (N):
        env.reset(verbose=False)
        while not env.complete():
            env.step(4)
    av_run_t = (timeit.default_timer() - start_t) / N
    assert pytest.approx(av_run_t, 0.25) == 0.3