import gym
import gym_msr


def test_init():
    """Test the initialised values of the environment."""
    env = gym.make('msr-fixed-win-v1')
    assert env.state == [0, 0, 15, 0]
    assert not env.complete()

def test_reset():
    """Test the reset function."""
    env = gym.make('msr-fixed-win-v1')
    for _ in range(10):
        env.step(1)

    # Check step has worked
    assert env.state != [0, 0, 15, 0]

    env.reset()
    assert env.state == [0, 0, 15, 0]

def test_complete():
    """Test the function which checks if race is complete."""
    env = gym.make('msr-fixed-win-v1')
    assert not env.complete()
    env.state = [0, 0, 249, 0]
    assert not env.complete()
    env.state = [0, 0, 250, 0]
    assert env.complete()

def test_reward():
    """Test the reward function."""
    env = gym.make('msr-fixed-win-v1')
    assert env.reward == 0
    env.rider_b_control = [1]

    while not env.complete():
        env.step(5)

    assert env.info['total_reward'] > 1


